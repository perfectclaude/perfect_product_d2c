# 3ds Max Wavefront OBJ Exporter v0.97b - (c)2007 guruware
# File Created: 07.09.2016 15:35:43

newmtl caisse_bois_1
	Ns 10.0000
	Ni 1.5000
	d 1.0000
	Tr 0.0000
	Tf 1.0000 1.0000 1.0000 
	illum 2
	Ka 0.5880 0.5880 0.5880
	Kd 0.5880 0.5880 0.5880
	Ks 0.0000 0.0000 0.0000
	Ke 0.0000 0.0000 0.0000
	map_Ka D:\LEROY_OLIVIER\Skype\export_GE_10.fbm\caisse_bois_01b.jpg
	map_Kd D:\LEROY_OLIVIER\Skype\export_GE_10.fbm\caisse_bois_01b.jpg

newmtl caisse_bois_2
	Ns 10.0000
	Ni 1.5000
	d 1.0000
	Tr 0.0000
	Tf 1.0000 1.0000 1.0000 
	illum 2
	Ka 0.5880 0.5880 0.5880
	Kd 0.5880 0.5880 0.5880
	Ks 0.0000 0.0000 0.0000
	Ke 0.0000 0.0000 0.0000
	map_Ka D:\LEROY_OLIVIER\Skype\export_GE_10.fbm\caisse_bois_01a.jpg
	map_Kd D:\LEROY_OLIVIER\Skype\export_GE_10.fbm\caisse_bois_01a.jpg

newmtl Material__21
	Ns 10.0000
	Ni 1.5000
	d 1.0000
	Tr 0.0000
	Tf 1.0000 1.0000 1.0000 
	illum 2
	Ka 0.5880 0.5880 0.5880
	Kd 0.5880 0.5880 0.5880
	Ks 0.0000 0.0000 0.0000
	Ke 0.0000 0.0000 0.0000
	map_Ka D:\LEROY_OLIVIER\Skype\export_GE_10.fbm\contrat_fake.jpg
	map_Kd D:\LEROY_OLIVIER\Skype\export_GE_10.fbm\contrat_fake.jpg

newmtl metal_simple
	Ns 10.0000
	Ni 1.5000
	d 1.0000
	Tr 0.0000
	Tf 1.0000 1.0000 1.0000 
	illum 2
	Ka 0.7176 0.7176 0.7176
	Kd 0.7176 0.7176 0.7176
	Ks 0.1170 0.1170 0.1170
	Ke 0.0000 0.0000 0.0000

newmtl murs
	Ns 10.0000
	Ni 1.5000
	d 1.0000
	Tr 0.0000
	Tf 1.0000 1.0000 1.0000 
	illum 2
	Ka 0.5880 0.5880 0.5880
	Kd 0.5880 0.5880 0.5880
	Ks 0.1170 0.1170 0.1170
	Ke 0.0000 0.0000 0.0000
	map_Ka D:\LEROY_OLIVIER\Skype\export_GE_10.fbm\murs.png
	map_Kd D:\LEROY_OLIVIER\Skype\export_GE_10.fbm\murs.png

newmtl toit
	Ns 13.0000
	Ni 1.5000
	d 1.0000
	Tr 0.0000
	Tf 1.0000 1.0000 1.0000 
	illum 2
	Ka 0.5880 0.5880 0.5880
	Kd 0.5880 0.5880 0.5880
	Ks 0.0000 0.0000 0.0000
	Ke 0.0000 0.0000 0.0000
	map_Ka D:\LEROY_OLIVIER\Skype\export_GE_10.fbm\AO_toit.png
	map_Kd D:\LEROY_OLIVIER\Skype\export_GE_10.fbm\AO_toit.png

newmtl portes
	Ns 10.0000
	Ni 1.5000
	d 1.0000
	Tr 0.0000
	Tf 1.0000 1.0000 1.0000 
	illum 2
	Ka 0.5880 0.5880 0.5880
	Kd 0.5880 0.5880 0.5880
	Ks 0.1170 0.1170 0.1170
	Ke 0.0000 0.0000 0.0000
	map_Ka D:\LEROY_OLIVIER\Skype\export_GE_10.fbm\portes.png
	map_Kd D:\LEROY_OLIVIER\Skype\export_GE_10.fbm\portes.png

newmtl alarm
	Ns 10.0000
	Ni 1.5000
	d 1.0000
	Tr 0.0000
	Tf 1.0000 1.0000 1.0000 
	illum 2
	Ka 0.5880 0.5880 0.5880
	Kd 0.5880 0.5880 0.5880
	Ks 0.0000 0.0000 0.0000
	Ke 0.0000 0.0000 0.0000
	map_Ka D:\LEROY_OLIVIER\Skype\export_GE_10.fbm\alarm.png
	map_Kd D:\LEROY_OLIVIER\Skype\export_GE_10.fbm\alarm.png
	map_d D:\LEROY_OLIVIER\Skype\export_GE_10.fbm\alarm.png

newmtl CO2
	Ns 10.0000
	Ni 1.5000
	d 1.0000
	Tr 0.0000
	Tf 1.0000 1.0000 1.0000 
	illum 2
	Ka 0.5880 0.5880 0.5880
	Kd 0.5880 0.5880 0.5880
	Ks 0.0000 0.0000 0.0000
	Ke 0.0000 0.0000 0.0000
	map_Ka D:\LEROY_OLIVIER\Skype\export_GE_10.fbm\CO2.png
	map_Kd D:\LEROY_OLIVIER\Skype\export_GE_10.fbm\CO2.png
	map_d D:\LEROY_OLIVIER\Skype\export_GE_10.fbm\CO2.png

newmtl verre
	Ns 10.0000
	Ni 1.5000
	d 0.2000
	Tr 0.8000
	Tf 0.2000 0.2000 0.2000 
	illum 2
	Ka 0.5922 0.8471 0.9294
	Kd 0.5922 0.8471 0.9294
	Ks 0.0000 0.0000 0.0000
	Ke 0.0000 0.0000 0.0000

newmtl tuyaux_ext
	Ns 10.0000
	Ni 1.5000
	d 1.0000
	Tr 0.0000
	Tf 1.0000 1.0000 1.0000 
	illum 2
	Ka 0.5880 0.5880 0.5880
	Kd 0.5880 0.5880 0.5880
	Ks 0.1170 0.1170 0.1170
	Ke 0.0000 0.0000 0.0000
	map_Ka D:\LEROY_OLIVIER\Skype\export_GE_10.fbm\tuyaux.png
	map_Kd D:\LEROY_OLIVIER\Skype\export_GE_10.fbm\tuyaux.png

newmtl tuyaux_int
	Ns 10.0000
	Ni 1.5000
	d 1.0000
	Tr 0.0000
	Tf 1.0000 1.0000 1.0000 
	illum 2
	Ka 0.5880 0.5880 0.5880
	Kd 0.5880 0.5880 0.5880
	Ks 0.1170 0.1170 0.1170
	Ke 0.0000 0.0000 0.0000
	map_Ka D:\LEROY_OLIVIER\Skype\export_GE_10.fbm\tuyau_interne.png
	map_Kd D:\LEROY_OLIVIER\Skype\export_GE_10.fbm\tuyau_interne.png

newmtl conduit
	Ns 10.0000
	Ni 1.5000
	d 1.0000
	Tr 0.0000
	Tf 1.0000 1.0000 1.0000 
	illum 2
	Ka 0.5880 0.5880 0.5880
	Kd 0.5880 0.5880 0.5880
	Ks 0.1170 0.1170 0.1170
	Ke 0.0000 0.0000 0.0000
	map_Ka D:\LEROY_OLIVIER\Skype\export_GE_10.fbm\conduit.png
	map_Kd D:\LEROY_OLIVIER\Skype\export_GE_10.fbm\conduit.png

newmtl echelle
	Ns 10.0000
	Ni 1.5000
	d 1.0000
	Tr 0.0000
	Tf 1.0000 1.0000 1.0000 
	illum 2
	Ka 0.5880 0.5880 0.5880
	Kd 0.5880 0.5880 0.5880
	Ks 0.1170 0.1170 0.1170
	Ke 0.0000 0.0000 0.0000
	map_Ka D:\LEROY_OLIVIER\Skype\export_GE_10.fbm\echelle.png
	map_Kd D:\LEROY_OLIVIER\Skype\export_GE_10.fbm\echelle.png

newmtl caillebotis
	Ns 10.0000
	Ni 1.5000
	d 1.0000
	Tr 0.0000
	Tf 1.0000 1.0000 1.0000 
	illum 2
	Ka 0.9059 0.9059 0.9059
	Kd 0.9059 0.9059 0.9059
	Ks 0.1170 0.1170 0.1170
	Ke 0.0000 0.0000 0.0000
	map_Ka D:\LEROY_OLIVIER\Skype\export_GE_10.fbm\caillebotis.png
	map_Kd D:\LEROY_OLIVIER\Skype\export_GE_10.fbm\caillebotis.png
	map_d D:\LEROY_OLIVIER\Skype\export_GE_10.fbm\caillebotis.png
