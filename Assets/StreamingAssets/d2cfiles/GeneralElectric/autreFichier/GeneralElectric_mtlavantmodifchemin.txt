# 3ds Max Wavefront OBJ Exporter v0.97b - (c)2007 guruware
# File Created: 07.09.2016 15:55:27

newmtl caisse_bois_1
	Ns 10.000000
	Ni 1.500000
	d 1.000000
	Tr 0.000000
	Tf 1.000000 1.000000 1.000000 
	illum 2
	Ka 0.588000 0.588000 0.588000
	Kd 0.588000 0.588000 0.588000
	Ks 0.000000 0.000000 0.000000
	Ke 0.000000 0.000000 0.000000
	map_Ka D:\LEROY_OLIVIER\Skype\export_GE_10.fbm\caisse_bois_01b.jpg
	map_Kd D:\LEROY_OLIVIER\Skype\export_GE_10.fbm\caisse_bois_01b.jpg

newmtl caisse_bois_2
	Ns 10.000000
	Ni 1.500000
	d 1.000000
	Tr 0.000000
	Tf 1.000000 1.000000 1.000000 
	illum 2
	Ka 0.588000 0.588000 0.588000
	Kd 0.588000 0.588000 0.588000
	Ks 0.000000 0.000000 0.000000
	Ke 0.000000 0.000000 0.000000
	map_Ka D:\LEROY_OLIVIER\Skype\export_GE_10.fbm\caisse_bois_01a.jpg
	map_Kd D:\LEROY_OLIVIER\Skype\export_GE_10.fbm\caisse_bois_01a.jpg

newmtl Material__21
	Ns 10.000000
	Ni 1.500000
	d 1.000000
	Tr 0.000000
	Tf 1.000000 1.000000 1.000000 
	illum 2
	Ka 0.588000 0.588000 0.588000
	Kd 0.588000 0.588000 0.588000
	Ks 0.000000 0.000000 0.000000
	Ke 0.000000 0.000000 0.000000
	map_Ka D:\LEROY_OLIVIER\Skype\export_GE_10.fbm\contrat_fake.jpg
	map_Kd D:\LEROY_OLIVIER\Skype\export_GE_10.fbm\contrat_fake.jpg

newmtl metal_simple
	Ns 9.999999
	Ni 1.500000
	d 1.000000
	Tr 0.000000
	Tf 1.000000 1.000000 1.000000 
	illum 2
	Ka 0.717647 0.717647 0.717647
	Kd 0.717647 0.717647 0.717647
	Ks 0.117000 0.117000 0.117000
	Ke 0.000000 0.000000 0.000000

newmtl murs
	Ns 10.000000
	Ni 1.500000
	d 1.000000
	Tr 0.000000
	Tf 1.000000 1.000000 1.000000 
	illum 2
	Ka 0.588000 0.588000 0.588000
	Kd 0.588000 0.588000 0.588000
	Ks 0.117000 0.117000 0.117000
	Ke 0.000000 0.000000 0.000000
	map_Ka D:\LEROY_OLIVIER\Skype\export_GE_10.fbm\murs.png
	map_Kd D:\LEROY_OLIVIER\Skype\export_GE_10.fbm\murs.png

newmtl toit
	Ns 13.000000
	Ni 1.500000
	d 1.000000
	Tr 0.000000
	Tf 1.000000 1.000000 1.000000 
	illum 2
	Ka 0.588000 0.588000 0.588000
	Kd 0.588000 0.588000 0.588000
	Ks 0.000000 0.000000 0.000000
	Ke 0.000000 0.000000 0.000000
	map_Ka D:\LEROY_OLIVIER\Skype\export_GE_10.fbm\AO_toit.png
	map_Kd D:\LEROY_OLIVIER\Skype\export_GE_10.fbm\AO_toit.png

newmtl portes
	Ns 10.000000
	Ni 1.500000
	d 1.000000
	Tr 0.000000
	Tf 1.000000 1.000000 1.000000 
	illum 2
	Ka 0.588000 0.588000 0.588000
	Kd 0.588000 0.588000 0.588000
	Ks 0.117000 0.117000 0.117000
	Ke 0.000000 0.000000 0.000000
	map_Ka D:\LEROY_OLIVIER\Skype\export_GE_10.fbm\portes.png
	map_Kd D:\LEROY_OLIVIER\Skype\export_GE_10.fbm\portes.png

newmtl alarm
	Ns 10.000000
	Ni 1.500000
	d 1.000000
	Tr 0.000000
	Tf 1.000000 1.000000 1.000000 
	illum 2
	Ka 0.588000 0.588000 0.588000
	Kd 0.588000 0.588000 0.588000
	Ks 0.000000 0.000000 0.000000
	Ke 0.000000 0.000000 0.000000
	map_Ka D:\LEROY_OLIVIER\Skype\export_GE_10.fbm\alarm.png
	map_Kd D:\LEROY_OLIVIER\Skype\export_GE_10.fbm\alarm.png
	map_d D:\LEROY_OLIVIER\Skype\export_GE_10.fbm\alarm.png

newmtl CO2
	Ns 10.000000
	Ni 1.500000
	d 1.000000
	Tr 0.000000
	Tf 1.000000 1.000000 1.000000 
	illum 2
	Ka 0.588000 0.588000 0.588000
	Kd 0.588000 0.588000 0.588000
	Ks 0.000000 0.000000 0.000000
	Ke 0.000000 0.000000 0.000000
	map_Ka D:\LEROY_OLIVIER\Skype\export_GE_10.fbm\CO2.png
	map_Kd D:\LEROY_OLIVIER\Skype\export_GE_10.fbm\CO2.png
	map_d D:\LEROY_OLIVIER\Skype\export_GE_10.fbm\CO2.png

newmtl verre
	Ns 10.000000
	Ni 1.500000
	d 0.200000
	Tr 0.800000
	Tf 0.200000 0.200000 0.200000 
	illum 2
	Ka 0.592157 0.847059 0.929412
	Kd 0.592157 0.847059 0.929412
	Ks 0.000000 0.000000 0.000000
	Ke 0.000000 0.000000 0.000000

newmtl tuyaux_ext
	Ns 10.000000
	Ni 1.500000
	d 1.000000
	Tr 0.000000
	Tf 1.000000 1.000000 1.000000 
	illum 2
	Ka 0.588000 0.588000 0.588000
	Kd 0.588000 0.588000 0.588000
	Ks 0.117000 0.117000 0.117000
	Ke 0.000000 0.000000 0.000000
	map_Ka D:\LEROY_OLIVIER\Skype\export_GE_10.fbm\tuyaux.png
	map_Kd D:\LEROY_OLIVIER\Skype\export_GE_10.fbm\tuyaux.png

newmtl tuyaux_int
	Ns 10.000000
	Ni 1.500000
	d 1.000000
	Tr 0.000000
	Tf 1.000000 1.000000 1.000000 
	illum 2
	Ka 0.588000 0.588000 0.588000
	Kd 0.588000 0.588000 0.588000
	Ks 0.117000 0.117000 0.117000
	Ke 0.000000 0.000000 0.000000
	map_Ka D:\LEROY_OLIVIER\Skype\export_GE_10.fbm\tuyau_interne.png
	map_Kd D:\LEROY_OLIVIER\Skype\export_GE_10.fbm\tuyau_interne.png

newmtl conduit
	Ns 10.000000
	Ni 1.500000
	d 1.000000
	Tr 0.000000
	Tf 1.000000 1.000000 1.000000 
	illum 2
	Ka 0.588000 0.588000 0.588000
	Kd 0.588000 0.588000 0.588000
	Ks 0.117000 0.117000 0.117000
	Ke 0.000000 0.000000 0.000000
	map_Ka D:\LEROY_OLIVIER\Skype\export_GE_10.fbm\conduit.png
	map_Kd D:\LEROY_OLIVIER\Skype\export_GE_10.fbm\conduit.png

newmtl echelle
	Ns 10.000000
	Ni 1.500000
	d 1.000000
	Tr 0.000000
	Tf 1.000000 1.000000 1.000000 
	illum 2
	Ka 0.588000 0.588000 0.588000
	Kd 0.588000 0.588000 0.588000
	Ks 0.117000 0.117000 0.117000
	Ke 0.000000 0.000000 0.000000
	map_Ka D:\LEROY_OLIVIER\Skype\export_GE_10.fbm\echelle.png
	map_Kd D:\LEROY_OLIVIER\Skype\export_GE_10.fbm\echelle.png

newmtl caillebotis
	Ns 9.999999
	Ni 1.500000
	d 1.000000
	Tr 0.000000
	Tf 1.000000 1.000000 1.000000 
	illum 2
	Ka 0.905882 0.905882 0.905882
	Kd 0.905882 0.905882 0.905882
	Ks 0.117000 0.117000 0.117000
	Ke 0.000000 0.000000 0.000000
	map_Ka D:\LEROY_OLIVIER\Skype\export_GE_10.fbm\caillebotis.png
	map_Kd D:\LEROY_OLIVIER\Skype\export_GE_10.fbm\caillebotis.png
	map_d D:\LEROY_OLIVIER\Skype\export_GE_10.fbm\caillebotis.png
