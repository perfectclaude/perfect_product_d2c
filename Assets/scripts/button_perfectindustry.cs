﻿using UnityEngine;
using System.Collections;

public class button_perfectindustry : buttonswap
{
	GameObject filemenu;
	tk2dTextMesh loadingwindowtext = null;
	GameObject loadingwindow = null;


	void Awake()
	{
		_Awake();
		filemenu = gameObject.transform.parent.gameObject.FindInChildren("filemenu");
	}

	public override void ButtonPressed ()
	{
		base.ButtonPressed();
		if (networkfiles.Filedir == null)
		{
			StartCoroutine("ErrorFileList");
		}
		else
		{
			if (networkfiles.Filedir.Length == 0)
			{
				StartCoroutine("ErrorFileList");
			}
			else
			{
				bufferme.SetInt("popupdropdownison",1);
				filemenu.SetActive(true);
			}
		}
	}

	IEnumerator ErrorFileList()
	{
		if (load3d_object.loadingwindow != null)	load3d_object.loadingwindow.SetActive(true);
        if (load3d_object.loadingwindowtext != null)	settext.ForceText(load3d_object.loadingwindowtext,"FILE_DIR_NOT_AVAIL_TEXT");
		yield return new WaitForSeconds(3.0f);
		if (load3d_object.loadingwindow != null)	load3d_object.loadingwindow.SetActive(false);
	}
}