﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class button_analyse_abc : buttonswap
{

	void Awake()
	{
		_Awake();
	}


    private int SortByPrice(pp_produit_composant el1, pp_produit_composant el2)
    {
        if (el1.UnitPrice_Val > el2.UnitPrice_Val)
            return -1;
        else if (el1.UnitPrice_Val < el2.UnitPrice_Val)
            return 1;
        else
            return 0;
    }

	public override void ButtonPressed ()
	{
		base.ButtonPressed();

        pp_produit prod = pp_manager.GetCurrentProduit();
        if (prod != null)
        {
            button_openanalyse.lastpressed = 0;

            List<pp_produit_composant> lstWithNoChild =  new List<pp_produit_composant>();
            float totalcost = 0.0f;
            float costcount = 0.0f;

            foreach (pp_produit_composant comp in prod.composants)
            {
                if (comp.childs.Count <= 0)
                {
                    lstWithNoChild.Add(comp);
                    totalcost += comp.UnitPrice_Val;
                }
            }

            lstWithNoChild.Sort(SortByPrice);

            foreach (pp_produit_composant comp in lstWithNoChild)
            {
                costcount += comp.UnitPrice_Val;

                if( costcount < (totalcost * 0.8f) )
                    ColorAllChilds(comp.Description, Color.red);
                
                else if( costcount < (totalcost * 0.95f) )
                    ColorAllChilds(comp.Description, new Color(1, 1, 0));
                
                else
                    ColorAllChilds(comp.Description, Color.green);
                    
                // Modif par CL 
                //costcount += comp.UnitPrice_Val;
            
                /*
                // calc color division
        		float stepper = 1.0f / (float)nruniquecomponents;
        		Color colset = new Color(1.0f,0.0f,0.0f);
        		for (int i=0;i<nruniquecomponents;i++)
        		{
        			Debug.Log (i.ToString() + ") "+ componentnames[i]);
        			ColorAllChilds(componentnames[i],colset);
        			colset.r -= stepper;
        			colset.g += stepper;
        		}
        		*/
            }
        }
	}


	void ColorAllChilds(string branchname,Color col)
	{
		GameObject father = load3d_object.objectcenter;

		foreach (Transform child in father.GetComponentsInChildren<Transform>(true))
		{
			if (child.name == ("#_"+branchname))
			{
				foreach (Transform inner in child.gameObject.GetComponentsInChildren<Transform>(true))
				{
					attachedcolor atta = (attachedcolor)inner.gameObject.GetComponent<attachedcolor>();
					if (atta != null)
					{
						Renderer rend;
						rend = inner.gameObject.GetComponent<Renderer>();
						
						rend.material.SetColor("_Color",col);
					}
				}
				break;
			}
		}
	}

}
