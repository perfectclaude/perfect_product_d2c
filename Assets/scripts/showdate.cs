﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class showdate : MonoBehaviour
{
	public bool SetDate = false;
	public bool SetTime = false;

	public static string [] months = new string[]
	{
     "JAN_TEXT",
	 "FEB_TEXT",
     "MAR_TEXT",
     "APR_TEXT",
     "MAY_TEXT",
     "JUN_TEXT",
     "JUL_TEXT",
     "AUG_TEXT",
     "SEP_TEXT",
     "OCT_TEXT",
     "NOV_TEXT",
     "DEC_TEXT"
	};

	public static string [] weekdays = new string[]
	{
        "SUNDAY_TEXT",
		"MONDAY_TEXT",
		"TUESDAY_TEXT",
		"WEDNESDAY_TEXT",
		"THURSDAY_TEXT",
		"FRIDAY_TEXT",
		"SATURDAY_TEXT"	
	};

	Text mytext;

	void Awake ()
	{
		mytext = gameObject.GetComponent<Text>();
	}


    public static string FormatDate( DateTime date, bool withDay, bool withMonth, bool withYear, bool withTime)
    {
        string te = "";

        if (withDay)
        {
            te += " " + localization.GetTextWithKey(weekdays[(int)(date.DayOfWeek)]);
            te += " " + date.Day;
        }

        if (withMonth)
            te += " " + localization.GetTextWithKey(months[(int)(DateTime.Now.Month - 1)]);

        if (withYear)
            te += " " + date.Year;

        if (withTime)
            te +=  " " +  date.ToString( "H:mm" );

        return te.Trim();
    }

	public static string TodaysDate()
	{
        return FormatDate(DateTime.Now, true, true, true, false);
	}

	public static string CurrentTime()
	{
        return FormatDate(DateTime.Now, false, false, false, true);
	}

	void Update ()
	{
		string doit = "";

		if (SetDate) doit += TodaysDate() + " ";
		if (SetTime) doit += CurrentTime();

		mytext.text = doit;
	}
}
