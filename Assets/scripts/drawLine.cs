﻿using UnityEngine;
using System.Collections;

public class drawLine : MonoBehaviour {
	public static Texture2D lineTex;
	
	GameObject cloneimage;
	// Use this for initialization
	void Start () {
		cloneimage=GameObject.Find("clone primitive");
	}
	
	// Update is called once per frame
	void Update () {
		//Debug.Log ("update");
	//	TraceLine (1,GameObject.Find("bouton_1"),Color.green,GameObject.Find("bouton_1").transform.position,GameObject.Find("boutonRight_1").transform.position);
	}


    public void TraceLine(int itemid,GameObject father, Color col, Vector3 vec1, Vector3 vec2)
	{ 
        Texture mytexture = cloneimage.GetComponent<Renderer>().material.mainTexture;
        GameObject newobj = GameObject.Instantiate(cloneimage) as GameObject;
        newobj.name = "curveline"+itemid;
        newobj.transform.parent = father.transform;
        newobj.transform.localPosition = new Vector3(0,0,0);
        newobj.transform.localScale = father.transform.localScale;
        CreateNewLine(newobj,col,vec1,vec2);

        newobj.GetComponent<Renderer>().material = new Material (Shader.Find("Sprites/Default"));

        newobj.GetComponent<Renderer>().material.mainTexture = mytexture;

    }

    void CreateNewLine(GameObject obj,Color color,Vector3 vec1,Vector3 vec2)
    {
        Vector3[]    vertex         = new Vector3    [4];
        int[]         triangle     = new int        [3*2];
        Vector2[]    uv            = new Vector2    [4];        
        Color[]        col            = new Color        [4];
        float        decal  = 0.01f;		// WIDTH !!!

        if (Mathf.Abs(vec1.x - vec2.x) > Mathf.Abs(vec1.y - vec2.y))        // horizontal
        {
            vertex[0] = new Vector3(vec1.x,vec1.y-decal,0);
            vertex[1] = new Vector3(vec2.x,vec2.y-decal,0);
            vertex[2] = new Vector3(vec1.x,vec1.y+decal,0);
            vertex[3] = new Vector3(vec2.x,vec2.y+decal,0);
        }
        else
        {
            vertex[0] = new Vector3(vec1.x-decal,vec1.y,0);
            vertex[1] = new Vector3(vec1.x+decal,vec1.y,0);
            vertex[2] = new Vector3(vec2.x-decal,vec2.y,0);
            vertex[3] = new Vector3(vec2.x+decal,vec2.y,0);
        }

        col[0] = col[1] = col[2] = col[3] = color;
        
        uv[0]        = new Vector2(0,0);
        uv[1]        = new Vector2(1,0);
        uv[2]        = new Vector2(0,1);
        uv[3]        = new Vector2(1,1);
        
        triangle[0] = 0;
        triangle[1] = 2;
        triangle[2] = 1;
        triangle[3] = 2;
        triangle[4] = 3;
        triangle[5] = 1;
        
        Mesh m = new Mesh();
        
        m.vertices  = vertex;
        m.colors    = col;
        m.uv        = uv;
        m.triangles    = triangle;
        
        m.RecalculateNormals();
        m.RecalculateBounds();

        obj.GetComponent<MeshFilter>().mesh = m;
    }

	
	/*public static void DrawLine(Rect rect) { DrawLine(rect, GUI.contentColor, 1.0f); }
	public static void DrawLine(Rect rect, Color color) { DrawLine(rect, color, 1.0f); }
	public static void DrawLine(Rect rect, float width) { DrawLine(rect, GUI.contentColor, width); }
	public static void DrawLine(Rect rect, Color color, float width) { DrawLine(new Vector2(rect.x, rect.y), new Vector2(rect.x + rect.width, rect.y + rect.height), color, width); }
	public static void DrawLine(Vector2 pointA, Vector2 pointB) { DrawLine(pointA, pointB, GUI.contentColor, 1.0f); }
	public static void DrawLine(Vector2 pointA, Vector2 pointB, Color color) { DrawLine(pointA, pointB, color, 1.0f); }
	public static void DrawLine(Vector2 pointA, Vector2 pointB, float width) { DrawLine(pointA, pointB, GUI.contentColor, width); }
	public static void DrawLine(Vector2 pointA, Vector2 pointB, Color color, float width)
	{
		// Save the current GUI matrix, since we're going to make changes to it.
		Matrix4x4 matrix = GUI.matrix;
		
		// Generate a single pixel texture if it doesn't exist
		if (!lineTex) {
			Debug.Log("init");
			lineTex = Texture2D.blackTexture; 
		}
		
		// Store current GUI color, so we can switch it back later,
		// and set the GUI color to the color parameter
		Color savedColor = GUI.color;
		GUI.color = color;
		
		// Determine the angle of the line.
		float angle = Vector3.Angle(pointB - pointA, Vector2.right);
		
		// Vector3.Angle always returns a positive number.
		// If pointB is above pointA, then angle needs to be negative.
		if (pointA.y > pointB.y) { angle = -angle; }
		
		// Use ScaleAroundPivot to adjust the size of the line.
		// We could do this when we draw the texture, but by scaling it here we can use
		//  non-integer values for the width and length (such as sub 1 pixel widths).
		// Note that the pivot point is at +.5 from pointA.y, this is so that the width of the line
		//  is centered on the origin at pointA.
		GUIUtility.ScaleAroundPivot(new Vector2((pointB - pointA).magnitude, width), new Vector2(pointA.x, pointA.y + 0.5f));
		
		// Set the rotation for the line.
		//  The angle was calculated with pointA as the origin.
		GUIUtility.RotateAroundPivot(angle, pointA);
		
		// Finally, draw the actual line.
		// We're really only drawing a 1x1 texture from pointA.
		// The matrix operations done with ScaleAroundPivot and RotateAroundPivot will make this
		//  render with the proper width, length, and angle.
		GUI.DrawTexture(new Rect(pointA.x, pointA.y, 1, 1),Texture2D.blackTexture);
		
		// We're done.  Restore the GUI matrix and GUI color to whatever they were before.
		GUI.matrix = matrix;
		GUI.color = savedColor;
	}*/

}
