﻿using System;
using System.Collections;
using System.Collections.Generic;


public class pp_list<T> : List<T>
{
    public delegate void OnPPListChangeDelegate(int index, T oldObj, bool inserted, bool deleted);
    private OnPPListChangeDelegate _listChangeCallBack = null;
    private object _syncRoot = new System.Object();

    public pp_list(OnPPListChangeDelegate Func)
        : base()
    {
        _listChangeCallBack = Func;
    }
        
    public new T this[int index]
    {
        get
        {
            lock (_syncRoot)
            {
                return base[index];
            }
        }
        set
        {
            T oldObj;

            lock (_syncRoot)
            {
                oldObj = base[index];
                base[index] = value;
            }

            if (_listChangeCallBack != null)
                _listChangeCallBack(index, oldObj, false, false);
        }
    }

    public new void Add(T item)
    {
        int index = -1;
        T oldObj = default(T);

        lock (_syncRoot)
        {
            index = base.Count - 1;
            if ((index >= 0) && (index < base.Count))
                oldObj = base[index];
            
            base.Add(item);
            index = base.Count - 1;
        }

        if (_listChangeCallBack != null)
            _listChangeCallBack(index, oldObj, true, false);
    }

    public new void Insert(int index, T item)
    {
        T oldObj = default(T);

        lock (_syncRoot)
        {
            if ((index >= 0) && (index < base.Count))
                oldObj = base[index];
   
            base.Insert(index, item);
        }

        if (_listChangeCallBack != null)
            _listChangeCallBack(index, oldObj, true, false);
    }

    public new bool Remove(T item)
    {
        bool bRet = false;
        int index = -1;
        T oldObj = default(T);

        lock (_syncRoot)
        {
            index = base.IndexOf(item);

            if (index >= 0)
                oldObj = base[index];
                
            bRet = base.Remove(item);
        }

        if (_listChangeCallBack != null)
            _listChangeCallBack(index, oldObj, false, true);

        return(bRet);
    }

    public new void RemoveAt(int index)
    {
        T oldObj = default(T);

        lock (_syncRoot)
        {
            if ((index >= 0) && (index < base.Count))
            {
                oldObj = base[index];
                
                base.RemoveAt(index);
            }
        }

        if (_listChangeCallBack != null)
            _listChangeCallBack(index, oldObj, false, true);
    }

    public new void Clear()
    {
        lock (_syncRoot)
        {
            base.Clear();
        }

        if (_listChangeCallBack != null)
            _listChangeCallBack(-1, default(T), false, true);
    }
}

