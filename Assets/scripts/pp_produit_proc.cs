﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;




public class pp_produit_proc
{
    //private string _ERP = "";
    private string _Short_Description = "";
    private string _Description = "";
    private int _Production_Speed = 0;
    private string _Unit = "";
    private string _Nb_Operator = "";
    private float _Engine_Cost = 0f;
    private float _Human_Cost = 0f;
    private int _Gamme = 0;
    private int _ProcessID = 0;

    private object _syncRoot = new System.Object();
    private pp_produit_composant _composant = null;
    private ppconvert _convert = null;

    public pp_produit_proc( string[] init_chps, pp_produit_composant comp )
    {
        _composant = comp;

        _convert = _composant.Convert;

        DecodeData(init_chps);
    }

    public pp_produit_proc( pp_produit_composant comp )
        : this( (string[])null, comp )
    {
    }

    public void DecodeData(string[] init_chps )
    {
        if (init_chps != null)
        {
            lock (_syncRoot)
            {
                //if (init_chps.Length > 0)
                //    _convert.TryParse(init_chps[0], out _ERP);

                if (init_chps.Length > 1)
                    _convert.TryParse(init_chps[1], out _Short_Description);

                if (init_chps.Length > 2)
                    _convert.TryParse(init_chps[2], out _Description);

                if (init_chps.Length > 3)
                    _convert.TryParse(init_chps[3], out _Production_Speed);

                if (init_chps.Length > 4)
                    _convert.TryParse(init_chps[4], out _Unit);

                if (init_chps.Length > 5)
                    _convert.TryParse(init_chps[5], out _Nb_Operator);

                if (init_chps.Length > 6)
                    _convert.TryParse(init_chps[6], out _Engine_Cost);

                if (init_chps.Length > 7)
                    _convert.TryParse(init_chps[7], out _Human_Cost);

                if (init_chps.Length > 8)
                    _convert.TryParse(init_chps[8], out _Gamme);

                if (init_chps.Length > 9)
                    _convert.TryParse(init_chps[9], out _ProcessID);
            }
        }
    }

    public string[] EncodeTitre()
    {
        lock (_syncRoot)
        {
            string[] chps = new string[10];

            chps[0] = "ERP";
            chps[1] = "Short_Description";
            chps[2] = "Description";
            chps[3] = "Production_Speed";
            chps[4] = "Unit";
            chps[5] = "Nb_Operator";
            chps[6] = "Engine_Cost";
            chps[7] = "Human_Cost";
            chps[8] = "Gamme";
            chps[9] = "ProcessID";

            return chps;
        }
    }

    public string[] EncodeData()
    {
        lock (_syncRoot)
        {
            string[] chps = new string[10];

            //chps[0] = _convert.ToString(_ERP);
            chps[0] = _composant.ERP;

            chps[1] = _convert.ToString(_Short_Description);
            chps[2] = _convert.ToString(_Description);
            chps[3] = _convert.ToString(_Production_Speed);
            chps[4] = _convert.ToString(_Unit);
            chps[5] = _convert.ToString(_Nb_Operator);
            chps[6] = _convert.ToString(_Engine_Cost);
            chps[7] = _convert.ToString(_Human_Cost);
            chps[8] = _convert.ToString(_Gamme);
            chps[9] = _convert.ToString(_ProcessID);

            return chps;
        }
    }

    // Getter / Setter data
    public string ERP
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _composant.ERP;
            }
        }
    }

    public string Short_Description
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(_Short_Description);
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _convert.TryParse(value, out _Short_Description);
            }
        }
    }

    public string Description
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(_Description);
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _convert.TryParse(value, out _Description);
            }
        }
    }

    public string Production_Speed
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(_Production_Speed);
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _convert.TryParse(value, out _Production_Speed);
            }
        }
    }

    public string Unit
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(_Unit);
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _convert.TryParse(value, out _Unit);
            }
        }
    }

    public string Nb_Operator
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(_Nb_Operator);
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _convert.TryParse(value, out _Nb_Operator);
            }
        }
    }

    public float Engine_Cost_Val
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _Engine_Cost;
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _Engine_Cost = value;
            }
        }
    }

    public string Engine_Cost
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(_Engine_Cost, true);
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _convert.TryParse(value, out _Engine_Cost, true);
            }
        }
    }

    public float Human_Cost_Val
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _Human_Cost;
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _Human_Cost = value;
            }
        }
    }

    public string Human_Cost
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(_Human_Cost, true);
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _convert.TryParse(value, out _Human_Cost, true);
            }
        }
    }


    public float Total_Cost_Val
    {
        get
        { 
            lock (_syncRoot)
            {
                return _Engine_Cost + _Human_Cost;
            }
        }
    }

    public string Total_Cost
    {
        get
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(_Engine_Cost + _Human_Cost, true);
            }
        }
    }

    public string Gamme
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(_Gamme);
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _convert.TryParse(value, out _Gamme);
            }
        }
    }

    public int ProcessID_Val
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _ProcessID;
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _ProcessID = value;
            }
        }
    }

    public string ProcessID
    {
        get 
        { 
            lock (_syncRoot)
            {
                string sVal = "";

                if (_ProcessID >= 0)
                    sVal =  _convert.ToString(_ProcessID);

                return sVal;
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _convert.TryParse(value, out _ProcessID);
            }
        }
    }


    public ppconvert Convert
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert;
            }
        }
    }

}
