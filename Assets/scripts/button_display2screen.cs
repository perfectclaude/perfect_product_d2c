﻿using UnityEngine;
using System.Collections;

public class button_display2screen : buttonswap
{
	GameObject fullscreen;
	GameObject leftwindow;
	GameObject rightwindow;


	void Awake()
	{
		_Awake ();
		fullscreen = GameObject.Find ("fullscreen");
		leftwindow = GameObject.Find ("leftwindow");
		rightwindow = GameObject.Find ("rightwindow");
	}
	
	public override void ButtonPressed ()
	{
		base.ButtonPressed();
		fullscreen.SetActive(false);
		leftwindow.SetActive(true);
		rightwindow.SetActive(true);
	}
}