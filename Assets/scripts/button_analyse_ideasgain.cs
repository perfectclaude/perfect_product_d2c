﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class button_analyse_ideasgain : buttonswap
{
    private pp_produit_composant _myActualComp = null;

    void Awake()
    {
        _Awake();
    }

    void Update()
    {

        if (button_openanalyse.lastpressed == 3)
        {
            if (button_openanalyse.open)
            {
                pp_produit_composant comp = pp_manager.GetCurrentComposant();

                if (comp != _myActualComp)
                {
                    RedoDisplay();
                }
            }
        }
    }
        
	
	public override void ButtonPressed ()
	{
		base.ButtonPressed();
		button_openanalyse.lastpressed = 3;
		RedoDisplay();
	}
	
    private int SortByGain(pp_produit_composant el1, pp_produit_composant el2)
    {
        if (el1.Gain_Val > el2.Gain_Val)
            return -1;
        else if (el1.Gain_Val < el2.Gain_Val)
            return 1;
        else
            return 0;
    }



	void RedoDisplay()
	{
        pp_produit_composant curComp = pp_manager.GetCurrentComposant();
        if (curComp != null)
        {
            _myActualComp = curComp;

            List<pp_produit_composant> lstChids =  new List<pp_produit_composant>();

            foreach (pp_produit_composant comp in curComp.childs)
            {
                //if( comp.Gain != 0 )
                lstChids.Add(comp);
            }

            lstChids.Sort(SortByGain);

            for (int i = 0; i < lstChids.Count; i++)
            {
                Debug.Log(i.ToString() + ")" + lstChids[i].Description + "  gains:" + lstChids[i].Gain_Val);
            }

            // calc color division
            float stepper = 1.0f / (float)lstChids.Count;
            Color colset = new Color(1.0f, 0.0f, 0.0f);
            for (int i = 0; i < lstChids.Count; i++)
            {
                ColorAllChilds(lstChids[i].Description, colset);
                colset.r -= stepper;
                colset.g += stepper;
            }
        }
	}
	
	
	void ColorAllChilds(string branchname,Color col)
	{
		GameObject father = load3d_object.objectcenter;
		
		foreach (Transform child in father.GetComponentsInChildren<Transform>(true))
		{
			if (child.name == ("#_"+branchname))
			{
				foreach (Transform inner in child.gameObject.GetComponentsInChildren<Transform>(true))
				{
					attachedcolor atta = (attachedcolor)inner.gameObject.GetComponent<attachedcolor>();
					if (atta != null)
					{
						Renderer rend;
						rend = inner.gameObject.GetComponent<Renderer>();
						
						rend.material.SetColor("_Color",col);
					}
				}
				break;
			}
		}
	}
	
}



