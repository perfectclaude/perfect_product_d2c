﻿using UnityEngine;
using System.Collections;

public class camerascaletoobject : MonoBehaviour
{

	public static bool dansZone;
	Camera 				m_ViewCamera;
	Camera 				m_ViewCamera2D;
	static float		borderlimitn = 40.0f;
	static float		borderlimits = 40.0f;
	static float		borderlimite = Screen.width * 0.3f;
	static float		borderlimitw = Screen.width * 0.1f;
	bool				zoomin = false;
	float				timecountdown;
	float				zoomdistance;
	GameObject			clone;

	Vector3 			v3StartRot;
	Vector3 			v2Axis;
	float 				fStartAngle;
	bool				mousedown = false;
	float				waittillalowed = 0.0f;
	GameObject			[] 	corners = new GameObject[8];

	GameObject			interfacebottombar = null;
	GameObject 			zoompoint = null;
	Vector3 			zoomcenter;
	bool				readytozoom = false;
	GameObject			zoomslider = null;
	GameObject			infoslider = null;
	GameObject			righttop = null;

	void Awake ()
	{
		dansZone = false;
		clone = GameObject.Find ("clone");
		m_ViewCamera = gameObject.GetComponent<Camera>();
		m_ViewCamera2D = GameObject.Find("Main Camera").GetComponent<Camera>();
		zoomslider = null;  //GameObject.Find("interface");
		if (zoomslider != null)
		{
			interfacebottombar = zoomslider.FindInChildren("bottombar");
			zoomslider = zoomslider.FindInChildren("zoomslider");
			zoompoint = zoomslider.FindInChildren("zoompoint");
		}
		GameObject tmpobj = GameObject.Find ("interface");
		if (tmpobj != null)
		{
			infoslider = tmpobj.FindInChildren("ScroolHierarchie");
			righttop = tmpobj.FindInChildren("righttop");
		}
		else
		{
			infoslider = null;
			righttop = null;
		}


	}

	void Update ()
	{
//		Debug.Log ("Update");
		//if (zoomslider == null)		return;
//		Debug.Log (myinterface.focusobject);
		if (myinterface.focusobject != null)
		{

			if (myinterface.lastfocussed == null)
			{
				waittillalowed = 1000.0f;
				StartCoroutine("testmove");
//				Calculate_targetpositions();
				myinterface.lastfocussed = myinterface.focusobject;
			}
		}
		if (waittillalowed > 0.0f)
		{
			waittillalowed = waittillalowed - Time.deltaTime;
		}
		else
		{
			RaycastHit hitInfo;
			Ray ray;
			
			ray = m_ViewCamera2D.ScreenPointToRay(Input.mousePosition);

			bool validateright = true;
			if (righttop != null)
			{
				if (righttop.active)
				{
					if (infoslider.GetComponent<Collider>().Raycast(ray, out hitInfo, 1.0e8f))
						validateright = false;
					Vector3 slidervec = m_ViewCamera2D.WorldToScreenPoint(infoslider.transform.position);
					if (Input.mousePosition.x > slidervec.x)
						validateright = false;
				}
			}
				if ((zoomslider) &&(!zoomslider.GetComponent<Collider>().Raycast(ray, out hitInfo, 1.0e8f)) &&
			    (!interfacebottombar.GetComponent<Collider>().Raycast(ray, out hitInfo, 1.0e8f)) &&
			    validateright )
			{
				Debug.Log("danseone"+dansZone);
			if(dansZone){
				if (Input.GetMouseButtonDown(0))
				{
					OnMouseDown();
					mousedown = true;
				}
				if (mousedown)
				{
					OnMouseDrag();
				}
				if (Input.GetMouseButtonUp(0))
				{
					mousedown = false;
				}
			}
			}
		}

		/*if (readytozoom)
		{
			Vector3 zoomvec = zoomcenter;
			zoomvec.z = zoomvec.z + zoompoint.transform.localPosition.y * 2500.0f;
			gameObject.transform.localPosition = zoomvec;
		}*/

	}

	IEnumerator testmove()
	{
		Vector3 camerapos = transform.position;

		SetCorners(myinterface.focusobject);
		BoxCollider col = (BoxCollider)myinterface.focusobject.GetComponent<BoxCollider>();
		Vector3 colpos = col.center + myinterface.focusobject.transform.position;
		float 	smallest = 10000000.0f;
		for (int i=0;i<8;i++)
		{
			if (smallest > corners[i].transform.position.z)				smallest = corners[i].transform.position.z;
		}
		colpos.z = smallest;
		transform.position = colpos;

// check i
		int objsize = TestColliderOnScreen(myinterface.focusobject);
		if (objsize == 1)		// too small
		{
			while (objsize == 1)
			{
//				yield return new WaitForSeconds(0.03f);
				colpos.z += 1.0f;
				transform.position = colpos;
				objsize = TestColliderOnScreen(myinterface.focusobject);
			}
		}
		if (objsize == 2)		// too big
		{
			while (objsize == 2)
			{
//				yield return new WaitForSeconds(0.03f);
				colpos.z -= 1.0f;
				transform.position = colpos;
				objsize = TestColliderOnScreen(myinterface.focusobject);
			}
		}
// on screen, now keep the positions to move towards them !
		zoomcenter = transform.position;
		transform.position = camerapos;

		iTween.MoveTo(gameObject, zoomcenter, 1.0f);
		//		iTween.LookTo(gameObject,(col.center + myinterface.focusobject.transform.position),3.0f);
		for (int i=0;i<8;i++)
		{
			Destroy (corners[i]);
		}
		waittillalowed = 1.5f;
		yield return new WaitForSeconds(1.5f);
		readytozoom = true;
	}



	// 0 on screen OK
	// 1 on screen too small
	// 2 on screen too large
	void SetCorners(GameObject obj)
	{
		BoxCollider col = obj.GetComponent<BoxCollider>();
		Vector3		vec;

		vec = new Vector3(col.bounds.min.x,col.bounds.min.y,col.bounds.min.z);
		corners[0] = GameObject.Instantiate(clone) as GameObject;
		corners[0].transform.parent = obj.transform;
		corners[0].transform.position = vec;

		vec = new Vector3(col.bounds.max.x,col.bounds.min.y,col.bounds.min.z);
		corners[1] = GameObject.Instantiate(clone) as GameObject;
		corners[1].transform.parent = obj.transform;
		corners[1].transform.position = vec;

		vec = new Vector3(col.bounds.min.x,col.bounds.max.y,col.bounds.min.z);
		corners[2] = GameObject.Instantiate(clone) as GameObject;
		corners[2].transform.parent = obj.transform;
		corners[2].transform.position = vec;

		vec = new Vector3(col.bounds.max.x,col.bounds.max.y,col.bounds.min.z);
		corners[3] = GameObject.Instantiate(clone) as GameObject;
		corners[3].transform.parent = obj.transform;
		corners[3].transform.position = vec;

		vec = new Vector3(col.bounds.min.x,col.bounds.min.y,col.bounds.max.z);
		corners[4] = GameObject.Instantiate(clone) as GameObject;
		corners[4].transform.parent = obj.transform;
		corners[4].transform.position = vec;
		
		vec = new Vector3(col.bounds.max.x,col.bounds.min.y,col.bounds.max.z);
		corners[5] = GameObject.Instantiate(clone) as GameObject;
		corners[5].transform.parent = obj.transform;
		corners[5].transform.position = vec;
		
		vec = new Vector3(col.bounds.min.x,col.bounds.max.y,col.bounds.max.z);
		corners[6] = GameObject.Instantiate(clone) as GameObject;
		corners[6].transform.parent = obj.transform;
		corners[6].transform.position = vec;
		
		vec = new Vector3(col.bounds.max.x,col.bounds.max.y,col.bounds.max.z);
		corners[7] = GameObject.Instantiate(clone) as GameObject;
		corners[7].transform.parent = obj.transform;
		corners[7].transform.position = vec;

	}

	int TestColliderOnScreen(GameObject obj)
	{
		Vector3 vec = Vector3.zero;
		// project to screen !!!
		float 	maxx = -10000000.0f;
		float 	minx = 10000000.0f;
		float 	maxy = -10000000.0f;
		float 	miny = 10000000.0f;
		for (int i=0;i<8;i++)
		{
			vec = m_ViewCamera.WorldToScreenPoint (corners[i].transform.position);
			if (maxx < vec.x)				maxx = vec.x;
			if (minx > vec.x)				minx = vec.x;
			if (maxy < vec.y)				maxy = vec.y;
			if (miny > vec.y)				miny = vec.y;
		}

//		Debug.Log ("ScreenposMINx:"+minx + "  Scrnwidth:" + Screen.width);
//		Debug.Log ("ScreenposMAXx:"+maxx + "  Scrnwidth:" + Screen.width);

		if (maxx > (Screen.width-borderlimite))			return 2;
		if (maxy > (Screen.height - borderlimitn))		return 2;
		if (minx < borderlimitw)						return 2;
		if (miny < borderlimits)						return 2;

		if (maxx < (Screen.width-borderlimite))			return 1;
		if (maxy < (Screen.height-borderlimitn))		return 1;
		if (minx > borderlimitw)						return 1;
		if (miny > borderlimits)						return 1;

		return (0);
	}


	void OnMouseDown()
	{
		if (myinterface.focusobject != null)
		{
			v3StartRot = Vector3.zero;
			v2Axis = Input.mousePosition;
		}
	}

	void OnMouseDrag()
	{	Debug.Log ("myInterface focus objet  " + myinterface.focusobject);
		if ((myinterface.focusobject != null) )
		{
			myinterface.focusobject.transform.localEulerAngles = myinterface.focusobject.transform.localEulerAngles - v3StartRot;
			/*
			foreach (Transform child in myinterface.focusobject.GetComponentsInChildren<Transform>(true))
			{
				if (child.parent == myinterface.focusobject.transform)		// we are on the right level
				{
					child.localEulerAngles = child.localEulerAngles - v3StartRot;
				}
			}
			*/

			Vector3 v2T = new Vector3 (0,0,0);

			v2T.x = (Input.mousePosition.y - v2Axis.y)/3.0f;
			v2T.y = (v2Axis.x - Input.mousePosition.x)/3.0f;
			v3StartRot = v2T;

			myinterface.focusobject.transform.localEulerAngles = myinterface.focusobject.transform.localEulerAngles + v3StartRot;
			/*
			foreach (Transform child in myinterface.focusobject.GetComponentsInChildren<Transform>(true))
			{
				if (child.parent == myinterface.focusobject.transform)		// we are on the right level
				{
					child.localEulerAngles = child.localEulerAngles + v3StartRot;
				}
			}
*/
		}
	}
}
