﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Text;
using System.Globalization;
using System.IO;
using System.Net.Security;
using TcpClient = SocketEx.TcpClient;
using System.Threading;
//using BestHTTP;
using MiniJSON;


public class pp_produit_vars
{
    private pp_manager _ppm = null;
    private pp_produit _prod = null;
    private pp_produit_composant _comp = null;
    private DateTime _prod_vers = DateTime.MinValue;
    private int _mainScene_NumOnglet = 0;

    public pp_produit_vars()
    {
        _ppm = pp_manager.Instance;
    }


    // Getter / Setter
    public pp_produit Produit
    {
        get
        {
            return _prod;
        }
    }

    public pp_produit_composant Composant
    {
        get
        {
            return _comp;
        }
    }

    public int MainScene_NumOnglet
    {
        get
        {
            return _mainScene_NumOnglet;
        }
    }
        
    // Return True si le produit a changer et pret a etre affiché
    public bool LoadNewVars_Prod()
    {
        return LoadNewVars( false );
    }

    public bool LoadNewVars_Comp()
    {
        return LoadNewVars( true );
    }

    private bool LoadNewVars( bool bTestComp )
    {
        bool bRet = false;

        if (_ppm != null)
        {
            pp_produit prod_sel = _ppm.CurrentProduit;
            pp_produit_composant comp_sel = _ppm.CurrentComposant;

            // Si Changement de produit dans cbo ou changement de version du produit -> On Reload
            if( (_prod != prod_sel) ||
                ((_prod != null) && (_prod.Version != _prod_vers)) )
            {
                bool bReload = false;

                if (prod_sel == null) 
                    bReload = true; // Aucun produit n'est selectionné

                else if ( prod_sel.IsLoaded || prod_sel.IsError ) // Fin de chargement du produit (meme si en error)
                    bReload = true; // Produit selectionné et ready

                if( bReload ) // Refresh possible que si un produit n'est pas en cours de chargement
                {
                    _prod = prod_sel;
                    if (_prod != null)
                        _prod_vers = _prod.Version;

                    bRet = true;
                }  
            }

            if (_comp != comp_sel)
            {
                bool bReload = false;

                if (_prod == null) 
                    bReload = true; // Aucun produit n'est selectionné

                else if ( _prod.IsLoaded || _prod.IsError ) // Fin de chargement du produit (meme si en error)
                    bReload = true; // Produit selectionné et ready

                if (bReload) // Refresh possible que si un produit n'est pas en cours de chargement
                {
                    _comp = comp_sel;

                    if (bTestComp)
                        bRet = true;
                }
            }
        }

        return bRet;
    }

    public bool DoRefresh_MainScene()
    {
        bool bRet = LoadNewVars( true );

        if (!bRet)
        {
            bool bReload = false;

            if (_prod == null) 
                bReload = true; // Aucun produit n'est selectionné

            else if ( _prod.IsLoaded || _prod.IsError ) // Fin de chargement du produit (meme si en error)
                bReload = true; // Produit selectionné et ready

            if (bReload) // Refresh possible que si un produit n'est pas en cours de chargement
            {
                if (_ppm.Memo_MainScene.NumOnglet != _mainScene_NumOnglet)
                    bRet = true;
                else
                    bRet = _ppm.Memo_MainScene.ForceRefresh;    
            }
        }
        
        if (bRet)
        {
            _ppm.Memo_MainScene.ForceRefresh = false;
            _mainScene_NumOnglet = _ppm.Memo_MainScene.NumOnglet;
        }

        return bRet;
    }
}


public class pp_memo_mainscene {
    private int _numOnglet = 0;
    private bool _forceRefresh = false;

    private object _syncRoot = new System.Object();


    public pp_memo_mainscene()
    {
    }

    // Getter / Setter
    public int NumOnglet
    {
        get
        {
            lock (_syncRoot)
            {
                return _numOnglet;
            }
        }
        set
        {
            lock (_syncRoot)
            {
                _numOnglet = value;
            }
        }
    }
        
    public bool ForceRefresh
    {
        get
        {
            lock (_syncRoot)
            {
                return _forceRefresh;
            }
        }
        set
        {
            lock (_syncRoot)
            {
                _forceRefresh = value;
            }
        }
    }
}


public class pp_manager : MonoBehaviour {
    private static volatile pp_manager _instance;
    private static object _syncRoot = new System.Object();

    private Thread _thTaskManager = null;
    private AutoResetEvent _eWaitTaskManager = null;
    private bool _bStopTaskManager = false;
    private int _loopTime = 3000; // 3secondes
    private int _maxTaskTry = 3; 

    string _d2cfilesfilespath = "";

    private List<pp_task> _taskList = null;
    private List<pp_produit> _produits = null;

    private pp_produit _currentProduit = null;
    private pp_produit_composant _currentComposant = null;
    private pp_produit_note _currentNote = null;

    private List<pp_produit_composant> _listFocusOrderComposant = null;

    private DateTime _version = DateTime.MinValue;

    private string _messagePopupKey = "";
    private bool   _messagePopupError = false;
    private string _messagePopupKeyDisplayed = "";


    private pp_memo_mainscene _sceneMain = null;


    private void Init()
    {

        if( _taskList != null )
            _taskList.Add( new pp_task( pp_task.TypePPTask.LIST_PROD ) );

        _bStopTaskManager = false;
        _eWaitTaskManager = new AutoResetEvent(false);
        _thTaskManager = new Thread (TaskManagerThread);
        _thTaskManager.Start ();
        
    }

    // Constructor
    private pp_manager()
    {
        _d2cfilesfilespath = Application.persistentDataPath + "/d2cfiles";

        _taskList = new List<pp_task>();
        _produits = new List<pp_produit>();
        _listFocusOrderComposant = new List<pp_produit_composant>();

        _sceneMain = new pp_memo_mainscene();
        // Deplacer le Init dans Start
        // Init()
    }

    public static pp_manager Instance
    {
        get 
        {
            if (_instance == null) 
            {
                // ATTENTION Ligne non superfulx : On Créé l'instance de logger et de localization avant de passer dans le constructeur de cette class
                logger instanceLogger = logger.Instance;
                localization instancelocalization = localization.Instance;

                lock (_syncRoot) 
                {
                    if (_instance == null)
                    {
                        GameObject singleton = new GameObject();

                        _instance = singleton.AddComponent<pp_manager>();

                        singleton.name = "pp_manager";

                        DontDestroyOnLoad(singleton);
                    }
                }
            }

            return _instance;
        }
    }

    void OnDestroy()
    {
        _bStopTaskManager = true;

        if (_eWaitTaskManager != null)
            _eWaitTaskManager.Set();

        if (_thTaskManager != null)
        {
            _thTaskManager.Join();
            _thTaskManager = null;
        }
    }


	// Use this for initialization
    IEnumerator Start()
    {
        while (!downloadmanager.AllDownloaded)
            yield return null;

        Init();
	
	}
	
	// Update is called once per frame
	void Update() 
    {
        string mpk;
        string mpkd;
        bool mpe;


        lock (_syncRoot)
        { 
            mpk = _messagePopupKey;
            mpkd = _messagePopupKeyDisplayed;
            mpe = _messagePopupError;
        }

        if(  mpk != mpkd )
        {
            //GameObject loadingwindow =  GameObject.Find("loadingwindow");
            GameObject loadingwindow = load3d_object.loadingwindow;
            tk2dTextMesh loadingwindowtext = null;

            if (loadingwindow != null)
            {
                loadingwindowtext = loadingwindow.FindInChildren("loadingwindowtext").GetComponent<tk2dTextMesh>();

                if (MessagePopupKey.Length > 0)
                {
                    loadingwindow.SetActive(true);
                    settext.ForceText(loadingwindowtext, mpe, mpk );
                }
                else
                {
                    loadingwindow.SetActive(false);
                }
            }

            lock (_syncRoot)
            {
                _messagePopupKeyDisplayed = mpk;
            }
        }
	}


    // Getter/ Setter
    public DateTime Version
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _version;
            }
        }
    }


    private string MessagePopupKey
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _messagePopupKey;
            }
        }
        /* Remplacer par MessagePopupKeySet
        set
        {
            lock (_syncRoot)
            {
                _messagePopupKey = value;
            }
        }
        */
    }

    private bool MessagePopupError
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _messagePopupError;
            }
        }
        /* Remplacer par MessagePopupKeySet
        set
        {
            lock (_syncRoot)
            {
                _messagePopupError = value;
            }
        }
        */
    }

    private void MessagePopupKeySet( string mess, bool err )
    {
        lock (_syncRoot)
        {
            _messagePopupKey = mess;
            _messagePopupError = err;
        }
    }

    public pp_memo_mainscene Memo_MainScene
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _sceneMain;
            }
        }
    }


    public pp_produit CurrentProduit
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _currentProduit;
            }
        }
    }

    public pp_produit_composant CurrentComposant
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _currentComposant;
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                AddFocusOrderAdd ( value );
          
                bool bCompChange = (_currentComposant != value);
                _currentComposant = value;

                if (bCompChange)
                {
                    _currentNote = null;
                    if (_currentComposant != null)
                    {
                        //List<pp_produit_note> notes = _currentComposant.Notes;
                        List<pp_produit_note> notes = _currentComposant.NotesWithChildsNotes;
                        if (notes.Count > 0)
                            _currentNote = notes[0];
                    }
                }
            }
        }
    }


    private int SortByHierachie(pp_produit_composant el1, pp_produit_composant el2)
    {
        if (el1.HierachieNumber > el2.HierachieNumber)
            return 1;
        else if (el1.HierachieNumber < el2.HierachieNumber)
            return -1;
        else
            return 0;
    }

    private void AddFocusOrderAdd ( pp_produit_composant comp )
    {
        if (comp != null)
        {
            int idx_new = _listFocusOrderComposant.IndexOf(comp);

            if (idx_new < 0)
            {
                _listFocusOrderComposant.Add(comp);
                _listFocusOrderComposant.Sort(SortByHierachie);
            }  
        }
    }

    public void CurrentComposant_GoNext()
    {
        lock (_syncRoot)
        {
            pp_produit_composant comp = _currentComposant;
			Debug.Log (comp.Description);
            if (_currentComposant == null) // On prends le premier de la hierachie
            {
				Debug.Log (_currentProduit.Nom + " // "+ _currentProduit.roothierarchie.Provider);
                if ((_currentProduit != null) && (_currentProduit.roothierarchie != null) && (_currentProduit.roothierarchie.childs.Count > 0))
                    comp = _currentProduit.roothierarchie.childs[0];
            }
            else
            {
				Debug.Log (_currentComposant.Description);
                int idx = _listFocusOrderComposant.IndexOf(_currentComposant);
				Debug.Log (idx);
                if( (idx >= 0) && ((idx + 1) < _listFocusOrderComposant.Count) ) // On prend le prochain de _listFocusOrderComposant
                {
                    comp = _listFocusOrderComposant[idx + 1];     
                }
                else if (_currentProduit != null )  // On prend le prochain de _currentComposant dans la hierachie (list des composants trié par hierachie)
                {
                    idx = _currentProduit.composants.IndexOf(_currentComposant);
					Debug.Log (idx);
                    if( (idx >= 0) && ((idx + 1) < _currentProduit.composants.Count) )
                        comp = _currentProduit.composants[idx + 1];
					Debug.Log (comp.Description);
                }
            }

            AddFocusOrderAdd(comp);

            bool bCompChange = (_currentComposant != comp);
            _currentComposant = comp;

            if (bCompChange)
            {
                _currentNote = null;
                if (_currentComposant != null)
                {
                    //List<pp_produit_note> notes = _currentComposant.Notes;
                    List<pp_produit_note> notes = _currentComposant.NotesWithChildsNotes;
                    if (notes.Count > 0)
                        _currentNote = notes[0];
                }
            }
        }   
    }

    public void CurrentComposant_GoPrev()
    {
        lock (_syncRoot)
        {
            pp_produit_composant comp = _currentComposant;

            if (_currentComposant == null) // On prends le premier de la hierachie
            {
                if ((_currentProduit != null) && (_currentProduit.roothierarchie != null) && (_currentProduit.roothierarchie.childs.Count > 0))
                    comp = _currentProduit.roothierarchie.childs[0];
            }
            else
            {
                int idx = _listFocusOrderComposant.IndexOf(_currentComposant);

                if( (idx >= 1) && ((idx - 1) < _listFocusOrderComposant.Count) ) // On prend le precedent de _listFocusOrderComposant
                {
                    comp = _listFocusOrderComposant[idx - 1];     
                }
                else if (_currentProduit != null ) // On prend le precedent de _currentComposant dans la hierachie (list des composants trié par hierachie)
                {
                    idx = _currentProduit.composants.IndexOf(_currentComposant);
                    if( (idx >= 1) && ((idx - 1) < _currentProduit.composants.Count) )
                        comp = _currentProduit.composants[idx - 1];
                }
            }
                
            AddFocusOrderAdd(comp);

            bool bCompChange = (_currentComposant != comp);
            _currentComposant = comp;

            if (bCompChange)
            {
                _currentNote = null;
                if (_currentComposant != null)
                {
                    //List<pp_produit_note> notes = _currentComposant.Notes;
                    List<pp_produit_note> notes = _currentComposant.NotesWithChildsNotes;
                    if (notes.Count > 0)
                        _currentNote = notes[0];
                }
            }
        } 
    }

    public void CurrentComposant_GoFirst()
    {
        lock (_syncRoot)
        {
            pp_produit_composant comp = _currentComposant;
		
            if( _currentComposant.parent != null )

            if (_currentProduit != null)
            {

                if ((_currentComposant.parent != null) && (_currentComposant.parent != _currentProduit.roothierarchie))
                    comp = _currentComposant.parent;
                else if((_currentProduit.roothierarchie != null) && (_currentProduit.roothierarchie.childs.Count > 0))
                    comp = _currentProduit.roothierarchie.childs[0];
            }

            AddFocusOrderAdd(comp);

            bool bCompChange = (_currentComposant != comp);
            _currentComposant = comp;

            if (bCompChange)
            {
                _currentNote = null;
                if (_currentComposant != null)
                {
                    //List<pp_produit_note> notes = _currentComposant.Notes;
                    List<pp_produit_note> notes = _currentComposant.NotesWithChildsNotes;
                    if (notes.Count > 0)
                        _currentNote = notes[0];
                }
            }
        } 
    }

    public int CurrentProduitId
    {
        get 
        { 
            int idx = -1;
            
            lock (_syncRoot)
            {
                if( (_produits != null) && (_currentProduit != null) )
                {
                    idx = _produits.IndexOf(_currentProduit);
                }   
            }

            return idx;
        }
        set
        {
            lock (_syncRoot)
            {
                if( (_produits != null) && (value >= 0) && (value < _produits.Count) )
                {
                    if (_currentProduit != _produits[value])
                    {

                        if ((_produits[value] != null) && !_produits[value].IsLoaded && !_produits[value].IsLoading)
                            _produits[value].LoadData();
                    
                        _currentProduit = _produits[value];
                        _currentComposant = null;
                        if ((_currentProduit != null) && (_currentProduit.roothierarchie != null) && (_currentProduit.roothierarchie.childs.Count > 0))
                            _currentComposant = _currentProduit.roothierarchie.childs[0];

                        _listFocusOrderComposant.Clear();
                        if (_currentComposant != null)
                            _listFocusOrderComposant.Add(_currentComposant);

                        _currentNote = null;
                        if (_currentComposant != null)
                        {
                            //List<pp_produit_note> notes = _currentComposant.Notes;
                            List<pp_produit_note> notes = _currentComposant.NotesWithChildsNotes;
                            if( notes.Count > 0 )
                                _currentNote = notes[0];
                        }
                    }
                }   
            }
        }
    }


    public pp_produit_note CurrentNote
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _currentNote;
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _currentNote = value;
            }
        }
    }

    public List<pp_produit> Produits
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _produits;
            }
        }
    }
        

    // Liste des taches a effecuter en tache de fond
    private int SortProduits(pp_produit el1, pp_produit el2)
    {
        return el1.Nom.CompareTo(el2.Nom);
    }

    bool TaskListProduit( pp_task task)
    {
        bool bRet = false;

        MessagePopupKeySet( "FILE_DIR_NOT_AVAIL_TEXT", false );
       
        //Thread.Sleep(5000);

        // A FINIR : ICI AJOUTER LA RECUPERATION DES DATAS SUR LE SITE WEB

        lock (_syncRoot)
        {
            if (_produits != null)
            {
                _produits.Clear();

                try
                {
                  
                    pp_produit prod_sel = null;

                    string [] testlist = Directory.GetDirectories(_d2cfilesfilespath);
                    for (int i=0;i<testlist.Length;i++)
                    {
                        string nom =  testlist[i].Replace("\\","/");
                        int idx = nom.LastIndexOf("/");

                        if( idx >= 0 )
                            nom = nom.Substring( idx + 1 );

                        _produits.Add( new pp_produit( nom, _d2cfilesfilespath ) );
                    }
               
                    _produits.Sort( SortProduits );


                    foreach( pp_produit prod in _produits )
                    {
                        if( (_currentProduit != null) && (prod.Nom == _currentProduit.Nom) )
                        {
                            prod_sel = prod;
                            break;
                        }
                        else if( prod_sel == null )
                            prod_sel = prod;
                        
                        else if( prod.Nom == "GeneralElectric" )
                            prod_sel = prod;
                    }

                    if( prod_sel != null )
                    {
                        if( prod_sel != _currentProduit )
                        {

                            if (!prod_sel.IsLoaded && !prod_sel.IsLoading)
                                prod_sel.LoadData();
                            
                            _currentProduit = prod_sel;
                            _currentComposant = null;
                            if ((_currentProduit != null) && (_currentProduit.roothierarchie != null) && (_currentProduit.roothierarchie.childs.Count > 0))
                                _currentComposant = _currentProduit.roothierarchie.childs[0];

                            _listFocusOrderComposant.Clear();
                            if (_currentComposant != null)
                                _listFocusOrderComposant.Add(_currentComposant);

                            _currentNote = null;
                            if (_currentComposant != null)
                            {
                                //List<pp_produit_note> notes = _currentComposant.Notes;
                                List<pp_produit_note> notes = _currentComposant.NotesWithChildsNotes;
                                if( notes.Count > 0 )
                                    _currentNote = notes[0];
                            }
                        }
                    }
                        
                    bRet = true;

                }
                catch( Exception ex )
                {
                    if (task != null)
                        task.ErrorMessage = ex.Message;
                    
                    logger.LogException("pp_manager", "TaskListProduit", ex.Message);
                }


                _version = DateTime.Now;
                    
            }  
        }


        MessagePopupKeySet( "", false );

        return bRet;
    }

    bool TaskLoadProduit( pp_task task)
    {
        bool bRet = true;
        bool bFin = false;

        while ( !bFin )
        {
            switch (task.Produit.EtatLoad)
            {
                case pp_produit.EtatPPProduitLoad.LOAD_BOM:
                    MessagePopupKeySet("LOADING_BOM_TEXT", false);

                    //Thread.Sleep(3000);

                    // A FINIR : ICI AJOUTER LA RECUPERATION DES DATAS SUR LE SITE WEB
                    task.Produit.LoadBEGIN();
                    if( bRet )
                        bRet = task.Produit.LoadBOM();
                    
                    if ( bRet )
                    {
                        task.Produit.EtatLoadSet( pp_produit.EtatPPProduitLoad.LOAD_PROC, task );
                    }
                    else
                    {
                        MessagePopupKeySet( "ERROR_LOAD_BOM_TEXT", true );
                        Thread.Sleep(3000);
                        bFin = true;
                    }
                    break;

                case pp_produit.EtatPPProduitLoad.LOAD_PROC:
                    MessagePopupKeySet( "LOADING_PROCEDURE_TEXT", false );

                    // A FINIR : ICI AJOUTER LA RECUPERATION DES DATAS SUR LE SITE WEB

                    if( bRet )
                        bRet = task.Produit.LoadPROC();
                    
                    if ( bRet )
                    {
                        task.Produit.EtatLoadSet( pp_produit.EtatPPProduitLoad.LOAD_NOTE, task );
                    }
                    else
                    {
                        MessagePopupKeySet( "ERROR_LOAD_PROCEDURE_TEXT", true );
                        Thread.Sleep(3000);
                        bFin = true;
                    }
                    break;

                case pp_produit.EtatPPProduitLoad.LOAD_NOTE:
                    MessagePopupKeySet( "LOADING_NOTES_TEXT", false );

                    // A FINIR : ICI AJOUTER LA RECUPERATION DES DATAS SUR LE SITE WEB

                    if( bRet )
                        bRet = task.Produit.LoadNOTE();
                    
                    if ( bRet )
                    {
                        task.Produit.EtatLoadSet( pp_produit.EtatPPProduitLoad.LOAD_FONC, task );
                    }
                    else
                    {
                        MessagePopupKeySet( "ERROR_LOAD_NOTES_TEXT", true );
                        Thread.Sleep(3000);
                        bFin = true;
                    }
                    break;

                case pp_produit.EtatPPProduitLoad.LOAD_FONC:
                    MessagePopupKeySet( "LOADING_FUNCTIONS_TEXT", false );

                    // A FINIR : ICI AJOUTER LA RECUPERATION DES DATAS SUR LE SITE WEB

                    if( bRet )
                        bRet = task.Produit.LoadFONC();

                    if ( bRet )
                    {
                        task.Produit.EtatLoadSet( pp_produit.EtatPPProduitLoad.LOAD_OBJ, task );
                    }
                    else
                    {
                        MessagePopupKeySet( "ERROR_LOAD_FUNCTIONS_TEXT", true );
                        Thread.Sleep(3000);
                        bFin = true;
                    }
                    break;


                case pp_produit.EtatPPProduitLoad.LOAD_OBJ:
                    MessagePopupKeySet( "LOADING_3D_OBJECT_TEXT", false );

                    // A FINIR : ICI AJOUTER LA RECUPERATION DES DATAS SUR LE SITE WEB

                    if( bRet )
                        bRet = task.Produit.LoadOBJ();
                    
                    if ( bRet )
                    {
                        task.Produit.EtatLoadSet( pp_produit.EtatPPProduitLoad.LOAD_MTL, task );
                    }
                    else
                    {
                        MessagePopupKeySet( "ERROR_LOAD_3D_OBJECT_TEXT", true );
                        Thread.Sleep(3000);
                        bFin = true;
                    }
                    break;


                case pp_produit.EtatPPProduitLoad.LOAD_MTL:
                    MessagePopupKeySet( "LOADING_MATERIAL_TEXT", false );

                    // A FINIR : ICI AJOUTER LA RECUPERATION DES DATAS SUR LE SITE WEB

                    if( bRet )
                        bRet = task.Produit.LoadMTL();
                    
                    if ( bRet )
                    {
                        task.Produit.EtatLoadSet( pp_produit.EtatPPProduitLoad.LOAD_INI, task );
                    }
                    else
                    {
                        MessagePopupKeySet( "ERROR_LOAD_MATERIAL_TEXT", true );
                        Thread.Sleep(3000);
                        bFin = true;
                    }
                    break;

                case pp_produit.EtatPPProduitLoad.LOAD_INI:
                    MessagePopupKeySet( "LOADING_BOM_LINK_FILE_TEXT", false );

                    // A FINIR : ICI AJOUTER LA RECUPERATION DES DATAS SUR LE SITE WEB

                    if( bRet )
                        bRet = task.Produit.LoadINI();

                    if ( bRet )
                    {
                        task.Produit.EtatLoadSet( pp_produit.EtatPPProduitLoad.LOAD_SCENARIO, task );
                    }
                    else
                    {
                        MessagePopupKeySet( "ERROR_LOAD_BOM_LINK_FILE_TEXT", true );
                        Thread.Sleep(3000);
                        bFin = true;
                    }
                    break;


                case pp_produit.EtatPPProduitLoad.LOAD_SCENARIO:
                    MessagePopupKeySet( "LOADING_SCENARIO_TEXT", false );

                    // A FINIR : ICI AJOUTER LA RECUPERATION DES DATAS SUR LE SITE WEB

                    if( bRet )
                        bRet = task.Produit.LoadSCENAR();

                    if ( bRet )
                    {
                        task.Produit.LoadEND();

                        lock (_syncRoot)
                        {
                            if (_currentProduit == task.Produit)
                            {
                                _currentComposant = null;
                                if ((_currentProduit != null) && (_currentProduit.roothierarchie != null) && (_currentProduit.roothierarchie.childs.Count > 0))
                                    _currentComposant = _currentProduit.roothierarchie.childs[0];

                                _listFocusOrderComposant.Clear();
                                if (_currentComposant != null)
                                    _listFocusOrderComposant.Add(_currentComposant);
                                
                                _currentNote = null;
                                if (_currentComposant != null)
                                {
                                    //List<pp_produit_note> notes = _currentComposant.Notes;
                                    List<pp_produit_note> notes = _currentComposant.NotesWithChildsNotes;
                                    if( notes.Count > 0 )
                                        _currentNote = notes[0];
                                }
                            }
                        }


                        task.Produit.EtatLoadSet( pp_produit.EtatPPProduitLoad.LOAD_COMPLETED, task );
                    }
                    else
                    {
                        MessagePopupKeySet( "ERROR_LOAD_SCENARIO_TEXT", true );
                        Thread.Sleep(3000);
                        bFin = true;
                    }
                    break;

                case pp_produit.EtatPPProduitLoad.LOAD_COMPLETED:
                    bRet = true;
                    bFin = true;
                    break;

                case pp_produit.EtatPPProduitLoad.LOAD_ERROR:
                default:
                    bRet = false;
                    bFin = true;
                    break;
                
            }

            if (task.IsCanceled)
            {
                bRet = (task.Produit.EtatLoad == pp_produit.EtatPPProduitLoad.LOAD_COMPLETED);
                bFin = true;
            }
        }
            
        MessagePopupKeySet( "", false );

        return bRet;
    }

    bool TaskSaveProduit( pp_task task)
    {
        bool bRet = true;
        bool bFin = false;

        while ( !bFin )
        {
            switch (task.Produit.EtatSave)
            {
                case pp_produit.EtatPPProduitSave.SAVE_BOM:
                    MessagePopupKeySet("SAVING_BOM_TEXT", false);

                    //Thread.Sleep(3000);

                    // A FINIR : ICI AJOUTER LA RECUPERATION DES DATAS SUR LE SITE WEB

                    if( bRet )
                        bRet = task.Produit.SaveBOM();

                    if ( bRet )
                    {
                        task.Produit.EtatSaveSet( pp_produit.EtatPPProduitSave.SAVE_PROC, task );
                    }
                    else
                    {
                        MessagePopupKeySet( "ERROR_SAVE_BOM_TEXT", true );
                        Thread.Sleep(3000);
                        bFin = true;
                    }
                    break;

                case pp_produit.EtatPPProduitSave.SAVE_PROC:
                    MessagePopupKeySet( "SAVING_PROCEDURE_TEXT", false );

                    // A FINIR : ICI AJOUTER LA RECUPERATION DES DATAS SUR LE SITE WEB

                    if( bRet )
                        bRet = task.Produit.SavePROC();

                    if ( bRet )
                    {
                        task.Produit.EtatSaveSet( pp_produit.EtatPPProduitSave.SAVE_NOTE, task );
                    }
                    else
                    {
                        MessagePopupKeySet( "ERROR_SAVE_PROCEDURE_TEXT", true );
                        Thread.Sleep(3000);
                        bFin = true;
                    }
                    break;

                case pp_produit.EtatPPProduitSave.SAVE_NOTE:
                    MessagePopupKeySet( "SAVING_NOTES_TEXT", false );

                    // A FINIR : ICI AJOUTER LA RECUPERATION DES DATAS SUR LE SITE WEB

                    if( bRet )
                        bRet = task.Produit.SaveNOTE();

                    if ( bRet )
                    {
                        task.Produit.EtatSaveSet( pp_produit.EtatPPProduitSave.SAVE_FONC, task );
                    }
                    else
                    {
                        MessagePopupKeySet( "ERROR_SAVE_NOTES_TEXT", true );
                        Thread.Sleep(3000);
                        bFin = true;
                    }
                    break;

                case pp_produit.EtatPPProduitSave.SAVE_FONC:
                    MessagePopupKeySet( "SAVING_FUNCTIONS_TEXT", false );

                    // A FINIR : ICI AJOUTER LA RECUPERATION DES DATAS SUR LE SITE WEB

                    if( bRet )
                        bRet = task.Produit.SaveFONC();

                    if ( bRet )
                    {
                        task.Produit.EtatSaveSet( pp_produit.EtatPPProduitSave.SAVE_OBJ, task );
                    }
                    else
                    {
                        MessagePopupKeySet( "ERROR_SAVE_FUNCTIONS_TEXT", true );
                        Thread.Sleep(3000);
                        bFin = true;
                    }
                    break;


                case pp_produit.EtatPPProduitSave.SAVE_OBJ:
                    MessagePopupKeySet( "SAVING_3D_OBJECT_TEXT", false );

                    // A FINIR : ICI AJOUTER LA RECUPERATION DES DATAS SUR LE SITE WEB

                    if( bRet )
                        bRet = task.Produit.SaveOBJ();

                    if ( bRet )
                    {
                        task.Produit.EtatSaveSet( pp_produit.EtatPPProduitSave.SAVE_MTL, task );
                    }
                    else
                    {
                        MessagePopupKeySet( "ERROR_SAVE_3D_OBJECT_TEXT", true );
                        Thread.Sleep(3000);
                        bFin = true;
                    }
                    break;


                case pp_produit.EtatPPProduitSave.SAVE_MTL:
                    MessagePopupKeySet( "SAVING_MATERIAL_TEXT", false );

                    // A FINIR : ICI AJOUTER LA RECUPERATION DES DATAS SUR LE SITE WEB

                    if( bRet )
                        bRet = task.Produit.SaveMTL();

                    if ( bRet )
                    {
                        task.Produit.EtatSaveSet( pp_produit.EtatPPProduitSave.SAVE_INI, task );
                    }
                    else
                    {
                        MessagePopupKeySet( "ERROR_SAVE_MATERIAL_TEXT", true );
                        Thread.Sleep(3000);
                        bFin = true;
                    }
                    break;

                case pp_produit.EtatPPProduitSave.SAVE_INI:
                    MessagePopupKeySet( "SAVING_BOM_LINK_FILE_TEXT", false );

                    // A FINIR : ICI AJOUTER LA RECUPERATION DES DATAS SUR LE SITE WEB

                    if( bRet )
                        bRet = task.Produit.SaveINI();

                    if ( bRet )
                    {
                        task.Produit.EtatSaveSet( pp_produit.EtatPPProduitSave.SAVE_SCENARIO, task );
                    }
                    else
                    {
                        MessagePopupKeySet( "ERROR_SAVE_BOM_LINK_FILE_TEXT", true );
                        Thread.Sleep(3000);
                        bFin = true;
                    }
                    break;

                case pp_produit.EtatPPProduitSave.SAVE_SCENARIO:
                    MessagePopupKeySet( "SAVING_SCENARIO_TEXT", false );

                    // A FINIR : ICI AJOUTER LA RECUPERATION DES DATAS SUR LE SITE WEB

                    if( bRet )
                        bRet = task.Produit.SaveSCENAR();

                    if ( bRet )
                    {
                        task.Produit.EtatSaveSet( pp_produit.EtatPPProduitSave.SAVE_COMPLETED, task );
                    }
                    else
                    {
                        MessagePopupKeySet( "ERROR_SAVE_SCENARIO_TEXT", true );
                        Thread.Sleep(3000);
                        bFin = true;
                    }
                    break;

                case pp_produit.EtatPPProduitSave.SAVE_COMPLETED:
                    bRet = true;
                    bFin = true;
                    break;

                case pp_produit.EtatPPProduitSave.SAVE_ERROR:
                default:
                    bRet = false;
                    bFin = true;
                    break;

            }

            if (task.IsCanceled)
            {
                bRet = (task.Produit.EtatSave == pp_produit.EtatPPProduitSave.SAVE_COMPLETED);
                bFin = true;
            }

        }

        MessagePopupKeySet( "", false );

        return bRet;
    }

    bool TaskPingServerWeb()
    {
        // A FINIR Ajouter ici le check des version des produits

        return true;
    }

    // TaskManager
    private void TaskManagerThread( object param )
    {
        int loopTime;
        bool bStopTaskManager;
        pp_task task;
        int maxTaskTry;
        DateTime curTime;
        DateTime lastTimeTryPingServerWeb = DateTime.MinValue;

        lock (_syncRoot)
        {
            bStopTaskManager = _bStopTaskManager;
        }

        while (!bStopTaskManager)
        {
            curTime = DateTime.Now;

            lock (_syncRoot)
            {
                loopTime = _loopTime;
                maxTaskTry = _maxTaskTry;

                task = null;
                if (_taskList != null)
                {
                    foreach (pp_task t in _taskList)
                    {
                        if (t.LastTimeTry.AddSeconds(loopTime / 1000) < curTime) // loopTime en ms -> / 1000
                        {
                            task = t;
                            break;
                        }
                    }
                }
            }

            // Traitement des task
            if (task != null)
            {
                bool bRet = false;

                if (!task.IsCanceled)
                {
                    switch (task.TypeTask)
                    {
                        case pp_task.TypePPTask.LIST_PROD:
                            bRet = TaskListProduit(task);
                            break;

                        case pp_task.TypePPTask.LOAD_PROD:
                            bRet = TaskLoadProduit(task);
                            break;

                        case pp_task.TypePPTask.SAVE_PROD:
                            bRet = TaskSaveProduit(task);
                            break;

                        default:
                            bRet = true;
                            break;
                    }
                }

                curTime = DateTime.Now;

                task.LastTimeTry = curTime;
                task.TryCount += 1;


                if (bRet || task.IsCanceled || (task.TryCount >= maxTaskTry))
                {
                    switch (task.TypeTask)
                    {
                        case pp_task.TypePPTask.LOAD_PROD:
                            if (bRet)
                                task.Produit.EtatLoadSet( pp_produit.EtatPPProduitLoad.LOAD_COMPLETED, task );
                            else
                                task.Produit.EtatLoadSet( pp_produit.EtatPPProduitLoad.LOAD_ERROR, task );
                            break;

                        case pp_task.TypePPTask.SAVE_PROD:
                            if (bRet)
                                task.Produit.EtatSaveSet( pp_produit.EtatPPProduitSave.SAVE_COMPLETED, task );
                            else
                                task.Produit.EtatSaveSet( pp_produit.EtatPPProduitSave.SAVE_ERROR, task );
                            break;

                        default:
                            break;
                    }
                                               
                    task.IsFinished = true;
                               
                    // On enleve la Tache
                    lock (_syncRoot)
                    {
                        if (_taskList != null)
                        {
                            _taskList.Remove(task);
                        }
                    }
                }
            }
            else
            {
                // Recuperation des Temps Réel
                if (lastTimeTryPingServerWeb.AddSeconds(loopTime / 1000) < curTime) // loopTime en ms -> / 1000
                {
                    TaskPingServerWeb();
                    lastTimeTryPingServerWeb = DateTime.Now;
                }
                else
                {
                    loopTime = (int)(curTime - lastTimeTryPingServerWeb).TotalMilliseconds;

                    if( loopTime < 1000 )
                        loopTime = 1000; // Minimun une oause de 1S
                }


                if (_eWaitTaskManager != null)
                    _eWaitTaskManager.WaitOne(loopTime);
                else
                    Thread.Sleep(loopTime);
            }

            lock (_syncRoot)
            {
                bStopTaskManager = _bStopTaskManager;
            }
        }
    }
        
    // Public Methode
    public pp_task GetDataProduit( pp_produit prod )
    {
        pp_task task = null;

        logger.LogDebug("pp_manager", "GetDataProduit", prod.Nom);

        lock (_syncRoot)
        {
            if (_taskList != null)
            {
                task = new pp_task(pp_task.TypePPTask.LOAD_PROD, prod);

                _taskList.Add( task );

                if (_eWaitTaskManager != null)
                    _eWaitTaskManager.Set();
            }
        }

        return task;
    }

    public pp_task SetDataProduit( pp_produit prod )
    {
        pp_task task = null;

        logger.LogDebug("pp_manager", "SetDataProduit", prod.Nom);

        lock (_syncRoot)
        {
            if (_taskList != null)
            {
                task = new pp_task(pp_task.TypePPTask.SAVE_PROD, prod);

                _taskList.Add( task );

                if (_eWaitTaskManager != null)
                    _eWaitTaskManager.Set();
            }
        }

        return task;
    }


    // Static Methode
    public static pp_task LoadDataProduit(pp_produit prod)
    {
        pp_manager ppManager = pp_manager.Instance;

        return ppManager.GetDataProduit( prod );
    }

    public static pp_task SaveDataProduit(pp_produit prod)
    {
		
        pp_manager ppManager = pp_manager.Instance;
	
        return ppManager.SetDataProduit( prod );
    }

    public static pp_produit GetCurrentProduit()
    {
        pp_manager ppManager = pp_manager.Instance;
        pp_produit prod = null;

        if (ppManager != null)
            prod = ppManager.CurrentProduit;

        return prod;
    }

    public static pp_produit_composant GetCurrentComposant()
    {
        pp_manager ppManager = pp_manager.Instance;
        pp_produit_composant comp = null;

        if (ppManager != null)
            comp = ppManager.CurrentComposant;


        return comp;
    }

    public static void SetCurrentComposant(pp_produit_composant comp)
    {
        pp_manager ppManager = pp_manager.Instance;

        if (ppManager != null)
            ppManager.CurrentComposant = comp;
    }

    public static void SetCurrentComposant(string compName)
    {
        pp_manager ppManager = pp_manager.Instance;

        if (ppManager != null)
        {
            pp_produit prod = ppManager.CurrentProduit;
            pp_produit_composant comp = null;

            if (prod != null)
                comp = prod.FindComposant(compName);

            ppManager.CurrentComposant = comp;
        }
    }


    public static void NextCurrentComposant()
    {
        pp_manager ppManager = pp_manager.Instance;

        if (ppManager != null)
        {
            ppManager.CurrentComposant_GoNext();
        }
    }

    public static void PrevCurrentComposant()
    {
        pp_manager ppManager = pp_manager.Instance;

        if (ppManager != null)
        {
            ppManager.CurrentComposant_GoPrev();
        }
    }

    public static void FirstCurrentComposant()
    {
        pp_manager ppManager = pp_manager.Instance;

        if (ppManager != null)
        {
            ppManager.CurrentComposant_GoFirst();
        }
    }



    public static pp_produit_note GetCurrentNote()
    {
        pp_manager ppManager = pp_manager.Instance;
        pp_produit_note note = null;

        if (ppManager != null)
            note = ppManager.CurrentNote;


        return note;
    }

    public static void SetCurrentNote(pp_produit_note note)
    {
        pp_manager ppManager = pp_manager.Instance;

        if (ppManager != null)
            ppManager.CurrentNote = note;
    }

    public static pp_memo_mainscene MainScene
    {
        get 
        { 
            pp_manager ppManager = pp_manager.Instance;
            pp_memo_mainscene mainScene = null;

            if (ppManager != null)
            {
                mainScene = ppManager.Memo_MainScene;
            }

            return mainScene;
        }
    }
}
