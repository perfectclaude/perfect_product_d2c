﻿using UnityEngine;
using System.Collections;

public class object_cell : MonoBehaviour
{
	public Vector3	direction;
	public Vector3	pos;
	public float	targetcolora = 1.0f;
	public int		open = 0;

	float	colora = 1.0f;
	Color	mycolor;
	float	bouncy = 0.0f;
	Renderer rend;

	void Awake()
	{
//		rend = GetComponent<Renderer>();
//		rend.material.shader = Shader.Find("GUI/Text Shader");
//		rend.material.SetColor("_EmissionColor",Color.white);
//		mycolor = rend.material.GetColor("_Color");
//		mycolor.a = colora;
	/*
		MeshFilter BaseMeshFilter = transform.GetComponent("MeshFilter") as MeshFilter;
		Mesh mesh = BaseMeshFilter.mesh;
		
		//reverse triangle winding
		int[] triangles= mesh.triangles;
		
		int numpolies = triangles.Length / 3;
		for(int t=0;t < numpolies;t++)
		{
			int tribuffer = triangles[t*3];
			triangles[t*3]=triangles[(t*3)+2];
			triangles[(t*3)+2]=tribuffer;
		}
		
		//readjust uv map for inner sphere projection
		Vector2[] uvs = mesh.uv;
		for(int uvnum = 0;uvnum < uvs.Length;uvnum++)
		{
			uvs[uvnum]=new Vector2(1-uvs[uvnum].x,uvs[uvnum].y);
		}
		
		//readjust normals for inner sphere projection    
		Vector3[] norms = mesh.normals;        
		for(int normalsnum = 0;normalsnum < norms.Length;normalsnum++)
		{
			norms[normalsnum]=-norms[normalsnum];
		}
		
		//copy local built in arrays back to the mesh
		mesh.uv = uvs;
		mesh.triangles=triangles;
		mesh.normals=norms;    
		*/
	}
	
	void Update()
	{
		if (open == 1)
		{
			bouncy += 0.1f;
			if (bouncy > Mathf.PI / 2.0f)
			{
				bouncy = Mathf.PI / 2.0f;
				open = 0;
			}
			float multiplier = (Mathf.Sin (bouncy)) * 1.0f;
			Vector3 destvec = direction;
			destvec.x = destvec.x * multiplier;
			
			gameObject.transform.localPosition = pos + (direction * multiplier);
		}
		if (open == 2)
		{
			bouncy -= 0.1f;
			if (bouncy < 0)
			{
				bouncy = 0;
				open = 0;
			}
			float multiplier = (Mathf.Sin (bouncy)) * 1.0f;
			Vector3 destvec = direction;
			destvec.x = destvec.x * multiplier;
			
			gameObject.transform.localPosition = pos + (direction * multiplier);
		}

		/*
		if (targetcolora != colora)
		{
			if (targetcolora > colora)
			{
				colora += 0.01f;
				if (targetcolora < colora)
					colora = targetcolora;
			}
			if (targetcolora < colora)
			{
				colora -= 0.01f;
				if (targetcolora > colora)
					colora = targetcolora;
			}
			mycolor.a = colora;
			rend.material.SetColor("_Color",mycolor);
		}
			*/

	}
}
