﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class scrollIdee : MonoBehaviour {
	public GameObject scroolPart;
    private float _maxDist = 0f;
    private Scrollbar _scrollbar = null; 
    private bool _onInit = false;


    public void init( float maxDist, float value )
    {
        _onInit = true;

        _scrollbar = GetComponent<Scrollbar>();

        if( (_scrollbar != null) && (scroolPart != null) )
        {
            _maxDist = maxDist;
            _scrollbar.value = value;

            if (_maxDist > 0)
            {
                gameObject.SetActive(true);
                scroolPart.transform.localPosition = new Vector3(0, _scrollbar.value * _maxDist, 0);
            }
            else
            {
                gameObject.SetActive(false);
                scroolPart.transform.localPosition = new Vector3(0, 0, 0);
            }
        }

        _onInit = false;

    }
	
    public void init( float maxDist )
    {
        _scrollbar = GetComponent<Scrollbar>();

        if (_scrollbar != null)
            init(maxDist, _scrollbar.value);
    }

	public void OnClick()
    {
        if( (_scrollbar != null) && (scroolPart != null) && !_onInit && (_maxDist > 0) )
            scroolPart.transform.localPosition = new Vector3 (0,  _scrollbar.value * _maxDist, 0);
		
	}



}
