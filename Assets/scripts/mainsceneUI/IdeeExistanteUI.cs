﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class IdeeExistanteUI : MonoBehaviour {
    private pp_produit_note _note = null;
	
	// Use this for initialization
    public void init(pp_produit_note note){
        _note = note;
        if (_note != null)
        {
            transform.GetChild(2).GetComponent<Text>().text = _note.Numero;
            transform.GetChild(3).GetComponent<Text>().text = _note.TitreIdee;
            transform.GetChild(5).GetComponent<Text>().text = _note.GainPotentielUnitaire + " €";
            transform.GetChild(4).GetComponent<Text>().text = _note.ComposantName;
        }

	}
	
	public  void OnClick ()
	{
	//	base.ButtonPressed();

        if (_note != null)
        {
            if (!GameObject.Find("Pop-up"))
            {
		
                GameObject nouv = Instantiate(Resources.Load("FicheIdeeAntoine")) as GameObject;
                nouv.name = "Pop-up";

                nouv.transform.SetParent(GameObject.Find("CanvasFiche").transform);
                nouv.transform.localPosition = Vector3.zero;
                nouv.transform.localScale = Vector3.one;
                nouv.GetComponent<RectTransform>().localScale = Vector3.one;
                nouv.GetComponent<ficheIdeeInit>().init(_note, false);

            }
        }
	}
}
