﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class gotofonctionnalityUI : MonoBehaviour {
	public  bool fon=false;

	public  void OnClick(){

        pp_produit prod = pp_manager.GetCurrentProduit();
        if( (prod != null) && (prod.Fonctionalites.Count > 0) )
        {
			fon = !fon;
			if(fon){
                transform.GetChild(0).GetComponent<Text>().text=localization.GetTextWithKey("IDEA_TEXT");
				transform.GetChild(1).GetComponent<Text>().text="I";
			}
			else{
                transform.GetChild(0).GetComponent<Text>().text=localization.GetTextWithKey("FUNCTIONALITY_TEXT");
				transform.GetChild(1).GetComponent<Text>().text="F";
			}
			//CreateHierarchieUI.chang = true;
		}	
	
	}
}
