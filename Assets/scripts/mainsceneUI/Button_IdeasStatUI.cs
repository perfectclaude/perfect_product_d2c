﻿using UnityEngine;
using System.Collections;

public class Button_IdeasStatUI : MonoBehaviour {

	public void OnClick()
    {

        if (gameObject.name == "fonctionality")
            bufferme.LoadLevel("fonctionality");
        else if (gameObject.name == "ajoutFonc")
        {
            pp_produit prod = pp_manager.GetCurrentProduit();
            if (prod != null)
            {
                int NbFonc = prod.Fonctionalites.Count;
                prod.Fonctionalites.Add("Functionality " + (NbFonc + 1).ToString());

                bufferme.LoadLevel("fonctionality");
            }
        }
        else
            bufferme.LoadLevel("ideastats");
    }

}
