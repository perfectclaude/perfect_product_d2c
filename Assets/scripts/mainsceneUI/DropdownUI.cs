using UnityEngine;
using System;
using System.Collections;
using UnityEngine.UI;

public class DropdownUI : MonoBehaviour {
   
	public string DebugGUI = "Debug";

	private GUIStyle _styleLabel;
    private pp_manager _ppm = null;
    private DateTime _ppm_vers = DateTime.MinValue;
    private Dropdown _cbo = null;

    private bool _init = false;

    void Start(){
        _ppm = pp_manager.Instance;
        _cbo = gameObject.GetComponent<Dropdown>();

        if( (_ppm != null) && (_cbo != null))
        {
            _ppm_vers = _ppm.Version;

            _cbo.options.Clear();
            foreach( pp_produit prod in _ppm.Produits )
            {
                _cbo.options.Add(new Dropdown.OptionData(prod.Nom));
            }

            _cbo.interactable = (_cbo.options.Count > 0);
            _cbo.value = _ppm.CurrentProduitId;
        }

        _init = true;
	}


	void Awake()
	{
		_styleLabel = new GUIStyle ();
		_styleLabel.normal.textColor = Color.blue;
		
        _cbo = gameObject.GetComponent<Dropdown>();
        if( _cbo != null )
            _cbo.interactable = false;

        _init = false;
	}

	void OnGUI(){
		//GUI.contentColor = Color.red;
		//GUI.Label (new Rect (Screen.width/2, Screen.height/2-50, 500, 250), DebugGUI,styleLabel);
	}

	void Update()
    {

        if( _init && (_ppm != null) && (_cbo != null) )
        {
            if (_ppm_vers != _ppm.Version)
            {
                _ppm_vers = _ppm.Version;

                _cbo.options.Clear();
                foreach (pp_produit prod in _ppm.Produits)
                {
                    _cbo.options.Add(new Dropdown.OptionData(prod.Nom));
                }

                _cbo.interactable = (_cbo.options.Count > 0);
            }

            _cbo.value = _ppm.CurrentProduitId;
        }
	}
        
	public  void OnCliCk ()
	{
        if (_init)
        {
            if ((_ppm != null) && (_cbo != null))
            {
                _ppm.CurrentProduitId = _cbo.value;
            }
        }
	}

	void OnLevelWasLoaded(int level) {
		
		if (level == 0) {
		//	Debug.Log("on loaded level");
			//transform.GetComponent<Dropdown> ().value=valeur;
			//transform.GetComponent<Dropdown> ().captionText.text=nom;
		}
		
	}

}
