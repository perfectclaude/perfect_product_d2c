﻿using UnityEngine;
using System.Collections;

public class ButtonCloseUI : MonoBehaviour {

	// Use this for initialization
	
	public  void OnClick ()
	{
        pp_produit prod = pp_manager.GetCurrentProduit();
        if (prod != null)
        {
            pp_manager.MainScene.ForceRefresh = true;
            Destroy(GameObject.Find("Pop-up"));

            prod.SaveData();


            try
            {
                GameObject.Find("BarreCreerScenario").GetComponent<CreateScenario>().init = true;
            }
            catch
            {
			
            }
        }
	}

}
