﻿using UnityEngine;
using System.Collections;

public class camerascaletoobjectUI : MonoBehaviour {

	public static bool dansZone;
	Camera 				m_ViewCamera;
	Camera 				m_ViewCamera2D;
	static float		borderlimitn = 40.0f;
	static float		borderlimits = 40.0f;
	static float		borderlimite = Screen.width * 0.3f;
	static float		borderlimitw = Screen.width * 0.1f;
	bool				zoomin = false;
	float				timecountdown;
	float				zoomdistance;
	GameObject			clone;

	float 				fStartAngle;
	bool				mousedown = false;
	float				waittillalowed = 0.0f;
	GameObject			[] 	corners = new GameObject[8];
	
	GameObject			interfacebottombar = null;
	GameObject 			zoompoint = null;
	Vector3 			zoomcenter;
	bool				readytozoom = false;
	GameObject			zoomslider = null;
	GameObject			infoslider = null;
	GameObject			righttop = null;
	GameObject 			sliderHierarchie=null;
	Vector3 mouseP;
	bool debut;

	public enum Rotation_modes
	{
		nothing,
		rotationXY,
		rotationZ
	};

	float distancelimit = 0;
	float distancedetect = 0;
	float lastcircle = -10000;
	float lastrotZ = 0;
	float lastrotX = 0;
	float lastrotY = 0;
	float rotSpeed = 10.0f;
	float startmpX;
	float startmpY;
	float olddiffx;
	float olddiffy;
	Rotation_modes rotation_mode = Rotation_modes.nothing;

	void Awake ()
	{
		distancelimit = (Screen.height * 0.8f) * 0.5f * 0.5f;
		debut = false;
		dansZone = false;
		clone = GameObject.Find ("clone");
		m_ViewCamera = gameObject.GetComponent<Camera>();
		m_ViewCamera2D = GameObject.Find("Main Camera").GetComponent<Camera>();
		zoomslider = GameObject.Find("scrollZoom");  //GameObject.Find("interface");
		sliderHierarchie=GameObject.Find("ScrollbarDroite");
		if (zoomslider != null)
		{
			//interfacebottombar = zoomslider.FindInChildren("bottombar");
			//zoomslider = zoomslider.FindInChildren("zoomslider");
			//zoompoint = zoomslider.FindInChildren("zoompoint");
		}
		GameObject tmpobj = GameObject.Find ("interface");
		if (tmpobj != null)
		{
			infoslider = tmpobj.FindInChildren("ScroolHierarchie");
			righttop = tmpobj.FindInChildren("righttop");
		}
		else
		{
			infoslider = null;
			righttop = null;
		}
		
		
	}
	
	void Update ()
	{
		if (myinterface.focusobject != null)
		{
			if (myinterface.lastfocussed == null)
			{
				waittillalowed = 1000.0f;
				StartCoroutine("testmove");
				//				Calculate_targetpositions();
				myinterface.lastfocussed = myinterface.focusobject;
			}
		}


		if (waittillalowed > 0.0f) {
			waittillalowed = waittillalowed - Time.deltaTime;
		} else {
			RaycastHit hitInfo;
			Ray ray;

			ray = m_ViewCamera2D.ScreenPointToRay (Input.mousePosition);
			
			bool validateright = true;
			if (righttop != null) {
				if (righttop.active) {
					if (infoslider.GetComponent<Collider> ().Raycast (ray, out hitInfo, 1.0e8f))
						validateright = false;
					Vector3 slidervec = m_ViewCamera2D.WorldToScreenPoint (infoslider.transform.position);
					if (Input.mousePosition.x > slidervec.x)
						validateright = false;
				}
			}
		
			if (Input.GetMouseButtonUp (0))
			{
				rotation_mode = Rotation_modes.nothing;
				lastcircle = -10000;
			}
			if ((zoomslider) && (!RectTransformUtility.RectangleContainsScreenPoint(zoomslider.GetComponent<RectTransform>(), Input.mousePosition))
			    /*&& (!RectTransformUtility.RectangleContainsScreenPoint(sliderHierarchie.GetComponent<RectTransform>(), Input.mousePosition))*/
			    /*validateright*/) {
			
				bool rotatemenow = false;
				bool movemenow = false;
				if ((lastrotX != 0) || (lastrotY != 0) || (lastrotZ != 0))
					rotatemenow = true;
				if ((olddiffx != 0) || (olddiffy != 0))
					movemenow = true;

				if(dansZone){
				if(changeMouvement.movement){
						rotatemenow = true;
					if (Input.GetMouseButtonDown (0)) {
						mousedown = true;
					}
					if (mousedown) {
					}
					if (Input.GetMouseButtonUp (0)) {
						mousedown = false;
					}
				}
				else{
						if (Input.GetMouseButton (0)||Input.touchCount==1) {
							movemenow = true;
						} 
				}
				}
				if (movemenow)
				{
					if ((debut) || (olddiffx != 0) || (olddiffy != 0)) {
						float diffx = 0;
						float diffy = 0;
						if (Input.GetMouseButton(0))
						{
							diffx= mouseP.x - Input.mousePosition.x;
							diffy = mouseP.y - Input.mousePosition.y;
							olddiffx = diffx;
							olddiffy = diffy;
						}
						else
						{
							olddiffx *=0.85f;
							if ((olddiffx > 0.0f) && (olddiffx < 0.0001f))		olddiffx = 0.0f;
							if ((olddiffx < 0.0f) && (olddiffx > -0.0001f))		olddiffx = 0.0f;
							olddiffy *=0.85f;
							if ((olddiffy > 0.0f) && (olddiffy < 0.0001f))		olddiffy = 0.0f;
							if ((olddiffy < 0.0f) && (olddiffy > -0.0001f))		olddiffy = 0.0f;
							diffx= olddiffx;
							diffy = olddiffy;
						}
						Vector3 vec = myinterface.focusobject.transform.localPosition;
						vec.x -= diffx;
						vec.y -= diffy;
						myinterface.focusobject.transform.localPosition = vec;
						mouseP = Input.mousePosition;
					} else {
						debut = true;
						mouseP = Input.mousePosition;
					}
				}
				if (rotatemenow)
				{
					if (myinterface.focusobject != null)
					{
						float newmouseX = 0;
						float newmouseY = 0;
						if (Input.GetMouseButton(0))
						{
							Vector3 vec1 = Input.mousePosition;
							vec1.z = 0;
							Vector3 vec2 = m_ViewCamera.WorldToScreenPoint(load3d_object.objectcenter.transform.position);
							vec2.z = 0;
							float posx = (vec1.x - vec2.x);
							float posy = (vec1.y - vec2.y);
							distancedetect = Vector3.Distance(vec1,vec2);
							if (((distancedetect > distancelimit) || (rotation_mode == Rotation_modes.rotationZ))
								&& (rotation_mode != Rotation_modes.rotationXY))
							{
								float rotateme = Mathf.Atan2(posy,posx);
								lastrotX = 0;
								lastrotY = 0;
								if (lastcircle == -10000)
								{
									lastcircle = rotateme;
									lastrotZ = 0;
								}
								else
								{
									lastrotZ = rotateme - lastcircle;
									lastcircle = rotateme;
								}
								rotation_mode = Rotation_modes.rotationZ;
							}
							else
							{
								newmouseX = Input.GetAxis("Mouse X");
								newmouseY = Input.GetAxis("Mouse Y");
								lastrotX = newmouseX;
								lastrotY = newmouseY;
								rotation_mode = Rotation_modes.rotationXY;
							}
						}
						else
						{	
							lastrotX *=0.85f;
							if ((lastrotX > 0.0f) && (lastrotX < 0.0001f))		lastrotX = 0.0f;
							if ((lastrotX < 0.0f) && (lastrotX > -0.0001f))		lastrotX = 0.0f;
							lastrotY *=0.85f;
							if ((lastrotY > 0.0f) && (lastrotY < 0.0001f))		lastrotY = 0.0f;
							if ((lastrotY < 0.0f) && (lastrotY > -0.0001f))		lastrotY = 0.0f;
							lastrotZ *=0.80f;
							if ((lastrotZ > 0.0f) && (lastrotZ < 0.0001f))		lastrotZ = 0.0f;
							if ((lastrotZ < 0.0f) && (lastrotZ > -0.0001f))		lastrotZ = 0.0f;
							newmouseX = lastrotX;
							newmouseY = lastrotY;
						}
						if (((distancedetect > distancelimit) || (rotation_mode == Rotation_modes.rotationZ))
							&& (rotation_mode != Rotation_modes.rotationXY))
						{
							myinterface.focusobject.transform.RotateAround(Vector3.forward, lastrotZ);
						}
						else
						{
							float rotX = newmouseX*rotSpeed*Mathf.Deg2Rad;
							float rotY = newmouseY*rotSpeed*Mathf.Deg2Rad;
							myinterface.focusobject.transform.RotateAround(Vector3.up, -rotX);
							myinterface.focusobject.transform.RotateAround(Vector3.right, rotY);
						}
					}
				}
				//}
			}

			if (!(Input.GetMouseButton (0)||Input.touchCount==1)){
				
				debut = false;
			}
			/*if (readytozoom)
		{
			Vector3 zoomvec = zoomcenter;
			zoomvec.z = zoomvec.z + zoompoint.transform.localPosition.y * 2500.0f;
			gameObject.transform.localPosition = zoomvec;
		}*/
		}
	}
	
	IEnumerator testmove()
	{
		Vector3 camerapos = transform.position;
		
		SetCorners(myinterface.focusobject);
		BoxCollider col = (BoxCollider)myinterface.focusobject.GetComponent<BoxCollider>();
		Vector3 colpos = col.center + myinterface.focusobject.transform.position;
		float 	smallest = 10000000.0f;
		for (int i=0;i<8;i++)
		{
			if (smallest > corners[i].transform.position.z)				smallest = corners[i].transform.position.z;
		}
		colpos.z = smallest;
		transform.position = colpos;
		
		// check i
		int objsize = TestColliderOnScreen(myinterface.focusobject);
		if (objsize == 1)		// too small
		{
			while (objsize == 1)
			{
				//				yield return new WaitForSeconds(0.03f);
				colpos.z += 1.0f;
				transform.position = colpos;
				objsize = TestColliderOnScreen(myinterface.focusobject);
			}
		}
		if (objsize == 2)		// too big
		{
			while (objsize == 2)
			{
				//				yield return new WaitForSeconds(0.03f);
				colpos.z -= 1.0f;
				transform.position = colpos;
				objsize = TestColliderOnScreen(myinterface.focusobject);
			}
		}
		// on screen, now keep the positions to move towards them !
		zoomcenter = transform.position;
		transform.position = camerapos;
		
		iTween.MoveTo(gameObject, zoomcenter, 1.0f);
		//		iTween.LookTo(gameObject,(col.center + myinterface.focusobject.transform.position),3.0f);
		for (int i=0;i<8;i++)
		{
			Destroy (corners[i]);
		}
		waittillalowed = 1.5f;
		yield return new WaitForSeconds(1.5f);
		readytozoom = true;
		//Debug.Log ("initialisation zoom");

		GameObject.Find ("scrollZoom").transform.GetComponent<scroolCameraZoom>().reinitialiser();
		GameObject.Find ("scrollZoom").transform.GetComponent<scroolCameraZoom>().cam = myinterface.my3dcamera;
		GameObject.Find ("scrollZoom").transform.GetComponent<scroolCameraZoom>().puissance = myinterface.my3dcamera.transform.localPosition.z;
		GameObject.Find ("scrollZoom").transform.GetComponent<scroolCameraZoom>().zoomOk=true;
	}
	
	
	
	// 0 on screen OK
	// 1 on screen too small
	// 2 on screen too large
	void SetCorners(GameObject obj)
	{
		BoxCollider col = obj.GetComponent<BoxCollider>();
		Vector3		vec;
		
		vec = new Vector3(col.bounds.min.x,col.bounds.min.y,col.bounds.min.z);
		corners[0] = GameObject.Instantiate(clone) as GameObject;
		corners[0].transform.parent = obj.transform;
		corners[0].transform.position = vec;
		
		vec = new Vector3(col.bounds.max.x,col.bounds.min.y,col.bounds.min.z);
		corners[1] = GameObject.Instantiate(clone) as GameObject;
		corners[1].transform.parent = obj.transform;
		corners[1].transform.position = vec;
		
		vec = new Vector3(col.bounds.min.x,col.bounds.max.y,col.bounds.min.z);
		corners[2] = GameObject.Instantiate(clone) as GameObject;
		corners[2].transform.parent = obj.transform;
		corners[2].transform.position = vec;
		
		vec = new Vector3(col.bounds.max.x,col.bounds.max.y,col.bounds.min.z);
		corners[3] = GameObject.Instantiate(clone) as GameObject;
		corners[3].transform.parent = obj.transform;
		corners[3].transform.position = vec;
		
		vec = new Vector3(col.bounds.min.x,col.bounds.min.y,col.bounds.max.z);
		corners[4] = GameObject.Instantiate(clone) as GameObject;
		corners[4].transform.parent = obj.transform;
		corners[4].transform.position = vec;
		
		vec = new Vector3(col.bounds.max.x,col.bounds.min.y,col.bounds.max.z);
		corners[5] = GameObject.Instantiate(clone) as GameObject;
		corners[5].transform.parent = obj.transform;
		corners[5].transform.position = vec;
		
		vec = new Vector3(col.bounds.min.x,col.bounds.max.y,col.bounds.max.z);
		corners[6] = GameObject.Instantiate(clone) as GameObject;
		corners[6].transform.parent = obj.transform;
		corners[6].transform.position = vec;
		
		vec = new Vector3(col.bounds.max.x,col.bounds.max.y,col.bounds.max.z);
		corners[7] = GameObject.Instantiate(clone) as GameObject;
		corners[7].transform.parent = obj.transform;
		corners[7].transform.position = vec;
		
	}
	
	int TestColliderOnScreen(GameObject obj)
	{
		Vector3 vec = Vector3.zero;
		// project to screen !!!
		float 	maxx = -10000000.0f;
		float 	minx = 10000000.0f;
		float 	maxy = -10000000.0f;
		float 	miny = 10000000.0f;
		for (int i=0;i<8;i++)
		{
			vec = m_ViewCamera.WorldToScreenPoint (corners[i].transform.position);
			if (maxx < vec.x)				maxx = vec.x;
			if (minx > vec.x)				minx = vec.x;
			if (maxy < vec.y)				maxy = vec.y;
			if (miny > vec.y)				miny = vec.y;
		}
		
		//		Debug.Log ("ScreenposMINx:"+minx + "  Scrnwidth:" + Screen.width);
		//		Debug.Log ("ScreenposMAXx:"+maxx + "  Scrnwidth:" + Screen.width);
		
		if (maxx > (Screen.width-borderlimite))			return 2;
		if (maxy > (Screen.height - borderlimitn))		return 2;
		if (minx < borderlimitw)						return 2;
		if (miny < borderlimits)						return 2;
		
		if (maxx < (Screen.width-borderlimite))			return 1;
		if (maxy < (Screen.height-borderlimitn))		return 1;
		if (minx > borderlimitw)						return 1;
		if (miny > borderlimits)						return 1;
		
		return (0);
	}

}
