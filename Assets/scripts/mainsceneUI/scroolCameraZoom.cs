﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class scroolCameraZoom : MonoBehaviour {
	public GameObject cam;
	public float puissance;
	public bool zoomOk;
	// Use this for initialization
	void awake () {
		cam = GameObject.Find ("3dcamera");
		puissance = 0;
		zoomOk = false;
	}

	void Update(){
		if (Input.touchCount == 2) {
			
			// Store both touches.
			Touch touchZero = Input.GetTouch (0);
			Touch touchOne = Input.GetTouch (1);

			// Find the position in the previous frame of each touch.
			Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
			Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

			// Find the magnitude of the vector (the distance) between the touches in each frame.
			float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
			float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

			// Find the difference in the distances between each frame.
			float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;
			transform.GetComponent<Scrollbar> ().value += deltaMagnitudeDiff * 0.1f;
		}

		
	}


	
	public void OnClick(){
		if (zoomOk) {
			Debug.Log (puissance);
			Vector3 vec = cam.transform.localPosition;
			cam.transform.localPosition = new Vector3 (vec.x, vec.y, puissance * 2 * transform.GetComponent<Scrollbar> ().value);
		} else {
			reinitialiser();
		}
	}

	public void reinitialiser ()
	{
		transform.GetComponent<Scrollbar> ().value = 0.5f;
	}
}
