using UnityEngine;
using System.Collections;
using UnityEngine.UI;



public class disassebleUI : MonoBehaviour {
	private bool assem ;
    private GameObject img1;
    private GameObject img2;
	public GameObject stat;
    private showtext txtAssem;


    public void init()
    {
        assem = true;

        txtAssem.mykey = "ECLATE_TEXT";
        img1.SetActive(true);
        img2.SetActive (false);
    }


    void Awake()
    {
        img1 = transform.GetChild (0).gameObject;
        img2 = transform.GetChild (1).gameObject;
        txtAssem = gameObject.FindInChildren("txtAss").GetComponent<showtext>();
    }

	void Start()
    {
        init();	
	}


	public void OnClick(){
		if(!stat.GetComponent<afficheStat>().open)
			stat.GetComponent<afficheStat>().OnClick();
        
		assem=!assem;
		if(assem){
			img2.SetActive(false);
			img1.SetActive(true);
            txtAssem.mykey = "ECLATE_TEXT";
		}
		else{
			img2.SetActive(true);
			img1.SetActive(false);
            txtAssem.mykey = "ASSEMBLAGE_TEXT";
		}
		camerascaletoobjectUI cameracomp = (camerascaletoobjectUI)myinterface.my3dcamera.GetComponent<camerascaletoobjectUI>();

        if (myinterface.focusobject != null)
        {
            load3d_object.AssembleObject(myinterface.focusobject, assem);

            if( load3d_object.cloneObjet != null )
                load3d_object.AssembleObject(load3d_object.cloneObjet, assem);
        }
	}
}
