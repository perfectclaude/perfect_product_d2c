﻿using UnityEngine;
using System.Collections;

public class ButtonScenarioOpenUI : MonoBehaviour {

	public void OnClick ()
	{
		StartCoroutine("Save");
		
	}
	
	IEnumerator Save()
	{
        pp_produit prod = pp_manager.GetCurrentProduit();

        if (prod != null)
        {
            pp_task task_save = prod.SaveData();

            if (task_save != null)
            {
                while( !task_save.IsFinished )
                    yield return new WaitForSeconds(0.05f);
            }
        }
		
		bufferme.LoadLevel("SceneScenarioAntoine");
	}
}
