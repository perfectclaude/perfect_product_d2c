﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class statKilo : MonoBehaviour {
	public static bool KiloOn;

	void Awake()
	{
		KiloOn = false;
	}

    private int SortByPriceWeight(pp_produit_composant el1, pp_produit_composant el2)
    {
        if (el1.UnitPriceByNetWeight_Val > el2.UnitPriceByNetWeight_Val)
            return -1;
        else if (el1.UnitPriceByNetWeight_Val < el2.UnitPriceByNetWeight_Val)
            return 1;
        else
            return 0;
    }

	private int SortByPriceWeightScenario(pp_produit_composant el1, pp_produit_composant el2)
	{
		if (el1.getPrixsurPoidsScenario(load3d_object.scenObjet) > el2.getPrixsurPoidsScenario(load3d_object.scenObjet))
			return -1;
		else if (el1.getPrixsurPoidsScenario(load3d_object.scenObjet) < el2.getPrixsurPoidsScenario(load3d_object.scenObjet))
			return 1;
		else
			return 0;
	}

	private int SortByPriceWeightSceanrioClone(pp_produit_composant el1, pp_produit_composant el2)
	{
		if (el1.getPrixsurPoidsScenario(load3d_object.scenClone) > el2.getPrixsurPoidsScenario(load3d_object.scenClone))
			return -1;
		else if (el1.getPrixsurPoidsScenario(load3d_object.scenClone) < el2.getPrixsurPoidsScenario(load3d_object.scenClone))
			return 1;
		else
			return 0;
	}

	public  void OnClick ()
	{
		statABC.AbcOn = false;
		statKilo.KiloOn = true;

        pp_produit prod = pp_manager.GetCurrentProduit();
        if (prod != null)
        {
            button_openanalyse.lastpressed = 1;
		
            List<pp_produit_composant> lstWithNoChild =  new List<pp_produit_composant>();
			List<pp_produit_composant> lstWithNoChildClone =  new List<pp_produit_composant>();

            foreach (pp_produit_composant comp in prod.composants)
            {
                if (comp.childs.Count <= 0)
                {
                    lstWithNoChild.Add(comp);
					if (load3d_object.cloneActif) {
						lstWithNoChildClone.Add (comp);
					}

                }
            }
			if (load3d_object.scenObjet == -1) {
				lstWithNoChild.Sort (SortByPriceWeight);
			} else {
				lstWithNoChild.Sort (SortByPriceWeightScenario);
			}
			if (load3d_object.cloneActif) {
				if (load3d_object.scenClone == -1) {
					lstWithNoChildClone.Sort (SortByPriceWeight);
				} else {
					lstWithNoChildClone.Sort (SortByPriceWeightSceanrioClone);
				}
			}

            // Raz des couleur a l'origine
            afficheStat.ColorAllChilds();


            float stepper = 1.0f / (float)lstWithNoChild.Count;
            Color colset = new Color(1.0f, 0.0f, 0.0f);
			int i = 0;
            foreach (pp_produit_composant comp in lstWithNoChild)
            {
                afficheStat.ColorAllChilds("#_" + comp.Description, colset,load3d_object.objectcenter);

                if (load3d_object.cloneActif)
                    afficheStat.ColorAllChilds( "cloneObjet_#_" + lstWithNoChildClone[i].Description, colset, load3d_object.cloneObjet);
                
                colset.r -= stepper;
                colset.g += stepper;
				i++;
            }  
        }
	}
}
