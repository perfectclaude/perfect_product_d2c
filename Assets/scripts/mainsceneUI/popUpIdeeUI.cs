﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class popUpIdeeUI : MonoBehaviour {

    private pp_produit_note _note = null;
	
    public void initPopup(pp_produit_note note)
    {
        _note = note;
        if (_note != null)
        {
            //transform.GetChild(0).GetChild(0).GetComponent<Dropdown>().value = loadcsv.notes[ligne][1];
            string date = "";
            setUpStatutUI.note = _note;
            setUpStatutUI.init = true;

            switch (_note.Pilote)
            {
                case "I":
                    date = _note.ComposantName;
                    transform.GetChild(0).GetComponent<Dropdown>().value = 0;
                    break;
                case "F":
                    transform.GetChild(0).GetComponent<Dropdown>().value = 1;
                    date = _note.SousEnsemble;
                    break;

                case "V":
                    transform.GetChild(0).GetComponent<Dropdown>().value = 2;
                    date = _note.LeverDeGain;
                    break;
                case "D":
                    transform.GetChild(0).GetComponent<Dropdown>().value = 3;
                    date = _note.StatutIdee;
                    break;

            }
            transform.GetChild(1).GetChild(0).GetComponent<Text>().text = date;
            transform.parent.GetChild(3).GetComponent<InputField>().text = _note.DescriptionIdee;
            transform.parent.GetChild(4).GetChild(0).GetComponent<InputField>().text = _note.DateMAJ;
            transform.GetChild(3).GetComponent<InputField>().text = _note.TitreIdee;
            transform.GetChild(4).GetComponent<InputField>().text = _note.ProduitName;
        }
	}
    public void initVide(pp_produit_note note)
    {
        _note = note;
        if (_note != null)
        {
            setUpStatutUI.note = _note;
            setUpStatutUI.changeStatut("I");
        }
	}

	public void ecrireCSV()
    {
        if (_note != null)
        {
            _note.DateMAJ = transform.parent.GetChild(4).GetChild(0).GetComponent<InputField>().text;
            _note.DescriptionIdee = transform.parent.GetChild(3).GetComponent<InputField>().text;

            _note.TitreIdee = transform.GetChild(3).GetComponent<InputField>().text;
            //_note.Produit = transform.GetChild(4).GetComponent<InputField>().text;
        }
	}
}
