using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class editPriceUI : MonoBehaviour {

	GameObject SecondPlan;
	GameObject PremierPlan;
	GameObject boutonAjout;

    private pp_produit_composant _composant;
    private bool _onInit = false; 


    public void init( pp_produit_composant composant )
    {
        _onInit = true;

        _composant = composant;
        if (_composant != null)
        {
            transform.GetChild (5).GetComponent<Text> ().text = _composant.Description;
            transform.GetChild (6).GetComponent<Text> ().text = _composant.UnitPrice + " €";

            ChangeOnglet(pp_manager.MainScene.NumOnglet);
        }

        _onInit = false;
    }

	 void Start(){
		SecondPlan = transform.GetChild (0).gameObject;
		PremierPlan=transform.GetChild (1).gameObject;
		boutonAjout=transform.GetChild (2).gameObject;
	}



	public void ChangeOnglet(int num){
		SecondPlan.GetComponent<SecondPlan> ().Change (num);
		PremierPlan.GetComponent<PremierPlan> ().Change (num);
		boutonAjout.GetComponent<ajouterQuoi> ().Change (num);


		if (num == 0) {
            transform.GetChild (3).GetComponent<Text> ().text = localization.GetTextWithKey("PARTS_TEXT");
		} else if (num == 1) {
            transform.GetChild (3).GetComponent<Text> ().text = localization.GetTextWithKey("IDEAS_TEXT");
		} else {
            transform.GetChild (3).GetComponent<Text> ().text = localization.GetTextWithKey("FUNCTIONS_TEXT");
		}

        if( !_onInit )
            pp_manager.MainScene.NumOnglet = num;
	}
}
