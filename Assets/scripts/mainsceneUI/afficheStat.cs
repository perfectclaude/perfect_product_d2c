﻿using UnityEngine;
using System.Collections;

public class afficheStat : MonoBehaviour {

	GameObject	analyse_popup;
	public  bool		open = true;


	void Awake()
	{
		open = true;
		analyse_popup = transform.GetChild(1).gameObject;
		analyse_popup.SetActive (false);

	}
	
	
	
	public  void OnClick ()
	{
		statABC.AbcOn = false;
		statKilo.KiloOn = false;
        pp_produit_composant comp = pp_manager.GetCurrentComposant();

        if (comp != null)
        {

            if (open)
            {
                analyse_popup.SetActive(true);
                //iTween.MoveTo(analyse_popup, mypos, 1.0f);
                open = false;

                // force all colors back
                ColorAllChilds();
                //ColorAllChilds("#_" + comp.Description, load3d_object.objectcenter);
                //ColorAllChilds("cloneObjet_#_" + comp.Description, load3d_object.cloneObjet);
                GameObject.Find("GoDown").transform.GetComponent<GoDownStat>().down();
            }
            else
            {
                //iTween.MoveTo(analyse_popup, new Vector3(mypos.x + 1.285f,mypos.y,mypos.z), 1.0f);
                open = true;

                ColorAllChilds();
                //ColorAllChilds("#_" + comp.Description, load3d_object.objectcenter);
                //ColorAllChilds("cloneObjet_#_" + comp.Description, load3d_object.cloneObjet);
                GameObject.Find("GoDown").transform.GetComponent<GoDownStat>().up();
            }
        }


		
	}

    public static void ColorAllChilds()
    {
        ColorAllChilds(load3d_object.objectcenter);
        ColorAllChilds(load3d_object.cloneObjet);
    }

    public static void ColorAllChilds(GameObject g)
    {
        if( g != null )
            ColorAllChilds(g.name, Color.white, g, true);
    }

    public static void ColorAllChilds(string branchname,GameObject g)
    {
        ColorAllChilds(branchname, Color.white, g, true);
    }

    public static void ColorAllChilds(string branchname, Color col, GameObject g)
    {
        ColorAllChilds(branchname, col, g, false);
    }

    public static void ColorAllChilds(string branchname,Color col, GameObject g, bool raz)
    {
        if (g != null)
        {
            foreach (Transform child in g.GetComponentsInChildren<Transform>(true))
            {
                if( child.name == branchname ) 
                {
                    foreach (Transform inner in child.gameObject.GetComponentsInChildren<Transform>(true))
                    {
                        attachedcolor atta = (attachedcolor)inner.gameObject.GetComponent<attachedcolor>();
                        if (atta != null)
                        {
                            Renderer rend;
                            rend = inner.gameObject.GetComponent<Renderer>();

                            if ( raz )
                                rend.material.SetColor("_Color", atta.mycolor); // Couleur par defaut
                            else
                                rend.material.SetColor("_Color", col);
                        }
                    }
                    break;
                }
            }
        }
    }

}
