﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class statGain : MonoBehaviour {

    private pp_produit_composant _myActualComp = null;
	
	void Awake()
	{
	}
	
	void Update()
	{

		if (button_openanalyse.lastpressed == 3)
		{
			if (button_openanalyse.open)
			{
                pp_produit_composant comp = pp_manager.GetCurrentComposant();

                if (comp != _myActualComp)
                {
                    RedoDisplay();
                }
			}
		}
	}
	
   
	
	public  void OnClick ()
	{
		statABC.AbcOn = false;
		statKilo.KiloOn = false;
		button_openanalyse.lastpressed = 3;
		RedoDisplay();
	}
	

    private int SortByGain(pp_produit_composant el1, pp_produit_composant el2)
    {
        if (el1.Gain_Val > el2.Gain_Val)
            return -1;
        else if (el1.Gain_Val < el2.Gain_Val)
            return 1;
        else
            return 0;
    }

	void RedoDisplay()
	{
        pp_produit_composant curComp = pp_manager.GetCurrentComposant();
        if (curComp != null)
        {
            _myActualComp = curComp;

            List<pp_produit_composant> lstChids =  new List<pp_produit_composant>();

            foreach (pp_produit_composant comp in curComp.childs)
            {
                //if( comp.Gain != 0 )
                    lstChids.Add(comp);
            }

            lstChids.Sort(SortByGain);

            for (int i = 0; i < lstChids.Count; i++)
            {
                 Debug.Log(i.ToString() + ")" + lstChids[i].Description + "  gains:" + lstChids[i].Gain_Val);
            }
		
            // Raz des couleur a l'origine
            afficheStat.ColorAllChilds();

            // calc color division
            float stepper = 1.0f / (float)lstChids.Count;
            Color colset = new Color(1.0f, 0.0f, 0.0f);
            for (int i = 0; i < lstChids.Count; i++)
            {
                afficheStat.ColorAllChilds("#_" + lstChids[i].Description, colset,load3d_object.objectcenter);
                afficheStat.ColorAllChilds("cloneObjet_#_" + lstChids [i].Description, colset, load3d_object.cloneObjet);
                colset.r -= stepper;
                colset.g += stepper;
            }
        }
	}

}
