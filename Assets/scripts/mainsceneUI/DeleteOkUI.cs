﻿using UnityEngine;
using System.Collections;

public class DeleteOkUI : MonoBehaviour {
    public pp_produit_note note = null;

	public void OnClick()
    {
        if (note != null)
        {
            pp_produit_composant comp = note.Composant;

            if (comp != null)
            {
                comp.Notes.Remove(note);
            }
        }

		Destroy (GameObject.Find ("Pop-up"));
	}
}
