﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class statIdee : MonoBehaviour {

    private pp_produit_composant _myActualComp = null;
	
	void Awake()
	{
	}
	
	void Update()
	{

		if (button_openanalyse.lastpressed == 2)
		{
			if (button_openanalyse.open)
			{
                pp_produit_composant comp = pp_manager.GetCurrentComposant();

                if (comp != _myActualComp)
				{
					RedoDisplay();
				}
			}
		}
	}
	

	public  void OnClick ()
	{
		statABC.AbcOn = false;
		statKilo.KiloOn = false;
		button_openanalyse.lastpressed = 2;
		RedoDisplay();
	}

    private int SortByNbIdeas(pp_produit_composant el1, pp_produit_composant el2)
    {
        /*
        if (el1.Notes.Count > el2.Notes.Count)
            return -1;
        else if (el1.Notes.Count < el2.Notes.Count)
            return 1;
        else
            return 0;
        */
        if (el1.NotesWithChildsNotes.Count > el2.NotesWithChildsNotes.Count)
            return -1;
        else if (el1.NotesWithChildsNotes.Count < el2.NotesWithChildsNotes.Count)
            return 1;
        else
            return 0;
    }


	
	void RedoDisplay()
	{
        pp_produit_composant curComp = pp_manager.GetCurrentComposant();
        if (curComp != null)
        {
            _myActualComp = curComp;
                
            List<pp_produit_composant> lstChids =  new List<pp_produit_composant>();

            foreach (pp_produit_composant comp in curComp.childs)
            {
                //if( comp.Notes.Count != 0 )
                    lstChids.Add(comp);
            }

            lstChids.Sort(SortByNbIdeas);

            for (int i = 0; i < lstChids.Count; i++)
            {
                //Debug.Log(i.ToString() + ")" + lstChids[i].Description + "  ideas:" + lstChids[i].Notes.Count);
                Debug.Log(i.ToString() + ")" + lstChids[i].Description + "  ideas:" + lstChids[i].NotesWithChildsNotes.Count);
            }


            // Raz des couleur a l'origine
            afficheStat.ColorAllChilds();
                
            // calc color division
            float stepper = 1.0f / (float)lstChids.Count;
            Color colset = new Color(1.0f, 0.0f, 0.0f);
            for (int i = 0; i < lstChids.Count; i++)
            {
                //			Debug.Log (i.ToString() + ") "+ componentnames[i]);
                afficheStat.ColorAllChilds("#_" + lstChids[i].Description, colset,load3d_object.objectcenter);
                afficheStat.ColorAllChilds("cloneObjet_#_" + lstChids [i].Description, colset, load3d_object.cloneObjet);
                colset.r -= stepper;
                colset.g += stepper;
            }
        }
	}

}
