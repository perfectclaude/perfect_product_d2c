﻿using UnityEngine;
using System.Collections;

public class ButtonPanelOffUI : MonoBehaviour {

	GameObject iconon = null;
	GameObject iconoff = null;
	GameObject newpanelfather;
	public GameObject stat;
	
	void Awake()
	{
	
		iconon = gameObject.FindInChildren("iconon");
		iconoff = gameObject.FindInChildren("iconoff");
		DisplayIcons();
	}
	
	void Update()
	{
	
		DisplayIcons();
	}
	
	void DisplayIcons()
	{
		newpanelfather = GameObject.Find ("fullscreen").FindInChildren("newpanelfather");
		if (autoloadcurrent3dobject.icondisplay)
		{
			if (iconon != null)
				iconon.SetActive(true);
			if (iconoff != null)
				iconoff.SetActive(false);
			if (newpanelfather != null)
				newpanelfather.SetActive(true);
		}
		else
		{
			if (iconon != null)
				iconon.SetActive(false);
			if (iconoff != null)
				iconoff.SetActive(true);
			if (newpanelfather != null)
				newpanelfather.SetActive(false);
		}
	}
	
	public void OnClick()
	{
		if(!stat.GetComponent<afficheStat>().open)
			stat.GetComponent<afficheStat>().OnClick();

		if (autoloadcurrent3dobject.icondisplay)
			autoloadcurrent3dobject.icondisplay = false;
		else
			autoloadcurrent3dobject.icondisplay = true;
		DisplayIcons();
	}

}
