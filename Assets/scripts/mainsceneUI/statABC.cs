﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class statABC : MonoBehaviour {
	public static bool AbcOn;

	void Awake()
	{
		AbcOn = false;
	}

    private int SortByPrice(pp_produit_composant el1, pp_produit_composant el2)
    {
        if (el1.UnitPrice_Val > el2.UnitPrice_Val)
            return -1;
        else if (el1.UnitPrice_Val < el2.UnitPrice_Val)
            return 1;
        else
            return 0;
    }

	private int SortByPriceScenario(pp_produit_composant el1, pp_produit_composant el2)
	{
		if (el1.getPrixScenario(load3d_object.scenObjet) > el2.getPrixScenario(load3d_object.scenObjet) )
			return -1;
		else if (el1.getPrixScenario(load3d_object.scenObjet) < el2.getPrixScenario(load3d_object.scenObjet))
			return 1;
		else
			return 0;
	}

	private int SortByPriceScenarioClone(pp_produit_composant el1, pp_produit_composant el2)
	{
		if (el1.getPrixScenario(load3d_object.scenClone) > el2.getPrixScenario(load3d_object.scenClone) )
			return -1;
		else if (el1.getPrixScenario(load3d_object.scenClone) < el2.getPrixScenario(load3d_object.scenClone))
			return 1;
		else
			return 0;
	}

       
	public  void OnClick ()
	{
		statABC.AbcOn = true;
		statKilo.KiloOn = false;
        pp_produit prod = pp_manager.GetCurrentProduit();
        if (prod != null)
        {
            button_openanalyse.lastpressed = 0;
		
            List<pp_produit_composant> lstWithNoChild =  new List<pp_produit_composant>();

			List<pp_produit_composant> lstWithNoChildClone = new List<pp_produit_composant> ();

			float totalcost = 0.0f; 
			float totalcostClone = 0.0f;

            float costcount = 0.0f;

            foreach (pp_produit_composant comp in prod.composants)
            {
                if (comp.childs.Count <= 0)
                {
					
					if (load3d_object.scenObjet == -1) {
						lstWithNoChild.Add (comp);
						totalcost += comp.UnitPrice_Val;
					} else {
						lstWithNoChild.Add (comp);
						totalcost += comp.getPrixScenario(load3d_object.scenObjet);
					}
					if (load3d_object.cloneActif) {
						if (load3d_object.scenClone == -1) {
							lstWithNoChildClone.Add (comp);
							totalcostClone += comp.UnitPrice_Val;
						} else {
							lstWithNoChildClone.Add (comp);
							totalcostClone += comp.getPrixScenario(load3d_object.scenClone);
						}
					}
                }
            }
			if (load3d_object.scenObjet == -1) {
				lstWithNoChild.Sort (SortByPrice);
			} else {
				lstWithNoChild.Sort (SortByPriceScenario);
			}
			if (load3d_object.cloneActif) {
				if (load3d_object.scenClone == -1) {
					lstWithNoChildClone.Sort (SortByPrice);
				} else {
					lstWithNoChildClone.Sort (SortByPriceScenarioClone);
				}
			}

            // Raz des couleur a l'origine
            afficheStat.ColorAllChilds();

            foreach (pp_produit_composant comp in lstWithNoChild)
            {
                //costcount += comp.UnitPrice_Val;

				if (costcount < (totalcost * 0.8f)) {
					
                    afficheStat.ColorAllChilds("#_" + comp.Description, Color.red,load3d_object.objectcenter);
				} else if (costcount < (totalcost * 0.95f)) {
                    afficheStat.ColorAllChilds("#_" + comp.Description, new Color (1, 1, 0),load3d_object.objectcenter);
				} else {
                    afficheStat.ColorAllChilds("#_" + comp.Description, Color.green,load3d_object.objectcenter);
				}

                // Modif par CL 

				//on regarde si le composant est le modele sinon on met la valeur du scenario correspondant
				if (load3d_object.scenObjet == -1) {
					costcount += comp.UnitPrice_Val;
				} else {
					Debug.Log ("passe par else");
					costcount += comp.getPrixScenario (load3d_object.scenObjet);


				}

               
            }
			//on fait de meme pour le clone si present
			if (load3d_object.cloneActif) {
				costcount = 0.0f;
				foreach (pp_produit_composant comp in lstWithNoChildClone) {
					//costcount += comp.UnitPrice_Val;

					if (costcount < (totalcostClone * 0.8f)) {

                        afficheStat.ColorAllChilds("cloneObjet_#_" + comp.Description, Color.red, load3d_object.cloneObjet);
					} else if (costcount < (totalcostClone * 0.95f)) {
                        afficheStat.ColorAllChilds("cloneObjet_#_" + comp.Description, new Color (1, 1, 0), load3d_object.cloneObjet);
					} else {
                        afficheStat.ColorAllChilds("cloneObjet_#_" + comp.Description, Color.green, load3d_object.cloneObjet);
					}

					// Modif par CL 
					if (load3d_object.scenClone == -1) {
						costcount += comp.UnitPrice_Val;
					} else {
						costcount += comp.getPrixScenario (load3d_object.scenClone);

					}


				}
			}

        }
	}

}
