﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class destroyIdeeUI : MonoBehaviour {

    public pp_produit_note note = null;
	public string getIdee;
	public string descr;


	public void OnClick(){
		if (!GameObject.Find ("Pop-up")) {

			GameObject nouv = Instantiate (Resources.Load ("PopupDecisionUI")) as GameObject;
			nouv.transform.parent=GameObject.Find("Canvas").transform;
			nouv.transform.localPosition=Vector3.zero;
			nouv.name = "Pop-up";
			nouv.transform.GetChild(0).GetChild (1).GetChild(0).GetComponent<Text> ().text = getIdee;
			nouv.transform.GetChild(0).GetChild (0).GetChild(0).GetComponent<Text> ().text = descr;
            nouv.transform.GetChild(0).GetChild (2).GetComponent<DeleteOkUI> ().note = note;
		}
	}
}
