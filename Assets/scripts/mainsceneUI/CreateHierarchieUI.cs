using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CreateHierarchieUI : MonoBehaviour {

	public static float maxdep;
	public static float maxdep2;
	public GameObject toggleA;
	static public int posDeb;
	GameObject fullscreen;
	GameObject ajoutIdee;
	public GameObject Process;
	public GameObject Scrool1;
	public GameObject Scrool2;
	public GameObject Scrool3;
	public GameObject Image;
	public GameObject piece;
	// Use this for initialization
	
	private Transform attachparent = null;
    private pp_produit_vars _prod = null;

    private scrollIdee ScroolIdee1;
    private scrollIdee ScroolIdee2;
    private scrollIdee ScroolIdee3;
	
	void Awake()
	{
		toggleA = GameObject.Find ("ToggleAll");
		toggleA.SetActive (false);
		Process.SetActive (false);
		attachparent = transform;

        ScroolIdee1 = Scrool1.GetComponent<scrollIdee>();
        ScroolIdee2 = Scrool2.GetComponent<scrollIdee>();
        ScroolIdee3 = Scrool3.GetComponent<scrollIdee>();
	}


	void Start ()
	{
		fullscreen = GameObject.Find ("fullscreen");
		myinterface.my3dcamera = fullscreen.FindInChildren("3dcamera");
		posDeb = 0;
		//		chang = true;
		//		objetActuel="RPC.2028.20";

        _prod = new pp_produit_vars();
	}

	
	// Update is called once per frame
	void Update ()
	{
        if (GameObject.Find("myfullscreen") && _prod.DoRefresh_MainScene() )
        {

            if (_prod.Composant != null) // Ancien setObjetActuel
            {
                string nom = "#_" + _prod.Composant.Description;

                // Si pas au moins un fils visible on reactive tout l'ensemble
                if( !_prod.Composant.Visible )
                    _prod.Composant.Visible = true;
                

                GameObject newFocusObject = myinterface.my3dcamera.gameObject.transform.parent.gameObject.FindInChildren (nom);

                if (newFocusObject != myinterface.focusobject)
                {
                    GameObject.Find("scrollZoom").transform.GetComponent<scroolCameraZoom>().zoomOk = false;

                    GameObject.Find("infoWIdee").transform.localPosition = Vector3.zero;
                    GameObject.Find("infoW").transform.localPosition = Vector3.zero;



                    myinterface.focusobject = newFocusObject;
                    myinterface.lastfocussed = null;

                    load3d_object.HideAllOthers(_prod.Composant);

                    ScroolIdee1.init(0, 0);
                    ScroolIdee2.init(0, 0);
                    ScroolIdee3.init(0, 0);
                }     
            }
                
            CacherElement ce = toggleA.GetComponent<CacherElement>();
            if (ce != null)
                ce.init(_prod.Composant);


            // Active / Maj du bon onglet
            editPriceUI ep = GameObject.Find("Top Price").transform.GetComponent<editPriceUI>();
            if (ep != null)
                ep.init(_prod.Composant);

            foreach (Transform child in attachparent)
            {
                Destroy(child.gameObject);
            }
	
            foreach (Transform child in GameObject.Find("infoWIdee").transform)
            {
                if (child.name != "Fond")
                {
                    Destroy(child.gameObject);
                }
            }
               
            ScroolIdee1.init(0);
            ScroolIdee2.init(0);
            ScroolIdee3.init(0);

            //Ecran Piece
            if (_prod.MainScene_NumOnglet == 0)
            {
                if (_prod.Composant != null)
                {
                    Process.SetActive(true);
                    toggleA.SetActive(true);
                    piece.SetActive(true);
                    //ajoutIdee.SetActive(true);

                    int i = 0;
                    char letter = 'A';
                    int nombreActuel = 1;
                    if (_prod.Composant.childs.Count > 0)
                    {
                        toggleA.SetActive(true);

                        foreach (pp_produit_composant fils in _prod.Composant.childs)
                        {
                            if (fils.childs.Count > 0)
                            {

                                GameObject nouvBouton = Instantiate(Resources.Load("buttonHierarchieUIGroupe 1")) as GameObject;
                                nouvBouton.transform.SetParent(attachparent); //transform; 
                                nouvBouton.transform.localScale = Vector3.one;

                                nouvBouton.transform.GetChild(0).GetComponent<Text>().text = fils.Description;
                                nouvBouton.transform.GetChild(2).GetChild(0).GetComponent<Text>().text = letter.ToString();
                                letter = (char)(((int)letter) + 1);
                                /*if(blue){
							nouvBouton.transform.GetComponent<Image> ().color=Color.cyan;
							
						}
						else{
							nouvBouton.transform.GetComponent<Image> ().color=new Color(0,0.75f,0.75f);
							
						}*/
                                //blue=!blue;
                                nouvBouton.transform.GetChild(1).GetComponent<Text>().text = fils.UnitPrice + " €";
 
                                Vector3 pos = new Vector3();
                                pos.x = 0;
                                pos.z = 0;
                                pos.y = 492 - 85 - 77f * (i);
                                nouvBouton.transform.localPosition = pos;
						
                                nouvBouton.transform.name = "boutonhierarchie" + i.ToString();


                                ToggleHierarchie th = nouvBouton.GetComponentInChildren<ToggleHierarchie>();
                                if (th != null)
                                    th.init(fils);
						
                                i++;

                            }
                            else
                            {
                                GameObject nouvBouton = Instantiate(Resources.Load("buttonHierarchieUI")) as GameObject;
                                nouvBouton.transform.SetParent(attachparent); //transform; 
                                nouvBouton.transform.localScale = Vector3.one;
                                nouvBouton.transform.GetChild(0).GetComponent<Text>().text = fils.Description;
                                nouvBouton.transform.GetChild(2).GetChild(0).GetComponent<Text>().text = nombreActuel.ToString() + "-";
                                nombreActuel++;
                                /*if(blue){
							nouvBouton.transform.GetComponent<Image> ().color=Color.white;
							
						}
						else{
							nouvBouton.transform.GetComponent<Image> ().color=new Color(0.75f,0.75f,0.75f);
							
						}
						blue=!blue;
						
					}*/
                                nouvBouton.transform.GetChild(1).GetComponent<Text>().text = fils.UnitPrice + " €";
                                Vector3 pos = new Vector3();
                                pos.x = 0;
                                pos.z = 0;
                                pos.y = 492 - 85 - 77f * (i);
                                nouvBouton.transform.localPosition = pos;
						
                                nouvBouton.transform.name = "boutonhierarchie" + i.ToString();

                                ToggleHierarchie th = nouvBouton.GetComponentInChildren<ToggleHierarchie>();
                                if (th != null)
                                    th.init(fils);
						
                                i++;

                            }
					
					
                        }
                        piece.transform.GetChild(1).GetComponent<Text>().text = _prod.Composant.UnitPriceChilds + " €";

                        ScroolIdee1.init((i - 6) * 77f);

                
                        /*GameObject boutonIdee = Instantiate (Resources.Load ("buttonIdeeUI")) as GameObject;
				boutonIdee.transform.parent = attachparent; //transform; 
				boutonIdee.transform.localScale=Vector3.one;
				Vector3 pos1 = new Vector3 ();
				pos1.x = 0;
				pos1.z = 0;
				pos1.y = 137 - 39.2f * (i-posDeb);
				boutonIdee.transform.localPosition = pos1;
				boutonIdee.name="bouton idee";
				i++;*/

                        //ajout process sous les composants 0/127/0
                        int k = 0;
                        foreach (pp_produit_proc proc in _prod.Composant.Process)
                        {
                            GameObject idee = Instantiate(Resources.Load("BoutonProcessUI")) as GameObject;
                            idee.transform.SetParent(GameObject.Find("infoWIdee").transform); //transform; 

                            ProcessUI pui = idee.GetComponentInChildren<ProcessUI>();
                            if (pui != null)
                                pui.Init(proc);

                            idee.transform.localScale = Vector3.one;
                            Vector3 pos3 = new Vector3();
                            pos3.x = 0;
                            pos3.z = 0;
					
                            pos3.y = -127 - 74f * (k);
                            idee.transform.localPosition = pos3;
                            idee.name = "process " + k.ToString();
                            idee.transform.GetChild(0).GetComponent<Text>().text = proc.ProcessID;

                            idee.transform.GetChild(1).GetComponent<Text>().text = proc.Short_Description;

                            idee.transform.GetChild(3).GetComponent<Text>().text = proc.Total_Cost + " €";
    
                            k++;
                        }
                        Process.transform.GetChild(1).GetComponent<Text>().text = _prod.Composant.Process_Cost + " €";

                        ScroolIdee2.init((k - 6) * 74f);

                        /*GameObject boutonAjouter = Instantiate (Resources.Load ("AjoutIdee")) as GameObject;
				boutonAjouter.transform.parent = attachparent; //transform; 
				Vector3 pos2 = new Vector3 ();
				pos2.x = 0;
				pos2.z = 0;
				pos2.y = 137 - 39.2f * (i-posDeb);
				boutonAjouter.transform.localPosition = pos2;
				boutonAjouter.name="bouton ajouter";*/



                    }
                    else
                    {
                        Process.SetActive(false);
                        toggleA.SetActive(false);
                        piece.SetActive(false);

                        Debug.Log(_prod.Composant.Description);
                        bool pair = true;
                        float ajout = 0;

                        string[] param_titre = _prod.Composant.EncodeTitre();
                        string[] param_data = _prod.Composant.EncodeData();

                        while( i < param_titre.Length )
                        {
                            GameObject nouvBouton = Instantiate(Resources.Load("TextInfoElement")) as GameObject;
                            nouvBouton.transform.SetParent(attachparent); //transform; 
                            nouvBouton.transform.localScale = Vector3.one;
                            int res = 0;
                            if (param_data[i].Length > 19)
                            {
                                int tmp = param_data[i].Length;
                                res = Mathf.FloorToInt(tmp / 19);
                                Vector2 stock = nouvBouton.GetComponent<RectTransform>().sizeDelta;
                                stock.y = 77 + 50 * res;
                                nouvBouton.GetComponent<RectTransform>().sizeDelta = stock;
                                stock = nouvBouton.transform.GetChild(1).GetComponent<RectTransform>().sizeDelta;
                                stock.y = 77 + 50 * res;
                                nouvBouton.transform.GetChild(1).GetComponent<RectTransform>().sizeDelta = stock;
                                stock = nouvBouton.transform.GetChild(0).GetComponent<RectTransform>().localPosition;
                                stock.y = (res * 50) / 2;
                                nouvBouton.transform.GetChild(0).GetComponent<RectTransform>().localPosition = stock;
                                nouvBouton.transform.GetChild(0).GetComponent<Text>().text = param_titre[i] + ":";
                                nouvBouton.transform.GetChild(0).GetComponent<Text>().color = Color.white;
                                nouvBouton.transform.GetChild(1).GetComponent<Text>().text = param_data[i];
                                nouvBouton.transform.GetChild(1).GetComponent<Text>().color = Color.white;

                                /*if(blue){
							nouvBouton.transform.GetComponent<Image> ().color=Color.cyan;
							
						}
						else{
							nouvBouton.transform.GetComponent<Image> ().color=new Color(0,0.75f,0.75f);
							
						}*/
                                //blue=!blue;

                                Vector3 pos = new Vector3();
                                pos.x = 0;
                                pos.z = 0;
                                pos.y = 492 - 77f * (i) - ajout - ((res)) * (50 / 2);
                                nouvBouton.transform.localPosition = pos;
                                ajout += (res) * 50;

                            }
                            else
                            {
                                nouvBouton.transform.GetChild(0).GetComponent<Text>().text = param_titre[i] + ":";
                                nouvBouton.transform.GetChild(0).GetComponent<Text>().color = Color.white;
                                nouvBouton.transform.GetChild(1).GetComponent<Text>().text = param_data[i];
                                nouvBouton.transform.GetChild(1).GetComponent<Text>().color = Color.white;
				
                                /*if(blue){
							nouvBouton.transform.GetComponent<Image> ().color=Color.cyan;
							
						}
						else{
							nouvBouton.transform.GetComponent<Image> ().color=new Color(0,0.75f,0.75f);
							
						}*/
                                //blue=!blue;
				
                                Vector3 pos = new Vector3();
                                pos.x = 0;
                                pos.z = 0;
                                pos.y = 492 - 77f * (i) - ajout;
                                nouvBouton.transform.localPosition = pos;
                            }
                            if (pair)
                            {
                                nouvBouton.GetComponent<Image>().color = Color.black;
                                pair = false;
                            }
                            else
                            {
                                nouvBouton.GetComponent<Image>().color = Color.grey;
                                pair = true;
                            }

                            nouvBouton.transform.name = "buttoninfo" + i.ToString();

                            i++;
                        }
   
                        ScroolIdee3.init((i - 15) * 77f + ajout);
			
                    }
                }
            }



	

			//Ecran Idée
            else if (_prod.MainScene_NumOnglet == 1)
            {
                if (_prod.Composant != null)
                {
                    /*GameObject ideeDemo = Instantiate (Resources.Load ("IdeeDemo")) as GameObject;
                    ideeDemo.transform.parent = GameObject.Find("infoW").transform; //transform; 
                    ideeDemo.transform.localScale=Vector3.one;

                    ideeDemo.transform.localPosition =Vector3.zero;
                    */

                    Process.SetActive(false);
                    toggleA.SetActive(false);
                    piece.SetActive(false);
                    int i = 0;
                    //foreach (pp_produit_note note in _prod.Composant.Notes)
                    foreach (pp_produit_note note in _prod.Composant.NotesWithChildsNotes)
                    {
                        if (note.StatutIdee == "Identifie")
                        {
                            if (note.PourcentageGain_Val >= 0)
                            {
                                GameObject idee = Instantiate(Resources.Load("IdeeIdentifieVert")) as GameObject;
                                idee.transform.SetParent(GameObject.Find("infoW").transform); //transform; 
                                idee.transform.localScale = Vector3.one;
                                Vector3 pos3 = new Vector3();
                                pos3.x = 0;
                                pos3.z = 0;
                                pos3.y = 445 - 135f * i;
                                idee.transform.localPosition = pos3;
							
                                idee.GetComponent<IdeeExistanteUI>().init(note);
                            }
                            else
                            {
                                GameObject idee = Instantiate(Resources.Load("IdeeIdentifieRouge")) as GameObject;
                                idee.transform.SetParent(GameObject.Find("infoW").transform); //transform; 
                                idee.transform.localScale = Vector3.one;
                                Vector3 pos3 = new Vector3();
                                pos3.x = 0;
                                pos3.z = 0;
                                pos3.y = 445 - 135f * i;
                                idee.transform.localPosition = pos3;
                                idee.GetComponent<IdeeExistanteUI>().init(note);
                            }
                        }
                        else if (note.StatutIdee == "Valide")
                        {
                            if (note.PourcentageGain_Val >= 0)
                            {
                                GameObject idee = Instantiate(Resources.Load("IdeeValideeVert")) as GameObject;
                                idee.transform.SetParent(GameObject.Find("infoW").transform); //transform; 
                                idee.transform.localScale = Vector3.one;
                                Vector3 pos3 = new Vector3();
                                pos3.x = 0;
                                pos3.z = 0;
                                pos3.y = 445 - 135f * i;
                                idee.transform.localPosition = pos3;
                                idee.GetComponent<IdeeExistanteUI>().init(note);
                            }
                            else
                            {
                                GameObject idee = Instantiate(Resources.Load("IdeeValideeRouge")) as GameObject;
                                idee.transform.SetParent(GameObject.Find("infoW").transform);//transform; 
                                idee.transform.localScale = Vector3.one;
                                Vector3 pos3 = new Vector3();
                                pos3.x = 0;
                                pos3.z = 0;
                                pos3.y = 445 - 135f * i;
                                idee.transform.localPosition = pos3;
                                idee.GetComponent<IdeeExistanteUI>().init(note);
                            }

                        }
                        else if (note.StatutIdee == "Implemte")
                        {

                            if (note.PourcentageGain_Val >= 0)
                            {
                                GameObject idee = Instantiate(Resources.Load("IdeeImplementerVert")) as GameObject;
                                idee.transform.SetParent(GameObject.Find("infoW").transform);//transform; 
                                idee.transform.localScale = Vector3.one;
                                Vector3 pos3 = new Vector3();
                                pos3.x = 0;
                                pos3.z = 0;
                                pos3.y = 445 - 135f * i;
                                idee.transform.localPosition = pos3;
                                idee.GetComponent<IdeeExistanteUI>().init(note);
                            }
                            else
                            {
                                GameObject idee = Instantiate(Resources.Load("IdeeImplementerRouge")) as GameObject;
                                idee.transform.SetParent(GameObject.Find("infoW").transform); //transform; 
                                idee.transform.localScale = Vector3.one;
                                Vector3 pos3 = new Vector3();
                                pos3.x = 0;
                                pos3.z = 0;
                                pos3.y = 445 - 135f * i;
                                idee.transform.localPosition = pos3;
                                idee.GetComponent<IdeeExistanteUI>().init(note);
                            }
					
                        }
                        else
                        {
					
                            if (note.PourcentageGain_Val >= 0)
                            {
                                GameObject idee = Instantiate(Resources.Load("IdeeAbandonneeVert")) as GameObject;
                                idee.transform.SetParent(GameObject.Find("infoW").transform); //transform; 
                                idee.transform.localScale = Vector3.one;
                                Vector3 pos3 = new Vector3();
                                pos3.x = 0;
                                pos3.z = 0;
                                pos3.y = 445 - 135f * i;
                                idee.transform.localPosition = pos3;
                                idee.GetComponent<IdeeExistanteUI>().init(note);
                            }
                            else
                            {
                                GameObject idee = Instantiate(Resources.Load("IdeeAbandonneeRouge")) as GameObject;
                                idee.transform.SetParent(GameObject.Find("infoW").transform); //transform; 
                                idee.transform.localScale = Vector3.one;
                                Vector3 pos3 = new Vector3();
                                pos3.x = 0;
                                pos3.z = 0;
                                pos3.y = 445 - 135f * i;
                                idee.transform.localPosition = pos3;
                                idee.GetComponent<IdeeExistanteUI>().init(note);
                            }
                        }
                        i++;
                    }
                    ScroolIdee3.init((i - 8) * 135f);
                }
            }
			
				/*treenode actuel = csv.getTree (objetActuel, csv.roothierarchie);
				//				csv = (loadcsv)gameObject.GetComponent<loadcsv> ();
				toggleA.SetActive(false);
				foreach (Transform child in attachparent) {
					Destroy (child.gameObject);
				}
				bool coul=true;
				int ind = loadcsv.getIndName (actuel.name);
				int j=0;
				foreach(string colonne in loadcsv.composants[ind]){
					GameObject nouvBouton = Instantiate (Resources.Load ("boutonFonctionUI")) as GameObject;
					nouvBouton.transform.parent = attachparent; //transform; 
					if (coul) {
						
						nouvBouton.transform.GetComponent<Image> ().color=new Color(0.75f,0.75f,0.75f);
						coul=false;
					} 
					else {
						
						nouvBouton.transform.GetComponent<Image> ().color=Color.gray;
						coul=true;
						
					}
					nouvBouton.transform.GetChild (0).GetComponent<Text> ().text = loadcsv.composants [0][j];
					nouvBouton.transform.GetChild (1).GetComponent<Text> ().text = colonne;

					Vector3 pos = new Vector3 ();
					pos.x = 0;
					pos.z = 0;
					pos.y = 137 - 39.2f * (j-posDeb);
					nouvBouton.transform.localPosition = pos;
					nouvBouton.transform.localScale=transform.parent.localScale;
					nouvBouton.transform.name = "boutonhierarchie" + j.ToString ();
					j++;
				}
				GameObject boutonIdee = Instantiate (Resources.Load ("buttonIdeeUI")) as GameObject;
				boutonIdee.transform.parent = attachparent; //transform; 
				Vector3 pos1 = new Vector3 ();
				pos1.x = 0;
				pos1.z = 0;
				pos1.y = 137 - 39.2f * (j-posDeb);
				boutonIdee.transform.localPosition = pos1;
				boutonIdee.name="bouton idee";
				boutonIdee.transform.localScale=boutonIdee.transform.parent.localScale;
				int k=0;
				int z=0;
				
				foreach(string[] ligne in loadcsv.notes){

					if(ligne[3]==objetActuel){
						GameObject idee = Instantiate (Resources.Load ("AffichageIdeeUI")) as GameObject;
						idee.transform.parent = GameObject.Find("infoWIdee").transform; //transform; 
						idee.transform.localScale=Vector3.one;
						Vector3 pos3 = new Vector3 ();
						pos3.x = 0;
						pos3.z = 0;
						
						pos3.y = -78- 39.2f *2* (k);
						idee.transform.localPosition = pos3;
						idee.name="idee "+k.ToString();
						idee.GetComponent<ideeExistante>().init(z);
						if(ligne[7]=="F"){
							idee.transform.GetComponent<Image> ().color=new Color(0.75f,0.75f,0.0f);
							
						}
						else if(ligne[7]=="D"){
							idee.transform.GetComponent<Image> ().color=new Color(0,0.5f,0.0f);
						}
						else if(ligne[7]=="V"){
							idee.transform.GetComponent<Image> ().color=new Color(0,0f,0.5f);
						}
						if(k%2==1){
							if(ligne[7]=="I"){
								idee.transform.GetComponent<Image> ().color=new Color(1,0.75f,0.5f);
							}
							else if(ligne[7]=="F"){
								idee.transform.GetComponent<Image> ().color=new Color(1,1,0.0f);
								
							}
							else if(ligne[7]=="D"){
								idee.transform.GetComponent<Image> ().color=new Color(0.5f,1,0.0f);
							}
							else if(ligne[7]=="V"){
								idee.transform.GetComponent<Image> ().color=new Color(0,0.5f,1f);
							}
						}
						k++;
					}
					z++;                       
				}
				/*GameObject boutonAjouter = Instantiate (Resources.Load ("AjoutIdee")) as GameObject;
			boutonAjouter.transform.parent = attachparent; //transform; 
			Vector3 pos2 = new Vector3 ();
			pos2.x = 0;
			pos2.z = 0;
			pos2.y = boutonAjouter.transform.localPosition.y - 0.11f * (j-posDeb) -(k*0.23f)- 0.28f;
			boutonAjouter.transform.localPosition = pos2;
			boutonAjouter.name="bouton ajouter";
				
				 maxdep2 = 78 * k;
				maxdep= 39.2f * (i-posDeb);
			}


		}*/
		// cas fonctionnalité
		else
            {
                Process.SetActive(false);
                toggleA.SetActive(false);
                piece.SetActive(false);
                //ajoutIdee.SetActive(false);

                Debug.Log("fonction");
                if (_prod.Composant != null)
                {
                    bool blue = true;
                    int i = 0;
                    foreach (pp_produit_fonctionnalite fonc in _prod.Composant.PourcentageFonctionalites)
                    {
                        if (fonc.Pourcent_Val > 0)
                        { 

                            GameObject nouvBouton = Instantiate(Resources.Load("BoutonFonctionUI 1")) as GameObject;
                            nouvBouton.transform.SetParent(attachparent); //transform; 
						
                            nouvBouton.transform.GetChild(0).GetComponent<Text>().text = (i+1).ToString();
                            nouvBouton.transform.GetChild(1).GetComponent<Text>().text = fonc.Titre;
                            nouvBouton.transform.GetChild(3).GetComponent<Text>().text = fonc.Pourcent + " %";
								
								
                            if (blue)
                            {
                                nouvBouton.transform.GetComponent<Image>().color = Color.cyan;
                            }
                            else
                            {
                                nouvBouton.transform.GetComponent<Image>().color = new Color(0, 0.75f, 0.75f);
                            }
                            blue = !blue;
								
								

                            Vector3 pos = new Vector3();
                            pos.x = 0;
                            pos.z = 0;
                            pos.y = 492 - 76f * i;
                            //j++;
                            nouvBouton.transform.localPosition = pos;
                            nouvBouton.transform.localScale = nouvBouton.transform.parent.localScale;
                            nouvBouton.transform.name = "boutonFonction" + (i+1).ToString();

                            i++;
								
                        }
 
                        ScroolIdee3.init((i - 14) * 76f);
                    }
                }
            }
        }
		
	}
        
}
