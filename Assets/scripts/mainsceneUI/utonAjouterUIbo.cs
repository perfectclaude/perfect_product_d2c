using UnityEngine;
using System.Collections;

public class utonAjouterUIbo : MonoBehaviour {

	public void OnClick(){
		if (!GameObject.Find ("Pop-up")) {

            pp_produit_composant cur_comp = pp_manager.GetCurrentComposant();

            if( cur_comp != null )
            {
                pp_produit_note note = new pp_produit_note(cur_comp);
                if (note != null)
                {
                    cur_comp.Notes.Add(note);

                    GameObject nouv = Instantiate(Resources.Load("FicheIdeeAntoine")) as GameObject;

                    nouv.name = "Pop-up";
                    nouv.transform.SetParent(GameObject.Find("CanvasFiche").transform);
                    nouv.transform.localPosition = Vector3.zero;
                    nouv.transform.localScale = new Vector3(1f, 1f, 1f);
                    nouv.transform.GetComponent<ficheIdeeInit>().init(note, true);
                }
            }
		}
	}
}
