﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class pp_curve : pp_serie 
{
    private Texture _lineTexture = null;

    private bool _refresh = false;

    private List<GameObject> _curve1 = null;
    private List<GameObject> _curve2 = null;
    private GameObject _missile = null;


    private int _missilePos = 0;
    private float _missilePosMemo = 0;
    private float _missileVitesse = 5; // 5s
        
    void Awake()
    {
        _lineTexture = Resources.Load("linetexture") as Texture;

        float[] data = new float[10];
        data[0] = 0;
        data[1] = 0;
        data[2] = 10;
        data[3] = 7;
        data[4] = 3;
        data[5] = 3;
        data[6] = -5;
        data[7] = 8;
        data[8] = 3;
        data[9] = 0;

        this.MaxX = 50;
        this.DataY = data;
        this.Resolution = 50;
        this.Filter = pp_serie.pp_serie_smooth_filter.SAVITZKY_GOLAY;
        this.FilterAgressiveMode = false;

        //this.Data[2] = new Vector3(this.Data[1].x, this.Data[1].y, this.Data[1].z);
    }

	// Use this for initialization
	void Start () 
    {
        _refresh = true;
	
	}
	

	// Update is called once per frame
	void Update () 
    {

        //if (_points != null)
        if( true )
        {

            if (_refresh)
            {
                _refresh = false;


                this.DestroyCurve(_curve1);
                this.DestroyCurve(_curve2);

                _curve1 = this.TraceRawCurve( 2.5f, _lineTexture, Color.red, new Vector3( 0, 0, 0 ) );
                _curve2 = this.TraceSmoothCurve( 2.5f, _lineTexture, Color.green, new Vector3( 0, 13, 10 ) );


            }
                
            _missilePosMemo += 1 / _missileVitesse * SmoothData.Count * Time.deltaTime;
            int newPos = (int)_missilePosMemo;

            if ((newPos < 1) || (newPos >= SmoothData.Count))
            {
                newPos = 1;
                _missilePosMemo = 1;
            }

            if (newPos != _missilePos)
            {
                _missilePos = newPos;

                DestroyLine(_missile);;

                _missile = TraceLine("missile", SmoothData,  _missilePos, 3f, _lineTexture, Color.magenta, gameObject, new Vector3(0, 13, 10), false);
            }

        }
                
	
	}



}
