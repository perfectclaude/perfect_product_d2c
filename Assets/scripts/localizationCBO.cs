﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class localizationCBO : MonoBehaviour {

    private Dropdown _cbo = null;
    private localization _localization = null;

	// Use this for initialization
	void Start () {
        _localization = localization.Instance;
        _cbo = gameObject.GetComponent<Dropdown>();

        if( (_localization != null) && (_cbo != null))
        {
            
            _cbo.options.Clear();
            foreach( localizationLang lang in _localization.ListLang )
            {
                _cbo.options.Add(new Dropdown.OptionData(lang.Libelle));
            }

            _cbo.value = _localization.CurrentLangId;;
        }
	}
	
	// Update is called once per frame
	void Update () {

        if( (_localization != null) && (_cbo != null))
        {
            _cbo.value = _localization.CurrentLangId;
        }
	}

    public void ChangeLanguage() 
    {
        if( (_localization != null) && (_cbo != null))
            _localization.ChangeLanguage(_cbo.value);
    }
}
