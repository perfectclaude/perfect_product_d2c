﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class pp_produit_composant
{
    private int _BOM = 0;
    private string _Description = "";
    private string _ERP = "";
    private string _ERP_Parent = "";
    private int _Transformation = 0;
    private int _Quantity = 0;
    private string _Unit = "";
    private string _Value = "";
    private string _Currency = "";
    private float _UnitPrice = 0f;
    private float _GrossWeight = 0f;
    private float _NetWeight = 0f;
    private string _SubpartType = "";
    private string _Material = "";
    private string _MaterialType = "";
    private string _Provider = "";
    private float _LastPrice = 0f;
    private float _StandardPrice = 0f;
    private string _BomID = "";
    private string _BomID_Parent = "";

    private bool _Visible = true;

    private List<pp_produit_fonctionnalite> _pourcentageFonctionalites = null;

    private pp_list<pp_produit_note> _notes = null;
    private bool _inSynchroNotes = false;

    private List<pp_produit_proc> _process = null;

    private List<pp_produit_obj> _objs = null;

    private List<pp_produit_composant> _childs = null;
    private pp_produit_composant _parent = null;


    // Variables d'animation
    private Color _Couleur =  Color.black;

    // Avant :
    // - 0 = Color.black
    // - 1 = Color.red
    // - 2 = Color.green

    private object _syncRoot = new System.Object();
    private pp_produit _produit = null;
    private ppconvert _convert = null;

	public float getPrixScenario(int numScen){
		float total = UnitPrice_Val;

		foreach (pp_produit_note note in NotesWithChildsNotes) {
			if (pp_manager.GetCurrentProduit ().Scenarios [numScen].Notes.Contains (note)) {
				
				total -= note.CoutScenarios [numScen];
			}
		}
		 
		return total;
	}

	public float getPrixsurPoidsScenario(int numScen){
		float total = UnitPrice_Val;
		float poid = _GrossWeight;
		foreach (pp_produit_note note in NotesWithChildsNotes) {
			if (pp_manager.GetCurrentProduit ().Scenarios [numScen].Notes.Contains (note)) {
				
				total -= note.CoutScenarios [numScen];
				poid -= note.GainDeMasse_Val;
			}
		}
		if (poid == 0) {
			poid = 1;
		}
		return total/poid;
	}

    public pp_produit_composant( string[] init_chps,  pp_produit prod )
    {
        _produit = prod;
        _convert = prod.Convert;

        DecodeData(init_chps);

        _Couleur =  Color.black;

        _pourcentageFonctionalites = new List<pp_produit_fonctionnalite>();

        _notes = new pp_list<pp_produit_note>(synchro_notes);
        _inSynchroNotes = false;

        _process = new List<pp_produit_proc>();

        _objs = new List<pp_produit_obj>();

        _childs = new List<pp_produit_composant>();
        _parent = null;
    }


    public pp_produit_composant(pp_produit prod)
        : this(null, prod)
    {
    } 

    public void DecodeData(string[] init_chps )
    {
        if (init_chps != null)
        {
            lock (_syncRoot)
            {

                if (init_chps.Length > 0)
                    _convert.TryParse(init_chps[0], out _BOM);

                if (init_chps.Length > 1)
                    _convert.TryParse(init_chps[1], out _Description);

                if (init_chps.Length > 2)
                   _convert.TryParse(init_chps[2], out _ERP);

                if (init_chps.Length > 3)
                    _convert.TryParse(init_chps[3], out _ERP_Parent);

                if (init_chps.Length > 4)
                    _convert.TryParse(init_chps[4], out _Transformation);

                if (init_chps.Length > 5)
                    _convert.TryParse(init_chps[5], out _Quantity);

                if (init_chps.Length > 6)
                    _convert.TryParse(init_chps[6], out _Unit);

                if (init_chps.Length > 7)
                    _convert.TryParse(init_chps[7], out _Value);

                if (init_chps.Length > 8)
                    _convert.TryParse(init_chps[8], out _Currency);

                if (init_chps.Length > 9)
                    _convert.TryParse(init_chps[9], out _UnitPrice, true);

                if (init_chps.Length > 10)
                    _convert.TryParse(init_chps[10], out _GrossWeight);

                if (init_chps.Length > 11)
                    _convert.TryParse(init_chps[11], out _NetWeight);

                if (init_chps.Length > 12)
                    _convert.TryParse(init_chps[12], out _SubpartType);

                if (init_chps.Length > 13)
                    _convert.TryParse(init_chps[13], out _Material);

                if (init_chps.Length > 14)
                    _convert.TryParse(init_chps[14], out _MaterialType);

                if (init_chps.Length > 15)
                    _convert.TryParse(init_chps[15], out _Provider);

                if (init_chps.Length > 16)
                    _convert.TryParse(init_chps[16], out _LastPrice);

                if (init_chps.Length > 17)
                    _convert.TryParse(init_chps[17], out _StandardPrice);

                if (init_chps.Length > 18)
                    _convert.TryParse(init_chps[18], out _BomID);

                if (init_chps.Length > 19)
                    _convert.TryParse(init_chps[19], out _BomID_Parent);
            }
        }
    }

    public string[] EncodeTitre()
    {
        lock (_syncRoot)
        {
            string[] chps = new string[20];

            chps[0] = "BOM";
            chps[1] = "Description";
            chps[2] = "ERP";
            chps[3] = "ERP_Parent";
            chps[4] = "Transformation";
            chps[5] = "Quantity";
            chps[6] = "Unit";
            chps[7] = "Value";
            chps[8] = "Currency";
            chps[9] = "UnitPrice";
            chps[10] = "GrossWeight";
            chps[11] = "NetWeight";
            chps[12] = "SubpartType";
            chps[13] = "Material";
            chps[14] = "MaterialType";
            chps[15] = "Provider";
            chps[16] = "LastPrice";
            chps[17] = "StandardPrice";
            chps[18] = "BomID";
            chps[19] = "BomID_Parent";

            return chps;
        }
    }

    public string[] EncodeData()
    {
        lock (_syncRoot)
        {
            string[] chps = new string[20];

            chps[0] = _convert.ToString(_BOM);
            chps[1] = _convert.ToString(_Description);
            chps[2] = _convert.ToString(_ERP);
            chps[3] = _convert.ToString(_ERP_Parent);
            chps[4] = _convert.ToString(_Transformation);
            chps[5] = _convert.ToString(_Quantity);
            chps[6] = _convert.ToString(_Unit);
            chps[7] = _convert.ToString(_Value);
            chps[8] = _convert.ToString(_Currency);
            chps[9] = _convert.ToString(_UnitPrice, true);
            chps[10] = _convert.ToString(_GrossWeight);
            chps[11] = _convert.ToString(_NetWeight);
            chps[12] = _convert.ToString(_SubpartType);
            chps[13] = _convert.ToString(_Material);
            chps[14] = _convert.ToString(_MaterialType);
            chps[15] = _convert.ToString(_Provider);
            chps[16] = _convert.ToString(_LastPrice);
            chps[17] = _convert.ToString(_StandardPrice);
            chps[18] = _convert.ToString(_BomID);
            chps[19] = _convert.ToString(_BomID_Parent);

            return chps;
        }
    }

    public void DecodeDataPourcentageFonctionalites( string[] chps_titre, string[] chps_pourcent )
    {
        lock (_syncRoot)
        {
            _pourcentageFonctionalites.Clear();
            if (chps_titre != null)
            {
                for (int i = 1; i < chps_titre.Length; i++)
                {
                    float pourcent = 0;
                    string titre = "";

                    _convert.TryParse(chps_titre[i], out titre );

                    if( (chps_pourcent != null) && (i < chps_pourcent.Length) )
                    {
                        _convert.TryParse(chps_pourcent[i], out pourcent);
                    }

                    _pourcentageFonctionalites.Add(new pp_produit_fonctionnalite( titre, pourcent, this ));
                }
            }
        }
    }

    public void DecodeDataPourcentageFonctionalites( string[] chps_titre )
    {
        DecodeDataPourcentageFonctionalites(chps_titre, null);
    }

    public string[] EncodeTitrePourcentageFonctionalites()
    {
        lock (_syncRoot)
        {
            string[] chps = new string[ _pourcentageFonctionalites.Count+1];

            chps[0] = "ERP";
            for (int i = 0; i < _pourcentageFonctionalites.Count; i++)
            {
                chps[i+1] = _convert.ToString(_pourcentageFonctionalites[i].Titre);
            }
        
            return chps;
        }
    }

    public string[] EncodeDataPourcentageFonctionalites()
    {
        lock (_syncRoot)
        {
            string[] chps = new string[ _pourcentageFonctionalites.Count+1];

            chps[0] = _convert.ToString(_ERP);
            for (int i = 0; i < _pourcentageFonctionalites.Count; i++)
            {
                chps[i+1] = _convert.ToString(_pourcentageFonctionalites[i].Pourcent_Val);
            }

            return chps;
        }
    }





    // Getter / Setter data
    public string BOM
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(_BOM);
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _convert.TryParse(value, out _BOM);
            }
        }
    }

    public string Description
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(_Description);
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _convert.TryParse(value, out _Description);
            }
        }
    }

    public string ERP
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(_ERP);
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _convert.TryParse(value, out _ERP);
            }
        }
    }

    public string ERP_Parent
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(_ERP_Parent);
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _convert.TryParse(value, out _ERP_Parent);;
            }
        }
    }

    public string Transformation
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(_Transformation);
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _convert.TryParse(value, out _Transformation);
            }
        }
    }

    public string Quantity
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(_Quantity);
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _convert.TryParse(value, out _Quantity);
            }
        }
    }

    public string Unit
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(_Unit);
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _convert.TryParse(value, out _Unit);
            }
        }
    }

    public string Value
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(_Value);
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _convert.TryParse(value, out _Value);
            }
        }
    }

    public string Currency

    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(_Currency);
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _convert.TryParse(value, out _Currency);
            }
        }
    }

    public float UnitPrice_Val
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _UnitPrice;
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _UnitPrice = value;
            }
        }
    }

    public string UnitPrice
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(_UnitPrice, true);
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _convert.TryParse(value, out _UnitPrice, true);
            }
        }
    }

    public string GrossWeight

    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(_GrossWeight);
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _convert.TryParse(value, out _GrossWeight);
            }
        }
    }

    public float NetWeight_Val
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _NetWeight;
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _NetWeight = value;
            }
        }
    }

    public string NetWeight
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(_NetWeight);
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _convert.TryParse(value, out _NetWeight);
            }
        }
    }

    public string SubpartType
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(_SubpartType);
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _convert.TryParse(value, out _SubpartType);
            }
        }
    }

    public string Material
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(_Material);
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _convert.TryParse(value, out _Material);
            }
        }
    }

    public string MaterialType
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(_MaterialType);
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _convert.TryParse(value, out _MaterialType);
            }
        }
    }

    public string Provider
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(_Provider);
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _convert.TryParse(value, out _Provider);
            }
        }
    }

    public string LastPrice
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(_LastPrice);
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _convert.TryParse(value, out _LastPrice);
            }
        }
    }

    public string StandardPrice
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(_StandardPrice);
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _convert.TryParse(value, out _StandardPrice);
            }
        }
    }

    public string BomID
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(_BomID);
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _convert.TryParse(value, out _BomID);
            }
        }
    }

    public string BomID_Parent
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(_BomID_Parent);
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _convert.TryParse(value, out _BomID_Parent);
            }
        }
    }



    public List<pp_produit_fonctionnalite> PourcentageFonctionalites
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _pourcentageFonctionalites;
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _pourcentageFonctionalites = value;
            }
        }
    }

    public List<pp_produit_note> Notes
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _notes;
            }
        }
    }

    private void synchro_notes(int index, pp_produit_note oldObj, bool inserted, bool deleted)
    {
        lock (_syncRoot)
        {
            if (!_inSynchroNotes)
            {
                _inSynchroNotes = true;

                if (deleted)
                {
                    if (_produit != null)
                    {
                        // Suppression de l'eventuelle note des scenarios l'utilisant 
                        foreach (pp_produit_scenario scenar in _produit.Scenarios)
                        {
                            scenar.Notes.Remove(oldObj);
                        }
                    }

                }

                _inSynchroNotes = false;
            }
        }
    }

    public List<pp_produit_note> NotesWithChildsNotes
    {
        get 
        { 
            lock (_syncRoot)
            {
                List<pp_produit_note> lst = new List<pp_produit_note>();

                foreach (pp_produit_note note in _notes)
                    lst.Add(note);

                foreach (pp_produit_composant comp in _childs)
                {
                    foreach (pp_produit_note note in comp.NotesWithChildsNotes )
                        lst.Add(note);
                }

                return lst;
            }
        }
       
    }

    public List<pp_produit_proc> Process
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _process;
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _process = value;
            }
        }
    }



    private float calc_Process_Cost()
    {
        float fval = 0f;

        foreach (pp_produit_proc proc in _process)
        {
            fval += proc.Total_Cost_Val;
        }

        return fval;
    }

    public float Process_Cost_Val
    {
        get 
        { 
            lock (_syncRoot)
            {
                return calc_Process_Cost();
            }
        }

    }

    public string Process_Cost
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(calc_Process_Cost(), true);
            }
        }

    }


    public List<pp_produit_obj> Objs
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _objs;
            }
        }
    }
        

    // Getter / Setter d'animations
    public Color Couleur
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _Couleur;
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _Couleur = value;
            }
        }
    }

    public float UnitPriceByNetWeight_Val
    {
        get 
        { 
            lock (_syncRoot)
            {
                float fVal = 0f;

                if (_NetWeight > 0)
                    fVal = _UnitPrice / _NetWeight;
                    
                return fVal;
            }
        }
    }

    public string UnitPriceChilds
    {
        get 
        { 
            lock (_syncRoot)
            {
                float fVal = 0f;

                foreach (pp_produit_composant comp in _childs)
                {
                    fVal += comp.UnitPrice_Val;
                }

                return _convert.ToString(fVal, true);;
            }
        }
    }

    public float Gain_Val
    {
        get 
        { 
            lock (_syncRoot)
            {
                float val = 0;

                foreach (pp_produit_note note in _notes)
                {
                    val += note.GainPotentielUnitaire_Val;
                }

                return val;
            }
        }
    }


    public bool Visible_AllChild
    {
        get
        {
            lock (_syncRoot)
            {
                bool visible = _Visible;

                if (_childs.Count > 0)
                {
                    // Si au moins un fils non visible je suis non visible
                    visible = true;
                    foreach (pp_produit_composant comp in _childs)
                    {
                        if (!comp.Visible_AllChild)
                        {
                            visible = false;
                            break;
                        }
                    }
                }

                return visible;
            }
        }
    }
        
    public bool Visible
    {
        get 
        { 
            lock (_syncRoot)
            {
                bool visible = _Visible;

                if (_childs.Count > 0)
                {
                    // Si au moins un fils visible je suis visible
                    visible = false;
                    foreach (pp_produit_composant comp in _childs)
                    {
                        if (comp.Visible)
                        {
                            visible = true;
                            break;
                        }
                    }
                }

                return visible;
            }
        }
        set
        {
            lock (_syncRoot)
            {
                _Visible = value;
                foreach (pp_produit_composant comp in _childs)
                {
                    comp.Visible = _Visible;
                }
            }
        }
    }


    public List<pp_produit_composant> childs
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _childs;
            }
        }

    }

    public pp_produit_composant parent
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _parent;
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _parent = value;
            }
        }

    }


    private List<pp_produit_composant> SameLevel(pp_produit_composant root, int root_level, int level  )
    {
        List<pp_produit_composant> lstSameLevel = new List<pp_produit_composant>();

        if (root_level == level)
            lstSameLevel.Add(root);
        
        else if( root_level < level )
        {
            foreach( pp_produit_composant root_child in root.childs )
            {
                List<pp_produit_composant> lst = SameLevel(root_child, root_level + 1, level);

                if( lst != null )
                    foreach( pp_produit_composant comp in lst )
                        lstSameLevel.Add(comp);    
            }
        }

        return lstSameLevel; 
    }
        

    public int HierachieNumber
    {
        /* Les Hirachies Number sont classé de la maniere suivant
         * 
         * 
         *                   12
         *              8
         *                   13
         *          3
         *              9
         * 
         *      1 
         *          4
         *          5
         * 
         *  0
         * 
         *          6
         * 
         *      2
         *                   14
         *              10
         *                   15
         *          7
         * 
         *              11
         * 
         * 
         */

        get
        { 
            lock (_syncRoot )
            {
                int num = 0;
                int level = 0;

                pp_produit_composant parent = _parent;
                pp_produit_composant root = this;
                while( parent != null )
                {
                    root = parent;
                    parent = parent.parent;
                    level++;
                }
                    
                List<pp_produit_composant> lst = SameLevel(root, 0, level );
                int idx = lst.IndexOf(this);

                if (idx > 0)
                    num = lst[idx - 1].HierachieNumber + 1;
                else
                {
                    lst = SameLevel(root, 0, level -1 );
                    idx = lst.Count;

                    if (idx > 0)
                        num = lst[idx - 1].HierachieNumber + 1;
                }

                
                return num;
            }
        }
    }
        

    private bool AddTreeComp(pp_produit_composant comp, bool recursif)
    {
        bool bRet = false;

        lock (_syncRoot)
        {
            if ((comp.ERP_Parent == _ERP) || (comp.ERP_Parent == _Description))
            { 
                // C'est un child direct
                // Chainage des eventuelle child temporaire
                int i = 0;
                while (i < _childs.Count)
                {
                    if ((_childs[i].ERP_Parent == comp.ERP) || (_childs[i].ERP_Parent == comp.Description))
                    {
                        comp.childs.Add(_childs[i]);
                        _childs[i].parent = comp;

                        _childs.Remove(_childs[i]);
                    }
                    else
                        i++;
                }

                _childs.Add(comp);
                comp.parent = this;

                bRet = true;
            }
            else
            {
                foreach (pp_produit_composant comp_child in _childs)
                {
                    bRet = comp_child.AddTreeComp(comp, true);
                    if (bRet)
                        break;
                }

                if (!bRet && !recursif)
                {
                    // Ce n'est pas un child mais temporairement on le class en child direct (de niveau 0)
                    // Chainage des eventuelle child temporaire
                    int i = 0;
                    while (i < _childs.Count)
                    {
                        if ((_childs[i].ERP_Parent == comp.ERP) || (_childs[i].ERP_Parent == comp.Description))
                        {
                            comp.childs.Add(_childs[i]);
                            _childs[i].parent = comp;

                            _childs.Remove(_childs[i]);

                        }
                        else
                            i++;
                    }

                    _childs.Add(comp);
                    comp.parent = this;
                }
            }
        }

        return bRet;
    }

    public void AddTreeComp(pp_produit_composant comp)
    {
        AddTreeComp( comp, false);
    }
        
    public void VerifCoherenceData()
    {
        float unitPriceRecalc = 0f;
        pp_produit_proc procPetP = null;


        foreach (pp_produit_composant comp in _childs)
        {
            comp.VerifCoherenceData();

            unitPriceRecalc += comp.UnitPrice_Val;
        }

        foreach( pp_produit_proc proc in _process )
        {
            unitPriceRecalc += proc.Total_Cost_Val;
            if (proc.ProcessID_Val < 0)
                procPetP = proc;  
        }


        if( unitPriceRecalc != _UnitPrice)
        { // On ajout un process "Pertes ET Profits"

            if (procPetP != null)
            {
                unitPriceRecalc -= procPetP.Total_Cost_Val;
            }

            if (unitPriceRecalc == _UnitPrice) // On supprime tout simplement le procPetP
            {
                if (procPetP != null)
                    _process.Remove(procPetP);
            }
            else
            {
                if (procPetP == null)
                {
                    procPetP = new pp_produit_proc(this);
                    _process.Add(procPetP);
                }

                procPetP.ProcessID_Val = -1;
                procPetP.Description = "Pertes et profits";
                procPetP.Short_Description = "P&P";
                procPetP.Human_Cost_Val =  _UnitPrice - unitPriceRecalc;
                procPetP.Engine_Cost_Val = 0;
            }
        }

    }

    public ppconvert Convert
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert;
            }
        }
    }

    public pp_produit Produit
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _produit;
            }
        }
    }
}

