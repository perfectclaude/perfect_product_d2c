﻿using UnityEngine;
using System.Collections;

public class button_display1screen : buttonswap
{
	GameObject fullscreen;
	GameObject rightwindow;

	void Awake()
	{
		_Awake ();
		fullscreen = GameObject.Find ("fullscreen");
		rightwindow = GameObject.Find ("rightwindow");
	}

	void Start()
	{
		fullscreen.SetActive(true);
		rightwindow.SetActive(true);
		myinterface.my3dcamera = fullscreen.FindInChildren("3dcamera");
		myinterface.my3dcameraright = rightwindow.FindInChildren("3dcamera");
		/*
		fullscreen.SetActive(false);
		leftwindow.SetActive(false);
		rightwindow.SetActive(false);
		*/
	}

	public override void ButtonPressed ()
	{
		base.ButtonPressed();
		fullscreen.SetActive(true);
		rightwindow.SetActive(true);
	}
}