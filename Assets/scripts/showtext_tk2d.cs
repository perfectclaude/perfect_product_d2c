﻿using UnityEngine;
using System.Collections;

public class showtext_tk2d : MonoBehaviour {

    public string mykey;
    public bool forceUpperCase;
    tk2dTextMesh mytext;

	// Use this for initialization
	void Start () {
        mytext = gameObject.GetComponent<tk2dTextMesh>();
	}
	
	// Update is called once per frame
    void Update ()
    {
        string texttoshow = localization.GetTextWithKey(mykey);
        texttoshow = texttoshow.Replace("<date>",showdate.TodaysDate());
        texttoshow = texttoshow.Replace("<lasthour>","LASTHOUR");
        texttoshow = texttoshow.Replace("<good>","PIECESBONNES");
        texttoshow = texttoshow.Replace("<starthour>","HEUREDEMARRAGE");

        if (forceUpperCase)
            mytext.text = texttoshow.ToUpper();
        else
            mytext.text = texttoshow;
    }
}
