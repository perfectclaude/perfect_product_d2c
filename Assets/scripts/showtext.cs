﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class showtext : MonoBehaviour
{
	public string mykey;
    public bool forceUpperCase;
    Text mytext;
   

	void Awake ()
	{
		mytext = gameObject.GetComponent<Text>();
	}

	void Update ()
	{
		string texttoshow = localization.GetTextWithKey(mykey);
		texttoshow = texttoshow.Replace("<date>",showdate.TodaysDate());
		texttoshow = texttoshow.Replace("<lasthour>","LASTHOUR");
		texttoshow = texttoshow.Replace("<good>","PIECESBONNES");
		texttoshow = texttoshow.Replace("<starthour>","HEUREDEMARRAGE");

        if (forceUpperCase)
            mytext.text = texttoshow.ToUpper();
        else
		    mytext.text = texttoshow;
	}
}
