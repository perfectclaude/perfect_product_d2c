﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;


public class pp_produit_obj
{
    private object _syncRoot = new System.Object();

    private string _nom = "";
    private int _texture = 0;
    private pp_produit_composant _composant = null;

    public pp_produit_obj(string nom)
    {
        _nom = nom;
        _composant = null;
    }

    // Getter  / Setter
    public string Nom
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _nom;
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _nom = value;
            }
        }
    }

    public int Texture
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _texture;
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _texture = value;
            }
        }
    }

    public pp_produit_composant Composant
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _composant;
            }
        }
        set 
        { 
            lock (_syncRoot)
            {

                if (_composant != value)
                {
                    if (_composant != null)
                    {
                        _composant.Objs.Remove(this);
                    }

                    if (value != null)
                    {
                        value.Objs.Add(this);
                    }

                    _composant = value;
                }
            }
        }
    }
}
