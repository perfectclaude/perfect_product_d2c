﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class pp_produit_note
{
    //private int _Numero = 0;
    private DateTime _DateMAJ = DateTime.Now;
    //private string _ProduitName = "";
    private string _ComposantName = "";
    //private string _SousEnsemble = "";
    private string _LeverDeGain = "Specification";
    private string _StatutIdee = "Identifie";
    private string _Pilote = "";
    private string _TitreIdee = "";
    private string _SujetIdee = "";
    private string _DescriptionIdee = "";
    private bool _IdeeVisibleClient = false;
    private string _PerimetreImpacte = "";
    private float _CoutDeReference = 0f;
    private string _DeviseDeReference = "€";
    private string _HypotheseDeCalculDeGain = "";
    private float _CoutProjete = 0f;
    //private float _GainPotentielUnitaire =  0f;
    //private float _PourcentageGain = 0f;
    private int _MaturiteDuPotentiel = 0;
    private int _QuantiteAnnuelle = 0;
    //private float _GainAnnuel = 0f;
    private float _MasseDeReference = 0f;
    private float _MasseProjete = 0f;
    private float _CoutScenario1 = 0f; // Ne sert plus : utiliser CoutScenarios
    private float _CoutScenario2 = 0f; // Ne sert plus : utiliser CoutScenarios
    private float _CoutScenario3 = 0f; // Ne sert plus : utiliser CoutScenarios
    private bool _TroisF = false;
    private string _CommentaireImpact = "";
    private string _RisqueALever = "";
    private string _PlanAction = "";
    private DateTime _DatePrevisionnelleValidation = DateTime.Today;
    private DateTime _DatePrevisionnelleImplantation = DateTime.Today;
    private float _EffortValidationEtDeveloppelement = 0f;
    private float _CoutEffortValidation = 0f;
    private float _CommentaireValidationEtDeveloppement = 0f;
    //private float _InvestissementsEtOutillages = "";
    private string _CommentaireInvestissement = ""; //localization.GetTextWithKey( "Investissement par défaut" );
    //private float _TotalNRC = 0f;
    private int _NoteEnergie = 1;
    //private float _RentabiliteROI = 0f;
    private int _NoteRentabilite = 1;
    private int _NiveauDifficulte = 0;
    private int _NoteAdhesion = 1;
    private List<float> _CoutScenarios = null;

    private object _syncRoot = new System.Object();
    private pp_produit_composant _composant = null;
    private ppconvert _convert = null;


    public pp_produit_note( string[] init_chps, pp_produit_composant comp )
    {
        _composant = comp;

        _convert = _composant.Convert;
        _CoutScenarios = new List<float>();

        DecodeData(init_chps);
    }

    public pp_produit_note(pp_produit_composant comp)
        : this((string[])null, comp)
    {
        _CoutDeReference = _composant.UnitPrice_Val;
    }
        
    public void DecodeData(string[] init_chps )
    {
        if (init_chps != null)
        {
            lock (_syncRoot)
            {
                //if (init_chps.Length > 0)
                //    _convert.TryParse(init_chps[0], out _Numero);

                if (init_chps.Length > 1)
                    _convert.TryParse(init_chps[1], out _DateMAJ);

                //if (init_chps.Length > 2)
                //    _convert.TryParse(init_chps[2], out _ProduitName);

                if (init_chps.Length > 3)
                    _convert.TryParse(init_chps[3], out _ComposantName);

                //if (init_chps.Length > 4)
                //    _convert.TryParse(init_chps[4], out _SousEnsemble);

                if (init_chps.Length > 5)
                    _convert.TryParse(init_chps[5], out _LeverDeGain);

                if (init_chps.Length > 6)
                    _convert.TryParse(init_chps[6], out _StatutIdee);

                if (init_chps.Length > 7)
                    _convert.TryParse(init_chps[7], out _Pilote);

                if (init_chps.Length > 8)
                    _convert.TryParse(init_chps[8], out _TitreIdee);

                if (init_chps.Length > 9)
                    _convert.TryParse(init_chps[9], out _DescriptionIdee);

                if (init_chps.Length > 10)
                    _convert.TryParse(init_chps[10], out _IdeeVisibleClient);

                if (init_chps.Length > 11)
                    _convert.TryParse(init_chps[11], out _PerimetreImpacte);

                if (init_chps.Length > 12)
                    _convert.TryParse(init_chps[12], out _CoutDeReference);

                if (init_chps.Length > 13)
                    _convert.TryParse(init_chps[13], out _SujetIdee);
                    //_convert.TryParse(init_chps[13], out _DeviseDeReference);

                if (init_chps.Length > 14)
                    _convert.TryParse(init_chps[14], out _HypotheseDeCalculDeGain);

                if (init_chps.Length > 15)
                    _convert.TryParse(init_chps[15], out _CoutProjete);

                //if (init_chps.Length > 16)
                //    _convert.TryParse(init_chps[16], out _GainPotentielUnitaire);

                //if (init_chps.Length > 17)
                //    _convert.TryParse(init_chps[17], out _PourcentageGain);

                if (init_chps.Length > 18)
                    _convert.TryParse(init_chps[18], out _MaturiteDuPotentiel);

                if (init_chps.Length > 19)
                    _convert.TryParse(init_chps[19], out _QuantiteAnnuelle);

                //if (init_chps.Length > 20)
                //    _convert.TryParse(init_chps[20], out _GainAnnuel);

                if (init_chps.Length > 21)
                    _convert.TryParse(init_chps[21], out _MasseDeReference);

                if (init_chps.Length > 22)
                    _convert.TryParse(init_chps[22], out _MasseProjete);

                if (init_chps.Length > 23)
                    _convert.TryParse(init_chps[23], out _CoutScenario1);

                if (init_chps.Length > 24)
                    _convert.TryParse(init_chps[24], out _CoutScenario2);

                if (init_chps.Length > 25)
                    _convert.TryParse(init_chps[25], out _CoutScenario3);

                if (init_chps.Length > 26)
                    _convert.TryParse(init_chps[26], out _TroisF);

                if (init_chps.Length > 27)
                    _convert.TryParse(init_chps[27], out _CommentaireImpact);

                if (init_chps.Length > 28)
                    _convert.TryParse(init_chps[28], out _RisqueALever);

                if (init_chps.Length > 29)
                    _convert.TryParse(init_chps[29], out _PlanAction);

                if (init_chps.Length > 30)
                    _convert.TryParse(init_chps[30], out _DatePrevisionnelleValidation);

                if (init_chps.Length > 31)
                    _convert.TryParse(init_chps[31], out _DatePrevisionnelleImplantation);

                if (init_chps.Length > 32)
                    _convert.TryParse(init_chps[32], out _EffortValidationEtDeveloppelement);

                if (init_chps.Length > 33)
                    _convert.TryParse(init_chps[33], out _CoutEffortValidation);

                if (init_chps.Length > 34)
                    _convert.TryParse(init_chps[34], out _CommentaireValidationEtDeveloppement);

                //if (init_chps.Length > 35)
                //    _convert.TryParse(init_chps[35], out _InvestissementsEtOutillages);

                if (init_chps.Length > 36)
                    _convert.TryParse(init_chps[36], out _CommentaireInvestissement);

                //if (init_chps.Length > 37)
                //    _convert.TryParse(init_chps[37], out _TotalNRC);

                if (init_chps.Length > 38)
                    _convert.TryParse(init_chps[38], out _NoteEnergie);

                //if (init_chps.Length > 39)
                //    _convert.TryParse(init_chps[39], out _RentabiliteROI);

                if (init_chps.Length > 40)
                    _convert.TryParse(init_chps[40], out _NoteRentabilite);

                if (init_chps.Length > 41)
                    _convert.TryParse(init_chps[41], out _NiveauDifficulte);

                if (init_chps.Length > 42)
                    _convert.TryParse(init_chps[42], out _NoteAdhesion);

                _CoutScenarios.Clear();
                for (int i = 43; i < init_chps.Length; i++)
                {
                    float cout = 0f;

                    _convert.TryParse(init_chps[i], out cout);
                    _CoutScenarios.Add(cout);

                }
            }
        }
    }

    public string[] EncodeTitre()
    {
        lock (_syncRoot)
        {
            string[] chps = new string[43+_CoutScenarios.Count];

            chps[0] = "Numero";
            chps[1] = "DateMAJ";
            chps[2] = "Produit";
            chps[3] = "Composant";
            chps[4] = "SousEnsemble";
            chps[5] = "LeverDeGain";
            chps[6] = "StatutIdee";
            chps[7] = "Pilote";
            chps[8] = "TitreIdee";
            chps[9] = "DescriptionIdee";
            chps[10] = "IdeeVisibleClient";
            chps[11] = "PerimetreImpacte";
            chps[12] = "CoutDeReference";

            //chps[13] = "DeviseDeReference";
            chps[13] = "Sujet";

            chps[14] = "HypotheseDeCalculDeGain";
            chps[15] = "CoutProjete";
            chps[16] = "GainPotentielUnitaire";
            chps[17] = "PourcentageGain";
            chps[18] = "MaturiteDuPotentiel";
            chps[19] = "QuantiteAnnuelle";
            chps[20] = "GainAnnuel";
            chps[21] = "MasseDeReference";
            chps[22] = "MasseProjete";
            chps[23] = "CoutScenario1";
            chps[24] = "CoutScenario2";
            chps[25] = "CoutScenario3";
            chps[26] = "TroisF";
            chps[27] = "CommentaireImpact";
            chps[28] = "RisqueALever";
            chps[29] = "PlanAction";
            chps[30] = "DatePrevisionnelleValidation";
            chps[31] = "DatePrevisionnelleImplantation";
            chps[32] = "EffortValidationEtDeveloppelement";
            chps[33] = "CoutEffortValidation";
            chps[34] = "CommentaireValidationEtDeveloppement";
            chps[35] = "InvestissementsEtOutillages";
            chps[36] = "CommentaireInvestissement";
            chps[37] = "TotalNRC";
            chps[38] = "NoteEnergie";
            chps[39] = "RentabiliteROI";
            chps[40] = "NoteRentabilite";
            chps[41] = "NiveauDifficulte";
            chps[42] = "NoteAdhesion";

            for (int i = 0; i < _CoutScenarios.Count; i++)
            {
                chps[43 + i] = "CoutScenario" + (i + 1).ToString();

            }

            return chps;
        }
    }

    public string[] EncodeData()
    {
        lock (_syncRoot)
        {
            string[] chps = new string[43+_CoutScenarios.Count];

            //chps[0] = _convert.ToString(_Numero);
            chps[0] = _convert.ToString(calc_Numero());

            chps[1] = _convert.ToString(_DateMAJ);

            //chps[2] = _convert.ToString(_ProduitName);
            chps[2] = _composant.Produit.Nom;

            chps[3] = _convert.ToString(_ComposantName);

            //chps[4] = _convert.ToString(_SousEnsemble);
            chps[4] = _composant.ERP;

            chps[5] = _convert.ToString(_LeverDeGain);
            chps[6] = _convert.ToString(_StatutIdee);
            chps[7] = _convert.ToString(_Pilote);
            chps[8] = _convert.ToString(_TitreIdee);
            chps[9] = _convert.ToString(_DescriptionIdee);
            chps[10] = _convert.ToString(_IdeeVisibleClient);
            chps[11] = _convert.ToString(_PerimetreImpacte);
            chps[12] = _convert.ToString(_CoutDeReference);

            //chps[13] = _convert.ToString(_DeviseDeReference);
            chps[13] = _convert.ToString(_SujetIdee);

            chps[14] = _convert.ToString(_HypotheseDeCalculDeGain);
            chps[15] = _convert.ToString(_CoutProjete);

            //chps[16] = _convert.ToString(_GainPotentielUnitaire);
            chps[16] = _convert.ToString(calc_GainPotentielUnitaire());

            //chps[17] = _convert.ToString(_PourcentageGain);
            chps[17] = _convert.ToString(calc_PourcentageGain());

            chps[18] = _convert.ToString(_MaturiteDuPotentiel);
            chps[19] = _convert.ToString(_QuantiteAnnuelle);

            //chps[20] = _convert.ToString(_GainAnnuel);
            chps[20] = _convert.ToString(calc_GainAnnuel());

            chps[21] = _convert.ToString(_MasseDeReference);
            chps[22] = _convert.ToString(_MasseProjete);
            chps[23] = _convert.ToString(_CoutScenario1);
            chps[24] = _convert.ToString(_CoutScenario2);
            chps[25] = _convert.ToString(_CoutScenario3);
            chps[26] = _convert.ToString(_TroisF);
            chps[27] = _convert.ToString(_CommentaireImpact);
            chps[28] = _convert.ToString(_RisqueALever);
            chps[29] = _convert.ToString(_PlanAction);
            chps[30] = _convert.ToString(_DatePrevisionnelleValidation);
            chps[31] = _convert.ToString(_DatePrevisionnelleImplantation);
            chps[32] = _convert.ToString(_EffortValidationEtDeveloppelement);
            chps[33] = _convert.ToString(_CoutEffortValidation);
            chps[34] = _convert.ToString(_CommentaireValidationEtDeveloppement);

            //chps[35] = _convert.ToString(_InvestissementsEtOutillages);
            chps[35] = _convert.ToString(calc_InvestissementsEtOutillages());

            chps[36] = _convert.ToString(_CommentaireInvestissement);

            //chps[37] = _convert.ToString(_TotalNRC);
            chps[37] = _convert.ToString(calc_TotalNRC());

            chps[38] = _convert.ToString(_NoteEnergie);

            //chps[39] = _convert.ToString(_RentabiliteROI);
            chps[39] = _convert.ToString(calc_RentabiliteROI());

            chps[40] = _convert.ToString(_NoteRentabilite);
            chps[41] = _convert.ToString(_NiveauDifficulte);
            chps[42] = _convert.ToString(_NoteAdhesion);


            for (int i = 0; i < _CoutScenarios.Count; i++)
            {
                chps[43 + i] = _convert.ToString(_CoutScenarios[i]);

            }

            return chps;
        }
    }

    // Getter / Setter data
    private int calc_Numero()
    {
        List<pp_produit_note> allNotes = _composant.Produit.notes;
        int num = allNotes.IndexOf(this);

        if (num >= 0)
            num += 1;
        else
            num = 0;


        return num; 
    }

    public int Numero_Val
    {
        get 
        { 
            lock (_syncRoot)
            {
                return calc_Numero();
            }
        }
    }

    public string Numero
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(calc_Numero());
            }
        }
    }

    public DateTime DateMAJ_Val
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _DateMAJ;
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _DateMAJ = value;
            }
        }
    }

    public string DateMAJ
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(_DateMAJ);
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _convert.TryParse(value, out _DateMAJ);
            }
        }
    }

    public string ProduitName
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _composant.Produit.Nom;
            }
        }
    }

    public string ComposantName
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(_ComposantName);
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _convert.TryParse(value, out _ComposantName);
            }
        }
    }

    public string SousEnsemble
    {
        get 
        { 
            lock (_syncRoot)
            {
                //return _convert.ToString(_SousEnsemble);
                return _composant.ERP;
            }
        }
    }

    public string LeverDeGain
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(_LeverDeGain);
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _convert.TryParse(value, out _LeverDeGain);
            }
        }
    }

    public string StatutIdee
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(_StatutIdee);
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _convert.TryParse(value, out _StatutIdee);
                _DateMAJ = DateTime.Now;
            }
        }
    }

    public string Pilote
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(_Pilote);
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _convert.TryParse(value, out _Pilote);
            }
        }
    }

    public string TitreIdee
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(_TitreIdee);
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _convert.TryParse(value, out _TitreIdee);
            }
        }
    }

    public string SujetIdee
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(_SujetIdee);
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _convert.TryParse(value, out _SujetIdee);
            }
        }
    }

    public string DescriptionIdee
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(_DescriptionIdee);
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _convert.TryParse(value, out _DescriptionIdee);
            }
        }
    }

    public bool IdeeVisibleClient_Val
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _IdeeVisibleClient;
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _IdeeVisibleClient = value;
            }
        }
    }

    public string IdeeVisibleClient
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(_IdeeVisibleClient);
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _convert.TryParse(value, out _IdeeVisibleClient);
            }
        }
    }

    public string PerimetreImpacte
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(_PerimetreImpacte);
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _convert.TryParse(value, out _PerimetreImpacte);
            }
        }
    }

    public float CoutDeReference_Val
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _CoutDeReference;
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _CoutDeReference = value;
            }
        }
    }

    public string CoutDeReference
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(_CoutDeReference);
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _convert.TryParse(value, out _CoutDeReference);
            }
        }
    }

    public string DeviseDeReference
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(_DeviseDeReference);
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _convert.TryParse(value, out _DeviseDeReference);
            }
        }
    }

    public string HypotheseDeCalculDeGain
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(_HypotheseDeCalculDeGain);
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _convert.TryParse(value, out _HypotheseDeCalculDeGain);
            }
        }
    }

    public float CoutProjete_Val
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _CoutProjete;
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _CoutProjete = value;
            }
        }
    }

    public string CoutProjete
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(_CoutProjete);
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _convert.TryParse(value, out _CoutProjete);
            }
        }
    }

    private float calc_GainPotentielUnitaire()
    {
        float gain = (_CoutDeReference - _CoutProjete);

        return gain;
    }

    public float GainPotentielUnitaire_Val
    {
        get 
        { 
            lock (_syncRoot)
            {
                return calc_GainPotentielUnitaire();
            }
        }
    }

    public string GainPotentielUnitaire
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(calc_GainPotentielUnitaire(), true);
            }
        }
    }


    private float calc_PourcentageGain()
    {
        float gain = 0;

        if( _CoutDeReference != 0 )
            gain = Mathf.FloorToInt(calc_GainPotentielUnitaire() / _CoutDeReference * 100);

        return gain;
    }

    public float PourcentageGain_Val
    {
        get 
        { 
            lock (_syncRoot)
            {
                return calc_PourcentageGain();
            }
        }

    }

    public string PourcentageGain
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(calc_PourcentageGain());
            }
        }

    }

    public string MaturiteDuPotentiel
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(_MaturiteDuPotentiel);
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _convert.TryParse(value, out _MaturiteDuPotentiel);
            }
        }
    }

    public int QuantiteAnnuelle_Val
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _QuantiteAnnuelle;
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _QuantiteAnnuelle = value;
            }
        }
    }

    public string QuantiteAnnuelle
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(_QuantiteAnnuelle);
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _convert.TryParse(value, out _QuantiteAnnuelle);
            }
        }
    }
        
    private float calc_GainAnnuel()
    {
        float gain = calc_GainPotentielUnitaire();
        float gainAnnuel = (gain * _QuantiteAnnuelle);

        return gainAnnuel;
    }

    public string GainAnnuel
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(calc_GainAnnuel(), true);
            }
        }
    }

    public float MasseDeReference_Val
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _MasseDeReference;
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _MasseDeReference = value;
            }
        }
    }

    public string MasseDeReference
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(_MasseDeReference);
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _convert.TryParse(value, out _MasseDeReference);
            }
        }
    }


    public float MasseProjete_Val
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _MasseProjete;
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _MasseProjete = value;
            }
        }
    }

    public string MasseProjete
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(_MasseProjete);
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _convert.TryParse(value, out _MasseProjete);
            }
        }
    }

    private float calc_GainDeMasse()
    {
        float gain = _MasseDeReference - _MasseProjete;

        return gain;
    }


    public float GainDeMasse_Val
    {
        get 
        { 
            lock (_syncRoot)
            {
                return calc_GainDeMasse();
            }
        }
    }

    public string GainDeMasse
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(calc_GainDeMasse());
            }
        }
    }

    public string CoutScenario1
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(_CoutScenario1);
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _convert.TryParse(value, out _CoutScenario1);
            }
        }
    }

    public string CoutScenario2
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(_CoutScenario2);
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _convert.TryParse(value, out _CoutScenario2);
            }
        }
    }

    public string CoutScenario3
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(_CoutScenario3);
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _convert.TryParse(value, out _CoutScenario3);
            }
        }
    }


    public bool TroisF_Val
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _TroisF;
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _TroisF = value;
            }
        }
    }

    public string TroisF
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(_TroisF);
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _convert.TryParse(value, out _TroisF);
            }
        }
    }

    public string CommentaireImpact
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(_CommentaireImpact);
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _convert.TryParse(value, out _CommentaireImpact);
            }
        }
    }

    public string RisqueALever
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(_RisqueALever);
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _convert.TryParse(value, out _RisqueALever);
            }
        }
    }

    public string PlanAction
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(_PlanAction);
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _convert.TryParse(value, out _PlanAction);
            }
        }
    }

    public string DatePrevisionnelleValidation
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(_DatePrevisionnelleValidation);
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _convert.TryParse(value, out _DatePrevisionnelleValidation);
            }
        }
    }

	public DateTime DatePrevisionnelleValidation_val
	{
		get 
		{ 
			lock (_syncRoot)
			{
				return _DatePrevisionnelleValidation;
			}
		}
		set 
		{ 
			lock (_syncRoot)
			{
				_DatePrevisionnelleValidation=value;
			}
		}
	}

    public string DatePrevisionnelleImplantation
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(_DatePrevisionnelleImplantation);
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _convert.TryParse(value, out _DatePrevisionnelleImplantation);
            }
        }
    }

	public DateTime DatePrevisionnelleImplantation_val
	{
		get 
		{ 
			lock (_syncRoot)
			{
				return _DatePrevisionnelleImplantation;
			}
		}
		set 
		{ 
			lock (_syncRoot)
			{
				_DatePrevisionnelleImplantation=value;
			}
		}
	}

    public float EffortValidationEtDeveloppelement_Val
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _EffortValidationEtDeveloppelement;
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _EffortValidationEtDeveloppelement = value;
            }
        }
    }

    public string EffortValidationEtDeveloppelement
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(_EffortValidationEtDeveloppelement);
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _convert.TryParse(value, out _EffortValidationEtDeveloppelement);
            }
        }
    }

    public float CoutEffortValidation_Val
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _CoutEffortValidation;
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _CoutEffortValidation = value;
            }
        }
    }

    public string CoutEffortValidation
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(_CoutEffortValidation);
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _convert.TryParse(value, out _CoutEffortValidation);
            }
        }
    }

    public float CommentaireValidationEtDeveloppement_Val
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _CommentaireValidationEtDeveloppement;
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _CommentaireValidationEtDeveloppement = value;
            }
        }
    }

    public string CommentaireValidationEtDeveloppement
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(_CommentaireValidationEtDeveloppement);
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _convert.TryParse(value, out _CommentaireValidationEtDeveloppement);
            }
        }
    }

    public float calc_InvestissementsEtOutillages()
    {
        float gainCout = _CommentaireValidationEtDeveloppement * _CoutEffortValidation;

        return gainCout;
    }

    public string InvestissementsEtOutillages
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(calc_InvestissementsEtOutillages(), true);
            }
        }
    }

    public string CommentaireInvestissement
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(_CommentaireInvestissement);
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _convert.TryParse(value, out _CommentaireInvestissement);
            }
        }
    }

    private float calc_TotalNRC()
    {
        float Nrc = _EffortValidationEtDeveloppelement + calc_InvestissementsEtOutillages();

        return Nrc;
    }

    public float TotalNRC_Val
    {
        get 
        { 
            lock (_syncRoot)
            {
                return calc_TotalNRC();
            }
        }
    }

    public string TotalNRC
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(calc_TotalNRC(), true);
            }
        }
    }

    public int NoteEnergie_Val
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _NoteEnergie;
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _NoteEnergie = value;
            }
        }
    }

    public string NoteEnergie
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(_NoteEnergie);
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _convert.TryParse(value, out _NoteEnergie);
            }
        }
    }
        
    private float calc_RentabiliteROI()
    {
        float nombreMois = 0; 
        float gain = calc_GainAnnuel();
        float Nrc = calc_TotalNRC();


        if (gain != 0)
            nombreMois = Nrc / (12 * gain);

        return nombreMois;
    }

    public string RentabiliteROI
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(calc_RentabiliteROI());
            }
        }
    }

    public int NoteRentabilite_Val
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _NoteRentabilite;
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _NoteRentabilite = value;
            }
        }
    }

    public string NoteRentabilite
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(_NoteRentabilite);
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _convert.TryParse(value, out _NoteRentabilite);
            }
        }
    }

    public string NiveauDifficulte
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(_NiveauDifficulte);
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _convert.TryParse(value, out _NiveauDifficulte);
            }
        }
    }


    public int NoteAdhesion_Val
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _NoteAdhesion;
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _NoteAdhesion = value;
            }
        }
    }

    public string NoteAdhesion
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(_NoteAdhesion);
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _convert.TryParse(value, out _NoteAdhesion);
            }
        }
    }

    public List<float> CoutScenarios
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _CoutScenarios;
            }
        }
    }


    public pp_produit_composant Composant
    {
        get
        { 
            lock (_syncRoot)
            {
                return _composant;
            }
        }
    }

    public pp_produit Produit
    {
        get
        { 
            lock (_syncRoot)
            {
                return _composant.Produit;
            }
        }
    }

    public ppconvert Convert
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert;
            }
        }
    }
}