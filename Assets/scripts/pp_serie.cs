﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class pp_serie : MonoBehaviour
{
    public enum pp_serie_smooth_filter
    {
        NONE = 0,
        SAVITZKY_GOLAY = 1
    }

    public enum pp_serie_smooth_axes
    {
        AXE_X = 1,
        AXE_Y = 2,
        AXE_XY = 3,
        AXE_Z = 4,
        AXE_XZ = 5,
        AXE_YZ = 6,
        AXE_XYZ = 7
    }

    private List<Vector3> _data = null;
    private List<Vector3> _smoothData = null;

    float _maxX = 0;
    float _maxY = 0;
    float _maxZ = 0;

    private bool _reSmooth = false;


    private int _resolution = 300;
    private pp_serie_smooth_filter _filter = pp_serie_smooth_filter.SAVITZKY_GOLAY;
    private bool _filterAgressiveMode = false;
    private pp_serie_smooth_axes _filterAxes = pp_serie_smooth_axes.AXE_YZ;

    private object _syncRoot = new System.Object();

    public pp_serie( float[] dataY, float maxX )
    {
        _data = new List<Vector3>();
        _smoothData = new List<Vector3>();

        _maxX = maxX;

        if( dataY != null ) 
        {
            for( int i = 0; i < dataY.Length; i ++ ) 
            {
                float x = 0;
                float z = 0;
                float y = dataY[i];

                if( dataY.Length > 1 )
                {
                    if( _maxX != 0 )
                        x = i * (_maxX / (dataY.Length - 1));

                    if( _maxZ != 0 )
                        z = i * (_maxZ / (dataY.Length - 1));
                }

                Vector3 pt = new Vector3(x, y, z);

                _data.Add(pt);
            }
        }

        _reSmooth = true;
    }

    public pp_serie( )
        : this( null, 0 )
    {
    }

    // Getter / Setter
    public int Resolution
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _resolution;
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _resolution = value;
                _reSmooth = true;
            }
        }
    }


    public pp_serie_smooth_filter Filter
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _filter;
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _filter = value;
                _reSmooth = true;
            }
        }
    }

    public bool FilterAgressiveMode
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _filterAgressiveMode;
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _filterAgressiveMode = value;
                _reSmooth = true;
            }
        }
    }



    public pp_serie_smooth_axes FilterAxes
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _filterAxes;
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _filterAxes = value;
                _reSmooth = true;
            }
        }
    }


    public List<Vector3> Data
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _data;
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _data = value;
                _reSmooth = true;
            }
        }
    }

   

    public float[] DataY
    {
        get 
        { 
            lock (_syncRoot)
            {
                float[] data = null;

                if (_data.Count > 0)
                {
                    data = new float[_data.Count];

                    for( int i = 0; i < _data.Count; i++ )   
                    {
                        data[i] = _data[i].y;
                    }
                }

                return data;
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                if (value != null)
                {
                    for (int i = 0; i < value.Length; i++)
                    {
                        float x = 0;
                        float z = 0;
                        float y = value[i];

                        if (value.Length > 1)
                        {
                            if (_maxX != 0)
                                x = i * (_maxX / (value.Length - 1));

                            if (_maxZ != 0)
                                z = i * (_maxZ / (value.Length - 1));
                        }

                        Vector3 pt = new Vector3(x, y, z);

                        _data.Add(pt);
                    }
                }
                else
                    _data.Clear();

                _reSmooth = true;
            }
        }
    }

    public float MaxX
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _maxX;
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _maxX = value;

                if(_maxX != 0)
                {
                    for (int i = 0; i < _data.Count; i++)
                    {
                        float x = 0;
                        float y = _data[i].y;
                        float z = _data[i].z;

                        if (_data.Count > 1)
                            x = i * (_maxX / (_data.Count - 1));

                        _data[i] = new Vector3(x, y, z);
                    }
                }

                _reSmooth = true;
            }
        }
    }


    public List<Vector3> SmoothData
    {
        get 
        { 
            lock (_syncRoot)
            {
                if (_reSmooth)
                    _smooth();

                return _smoothData;
            }
        }
    }

    // Methodes
    private List<Vector3> _GetSmoothWindow( List<Vector3> data, int idx, int win_size )
    {
        List<Vector3> window = new List<Vector3>();

        for (int i = 0; i <win_size; i++)
            window.Add( new Vector3( 0, 0, 0 ) );

        if ((data != null) && (idx >= 0) && (data.Count > idx))
        {
            for (int i = 0; i < win_size; i++)
            {
                int j = idx - (win_size / 2) + i;

                if ((j >= 0) && (j < data.Count))
                    window[i] = data[j];
                else if( data.Count > 1 ) 
                {
                    if( j < 0 )
                    {
                        window[i] = data[0] + j * (data[1] - data[0]);
                    }
                    else
                    {
                        window[i] = data[data.Count -1] + (j-data.Count) * (data[data.Count -1] - data[data.Count -2]);
                    }

                }
                else
                    window[i] = data[idx];
            }
        }

        return window;
    }

    private Vector3 _SmoothFilterSavityGolay( List<Vector3> data, int idx )
    {
        Vector3 fval = new Vector3( 0, 0, 0 );

        if ((data != null) && (idx >= 0) && (data.Count > idx))
        {
            fval = data[idx];

            if ( !_filterAgressiveMode )
            {
                List<Vector3> window = _GetSmoothWindow(data, idx, 5);

                if ((_filterAxes & pp_serie_smooth_axes.AXE_X) > 0)
                {
                    fval.x = (1.0f / 35.0f) *
                    (-3.0f * window[0].x
                    + 12.0f * window[1].x

                    + 17.0f * window[2].x

                    + 12.0f * window[3].x
                    - 3.0f * window[4].x);
                }

                if ((_filterAxes & pp_serie_smooth_axes.AXE_Y) > 0)
                {
                    fval.y = (1.0f / 35.0f) *
                    (-3.0f * window[0].y
                    + 12.0f * window[1].y

                    + 17.0f * window[2].y

                    + 12.0f * window[3].y
                    - 3.0f * window[4].y);
                }

                if ((_filterAxes & pp_serie_smooth_axes.AXE_Z) > 0)
                {
                    fval.z = (1.0f / 35.0f) *
                    (-3.0f * window[0].z
                    + 12.0f * window[1].z

                    + 17.0f * window[2].z

                    + 12.0f * window[3].z
                    - 3.0f * window[4].z);
                }
            }
            else if (true)
            {
                List<Vector3> window = _GetSmoothWindow(data, idx, 25);

                if ((_filterAxes & pp_serie_smooth_axes.AXE_X) > 0)
                {
                    fval.x = (1.0f / 5177.0f) *
                    (-253.0f * window[0].x
                    - 138.0f * window[1].x
                    - 33.0f * window[2].x
                    + 62.0f * window[3].x
                    + 147.0f * window[4].x
                    + 222.0f * window[5].x
                    + 287.0f * window[6].x
                    + 343.0f * window[7].x
                    + 387.0f * window[8].x
                    + 422.0f * window[9].x
                    + 447.0f * window[10].x
                    + 462.0f * window[11].x

                    + 467.0f * window[12].x

                    + 462.0f * window[13].x
                    + 447.0f * window[14].x
                    + 422.0f * window[15].x
                    + 387.0f * window[16].x
                    + 343.0f * window[17].x
                    + 287.0f * window[18].x
                    + 222.0f * window[19].x
                    + 147.0f * window[20].x
                    + 62.0f * window[21].x
                    - 33.0f * window[22].x
                    - 138.0f * window[23].x
                    - 253.0f * window[24].x); 
                }
                    
                if ((_filterAxes & pp_serie_smooth_axes.AXE_Y) > 0)
                {
                    fval.y = (1.0f / 5177.0f) *
                    (-253.0f * window[0].y
                    - 138.0f * window[1].y
                    - 33.0f * window[2].y
                    + 62.0f * window[3].y
                    + 147.0f * window[4].y
                    + 222.0f * window[5].y
                    + 287.0f * window[6].y
                    + 343.0f * window[7].y
                    + 387.0f * window[8].y
                    + 422.0f * window[9].y
                    + 447.0f * window[10].y
                    + 462.0f * window[11].y

                    + 467.0f * window[12].y

                    + 462.0f * window[13].y
                    + 447.0f * window[14].y
                    + 422.0f * window[15].y
                    + 387.0f * window[16].y
                    + 343.0f * window[17].y
                    + 287.0f * window[18].y
                    + 222.0f * window[19].y
                    + 147.0f * window[20].y
                    + 62.0f * window[21].y
                    - 33.0f * window[22].y
                    - 138.0f * window[23].y
                    - 253.0f * window[24].y); 
                }

                if ((_filterAxes & pp_serie_smooth_axes.AXE_Z) > 0)
                {
                    fval.z = (1.0f / 5177.0f) *
                    (-253.0f * window[0].z
                    - 138.0f * window[1].z
                    - 33.0f * window[2].z
                    + 62.0f * window[3].z
                    + 147.0f * window[4].z
                    + 222.0f * window[5].z
                    + 287.0f * window[6].z
                    + 343.0f * window[7].z
                    + 387.0f * window[8].z
                    + 422.0f * window[9].z
                    + 447.0f * window[10].z
                    + 462.0f * window[11].z

                    + 467.0f * window[12].z

                    + 462.0f * window[13].z
                    + 447.0f * window[14].z
                    + 422.0f * window[15].z
                    + 387.0f * window[16].z
                    + 343.0f * window[17].z
                    + 287.0f * window[18].z
                    + 222.0f * window[19].z
                    + 147.0f * window[20].z
                    + 62.0f * window[21].z
                    - 33.0f * window[22].z
                    - 138.0f * window[23].z
                    - 253.0f * window[24].z); 
                }
            }
        }

        return(fval);
    }


    private List<Vector3> _SmoothFilterSavityGolay( List<Vector3> data )
    {
        List<Vector3> dataSmooth = null;

        if ((data != null) && (data.Count > 0))
        {
            dataSmooth =  new List<Vector3>();

            for (int i = 0; i < data.Count; i++)
            {
                Vector3 pt = _SmoothFilterSavityGolay(data, i);

                dataSmooth.Add(pt);
            }
        }
       
        
        return dataSmooth;
    }

    private void _smooth()
    {
        _smoothData.Clear();
        _reSmooth = false;

        if( _data.Count > 0 )
        {
            // _Resolution est la resolution minimum
            // afin de ne pas deformer la courbe, la resolution utilisée sera le multiple superieur de la taille des data
            int resolution = (_resolution > 0 ? _resolution : 1 );
            int nb_segment = _data.Count - 1;
            float pasX = 0;
            Vector3 pasY = new Vector3( 0, 0, 0);
            int coefX = (_resolution > 0 ? _resolution : 1 );
           

            if (nb_segment > 0) // Sinon c'est que j'ai qu'un point -> Droite
            {
                resolution = (int)((_resolution > 0 ? _resolution : 1) / nb_segment) * nb_segment;
                resolution += 1;

                if( (resolution < (_resolution > 0 ? _resolution : 1) ) || (resolution < nb_segment) )
                    resolution += nb_segment;

                coefX = (resolution - 1) / nb_segment;
            }


            List<Vector3> dataReso = new List<Vector3>();
            if (dataReso != null)
            {
                for (int i = 0; i < resolution; i++)
                {
                    float x, y, z;

                    int j = (i / coefX);
                    int k = i % coefX;

                    if ((j + 1) < _data.Count)
                    {
                        pasY.x = _data[j + 1].x - _data[j].x;
                        pasY.y = _data[j + 1].y - _data[j].y;
                        pasY.z = _data[j + 1].z - _data[j].z;
                    }

                    if (nb_segment > 0) // Sinon c'est que j'ai qu'un point -> Droite
                        pasX = (float)(nb_segment * k) / (resolution - 1);
                    else
                        pasX = 0;

                    x = _data[j].x + pasX * pasY.x;
                    y = _data[j].y + pasX * pasY.y;
                    z = _data[j].z + pasX * pasY.z;

                    dataReso.Add(new Vector3(x, y, z) );
                }
            }

            List<Vector3> dataSmooth = null;
            switch (_filter)
            {
                case pp_serie_smooth_filter.SAVITZKY_GOLAY:
                    _smoothData = _SmoothFilterSavityGolay(dataReso);
                    break;
                    
                default :
                    dataSmooth = dataReso;
                    break;
            }
            if (dataSmooth != null)
            {
                foreach (Vector3 pt in dataSmooth)
                {
                    _smoothData.Add(pt);
                }
            }
        }
    }





    public List<GameObject> TraceCurve( string prefixe, List<Vector3> data, float width, Texture texture, Color color, GameObject parent, Vector3 ori )
    {
        List<GameObject> lst = new List<GameObject>();

        if( (lst != null) && (data != null) && (data.Count > 1 ) )
        {
            for(int i = 1; i < data.Count; i++)
            {
                GameObject go = TraceLine(prefixe, data, i, width, texture, color, parent, ori, true);

                if (go != null)
                    lst.Add(go);
            }
        }

        return lst;
    }


    public List<GameObject> TraceCurve( string prefixe, List<Vector3> data, float width, Texture texture, Color color, Vector3 ori )
    {
        return TraceCurve(prefixe, data, width, texture, color, gameObject, ori);
    }

    public List<GameObject> TraceSmoothCurve( float width, Texture texture, Color color, Vector3 ori )
    {
        return TraceCurve("smooth", SmoothData, width, texture, color, gameObject, ori);
    }

    public List<GameObject> TraceRawCurve( float width, Texture texture, Color color, Vector3 ori )
    {
        return TraceCurve("raw", Data, width, texture, color, gameObject, ori);
    }



    //public GameObject TraceLine(string prefixe, int itemid, GameObject father, Texture texture, Color col, Vector3 vec1, Vector3 vec2)
    public GameObject TraceLine(string prefixe, List<Vector3> data, int idxData, float width, Texture texture, Color color, GameObject parent, Vector3 ori, bool modeLine )
    {
        //GameObject newobj = GameObject.CreatePrimitive(PrimitiveType.Quad);
        //GameObject newobj = GameObject.CreatePrimitive(PrimitiveType.Cube);
        GameObject newobj = new GameObject();

        newobj.AddComponent<MeshCollider>().name = "";
        newobj.AddComponent<MeshFilter>();
        newobj.AddComponent<MeshRenderer>();


        newobj.name = parent.name + "_" + prefixe + "_line_" + idxData;
        newobj.transform.parent = parent.transform;
        newobj.transform.localPosition = new Vector3(0,0,0.2f);
        newobj.transform.localScale = parent.transform.localScale;

        //CreateNewLine_Flat(data, idxData, width, color, newobj, ori, modeLine);
        CreateNewLine_3D(data, idxData, width, color, newobj, ori, modeLine);


        newobj.GetComponent<MeshRenderer>().sharedMaterial = new Material(Shader.Find("Sprites/Default"));
        newobj.GetComponent<MeshRenderer>().sharedMaterial = new Material(Shader.Find("Standard"));
        //newobj.GetComponent<MeshRenderer>().sharedMaterial = new Material(Shader.Find("Mobile/Diffuse"));
        newobj.GetComponent<MeshRenderer>().sharedMaterial.mainTexture = texture;
        newobj.GetComponent<MeshRenderer>().sharedMaterial.color = color;

        newobj.SetActive(true);

        return (newobj);
    }

   


    private Vector3[] VertexLine_Flat(List<Vector3> data, int idxData, float width, Vector3 ori, bool modeLine)
    {
        Vector3[] vecX = new Vector3[3];
        Vector3[] vecY = new Vector3[3];

        bool nextSegOK = false;
        bool prevSegOK = false;

        if (false)
        {
            if ((idxData - 2) >= 0)
            {
                vecX[0] = data[idxData - 2] + ori;
                vecY[0] = data[idxData - 1] + ori;

                prevSegOK = modeLine;
            }

            vecX[1] = data[idxData - 1] + ori;
            vecY[1] = data[idxData] + ori;

            if ((idxData + 1) < data.Count)
            {
                vecX[2] = data[idxData] + ori;
                vecY[2] = data[idxData + 1] + ori;

                nextSegOK = modeLine;
            }
        }
        else
        {
            vecX[0] = data[idxData] + ori;
            vecY[0] = data[idxData] + ori;

            vecX[1] = data[idxData] + ori;
            vecY[1] = data[idxData] + ori;

            vecX[2] = data[idxData] + ori;
            vecY[2] = data[idxData] + ori;

            // Recherche segment d'avant qui a des coordonnées differentes
            for( int i = idxData - 1; i >= 0; i-- )
            {
                if( data[idxData] != data[i] )
                {
                    vecX[0] = data[i] + ori;
                    vecY[0] = data[i] + ori;
                    vecX[1] = data[i] + ori;
                    for( int j = i - 1; j >= 0; j-- )
                    {
                        if( data[i] != data[j] )
                        {
                            vecX[0] = data[j] + ori;

                            prevSegOK = modeLine;

                            break;
                        }
                    }

                    break;
                }
            }

            // Recherche segment d'apres qui a des coordonnées differentes
            for (int i = idxData + 1; i < data.Count; i++)
            {
                if( data[idxData] != data[i] )
                {
                    vecY[2] = data[i] + ori;
                    nextSegOK = modeLine;
                    break;
                }
            }

        }

        /*
        Soit on considere un rectangle  ABCD :


        A                          D

        WIDHT/2

        X                          Y

        WIDHT/2

        B                          C


        AD, XY et BC on le meme coeficient directeur (paralele )

        pour AD:  Y = n * X + r
        pour BC:  Y = n * X + s

        AB et DC etant perpendiculaire a XY on determine que les droite AD  et BC on le meme coeficient directeur 
        et que donc :

        pour AB:  Y = m * X + p    (Y - m * X = p)
        pour DC:  Y = m * X + q

        avec m = -n;


        Pour calcule intersection AD et A'D'

        Y = n * X + r
        Y = n' * X + r'

        Donc X = (r' - r) / (n - n')  (avec (n - n') = 0 si paralelle)


        */

        float[] n = new float[3];
        float[] signe_n = new float[3];


        float[] r = new float[3];
        float[] s = new float[3];

        Vector3[] A = new Vector3[3];
        Vector3[] B = new Vector3[3];
        Vector3[] C = new Vector3[3];
        Vector3[] D = new Vector3[3];

        Vector3[] AD_AD = new Vector3[2];
        Vector3[] BC_BC = new Vector3[2];

        bool[] AD_BC_OK = new bool[2];

        for (int i = 0; i < 2; i++)
            AD_BC_OK[i] = false;

        for (int i = (prevSegOK ? 0 : 1); i < (nextSegOK ? 3 : 2); i++)
        {
            float m = -1f * (vecY[i].x - vecX[i].x) / (vecY[i].y - vecX[i].y); // Coef directeur de AB et DC
            float sign_m = 1;

            if (float.IsNaN(m)) // X et Y au meme coordonnée
                sign_m = 1;

            else if (float.IsInfinity(m)) // X et Y meme ordonnée
                sign_m = (float.IsPositiveInfinity(m) ? -1 : 1);

            else if (m == 0) // X et Y meme abscisse
                sign_m = -1f * (vecY[i].y - vecX[i].y) / Mathf.Abs(vecY[i].y - vecX[i].y);

            else
                sign_m = m / Mathf.Abs(m); 

            float p = vecX[i].y - m * vecX[i].x;
            float q = vecY[i].y - m * vecY[i].x;

            if( !float.IsNaN( m ) &&  !float.IsInfinity( m ) )
            {
                // Forme Y = mX + p (Droite AB)
                // Forme Y = mX + q (Droite DC)

                A[i].x = vecX[i].x + sign_m * width / (2f * Mathf.Sqrt(m * m + 1f));  
                A[i].y = m * A[i].x + p;
                A[i].z = ori.z; // Non géré pour l'instant


                B[i].x = vecX[i].x + sign_m * -1f * width / (2f * Mathf.Sqrt(m * m + 1f));  
                B[i].y = m * B[i].x + p;
                B[i].z = ori.z; // Non géré pour l'instant


                C[i].x = vecY[i].x + sign_m * -1f * width / (2f * Mathf.Sqrt(m * m + 1f));  
                C[i].y = m * C[i].x + q;
                C[i].z = ori.z; // Non géré pour l'instant


                D[i].x = vecY[i].x + sign_m * width / (2f * Mathf.Sqrt(m * m + 1f));  
                D[i].y = m * D[i].x + q;
                D[i].z = ori.z; // Non géré pour l'instant

            }
            else // AB et DC paralelle a l'ordonnée -> xA = xB = xX et xD = xC = xY
            {
                A[i].x = vecX[i].x;
                A[i].y = vecX[i].y + sign_m * width / 2f;
                A[i].z = ori.z; // Non géré pour l'instant

                B[i].x = vecX[i].x;
                B[i].y = vecX[i].y + sign_m * -1f * width / 2f;
                B[i].z = ori.z; // Non géré pour l'instant

                C[i].x = vecY[i].x;
                C[i].y = vecY[i].y + sign_m * -1f * width / 2f;
                C[i].z = ori.z; // Non géré pour l'instant

                D[i].x = vecY[i].x;
                D[i].y = vecY[i].y + sign_m * width / 2f;
                D[i].z = ori.z; // Non géré pour l'instant
            }


            // Gestion intersection des droites AD et BC du segment courant avec les droites AD et BC des segments precedent et suivant
            n[i] = (vecY[i].y - vecX[i].y) / (vecY[i].x - vecX[i].x); // Coef directeur de XY, AD, BC

            if (float.IsNaN(n[i])) // X et Y au meme coordonnée
                signe_n[i] = 1;

            else if (float.IsInfinity(n[i])) // X et Y meme abscisse
                signe_n[i] = (float.IsPositiveInfinity(n[i]) ? -1 : 1);

            else if (n[i] == 0) // X et Y meme ordonnee
                signe_n[i] = (vecY[i].x - vecX[i].x) / Mathf.Abs(vecY[i].x - vecX[i].x);

            else
                signe_n[i] = n[i] / Mathf.Abs(n[i]); 

            r[i] = A[i].y - n[i] * A[i].x;
            s[i] = B[i].y - n[i] * B[i].x;


            if (idxData == 185)
            {
                int toto;

                toto = 1;
            }

            // Y = n * X + r
            // Y = n' * X + r'
            // Donc X = (r' - r) / (n - n')  (avec (n - n')
            if( ((i == 1) && prevSegOK) || ((i == 2) && nextSegOK) )
            {
                if (float.IsNaN(n[i - 1]) && float.IsNaN(n[i])) // les droite sont des points (cas coordonnées X(x,y) = Y(x,y))
                {
                    AD_BC_OK[i - 1] = false;
                }
                else if (float.IsInfinity(n[i - 1]) && float.IsInfinity(n[i])) // Droite paralele (a l'ordonnee) = pas d'intersection
                {
                    AD_BC_OK[i - 1] = false;
                }
                else if (float.IsInfinity(n[i - 1]) && float.IsNaN(n[i])) // L'une des droite est de la forme Y=nX+r et l'autre en paralelle a l'ordonnee l'autre est un point
                {
                    // Les droites du 1ere segment sont paralelles a l'ordonnée donc
                    // Et on considere l'autre comme paralelles en abscisse 
                    // Pour AD il se croise en XY qd X=Xa=Xd et Y=Ya=Yd
                    // pour BC qd X=Xb=Xc et X=Yb=Yc
                    AD_BC_OK[i - 1] = true;

                    AD_AD[i - 1].x = A[i - 1].x;
                    AD_AD[i - 1].y = A[i].y;
                    AD_AD[i - 1].z = ori.z; // Non géré pour l'instant

                    BC_BC[i - 1].x = B[i - 1].x;
                    BC_BC[i - 1].y = B[i].y;
                    BC_BC[i - 1].z = ori.z; // Non géré pour l'instant
                }
                else if (float.IsInfinity(n[i]) && float.IsNaN(n[i - 1])) // L'une des droite est de la forme Y=nX+r et l'autre en paralelle a l'ordonnee l'autre est un point
                {
                    // Les droites du 2eme segment sont paralelles a l'ordonnée donc
                    // Pour AD il se croise en XY qd X=Xa=Xd et Y=Ya=Yd
                    // pour BC qd X=Xb=Xc et X=Yb=Yc
                    AD_BC_OK[i - 1] = true;

                    AD_AD[i - 1].x = A[i].x;
                    AD_AD[i - 1].y = A[i - 1].y;
                    AD_AD[i - 1].z = ori.z; // Non géré pour l'instant

                    BC_BC[i - 1].x = B[i].x;
                    BC_BC[i - 1].y = B[i - 1].y;
                    BC_BC[i - 1].z = ori.z; // Non géré pour l'instant
                }
                else if (float.IsNaN(n[i - 1]) && (n[i] == 0)) //une droite est un point (cas coordonnées X(x,y) = Y(x,y)) (on considere paralelle a l'abscisse) et l'autre est paralelle a l'abcisse aussi
                {
                    AD_BC_OK[i - 1] = false;
                }
                else if (float.IsNaN(n[i]) && (n[i - 1] == 0)) //une droite est un point (cas coordonnées X(x,y) = Y(x,y)) (on considere paralelle a l'abscisse) et l'autre est paralelle a l'abcisse aussi
                {
                    AD_BC_OK[i - 1] = false;
                }
                else if (float.IsNaN(n[i - 1])) // une droite est un point (cas coordonnées X(x,y) = Y(x,y)) (on considere paralelle a l'abscisse)
                {
                    // Les droites du 1ere segment sont paralelles a l'abscisse donc
                    // Pour AD il se croise en XY qd Y=Ya=Yd
                    // pour BC qd Y=Yb=Yc
                    // l'autre de la forme Y=nX+r -> X = (Y - r)/n
                    AD_BC_OK[i - 1] = true;

                    AD_AD[i - 1].x = (A[i - 1].y - r[i]) / n[i];
                    AD_AD[i - 1].y = A[i - 1].y;
                    AD_AD[i - 1].z = ori.z; // Non géré pour l'instant

                    BC_BC[i - 1].x = (B[i - 1].y - s[i]) / n[i];
                    BC_BC[i - 1].y = B[i - 1].y;
                    BC_BC[i - 1].z = ori.z; // Non géré pour l'instant
                }
                else if (float.IsNaN(n[i])) // une droite est un point (cas coordonnées X(x,y) = Y(x,y)) (on considere paralelle a l'abscisse)
                {
                    // Les droites du 2eme segment sont paralelles a l'abscisse donc
                    // Pour AD il se croise en XY qd Y=Ya=Yd
                    // pour BC qd Y=Yb=Yc
                    // l'autre de la forme Y=nX+r -> X = (Y - r)/n
                    AD_BC_OK[i - 1] = true;

                    AD_AD[i - 1].x = (A[i].y - r[i - 1]) / n[i - 1];
                    AD_AD[i - 1].y = A[i].y;
                    AD_AD[i - 1].z = ori.z; // Non géré pour l'instant

                    BC_BC[i - 1].x = (B[i].y - s[i - 1]) / n[i - 1];
                    BC_BC[i - 1].y = B[i].y;
                    BC_BC[i - 1].z = ori.z; // Non géré pour l'instant
                }
                else if (float.IsInfinity(n[i - 1])) // L'une des droite est de la forme Y=nX+r et l'autre en paralelle a l'ordonnee
                {
                    // Les droites du 1ere segment sont paralelles a l'ordonnée donc
                    // Pour AD il se croise en XY qd X=Xa=Xd
                    // pour BC qd X=Xb=Xc
                    AD_BC_OK[i - 1] = true;

                    AD_AD[i - 1].x = A[i - 1].x;
                    AD_AD[i - 1].y = n[i] * A[i - 1].x + r[i];
                    AD_AD[i - 1].z = ori.z; // Non géré pour l'instant

                    BC_BC[i - 1].x = B[i - 1].x;
                    BC_BC[i - 1].y = n[i] * B[i - 1].x + s[i];
                    BC_BC[i - 1].z = ori.z; // Non géré pour l'instant
                }
                else if (float.IsInfinity(n[i])) // L'une des droite est de la forme Y=nX+r et l'autre en paralelle a l'ordonnee
                {
                    // Les droites du 2eme segment sont paralelles a l'ordonnée donc
                    // Pour AD il se croise en XY qd X=Xa=Xd
                    // pour BC qd X=Xb=Xc
                    AD_BC_OK[i - 1] = true;

                    AD_AD[i - 1].x = A[i].x;
                    AD_AD[i - 1].y = n[i - 1] * A[i].x + r[i - 1];
                    AD_AD[i - 1].z = ori.z; // Non géré pour l'instant

                    BC_BC[i - 1].x = B[i].x;
                    BC_BC[i - 1].y = n[i - 1] * B[i].x + s[i - 1];
                    BC_BC[i - 1].z = ori.z; // Non géré pour l'instant

                }
                else if ((n[i - 1] - n[i]) == 0) // Droite paralele = pas d'intersection
                {
                    AD_BC_OK[i - 1] = false;
                }
                else if( (Mathf.Abs(n[i - 1] - n[i]) < 0.01f) && (Mathf.Abs(r[i - 1] - r[i]) < 0.01f) && (Mathf.Abs(s[i - 1] - s[i]) < 0.01f)) // (imperfection calcule du a la precision du float -> n = 0) Droite paralele  = pas d'intersection
                {
                    AD_BC_OK[i - 1] = false;
                }
                else
                {
                    AD_BC_OK[i - 1] = true;

                    AD_AD[i - 1].x = (r[i] - r[i-1]) / (n[i - 1] - n[i]);
                    AD_AD[i - 1].y = n[i] * AD_AD[i - 1].x + r[i];
                    AD_AD[i - 1].z = ori.z; // Non géré pour l'instant

                    BC_BC[i - 1].x = (s[i] - s[i - 1]) / (n[i - 1] - n[i]);
                    BC_BC[i - 1].y = n[i] * BC_BC[i - 1].x + s[i];
                    BC_BC[i - 1].z = ori.z; // Non géré pour l'instant
                }

            }
        }
            
        Vector3[] vertex    = new Vector3[4];

        if (AD_BC_OK[0])
        {
            vertex[0] = AD_AD[0]; 
            vertex[3] = BC_BC[0];
        }
        else
        {
            vertex[0] = A[1]; 
            vertex[3] = B[1];
        }

        if (AD_BC_OK[1])
        {
            vertex[1] = AD_AD[1];
            vertex[2] = BC_BC[1];
        }
        else if( nextSegOK ) // Du imperfection calcule float on fait joindre le segment avec le suivant
        {
            vertex[1] = A[2];
            vertex[2] = B[2];
        }
        else
        {
            vertex[1] = D[1];
            vertex[2] = C[1];
        }

        return vertex;
    }

    private void CreateNewLine_Flat(List<Vector3> data, int idxData, float width, Color color, GameObject parent, Vector3 ori, bool modeLine)
    {

        Vector3[] vertex    = VertexLine_Flat( data, idxData, width, ori, modeLine);
        Color[]    col      = new Color[4];
        int[]     triangle  = new int[3*2];
        Vector2[] uv        = new Vector2[4]; 

        col[0] = col[1] = col[2] = col[3] = color;

        uv[0]        = new Vector2(0,0);
        uv[1]        = new Vector2(1,0);
        uv[2]        = new Vector2(1,1);
        uv[3]        = new Vector2(0,1);

        triangle[0] = 0;
        triangle[1] = 1;
        triangle[2] = 2;
        triangle[3] = 0;
        triangle[4] = 2;
        triangle[5] = 3;

        Mesh me = new Mesh();

        me.vertices  = vertex;
        me.colors    = col;
        me.uv        = uv;
        me.triangles    = triangle;

        me.RecalculateNormals();
        me.RecalculateBounds();

        parent.GetComponent<MeshFilter>().sharedMesh = me;
        parent.GetComponent<MeshCollider>().sharedMesh = me;
    }

    private void CreateNewLine_3D(List<Vector3> data, int idxData, float width, Color color, GameObject parent, Vector3 ori, bool modeLine)
    {
        Vector3[] vertexFlat = VertexLine_Flat( data, idxData, width, ori, modeLine);

        int nbVertex = 16;
        int v;

        if (!modeLine || idxData <= 1)
            nbVertex += 4;
       
        if (!modeLine || (idxData >= (data.Count - 1)) )
            nbVertex += 4;


        Vector3[] vertex     = new Vector3[nbVertex];
        Color[]   col        = new Color[nbVertex];
        int[]     triangle   = new int[(nbVertex/2)*3];
        Vector2[] uv         = new Vector2[nbVertex]; 

        v = 0;
        // Front
        vertex[v++] = vertexFlat[0] + new Vector3(0, 0, width / -2f);
        vertex[v++] = vertexFlat[1] + new Vector3(0, 0, width / -2f);
        vertex[v++] = vertexFlat[2] + new Vector3(0, 0, width / -2f);
        vertex[v++] = vertexFlat[3] + new Vector3(0, 0, width / -2f);

        // Back
        vertex[v++] = vertexFlat[3] + new Vector3(0, 0, width / 2f);
        vertex[v++] = vertexFlat[2] + new Vector3(0, 0, width / 2f);
        vertex[v++] = vertexFlat[1] + new Vector3(0, 0, width / 2f);
        vertex[v++] = vertexFlat[0] + new Vector3(0, 0, width / 2f);

        // Top
        vertex[v++] = vertexFlat[0] + new Vector3(0, 0, width / 2f);
        vertex[v++] = vertexFlat[1] + new Vector3(0, 0, width / 2f);
        vertex[v++] = vertexFlat[1] + new Vector3(0, 0, width / -2f);
        vertex[v++] = vertexFlat[0] + new Vector3(0, 0, width / -2f);

        // Bottom
        vertex[v++] = vertexFlat[3] + new Vector3(0, 0, width / -2f);
        vertex[v++] = vertexFlat[2] + new Vector3(0, 0, width / -2f);
        vertex[v++] = vertexFlat[2] + new Vector3(0, 0, width / 2f);
        vertex[v++] = vertexFlat[3] + new Vector3(0, 0, width / 2f);

        if (!modeLine || idxData <= 1)
        {
            // Left
            vertex[v++] = vertexFlat[0] + new Vector3(0, 0, width / 2f);
            vertex[v++] = vertexFlat[0] + new Vector3(0, 0, width / -2f);
            vertex[v++] = vertexFlat[3] + new Vector3(0, 0, width / -2f);
            vertex[v++] = vertexFlat[3] + new Vector3(0, 0, width / 2f);
        }

        if (!modeLine || (idxData >= (data.Count - 1)) )
        {
            // Rigth
            vertex[v++] = vertexFlat[1] + new Vector3(0, 0, width / -2f);
            vertex[v++] = vertexFlat[1] + new Vector3(0, 0, width / 2f);
            vertex[v++] = vertexFlat[2] + new Vector3(0, 0, width / 2f);
            vertex[v++] = vertexFlat[2] + new Vector3(0, 0, width / -2f);
        }
            
        for (int i = 0; i < vertex.Length; i += 4)
        {
            col[(i/4)*4+0] = color;
            col[(i/4)*4+1] = color;
            col[(i/4)*4+2] = color;
            col[(i/4)*4+3] = color;

            triangle[(i/4)*6+0] = (i/4)*4+0;
            triangle[(i/4)*6+1] = (i/4)*4+1;
            triangle[(i/4)*6+2] = (i/4)*4+2;
            triangle[(i/4)*6+3] = (i/4)*4+0;
            triangle[(i/4)*6+4] = (i/4)*4+2;
            triangle[(i/4)*6+5] = (i/4)*4+3;

            uv[(i/4)*4+0] = new Vector2(0,1);
            uv[(i/4)*4+1] = new Vector2(1,1);
            uv[(i/4)*4+2] = new Vector2(1,0);
            uv[(i/4)*4+3] = new Vector2(0,0);

        }
            

        Mesh me = new Mesh();

        me.vertices  = vertex;
        me.colors    = col;
        me.uv        = uv;
        me.triangles    = triangle;

        me.RecalculateNormals();
        me.RecalculateBounds();

        parent.GetComponent<MeshFilter>().sharedMesh = me;
        parent.GetComponent<MeshCollider>().sharedMesh = me;
    }


    public void DestroyCurve(List<GameObject> lst )
    {
        if (lst != null)
        {
            foreach (GameObject go in lst)
            {
                DestroyLine(go);
            }

        }
    }

    public void DestroyLine(GameObject obj)
    {
        if (obj != null)
        {
            Mesh     omesh = obj.GetComponent<MeshFilter>().sharedMesh;
            Material omat = obj.GetComponent<Renderer>().sharedMaterial;

            if(omesh != null)
                Destroy(omesh);

            if (omat != null)
                Destroy(omat);

            Destroy(obj);
        }
    }

}


