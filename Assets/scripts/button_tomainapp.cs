﻿ using UnityEngine;
using System.Collections;

public class button_tomainapp : buttonswap
{
	public override void ButtonPressed ()
	{
		base.ButtonPressed();
// verify ik Link completed
		StartCoroutine("SaveIniAndGotoMainApp");
	}

	IEnumerator SaveIniAndGotoMainApp()
	{
		while (linkeditor.jsonLoader==false)
			yield return new WaitForSeconds(0.05f);
        
        pp_produit prod = pp_manager.GetCurrentProduit();

        if (prod != null)
        {
            pp_task taskSave = prod.SaveData();

            if (taskSave != null)
            {
                while( !taskSave.IsFinished )
                    yield return new WaitForSeconds(0.05f);
            }
        }
            
		Application.LoadLevel("mainsceneUI");
	}

}
