﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;




public class pp_produit_scenario
{
    protected string _Titre = "";
    protected string _Description = "";
   
    protected object _syncRoot = new System.Object();
    protected ppconvert _convert = null;
    protected pp_produit _prod = null;
    private List<pp_produit_note> _notes;

    public pp_produit_scenario( string[] init_chps, pp_produit prod )
    {
        _prod = prod;
        _convert = prod.Convert;

        _notes = new List<pp_produit_note>();

        DecodeData(init_chps);
    }

    public pp_produit_scenario( string titre, string description, pp_produit prod )
        : this( (string[])null, prod )
    {
        _Titre = titre;
        _Description = description;

        //_convert.TryParse(titre, out _Titre);
        //_convert.TryParse(description, out _Description);
    }

    public void DecodeData(string[] init_chps )
    {
        if (init_chps != null)
        {
            lock (_syncRoot)
            {
                if (init_chps.Length > 0)
                    _convert.TryParse(init_chps[0], out _Titre);

                if (init_chps.Length > 1)
                    _convert.TryParse(init_chps[1], out _Description);

                if (init_chps.Length > 2)
                {
                    string listIdee = "";
                    List<pp_produit_note> lstNotes = _prod.notes;

                    _convert.TryParse(init_chps[2], out listIdee);

                    foreach (string idee in listIdee.Split( ", ".ToCharArray(), StringSplitOptions.RemoveEmptyEntries ))
                    {
                        int num = -1;

                        _convert.TryParse(idee, out num);

                        if ((num >= 0) && (num < lstNotes.Count))
                            _notes.Add(lstNotes[num]);
                    }
                }
            }
        }
    }

    public string[] EncodeTitre()
    {
        lock (_syncRoot)
        {
            string[] chps = new string[3];

            chps[0] = "Title";
            chps[1] = "Description";
            chps[2] = "Ideas";
           
            return chps;
        }
    }

    public string[] EncodeData()
    {
        lock (_syncRoot)
        {
            string[] chps = new string[3];
            string listIdee = "";
            List<pp_produit_note> lstNotes = _prod.notes;

            foreach (pp_produit_note note in _notes)
            {
                int idx = lstNotes.IndexOf(note);
                if (idx >= 0)
                {
                    if (listIdee.Length > 0)
                        listIdee += ",";
                    listIdee += _convert.ToString(idx);
                }
            }

            chps[0] = _convert.ToString(_Titre);
            chps[1] = _convert.ToString(_Description);
            chps[2] = _convert.ToString(listIdee);

            return chps;
        }
    }

    // Getter / Setter data
    public string Titre
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(_Titre);
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _convert.TryParse(value, out _Titre);
            }
        }
    }

    public string Description
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(_Description);
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _convert.TryParse(value, out _Description);
            }
        }
    }


    private float calc_GainPotentielUnitaire()
    {
        float fval = 0;

        foreach (pp_produit_note note in _notes)
        {
            fval += note.GainPotentielUnitaire_Val;
        }

        return fval;
    }

    public float GainPotentielUnitaire_Val
    {
        get 
        { 
            lock (_syncRoot)
            {
                return calc_GainPotentielUnitaire();
            }
        }
    }
        
    public string GainPotentielUnitaire
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(calc_GainPotentielUnitaire(), true);
            }
        }
    }




    private float calc_GainDeMasse()
    {
        float fval = 0;

        foreach (pp_produit_note note in _notes)
        {
            fval += note.GainDeMasse_Val;
        }

        return fval;
    }

    public float GainDeMasse_Val
    {
        get 
        { 
            lock (_syncRoot)
            {
                return calc_GainDeMasse();
            }
        }
    }

    public string GainDeMasse
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(calc_GainDeMasse());
            }
        }
    }


    private float calc_TotalNRC()
    {
        float fval = 0;

        foreach (pp_produit_note note in _notes)
        {
            fval += note.TotalNRC_Val;
        }

        return fval;
    }

    public float TotalNRC_Val
    {
        get 
        { 
            lock (_syncRoot)
            {
                return calc_TotalNRC();
            }
        }
    }

    public string TotalNRC
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(calc_TotalNRC(), true);
            }
        }
    }

    public pp_produit Produit
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _prod;
            }
        }
    }

    public List<pp_produit_note> Notes
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _notes;
            }
        }
    }

    public ppconvert Convert
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert;
            }
        }
    }
}
