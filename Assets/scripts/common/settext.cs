using UnityEngine;
using System.Collections;

public class settext : MonoBehaviour
{
	public string m_key;
	
	void Awake ()
	{
		tk2dTextMesh textje = gameObject.GetComponent<tk2dTextMesh>();
		ForceText(textje,languagetreatment.GetTextWithKey(m_key));
	}
	
	void Update ()
	{
		Awake();
	}

    public static void ForceText(tk2dTextMesh mesh,bool bError, string keyStr1, string keyStr2)
	{
        string str1 = localization.GetTextWithKey(keyStr1);
        string str2 = localization.GetTextWithKey(keyStr2);

        string str = str1;
        if (str2.Length > 0)
            str += " " + str2;
     
        mesh.color = (bError ? Color.red : Color.black);
		mesh.text = str;
		mesh.maxChars = str.Length;
		mesh.Commit();
	}

    public static void ForceText(tk2dTextMesh mesh, string keyStr1, string keyStr2)
    {
        ForceText(mesh, false, keyStr1, keyStr2);
    }

    public static void ForceText(tk2dTextMesh mesh,string keyStr)
    {
        ForceText(mesh, false, keyStr, "");
    }

    public static void ForceText(tk2dTextMesh mesh,bool bError, string keyStr)
    {
        ForceText(mesh, bError, keyStr, "");
    }
        
}
