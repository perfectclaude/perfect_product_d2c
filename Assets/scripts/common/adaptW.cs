using UnityEngine;
using System.Collections;

public class adaptW : MonoBehaviour
{
	Camera 	m_ViewCamera;

	void ResetPosition ()
	{
		Vector3 	vec;
		Vector3	res;
		
		vec.x = 0;
		vec.y = Screen.height/2;
		vec.z = 0;
		res = m_ViewCamera.ScreenToWorldPoint (vec);
		res.z = gameObject.transform.position.z;
		gameObject.transform.position = res;
	}

	void Awake ()
	{
		m_ViewCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
		ResetPosition();
	}
	
	void Update ()
	{
		if (!ni_edittext.scrollingup)
			ResetPosition();
	}
}
