﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using System;
using BestHTTP;

public class networkfiles : MonoBehaviour
{
    public static string localfilespath = null;
	public static byte [] Data = null;
	public static string DataStr = null;
	public static string [] Filedir = null;

	static bool _SaveFile = false;
	static bool _LoadFile = false;
	static string _LoadFile_path;
	static string _LoadFile_filename;
	public static bool networkfilesbusy = false;
	static bool _DirFile = false;
	static float _WaitForInternet = 0.0f;
	public string DebugGUI="Debug";
	GUIStyle styleLabel;

	static bool LOCALFILESONLY = true;
 


    void Awake ()
    {
        localfilespath = Application.persistentDataPath + "/d2cfiles";

        styleLabel = new GUIStyle ();
        styleLabel.normal.textColor = Color.magenta;
        //      LoadFile("bras","bras_obj.txt");
        //      DirFile();
    }

	public static bool FileExists(string path)
	{
		bool goahead = false;
		if (path.Contains(Application.persistentDataPath))		goahead = true;
		if (!goahead)
		{
			
		}
		return(System.IO.File.Exists(path));
	}

	public static string FileReadAllText(string path)
	{
		return(File.ReadAllText(path));
	}

	public static byte [] FileReadAllBytes(string path)
	{
		return(File.ReadAllBytes(path));
	}

	public static void FileWriteAllBytes(string path,byte [] data)
	{
		File.WriteAllBytes(path,data);
	}

	public static void FileWriteAllText(string path,string data)
	{
		File.WriteAllText(path,data);
	}








	public static bool SaveFile(string path, string filename, string data)
	{
		Debug.Log (networkfilesbusy);
		if (!LOCALFILESONLY)
			if (networkfilesbusy)		return false;
		Data = null;
		DataStr = data;
		networkfilesbusy = true;
		_LoadFile_path = path;
		_LoadFile_filename = filename;
		_SaveFile = true;
		return true;
	}

	public static bool SaveFile(string path, string filename, byte [] data)
	{
		if (!LOCALFILESONLY)
			if (networkfilesbusy)		return false;
		Data = data;
		DataStr = null;
		networkfilesbusy = true;
		_LoadFile_path = path;
		_LoadFile_filename = filename;
		_SaveFile = true;
		return true;
	}

	public static bool LoadFile(string path, string filename)
	{
		
		if (!LOCALFILESONLY)
			if (networkfilesbusy)		return false;
		//Debug.Log ("Loading : "+filename);
		networkfilesbusy = true;
		_LoadFile_path = path;
		_LoadFile_filename = filename;
		_LoadFile = true;
		return true;
	}

	public static bool DirFile()
	{
		if (!LOCALFILESONLY)
			if (networkfilesbusy)		return false;
		networkfilesbusy = true;
		_DirFile = true;
		_WaitForInternet = 0.0f;
		return true;
	}

	public static bool DirFileWaitConnected()
	{
		if (!LOCALFILESONLY)
			if (networkfilesbusy)		return false;
		networkfilesbusy = true;
		_DirFile = true;
		_WaitForInternet = Time.time + 3.0f;
		return true;
	}

	
	
	void Update ()
	{
		if (_LoadFile)
		{
			DebugGUI="loadfile";
			StopCoroutine("i_LoadFile");
			_LoadFile = false;
			StartCoroutine("i_LoadFile");
		}
		if (_SaveFile)
		{
			if (!_LoadFile){
				DebugGUI="Savefile";
			}
			StopCoroutine("i_SaveFile");
			_SaveFile = false;
			StartCoroutine("i_SaveFile");
		}
		if (_DirFile)
		{
			if (!_LoadFile && !_SaveFile){
				DebugGUI="DirFile";
			}

			StopCoroutine("i_DirFile");
			_DirFile = false;
			StartCoroutine("i_DirFile");
		}
	
	}

    private int SortBomName(string el1, string el2)
    {
        return el1.CompareTo(el2);
    }
        
	IEnumerator i_DirFile()
	{
		if (LOCALFILESONLY)
		{
			DebugGUI="debut load dir";
			localfilespath = Application.persistentDataPath + "/d2cfiles";

			string [] testlist = System.IO.Directory.GetDirectories(localfilespath);
			for (int i=0;i<testlist.Length;i++)
			{
				testlist[i] = testlist[i].Replace("\\","/");
//				Debug.Log ("FILE: <" + testlist[i] + ">");

				testlist[i] = testlist[i].Substring(testlist[i].LastIndexOf("/")+1);
			}

            Array.Sort(testlist, SortBomName);

			Filedir = testlist;
			networkfilesbusy = false;
			DebugGUI="dir loaded";
			yield break;
		}

		while (Time.time < _WaitForInternet)
		{
			yield return new WaitForSeconds(0.03f);
			if (network.internetconnected)
				break;
		}
		if (network.internetconnected)
		{
			network.GetFileDir();
			while (network.waitingnetwork)
				yield return new WaitForSeconds(0.03f);
			if (network.returntext.Substring(network.returntext.Length-2,1) == ";")
			{
				network.returntext = network.returntext.Substring(0,network.returntext.Length-2);
			}
			Filedir = network.returntext.Split(';');
		}
		else
		{
// get local file dir
			string [] testlist = System.IO.Directory.GetDirectories(localfilespath);
			for (int i=0;i<testlist.Length;i++)
			{
				testlist[i] = testlist[i].Substring(testlist[i].LastIndexOf("/")+1);
			}

            Array.Sort(testlist, SortBomName);

			Filedir = testlist;
		}
		networkfilesbusy = false;
//		for (int i=0;i<Filedir.Length;i++)
//			Debug.Log ("F:"+Filedir[i]);




	}

/*	void OnGUI(){
		//GUI.contentColor = Color.red;
		GUI.Label (new Rect (Screen.width/2, Screen.height/2+50, 500, 250), DebugGUI,styleLabel);
	}*/
	IEnumerator i_SaveFile()
	{
		string targetpath = localfilespath;
		byte [] data = null;
		int i_servertimedate = 0;
		double milisec;
		int savedate;

		if (LOCALFILESONLY)
		{
			localfilespath = Application.persistentDataPath + "/d2cfiles";
			targetpath = localfilespath;

			System.IO.Directory.CreateDirectory(targetpath);
			targetpath = targetpath + "/" + _LoadFile_path;
			System.IO.Directory.CreateDirectory(targetpath);
			targetpath = targetpath + "/" + _LoadFile_filename;
			Debug.Log ("path :€€€€€€€€€€€€  :" + targetpath);
			// Save locally with new date
			if (DataStr != null)	System.IO.File.WriteAllText(targetpath,DataStr);
			if (Data != null)		System.IO.File.WriteAllBytes(targetpath,Data);
			milisec = network.GetMiliseconds(DateTime.Now);
			milisec = milisec / 1000.0;
			savedate = (int)milisec;
			System.IO.File.WriteAllText(targetpath+"_date",savedate.ToString());

			networkfilesbusy = false;
			yield break;
		}

		System.IO.Directory.CreateDirectory(targetpath);
		targetpath = targetpath + "/" + _LoadFile_path;
		System.IO.Directory.CreateDirectory(targetpath);
		targetpath = targetpath + "/" + _LoadFile_filename;

// Save locally with new date
		if (DataStr != null)	System.IO.File.WriteAllText(targetpath,DataStr);
		if (Data != null)		System.IO.File.WriteAllBytes(targetpath,Data);
		milisec = network.GetMiliseconds(DateTime.Now);
		milisec = milisec / 1000.0;
		savedate = (int)milisec;
		System.IO.File.WriteAllText(targetpath+"_date",savedate.ToString());

		if (network.internetconnected)
		{
			network.GetFileDates(_LoadFile_path);
			while (network.waitingnetwork)
				yield return new WaitForSeconds(0.03f);
			
			string [] allfiles = network.returntext.Split(';');
			
			for (int i=0;i<allfiles.Length;i+=2)
			{
				if (allfiles[i] == _LoadFile_filename)
				{
					int.TryParse(allfiles[i+1],out i_servertimedate);
					break;
				}
			}
			if (i_servertimedate < savedate)
			{
				network.PutFile(_LoadFile_path,_LoadFile_filename);
				while (network.waitingnetwork)
					yield return new WaitForSeconds(0.03f);

				// internet connected and out new file -> upload
			}
		}
		networkfilesbusy = false;
	}



	IEnumerator i_LoadFile()
	{
		//Debug.Log ("*************local path***********" + localfilespath);
		string targetpath = localfilespath;
	
		byte [] data = null;
		int i_servertimedate = 0;

		if (LOCALFILESONLY)
		{
		
			targetpath = localfilespath;
			
			System.IO.Directory.CreateDirectory(targetpath);
			targetpath = targetpath + "/" + _LoadFile_path;
			System.IO.Directory.CreateDirectory(targetpath);
			targetpath = targetpath + "/" + _LoadFile_filename;
		


			if (FileExists(targetpath) /*&& FileExists(targetpath+"_date")*/)
			{
				/*string	localtimedate = System.IO.File.ReadAllText(targetpath+"_date");
				int		i_localtimedate;
				int.TryParse(localtimedate,out i_localtimedate);
				// check date
				if ((i_servertimedate == 0) || (i_servertimedate <= i_localtimedate))
				{*/
					data = System.IO.File.ReadAllBytes(targetpath);
				//}
			}

			Data = data;

			networkfilesbusy = false;
			yield break;
		}

		System.IO.Directory.CreateDirectory(targetpath);
		targetpath = targetpath + "/" + _LoadFile_path;
		System.IO.Directory.CreateDirectory(targetpath);
		targetpath = targetpath + "/" + _LoadFile_filename;

		Debug.Log ("Start Loading : "+targetpath);

		if (network.internetconnected)
		{
			network.GetFileDates(_LoadFile_path);
			while (network.waitingnetwork)
				yield return new WaitForSeconds(0.03f);

			string [] allfiles = network.returntext.Split(';');

			for (int i=0;i<allfiles.Length;i+=2)
			{
				if (allfiles[i] == _LoadFile_filename)
				{
					int.TryParse(allfiles[i+1],out i_servertimedate);
					break;
				}
			}
		}
		if (FileExists(targetpath) && FileExists(targetpath+"_date"))
		{
			string	localtimedate = System.IO.File.ReadAllText(targetpath+"_date");
			int		i_localtimedate;
			int.TryParse(localtimedate,out i_localtimedate);
// check date
			if ((i_servertimedate == 0) || (i_servertimedate <= i_localtimedate))
			{
				data = System.IO.File.ReadAllBytes(targetpath);
			}
		}

		if (i_servertimedate == 0)
		{
			// file does not exist
		}
		else
		{
			if (data == null)
			{
//				Debug.Log ("Load Server");
				// get online file and update versioning
//				Debug.Log("Get file "+_LoadFile_path+" "+_LoadFile_filename);
				network.GetFile(_LoadFile_path,_LoadFile_filename);
				while (network.waitingnetwork)
					yield return new WaitForSeconds(0.03f);
				if (network.returndata != null)
				{
//					Debug.Log ("Save local data");
					// save both files
					System.IO.File.WriteAllBytes(targetpath,network.returndata);
					System.IO.File.WriteAllText(targetpath+"_date",i_servertimedate.ToString());
				}
				data = network.returndata;
			}
		}
		Data = data;
//
		networkfilesbusy = false;
	}
}
