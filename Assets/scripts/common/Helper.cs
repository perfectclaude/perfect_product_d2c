using System.Linq;

using UnityEngine;

 

public static class Helper
{

    public static GameObject FindInChildren(this GameObject go, string name)
    {
		foreach (Transform child in go.GetComponentsInChildren<Transform>(true))
	    {
			if (child.gameObject.name == name)		return (child.gameObject);
//    	   Debug.Log (list.name);
    	}
		return null;
		/*
//		Debug.Log("***** Looking for a " + name + " in "+ go.name);
//		if (Helper.DEBUG) Debug.Log (go.name+ " find "+name);
        return (from x in go.GetComponentsInChildren<Transform>()

                where x.gameObject.name == name

                select x.gameObject).First();
		 */
    }
	

	public static void SetVisible(this GameObject go, bool bVisible)
	{
        Renderer[] renderers = go.GetComponentsInChildren<Renderer>();
		
		foreach (Renderer child in renderers)
		{
			child.enabled = bVisible;
		}
		
		if (go.GetComponent<Renderer>() != null)
		{
			go.GetComponent<Renderer>().enabled = bVisible;
		}
            
	}
	
	
	public static Quaternion QuaternionFromMatrix(Matrix4x4 m) 
	{
		// Adapted from: http://www.euclideanspace.com/maths/geometry/rotations/conversions/matrixToQuaternion/index.htm
		Quaternion q = new Quaternion();
		q.w = Mathf.Sqrt( Mathf.Max( 0, 1 + m[0,0] + m[1,1] + m[2,2] ) ) / 2; 
		q.x = Mathf.Sqrt( Mathf.Max( 0, 1 + m[0,0] - m[1,1] - m[2,2] ) ) / 2; 
		q.y = Mathf.Sqrt( Mathf.Max( 0, 1 - m[0,0] + m[1,1] - m[2,2] ) ) / 2; 
		q.z = Mathf.Sqrt( Mathf.Max( 0, 1 - m[0,0] - m[1,1] + m[2,2] ) ) / 2; 
		q.x *= Mathf.Sign( q.x * ( m[2,1] - m[1,2] ) );
		q.y *= Mathf.Sign( q.y * ( m[0,2] - m[2,0] ) );
		q.z *= Mathf.Sign( q.z * ( m[1,0] - m[0,1] ) );
		return q;
	}
	
	public static  float ElasticeaseOut (float t, float b, float c, float d, float a, float p)
	{
		if (t==0f) 		return b;  
		if ((t/=d)==1) 	return b+c;  
		if (p==0f) p=d*0.3f;
		float s = 0f;
		if ((a==0f) || a < Mathf.Abs(c)) 
		{ 
			a=c; 
			s=p/4; 
		}
		else
		{
			s = p/(2*Mathf.PI) * Mathf.Asin (c/a);
		}
		return (a*Mathf.Pow(2,-10*t) * Mathf.Sin( (t*d-s)*(2*Mathf.PI)/p ) + c + b);
	}
	
#if UNITY_EDITOR
	static public bool DEBUG = false;
	

	static public void DebugTrace(string debugString)
	{
		Debug.Log(debugString);
	}
	
#else
	static public bool DEBUG = false;
	//#define if (Helper.DEBUG) Debug.Log	
	static public void DebugTrace(string debugString)
	{
		
	}
	
#endif
}