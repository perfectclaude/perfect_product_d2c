using UnityEngine;
using System.Collections;

public class buttonswap : MonoBehaviour
{
	AudioSource audioSource;
	AudioClip soundbutton1;
	AudioClip soundbutton2;
	AudioClip soundbutton3;

	public static bool myactive = true;
	public Camera 		m_ViewCamera = null;
	public bool inputbusy = false;
	Vector3		MyScaler;
	Vector3		MyScalerTarget;
	float		Mymultiplier;
	bool		wasIpressed = false;
	public static bool anybuttonpressed = false;

	public GameObject	on;
	public GameObject	off;
	public bool soundon = true;
	public string defaultvalue = "";

	void Awake ()
	{
		_Awake ();
	}

	public void _Awake ()
	{
//		myactive = true;
		anybuttonpressed = false;
		audioSource = gameObject.AddComponent<AudioSource>();
		soundbutton1 =  Resources.Load("sfx_menu_clic_01") as AudioClip;
		soundbutton2 =  Resources.Load("sfx_menu_clic_02") as AudioClip;
		soundbutton3 =  Resources.Load("sfx_menu_clic_03") as AudioClip;
		MyScaler = gameObject.transform.localScale;
		ButtonCamera ();
		BoxCollider col = GetComponent<BoxCollider>();
		if (col == null)
			gameObject.AddComponent<BoxCollider>();
		on = gameObject.FindInChildren("on");
		off = gameObject.FindInChildren("off");
		ButtonShowNormal ();
	}
	
	void OnEnable()
	{
		_OnEnable();
	}

	public void _OnEnable()
	{
		ButtonShowNormal ();
		OnDisable();
	}

	void Update()
	{
		_Update();
	}

	public void _Update ()
	{
		if (!gameObject.name.Contains("override"))
		{
			if (!myactive)		return;
			
			if (bufferme.HasKey("popupdropdownison"))
			{
				if (!gameObject.name.Contains("statisticsline"))
					return;
			}
		}

		//		if (SNAILRUSHDATEPICKER.DatePicker)		return;
		//		if (yesno.YesNo)		return;
		RaycastHit hitInfo;
		Ray ray;
		
		ray = m_ViewCamera.ScreenPointToRay (Input.mousePosition);
		if (Input.GetMouseButtonDown (0))
		{
//			Debug.Log ("Mybutton:"+gameObject.name);
			if (gameObject.GetComponent<Collider> ().Raycast (ray, out hitInfo, 1.0e8f)) {
				
				// Debug.Log ("Hittin:"+gameObject.name);
				wasIpressed = true;
				anybuttonpressed = true;
			}
		}
		#if UNITY_IPHONE || UNITY_ANDROID
		#else
		if (!wasIpressed) {
			if (gameObject.GetComponent<Collider> ().Raycast (ray, out hitInfo, 1.0e8f))
				ButtonShowRollover ();
			else
				ButtonShowNormal ();
		}
		#endif
		if (Input.GetMouseButtonUp (0)) {
			if ((wasIpressed) && (!inputbusy) && (gameObject.GetComponent<Collider> ().Raycast (ray, out hitInfo, 1.0e8f))) {
				MyScalerTarget.x = MyScaler.x * 1.3f;
				MyScalerTarget.y = MyScaler.y * 1.3f;
				MyScalerTarget.z = MyScaler.z * 1.3f;
				Mymultiplier = 1.3f;
				gameObject.transform.localScale = MyScalerTarget;
				
				StartCoroutine ("Scaleback");
				if (soundon) {
					if (audioSource != null) {
						switch (Random.Range (0, 3)) {
						case 0:
							audioSource.clip = soundbutton1;
							break;
						case 1:
							audioSource.clip = soundbutton2;
							break;
						case 2:
							audioSource.clip = soundbutton3;
							break;
						}
						audioSource.Play ();	
					}
				}
			}
			wasIpressed = false;
			anybuttonpressed = false;
		}
		#if (!UNITY_EDITOR) && (UNITY_IPHONE || UNITY_ANDROID)
		#else
		else {
			if (!wasIpressed) {
				if (gameObject.GetComponent<Collider> ().Raycast (ray, out hitInfo, 1.0e8f))
					ButtonShowRollover ();
				else
					ButtonShowNormal ();
			}
		}
		#endif
	}
	
	IEnumerator Scaleback()
	{
		while (Mymultiplier > 1.0f)
		{
			yield return new WaitForSeconds(0.03f);
			Mymultiplier -= 0.04f;
			if (Mymultiplier < 1.0f)			Mymultiplier = 1.0f;
			MyScalerTarget.x = MyScaler.x * Mymultiplier;
			MyScalerTarget.y = MyScaler.y * Mymultiplier;
			MyScalerTarget.z = MyScaler.z * Mymultiplier;
			gameObject.transform.localScale = MyScalerTarget;
		}
		ButtonPressed();
	}

	public virtual void ButtonPressed()
	{
	}

	public virtual void ButtonCamera()
	{
		ButtonSetCamera("Main Camera");
	}

	public virtual void ButtonShowRollover()
	{
		#if (!UNITY_EDITOR) && (UNITY_IPHONE || UNITY_ANDROID)
		#else
		if (on != null)		on.SetActive (true);
		if (off != null)	off.SetActive (false);
		#endif
	}

	public virtual void ButtonShowNormal()
	{
		if (on != null)		on.SetActive (false);
		if (off != null)	off.SetActive (true);
	}
	
	public void ButtonSetCamera(string cameraname)
	{
		if (m_ViewCamera == null)
		{
			m_ViewCamera = GameObject.Find(cameraname).GetComponent<Camera>();
		}
	}

	public void ForceSize()
	{
		Mymultiplier = 1.0f;
		MyScalerTarget.x = MyScaler.x * Mymultiplier;
		MyScalerTarget.y = MyScaler.y * Mymultiplier;
		MyScalerTarget.z = MyScaler.z * Mymultiplier;
		gameObject.transform.localScale = MyScalerTarget;
	}

	void OnDisable()
	{
		_OnDisable();
	}

	public void _OnDisable()
	{
		ForceSize();
		if (audioSource != null)
			audioSource.Stop();	
	}

}


