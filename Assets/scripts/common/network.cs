﻿using UnityEngine;
using System;
using MiniJSON;
using System.Text;
using System.Collections;
//using System.Collections.Generic;
using System.Net;
using System.IO;
using System.Text.RegularExpressions;
using BestHTTP;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.Security;
using System.Threading;

public class network : MonoBehaviour
{
	public static string WSUrl = "http://www.piserver.fr/api/";

	static bool _GetFileDir = false;
	static bool _GetFileDates = false;
	static string _GetFileDatesPath = "";

	static bool _GetFile = false;
	static string _GetFileFilename = "";

	static bool _PutFile = false;
	public static bool waitingnetwork = false;
	public static string returntext;
	public static byte [] returndata;

	public static bool internetconnected = false;

	public delegate void MyDelegate();

	protected static MyDelegate getfiledircallback = null;
	protected static MyDelegate getfiledatescallback = null;
	protected static MyDelegate getfilecallback = null;

	int		pausecounter = 0;
	byte [] data;
	// call :
//	
//	network.GetFileDates("bras");
//	
//	


	void Awake()
	{
		StartCoroutine("CheckInternet");
//		network.GetFileDir();
//		network.GetFileDates("bras");
//		network.GetFile("bras","bras_obj.txt");
//		network.PutFile("bras","bras_test.txt");
	}
	
	void Start ()
	{
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
	}

	void OnApplicationPause(bool pauseStatus)
	{
		if (!pauseStatus)
		{
			if (pausecounter != 0)
			{
				StopAllCoroutines();
				waitingnetwork = false;
			}
			pausecounter++;
		}
	}

/// <summary>
/// Gets the file dir.
/// </summary>
/// <returns><c>true</c>, if file dir was gotten, <c>false</c> otherwise.</returns>
/// <param name="callback">Callback.</param>
	public static bool GetFileDir(MyDelegate callback=null)			//
	{
		if (waitingnetwork)	return false;
		returntext = null;
		returndata = null;
		waitingnetwork = true;
		getfiledircallback = callback;
		_GetFileDir = true;
		return true;
	}

/// <summary>
/// Gets the file dates.
/// </summary>
/// <returns><c>true</c>, if file dates was gotten, <c>false</c> otherwise.</returns>
/// <param name="pathname">Pathname.</param>
/// <param name="callback">Callback.</param>
	public static bool GetFileDates(string pathname,MyDelegate callback=null)
	{
		if (waitingnetwork)	return false;
		returntext = null;
		returndata = null;
		waitingnetwork = true;
		getfiledatescallback = callback;
		_GetFileDatesPath = pathname;
		_GetFileDates = true;
		return true;
	}

/// <summary>
/// Gets the file.
/// </summary>
/// <returns><c>true</c>, if file was gotten, <c>false</c> otherwise.</returns>
/// <param name="pathname">Pathname.</param>
/// <param name="callback">Callback.</param>
	public static bool GetFile(string pathname,string filename, MyDelegate callback=null)
	{
		if (waitingnetwork)	return false;
		returntext = null;
		returndata = null;
		waitingnetwork = true;
		getfilecallback = callback;
		_GetFileDatesPath = pathname;
		_GetFileFilename = filename;
		_GetFile = true;
		return true;
	}

/// <summary>
/// Puts the file.
/// </summary>
/// <returns><c>true</c>, if file was put, <c>false</c> otherwise.</returns>
/// <param name="pathname">Pathname.</param>
/// <param name="data">Data.</param>
	public static bool PutFile(string pathname,string filename)
	{
		if (waitingnetwork)	return false;
		returntext = null;
		returndata = null;
		waitingnetwork = true;
		_GetFileDatesPath = pathname;
		_GetFileFilename = filename;
		_PutFile = true;
		return true;
	}

	bool waitinternettest = false;
	void OnRequestFinished(HTTPRequest request, HTTPResponse response)
	{
		if (response.IsSuccess)
		{
			internetconnected = true;
		}
		else
		{
			internetconnected = false;
		}
		waitinternettest = false;
	}

	IEnumerator CheckInternet()
	{
		while (true)
		{
			waitinternettest = true;
			HTTPRequest request = new HTTPRequest(new Uri("http://www.google.com"),HTTPMethods.Head,OnRequestFinished);
			request.Send();
			yield return new WaitForSeconds(0.5f);
			while (waitinternettest)
				yield return new WaitForSeconds(0.1f);
		}
	}


	void Update ()
	{
		//transform.GetComponent<Camera>().Render();
		if (_GetFileDir)
		{
			_GetFileDir = false;
			string	mymethod = "perfectproduct/dir.php";
			HTTPRequest request = new HTTPRequest(new Uri(WSUrl+mymethod), HTTPMethods.Get, i_GetFileDirFinished);
			request.Send();
		}
		if (_GetFileDates)
		{
			_GetFileDates = false;
			string	mymethod = "perfectproduct/getdate.php";
			HTTPRequest request = new HTTPRequest(new Uri(WSUrl+mymethod+"?nom="+_GetFileDatesPath), HTTPMethods.Get, i_GetFileDatesFinished);
//			request.AddField("path",_GetFileDatesPath);
			request.Send();
		}
		if (_GetFile)
		{
			_GetFile = false;
			i_GetFile();
		}
		if (_PutFile)
		{
			_PutFile = false;
			string	mymethod = "perfectproduct/putfile.php";

			string	filename =  Application.persistentDataPath + "/d2cfiles/" + _GetFileDatesPath + "/" + _GetFileFilename;

			Debug.Log (WSUrl+mymethod+"?nom="+_GetFileDatesPath+"&nomFichier="+_GetFileFilename);

			HTTPRequest request = new HTTPRequest(new Uri(WSUrl+mymethod+"?nom="+_GetFileDatesPath+"&nomFichier="+_GetFileFilename), HTTPMethods.Post, i_PutFileFinished);
//			Debug.Log (filename);
			string mytext = System.IO.File.ReadAllText(filename);
//			Debug.Log (mytext);
			request.AddBinaryData("text",Encoding.UTF8.GetBytes(mytext),"temp.txt");
//			request.UploadStream = new FileStream(filename, FileMode.Open);
//			request.OnProgress = OnUploadProgress;
			request.Send();
		}
	}

	void i_PutFileFinished(HTTPRequest request, HTTPResponse response)
	{
		if (response.IsSuccess)
		{

//			Debug.Log ("Put done");
//			Debug.Log ("["+response.DataAsText+"]");
		}
		else
		{
		}
//		Debug.Log ("Put ended");

		data = null;
		waitingnetwork = false;
	}


	void i_GetFileDirFinished(HTTPRequest request, HTTPResponse response)
	{
		if (response.IsSuccess)
		{
			returntext = response.DataAsText;
			if (getfiledircallback != null)
				getfiledircallback();
		}
		else
		{
		}
		waitingnetwork = false;
	}

	void i_GetFileDatesFinished(HTTPRequest request, HTTPResponse response)
	{
		if (response.IsSuccess)
		{
			returntext = response.DataAsText;
//			Debug.Log (response.DataAsText);
			if (getfiledatescallback != null)
				getfiledatescallback();
		}
		else
		{
		}
		waitingnetwork = false;
	}

	void OnDownloadProgress(HTTPRequest request, int downloaded, int length)
	{
		float progressPercent = (downloaded / (float)length) * 100.0f;
		int percent = (int)progressPercent;
//		Debug.Log (percent + "%");		
	}

	void i_GetFile()
	{
//		_GetFileDatesPath;
//		_GetFileFilename;
		string	mymethod = "perfectproduct/getfile.php";

		string	filename =  Application.persistentDataPath + "/" + _GetFileDatesPath;
		System.IO.Directory.CreateDirectory(filename);

		string	targetfile = filename + "/" + _GetFileFilename;
		filename = targetfile + "_tmp";
		System.IO.File.Delete(filename);

		Debug.Log ("REQUEST:"+WSUrl+mymethod+"?nom="+_GetFileDatesPath+"&nomFichier="+_GetFileFilename);
		HTTPRequest request = new HTTPRequest(new Uri(WSUrl+mymethod+"?nom="+_GetFileDatesPath+"&nomFichier="+_GetFileFilename), (req, resp) =>
		{
			List<byte[]> fragments = resp.GetStreamedFragments();
			// Write out the downloaded data to a file:
			using (FileStream fs = new FileStream(filename, FileMode.Append))
			{
				if (fragments != null)
				{
					if (fragments.Count > 0)
					{
						foreach(byte[] data in fragments)
							fs.Write(data, 0, data.Length);
					}
				}
			}
			if (!resp.IsSuccess)
			{
				Debug.Log ("FAILED STREAMING");
				waitingnetwork = false;
			}
			if (resp.IsStreamingFinished)
			{
				File.Move(filename,targetfile);
				returndata = File.ReadAllBytes(targetfile);
				File.Delete(targetfile);

				if (getfilecallback != null)
					getfilecallback();
				waitingnetwork = false;
			}
		});

		request.OnProgress = OnDownloadProgress;
		request.UseStreaming = true;
		request.StreamFragmentSize = 1 * 1024 * 1024; // 1 megabyte
		request.DisableCache = true; // already saving to a file, so turn off caching
		request.Send();
	}



/// <summary>
/// Gets the miliseconds.
/// </summary>
/// <returns>The miliseconds.</returns>
/// <param name="mytime">Mytime.</param>
	public static double GetMiliseconds(DateTime mytime)
	{
		DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, 0).ToLocalTime();
		TimeSpan span = (mytime - epoch);
		return span.TotalMilliseconds;
	}

	public static DateTime GetDateTime(double epoch)
	{
		DateTime display = new DateTime(1970, 1, 1,0,0,0,0); //.ToLocalTime();
		epoch = epoch / 1000;
		display = display.AddSeconds(epoch);
		return(display);
	}

}
