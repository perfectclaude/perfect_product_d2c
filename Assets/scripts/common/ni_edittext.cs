﻿#if (UNITY_IPHONE || UNITY_ANDROID || UNITY_WP8)
#define TOUCH_SCREEN_KEYBOARD
#endif

using UnityEngine;
using System.Collections;

public class ni_edittextPopUp : buttonPopup
{
	public static bool scrollingup = false;
	public static bool locked = false;
	public static bool allinputbusy = false;
	#if TOUCH_SCREEN_KEYBOARD
	private TouchScreenKeyboard keyboard = null;
	#endif

	int cursorposition = 0;
	Vector3 testmousepos;
	string initialtext = "";
	float	startypos;

	void Awake()
	{
		_Awake ();
		soundon = false;
		tk2dTextMesh tm = (tk2dTextMesh)this.gameObject.GetComponent<tk2dTextMesh> ();
		initialtext = tm.text;
	}


	public override void ButtonPressed ()
	{
		tk2dTextMesh tm = (tk2dTextMesh)this.gameObject.GetComponent<tk2dTextMesh> ();

		if (tm.text == initialtext)
		{
			settext.ForceText(tm,defaultvalue);
		}
//		if (ni_admin_edit_datafields.wasIpressed)		return;
		base.ButtonPressed();

		if ((Mathf.Abs(testmousepos.x - Input.mousePosition.x) > 5) || (Mathf.Abs(testmousepos.y - Input.mousePosition.y) > 5))
			return;		// don't button edit !!!
		if (locked)
			return;

		if (!inputbusy)
		{
			cursorposition = tm.text.Length;
			#if UNITY_EDITOR || (!TOUCH_SCREEN_KEYBOARD)
			tm.text = tm.text.Replace("|","") + "|";
#endif
			tm.maxChars = tm.text.Length;
			tm.Commit ();
		}
		inputbusy = true;
		allinputbusy = true;
		StartCoroutine("Blink");
		// Scroll to show item

		#if TOUCH_SCREEN_KEYBOARD
		if (Application.platform != RuntimePlatform.WindowsEditor &&
		    Application.platform != RuntimePlatform.OSXEditor)
		{
			StartCoroutine("ScrollUp");
			
			#if UNITY_ANDROID //due to a delete key bug in Unity Android
			TouchScreenKeyboard.hideInput = false;
			#else
			TouchScreenKeyboard.hideInput = true;
			#endif
			keyboard = TouchScreenKeyboard.Open(tm.text, TouchScreenKeyboardType.Default, true, true, false, false);
			TouchScreenKeyboard.hideInput = false;
			StartCoroutine(TouchScreenKeyboardLoop());
		}
		#endif
	}

	#if TOUCH_SCREEN_KEYBOARD
	void OnDisable()
	{
		Vector3 vec;
		vec = m_ViewCamera.transform.position;
		vec.y = startypos;
		m_ViewCamera.transform.position = vec;
	}

	IEnumerator ScrollUp()
	{
		Vector3 onscreen = m_ViewCamera.WorldToScreenPoint(gameObject.transform.position);
		startypos = m_ViewCamera.transform.position.y;
		Vector3 vec;
		scrollingup = true;
		while (inputbusy && (onscreen.y < Screen.height*4.0f/5.0f))
		{
			yield return new WaitForSeconds(0.03f);
			vec = m_ViewCamera.transform.position;
			vec.y -= 0.1f;
			m_ViewCamera.transform.position = vec;
			onscreen = m_ViewCamera.WorldToScreenPoint(gameObject.transform.position);
		}

		while (inputbusy)
		{
			yield return new WaitForSeconds(0.3f);
		}

		while (m_ViewCamera.transform.position.y < startypos)
		{
			vec = m_ViewCamera.transform.position;
			vec.y += 0.1f;
			m_ViewCamera.transform.position = vec;

			yield return new WaitForSeconds(0.03f);
		}
		scrollingup = false;
	}

	
	private IEnumerator TouchScreenKeyboardLoop()
	{
		tk2dTextMesh tm = (tk2dTextMesh)this.gameObject.GetComponent<tk2dTextMesh> ();
		while (keyboard != null && !keyboard.done && keyboard.active)
		{
			tm.text = keyboard.text;
			tm.maxChars = tm.text.Length;
			tm.Commit ();
			yield return null;
		}
		
		if (keyboard != null)
		{
			tm.text = keyboard.text;
		}

		if ((keyboard != null) && (!keyboard.active))
			InputCanceled();
		if ((keyboard != null) && (keyboard.done))
			InputCanceled();
	}
	#endif

	IEnumerator Blink()
	{
		tk2dTextMesh tm = (tk2dTextMesh)this.gameObject.GetComponent<tk2dTextMesh> ();
		while (inputbusy)
		{
			tm.color = Color.black;
			tm.Commit();
			yield return new WaitForSeconds(0.3f);
			tm.color = Color.gray;
			tm.Commit();
			yield return new WaitForSeconds(0.1f);
		}
		tm.color = Color.black;
		tm.Commit();
	}

	void InputCanceled()
	{
		tk2dTextMesh tm = (tk2dTextMesh)this.gameObject.GetComponent<tk2dTextMesh> ();
		inputbusy = false;
		allinputbusy = false;
		tm.text = tm.text.Replace("|","");
		tm.maxChars = tm.text.Length;
		tm.Commit ();
		#if TOUCH_SCREEN_KEYBOARD

		if (keyboard!=null)
		{
			if (!keyboard.done)
			{
				keyboard.active = false;
			}
		}
		keyboard = null;
		#endif
	}

	void Update()
	{
		base._Update ();
		tk2dTextMesh tm = (tk2dTextMesh)this.gameObject.GetComponent<tk2dTextMesh> ();

		RaycastHit hitInfo;
		Ray ray;
		
		ray = base.m_ViewCamera.ScreenPointToRay(Input.mousePosition);
		if (!inputbusy)
		{
			if (Input.GetMouseButtonDown(0))
				testmousepos = Input.mousePosition;
			if (Input.GetMouseButtonUp(0))
			{
				if ((Mathf.Abs(testmousepos.x - Input.mousePosition.x) > 5) || (Mathf.Abs(testmousepos.y - Input.mousePosition.y) > 5))
				{
					StopAllCoroutines();
					ForceSize();
				}
				if (locked)
				{
					StopAllCoroutines();
					ForceSize();
				}
			}
		}

		if (inputbusy)
		{
			if (Input.GetMouseButtonDown(0))
			{
				if (!gameObject.GetComponent<Collider>().Raycast(ray, out hitInfo, 1.0e8f))
					InputCanceled();
				else
				{
					// place cursor
					Vector3 vec = m_ViewCamera.ScreenToWorldPoint(Input.mousePosition);
					int placecursorhere = TextTest(this.gameObject,vec);
					if (placecursorhere != -1)
					{
						#if UNITY_EDITOR || (!TOUCH_SCREEN_KEYBOARD)

						cursorposition = placecursorhere;
						tm.text = tm.text.Replace("|","");
						tm.text = tm.text.Substring(0,cursorposition) + "|" + tm.text.Substring(cursorposition,tm.text.Length-cursorposition);
						tm.maxChars = tm.text.Length;
						tm.Commit ();
#endif
					}
				}
			}

#if UNITY_EDITOR || (!TOUCH_SCREEN_KEYBOARD)
			foreach (char c in Input.inputString)
			{
				if (Input.GetKey(KeyCode.Backspace))
				{
					if (cursorposition > 0)
					{
						tm.text = tm.text.Remove(cursorposition-1,1);
						cursorposition--;
//						if (tm.text.Length == 1)	tm.text = "";
//						if (tm.text.Length > 1)		tm.text = tm.text.Substring(0,tm.text.Length-1);
//						tm.text = tm.text + "|";
					}
				}
				else
				{
					if (Input.GetKey(KeyCode.KeypadEnter) || Input.GetKey (KeyCode.Return)) 
					{
						InputCanceled();
					}
					else
					{
						if (Input.GetKey(KeyCode.LeftArrow)) 
						{
							if (cursorposition > 0)	
							{
								cursorposition--;
								tm.text = tm.text.Replace("|","");
								tm.text = tm.text.Substring(0,cursorposition) + "|" + tm.text.Substring(cursorposition,tm.text.Length-cursorposition);
							}
						}
						else
						{
							if (Input.GetKey(KeyCode.RightArrow)) 
							{
								if (cursorposition < tm.text.Length-1)
								{
									cursorposition++;
									tm.text = tm.text.Replace("|","");
									tm.text = tm.text.Substring(0,cursorposition) + "|" + tm.text.Substring(cursorposition,tm.text.Length-cursorposition);
								}
							}
							else
							{
								tm.text = tm.text.Substring(0,cursorposition) + c + tm.text.Substring(cursorposition,tm.text.Length-cursorposition);
								cursorposition++;
							}
						}
					}
				}
				tm.maxChars = tm.text.Length;
			}
			#endif
		}
	}

	public static bool PointInTriangle2(Vector3 test,Vector3 A,Vector3 B,Vector3 C)
	{
		float x1 = 10000.0f;
		float x2 = -10000.0f;
		float y1 = 10000.0f;
		float y2 = -10000.0f;
		
		if (A.x < x1)		x1 = A.x;
		if (A.x > x2)		x2 = A.x;
		if (B.x < x1)		x1 = B.x;
		if (B.x > x2)		x2 = B.x;
		if (C.x < x1)		x1 = C.x;
		if (C.x > x2)		x2 = C.x;
		
		if (A.y < y1)		y1 = A.y;
		if (A.y > y2)		y2 = A.y;
		if (B.y < y1)		y1 = B.y;
		if (B.y > y2)		y2 = B.y;
		if (C.y < y1)		y1 = C.y;
		if (C.y > y2)		y2 = C.y;
		
		if ((test.x >= x1) && (test.x <= x2) && (test.y >= y1) && (test.y <= y2))
			return true;
		return false;
	}
	
	int TextTest(GameObject obj,Vector3 mouseposition)
	{
		tk2dTextMesh tm = (tk2dTextMesh)obj.GetComponent<tk2dTextMesh>();
		Mesh me = tm.GetFontMesh();
		Vector3 [] myvectors = new Vector3[me.vertexCount];
		int [] tris = me.GetTriangles(0);
		
		for (int i=0;i<me.vertexCount;i++)
			myvectors[i] = obj.transform.position + me.vertices[i];
		
		for (int i=0;i<tris.Length;i+=3)
		{
			if (PointInTriangle2(mouseposition,myvectors[tris[i+0]],myvectors[tris[i+2]],myvectors[tris[i+1]]))
			{
				int n = i / 6;
				return (n);
				//				Debug.Log ("Point in triangle :" + tm.text.Substring(n,1));
				//				break;
			}
		}
		return (-1);
	}
	
}