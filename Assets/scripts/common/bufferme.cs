﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using System;
using BestHTTP;

// dbase : padexpos_perfect
// padexpos_perfect
// KafVgdH^zOh4

public class bufferme : MonoBehaviour
{
	public static string clientid =  	"0CC68B2D-D434_";
	public static string clientkey = "CC68B2D-D434-4EDXX";

	public static string encryptionkey = "";
	/*
	public static string RequestUID()
	{
		#if UNITY_EDITOR
		return ("0CC68B2D-D434-4ED6-91C4-9A69F0F6630A");
		#else
		return(SystemInfo.deviceUniqueIdentifier);
		#endif
	}
	*/

	public static string GetClientId()
	{
		string newclient = clientid.ToLower();
		newclient = newclient.Replace("-","_");
		return(newclient);
	}

	void Awake()
	{
		encryptionkey = clientkey + clientid;
		if (!HasKey("bufferme_start"))			SetInt("bufferme_start",0);
		if (!HasKey("bufferme_end"))			SetInt("bufferme_end",0);
		StartCoroutine(CheckInternetconnection());
	}

// Buffer me here
	// --------------------------------------
	public static void AddToBuffer(string json, string datetime, string specialclient,string keywords="")
	{
//		Debug.Log ("Adding:"+json);
		int		index = GetInt("bufferme_end");
		if (BufferEmpty())
		{
			index = 0;
			SetInt("bufferme_start",0);
		}
		SetString("bufferme"+index,json);	
		SetString("buffermedt"+index,datetime);	
		SetString("bufferclient"+index,specialclient+GetClientId());
		SetString("buffermekeywords"+index,keywords);
		index ++;
		SetInt("bufferme_end",index);
	}
	
	// --------------------------------------
	static bool BufferEmpty()
	{
		if (GetInt("bufferme_start") == GetInt("bufferme_end"))		return(true);
		return (false);
	}
	
	// --------------------------------------
	IEnumerator	 CheckInternetconnection()
	{
			yield return new WaitForSeconds(2.0f);
	}
	
	



	public static string GetString(string key)
	{
		string newkey = Encrypt(key);
		string decrypt = PlayerPrefs.GetString(newkey, "");
		if (decrypt == null || decrypt == "") return "";
		return (Decrypt(decrypt));
	}
	public static void SetString(string key,string value)
	{
		string newkey = Encrypt(key);
		string newvalue = Encrypt(value);
		PlayerPrefs.SetString(newkey,newvalue);
		PlayerPrefs.Save();
	}
	public static bool HasKey(string key)
	{
		string newkey = Encrypt(key);
		return(PlayerPrefs.HasKey(newkey));
	}
	public static void    DeleteKeyQuick(string key)
	{
		string newkey = Encrypt(key);
		PlayerPrefs.DeleteKey(newkey);
	}
	public static void    DeleteKey(string key)
	{
		string newkey = Encrypt(key);
		PlayerPrefs.DeleteKey(newkey);
		PlayerPrefs.Save();
	}
	public static int GetInt(string key, int i = 0)
	{
		string newkey = Encrypt(key);
		if (PlayerPrefs.HasKey(newkey)) return (PlayerPrefs.GetInt(newkey));
		
		return i;   
	}
	public static void SetInt(string key,int value)
	{
		string newkey = Encrypt(key);
		PlayerPrefs.SetInt(newkey,value);
		PlayerPrefs.Save();
	}
	public static float GetFloat(string key)
	{
		string newkey = Encrypt(key);
		return (PlayerPrefs.GetFloat(newkey));
	}
	public static void SetFloat(string key,float value)
	{
		string newkey = Encrypt(key);
		PlayerPrefs.SetFloat(newkey,value);
		PlayerPrefs.Save();
	}
	public static void Save() 
	{
		PlayerPrefs.Save();
	}

	public static string Encrypt (string toEncrypt)
	{
		string createkey = clientkey + clientid;
		string	mykey = createkey;
		if (createkey.Length > 32)		mykey = createkey.Substring(0,32);
		
		 byte[] keyArray = UTF8Encoding.UTF8.GetBytes (mykey);
		 byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes (toEncrypt);
		
 		 RijndaelManaged rDel = new RijndaelManaged ();
 		 rDel.Key = keyArray;
 		 rDel.Mode = CipherMode.ECB;
		 rDel.Padding = PaddingMode.PKCS7;
 // better lang support
		ICryptoTransform cTransform = rDel.CreateEncryptor ();
		byte[] resultArray = cTransform.TransformFinalBlock (toEncryptArray, 0, toEncryptArray.Length);
		return Convert.ToBase64String (resultArray, 0, resultArray.Length);
	}
 
	public static string Decrypt (string toDecrypt)
	{
		string createkey = clientkey + clientid;
		string	mykey = createkey;
		if (createkey.Length > 32)		mykey = createkey.Substring(0,32);

		byte[] keyArray = UTF8Encoding.UTF8.GetBytes (mykey);
		byte[] toEncryptArray = Convert.FromBase64String (toDecrypt);
		RijndaelManaged rDel = new RijndaelManaged ();
		rDel.Key = keyArray;
		rDel.Mode = CipherMode.ECB;
		rDel.Padding = PaddingMode.PKCS7;

		ICryptoTransform cTransform = rDel.CreateDecryptor ();
		byte[] resultArray = cTransform.TransformFinalBlock (toEncryptArray, 0, toEncryptArray.Length);
		return UTF8Encoding.UTF8.GetString (resultArray);

	}

	public static void LoadLevel(string name)
	{
		GameObject obj = GameObject.Find ("ObjectManager");
		Destroy (obj);
		Application.LoadLevel(name);
	}


}
