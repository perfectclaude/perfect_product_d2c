using UnityEngine;
using System.Collections;

public class adaptNW : MonoBehaviour
{
	Camera 	m_ViewCamera;

	void ResetPosition ()
	{
//		m_ViewCamera = GameObject.Find("Main Camera").GetComponent<Camera>();

		Vector3 	vec;
		Vector3	res;
		
		vec.x = 0;
		vec.y = Screen.height;
		vec.z = 0;
		res = m_ViewCamera.ScreenToWorldPoint (vec);
		res.z = gameObject.transform.position.z;
		gameObject.transform.position = res;
	}

	void Awake ()
	{
		m_ViewCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
		ResetPosition();
	}
	
	void Update ()
	{
		if (!ni_edittext.scrollingup)
			ResetPosition();
	}
}
