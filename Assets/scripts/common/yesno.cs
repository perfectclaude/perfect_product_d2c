﻿using UnityEngine;
using System.Collections;

public class yesno : MonoBehaviour
{
	public static bool YesNo = false;
	public static string phrase = "Temporaryphrase";
	public static bool YesNoDecision = false;
	public GUISkin skin;
	public Texture2D greytext;
	Rect fullscreen;
	Rect Rectwindow;
	Rect RectTitle;
	Rect RectText;
	Rect RectYes;
	Rect RectNo;

	void Awake ()
	{
		fullscreen = new Rect(0,0,Screen.width, Screen.height);
		Rectwindow = new Rect(Screen.width/4,Screen.height/4,Screen.width/2,Screen.height/2);
		RectTitle = Rectwindow;
		RectTitle.height = Rectwindow.height / 5;

		RectText = Rectwindow;
		RectText.height = Rectwindow.height*2 / 5;
		RectText.y = Rectwindow.height / 5;

		RectYes = Rectwindow;
		RectYes.y = Rectwindow.height*3 / 5;
		RectYes.height = Rectwindow.height / 5;
		RectYes.width = Rectwindow.width/2;

		RectNo = Rectwindow;
		RectNo.x = Rectwindow.x + Rectwindow.width/2;
		RectNo.y = Rectwindow.height*3 / 5;
		RectNo.height = Rectwindow.height / 5;
		RectNo.width = Rectwindow.width/2;


	}
	
	void Update ()
	{
	
	}

	void OnGUI()
	{
		if (YesNo)
		{
			GUI.DrawTexture(fullscreen,greytext);
			GUI.skin=skin;

			var centeredStyle = GUI.skin.GetStyle("Label");
			centeredStyle.alignment = TextAnchor.UpperCenter;

			GUI.Label(RectTitle,"SELECT",centeredStyle);
			GUI.Label(RectText,phrase,centeredStyle);
			
			if (GUI.Button(RectYes,"YES",centeredStyle))
			{
				YesNoDecision = true;
				YesNo = false;
			}
			if (GUI.Button(RectNo,"NO",centeredStyle))
			{
				YesNoDecision = false;
				YesNo = false;
			}
		}
	}

}
