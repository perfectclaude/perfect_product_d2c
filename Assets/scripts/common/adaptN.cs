using UnityEngine;
using System.Collections;

public class adaptN : MonoBehaviour
{
	Camera 	m_ViewCamera;

	void ResetPosition ()
	{
		Vector3 	vec;
		Vector3	res;
		
		vec.x = Screen.width/2;
		vec.y = Screen.height;
		vec.z = 0;
		res = m_ViewCamera.ScreenToWorldPoint (vec);
		res.z = gameObject.transform.position.z;
		gameObject.transform.position = res;
	}

	void Awake ()
	{
		m_ViewCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
		ResetPosition();
	}
	
	void Update ()
	{
		if (!ni_edittext.scrollingup)
			ResetPosition();
	}
}
