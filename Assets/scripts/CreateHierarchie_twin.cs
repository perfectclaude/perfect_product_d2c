﻿using UnityEngine;
using System.Collections;
using System.Text;
using System;

public class CreateHierarchie_twin : MonoBehaviour // loadcsv
{
	public static bool chang = false;
	public static string objetActuel;
	static public int posDeb;
	// Use this for initialization

	Transform attachparent = null;

	void Awake()
	{
		attachparent = (Transform)GameObject.Find ("interface").FindInChildren("righttop").FindInChildren("scrollwindow").transform;
	}
	void Start ()
	{
		posDeb = 0;
//		chang = true;
//		objetActuel="RPC.2028.20";
	}
	
	// Update is called once per frame
	void Update ()
	{
		/*
		if (GameObject.Find ("rightwindow") && chang && loadcsv.initHierarchie) {
			

			int getind = loadcsv.getIndName (objetActuel);
			GameObject.Find("topprice").transform.GetComponent<prixObjetActuel>().setText(objetActuel,loadcsv.composants [getind][9]);
			foreach (Transform child in attachparent)
			{
				Destroy (child.gameObject);
			}
//			Debug.Log ("NAME:"+objetActuel);
			if(!goToFonctionnality.fon){
			treenode actuel = csv.getTree (objetActuel, csv.roothierarchie);
			int i = 0;
			char letter = 'A';
			int nombreActuel = 1;
			if (csv.possedeFils (actuel)) {
				bool blue=true;

				foreach (treenode fils in actuel.childs) {
					GameObject nouvBouton = Instantiate (Resources.Load ("buttonHierarchie")) as GameObject;
					nouvBouton.transform.parent = attachparent; //transform; 

					if (csv.possedeFils (fils)) {

						nouvBouton.transform.GetChild (0).GetComponent<tk2dTextMesh> ().text =fils.name;
						nouvBouton.transform.GetChild (2).GetComponent<tk2dTextMesh> ().text =letter + "-";
						letter = (char)(((int)letter) + 1);
						if(blue){
							nouvBouton.transform.GetComponent<tk2dSlicedSprite> ().color=Color.cyan;

						}
						else{
							nouvBouton.transform.GetComponent<tk2dSlicedSprite> ().color=new Color(0,0.75f,0.75f);
						
						}
						blue=!blue;
					} 
					else {
						nouvBouton.transform.GetChild (0).GetComponent<tk2dTextMesh> ().text =  fils.name;
						nouvBouton.transform.GetChild (2).GetComponent<tk2dTextMesh> ().text =nombreActuel.ToString () + "-" ;
						nombreActuel++;
						if(blue){
							nouvBouton.transform.GetComponent<tk2dSlicedSprite> ().color=Color.white;
							
						}
						else{
							nouvBouton.transform.GetComponent<tk2dSlicedSprite> ().color=new Color(0.75f,0.75f,0.75f);
							
						}
						blue=!blue;

					}
					int ind = loadcsv.getIndName (fils.name);
					nouvBouton.transform.GetChild (1).GetComponent<tk2dTextMesh> ().text = loadcsv.composants [ind] [9];
					Vector3 pos = new Vector3 ();
					pos.x = 0;
					pos.z = 0;
					pos.y = nouvBouton.transform.localPosition.y - 0.11f * (i-posDeb) + 0.021f; //- 0.04f;
					nouvBouton.transform.localPosition = pos;

					nouvBouton.transform.name = "boutonhierarchie" + i.ToString ();

					i++;


				}
				GameObject boutonIdee = Instantiate (Resources.Load ("buttonIdee")) as GameObject;
				boutonIdee.transform.parent = attachparent; //transform; 
				Vector3 pos1 = new Vector3 ();
				pos1.x = 0;
				pos1.z = 0;
				pos1.y = boutonIdee.transform.localPosition.y - 0.11f * (i-posDeb) - 0.08f;
				boutonIdee.transform.localPosition = pos1;
				boutonIdee.name="bouton idee";
				int k=0;
				int z=0;
				foreach(string[] ligne in loadcsv.notes){
					if(ligne[0]==objetActuel){
						GameObject idee = Instantiate (Resources.Load ("AffichageIdee")) as GameObject;
						idee.transform.parent = attachparent; //transform; 
						Vector3 pos3 = new Vector3 ();
						pos3.x = 0;
						pos3.z = 0;

						pos3.y = idee.transform.localPosition.y - 0.11f * (i-posDeb)-(k*0.23f)- 0.28f;
						idee.transform.localPosition = pos3;
						idee.name="idee "+k.ToString();
						idee.GetComponent<ideeExistante>().init(z);
						if(ligne[7]=="F"){
							idee.transform.GetComponent<tk2dSlicedSprite> ().color=new Color(0.75f,0.75f,0.2f);
							
						}
						else if(ligne[7]=="D"){
							idee.transform.GetComponent<tk2dSlicedSprite> ().color=new Color(0.2f,0.75f,0.2f);
						}
						else if(ligne[7]=="V"){
							idee.transform.GetComponent<tk2dSlicedSprite> ().color=new Color(0.2f,0.2f,0.75f);
						}
						if(k%2==1){
							if(ligne[7]=="I"){
								idee.transform.GetComponent<tk2dSlicedSprite> ().color=new Color(1,0.75f,0.0f);
							}
							else if(ligne[7]=="F"){
								idee.transform.GetComponent<tk2dSlicedSprite> ().color=new Color(1,1,0.3f);
								
							}
							else if(ligne[7]=="D"){
								idee.transform.GetComponent<tk2dSlicedSprite> ().color=new Color(0.5f,1,0.0f);
							}
							else if(ligne[7]=="V"){
								idee.transform.GetComponent<tk2dSlicedSprite> ().color=new Color(0,0.5f,1f);
							}
						}
						k++;
					}
					z++;
					
				}

				GameObject boutonAjouter = Instantiate (Resources.Load ("AjoutIdee")) as GameObject;
				boutonAjouter.transform.parent = attachparent; //transform; 
				Vector3 pos2 = new Vector3 ();
				pos2.x = 0;
				pos2.z = 0;
				pos2.y = boutonAjouter.transform.localPosition.y - 0.11f * (i-posDeb)-(k*0.23f) - 0.28f;
				boutonAjouter.transform.localPosition = pos2;
				boutonAjouter.name="bouton ajouter";

				}
			else{
//				csv = (loadcsv)gameObject.GetComponent<loadcsv> ();
				foreach (Transform child in attachparent) {
					Destroy (child.gameObject);
				}
				bool coul=true;
				int ind = loadcsv.getIndName (actuel.name);
				int j=0;
				foreach(string colonne in loadcsv.composants[ind]){
					GameObject nouvBouton = Instantiate (Resources.Load ("buttonInfo")) as GameObject;
					nouvBouton.transform.parent = attachparent; //transform; 
					if (coul) {

						nouvBouton.transform.GetComponent<tk2dSlicedSprite> ().color=new Color(0.75f,0.75f,0.75f);
						coul=false;
					} 
					else {

						nouvBouton.transform.GetComponent<tk2dSlicedSprite> ().color=Color.gray;
						coul=true;

					}
					nouvBouton.transform.GetChild (0).GetComponent<tk2dTextMesh> ().text = loadcsv.composants [0][j];
					nouvBouton.transform.GetChild (1).GetComponent<tk2dTextMesh> ().text = colonne;
					Vector3 pos = new Vector3 ();
					pos.x = 0;
					pos.z = 0;
					pos.y = nouvBouton.transform.localPosition.y - 0.11f * (j-posDeb)+ 0.021f;// - 0.04f;
					nouvBouton.transform.localPosition = pos;
					nouvBouton.transform.name = "boutonhierarchie" + j.ToString ();
					j++;
				}
				GameObject boutonIdee = Instantiate (Resources.Load ("buttonIdee")) as GameObject;
				boutonIdee.transform.parent = attachparent; //transform; 
				Vector3 pos1 = new Vector3 ();
				pos1.x = 0;
				pos1.z = 0;
				pos1.y = boutonIdee.transform.localPosition.y - 0.11f * (j-posDeb) - 0.08f;
				boutonIdee.transform.localPosition = pos1;
				boutonIdee.name="bouton idee";
				int k=0;
				int z=0;

				foreach(string[] ligne in loadcsv.notes){
					if(ligne[0]==objetActuel){
						GameObject idee = Instantiate (Resources.Load ("AffichageIdee")) as GameObject;
						idee.transform.parent = attachparent; //transform; 
						Vector3 pos3 = new Vector3 ();
						pos3.x = 0;
						pos3.z = 0;
						pos3.y = idee.transform.localPosition.y - 0.11f * (j-posDeb)-(k*0.23f)- 0.28f;
						idee.transform.localPosition = pos3;
						idee.name="idee "+k.ToString();
						idee.GetComponent<ideeExistante>().init(z);
						if(ligne[7]=="F"){
							idee.transform.GetComponent<tk2dSlicedSprite> ().color=new Color(0.75f,0.75f,0.0f);

						}
						else if(ligne[7]=="D"){
							idee.transform.GetComponent<tk2dSlicedSprite> ().color=new Color(0,0.5f,0.0f);
						}
						else if(ligne[7]=="V"){
							idee.transform.GetComponent<tk2dSlicedSprite> ().color=new Color(0,0f,0.5f);
						}
						if(k%2==1){
							if(ligne[7]=="I"){
							idee.transform.GetComponent<tk2dSlicedSprite> ().color=new Color(1,0.75f,0.5f);
							}
							else if(ligne[7]=="F"){
								idee.transform.GetComponent<tk2dSlicedSprite> ().color=new Color(1,1,0.0f);

							}
							else if(ligne[7]=="D"){
								idee.transform.GetComponent<tk2dSlicedSprite> ().color=new Color(0.5f,1,0.0f);
							}
							else if(ligne[7]=="V"){
								idee.transform.GetComponent<tk2dSlicedSprite> ().color=new Color(0,0.5f,1f);
							}
						}
						k++;
					}
					z++;                       
				}
				GameObject boutonAjouter = Instantiate (Resources.Load ("AjoutIdee")) as GameObject;
				boutonAjouter.transform.parent = attachparent; //transform; 
				Vector3 pos2 = new Vector3 ();
				pos2.x = 0;
				pos2.z = 0;
				pos2.y = boutonAjouter.transform.localPosition.y - 0.11f * (j-posDeb) -(k*0.23f)- 0.28f;
				boutonAjouter.transform.localPosition = pos2;
				boutonAjouter.name="bouton ajouter";



			}
			}
			else{
			
				foreach(string[] tab in loadcsv.fonctions){
					if(tab[0]==objetActuel){
						int i=1;
						int k=0;
						bool blue=true;
						foreach(string mot in tab){
							if(k>0 && mot!=null && mot!=""){
								Debug.Log("mot="+mot);
								float nombre=float.Parse(mot);
								if(nombre>0){
									GameObject nouvBouton = Instantiate (Resources.Load ("boutonFonction")) as GameObject;
									nouvBouton.transform.parent = attachparent; //transform; 

										
									nouvBouton.transform.GetChild (0).GetComponent<tk2dTextMesh> ().text = loadcsv.fonctions[1][i];
									nouvBouton.transform.GetChild (1).GetComponent<tk2dTextMesh> ().text = nombre.ToString();
										
										if(blue){
											nouvBouton.transform.GetComponent<tk2dSlicedSprite> ().color=Color.cyan;
											
										}
										else{
											nouvBouton.transform.GetComponent<tk2dSlicedSprite> ().color=new Color(0,0.75f,0.75f);
											
										}
										blue=!blue;

									

									Vector3 pos = new Vector3 ();
									pos.x = 0;
									pos.z = 0;
									pos.y = nouvBouton.transform.localPosition.y - 0.11f * ((i-1)-posDeb) + 0.021f; //- 0.04f;
									nouvBouton.transform.localPosition = pos;
									
									nouvBouton.transform.name = "boutonFonction" + i.ToString ();
									
									i++;
								}

							}
							k++;
					}
				}
				}


			}

				chang = false;
			}
*/
		}


	public static void setObjetActuel(string nom,bool withundo = true)
	{
		/*
		scroolHierarchie.ResetPos();
		Transform tr = (Transform)GameObject.Find ("interface").FindInChildren("righttop").FindInChildren("scrollwindow").transform;
		tr.localPosition = Vector3.zero;
		if (withundo)		button_undo.AddUndoFocusObject(nom);
		myinterface.focusobject = myinterface.my3dcamera.gameObject.transform.parent.gameObject.FindInChildren (nom);
		load3d_object.HideAllOthers(nom);
		nom = nom.Replace("#_","");

		string usedname = "";
		if (nom == autoloadcurrent3dobject.StartRootName)
			usedname = loadcsv.GetProcessDescriptions("parent");
		else
			usedname = loadcsv.GetProcessDescriptions(nom);
//		Debug.Log("Procedure used name: "+usedname);
		processitems.CreateProcessBar();

		GameObject obj = GameObject.Find ("topbar").FindInChildren("text");
		tk2dTextMesh tm = (tk2dTextMesh)obj.GetComponent<tk2dTextMesh>();
		settext.ForceText(tm,nom);
		
		myinterface.lastfocussed = null;
		
		objetActuel = nom;
		chang= true;
*/
	}

	public static void setObjetActuel(string nom,int position)
	{
		/*
		scroolHierarchie.ResetPos();
		Transform tr = (Transform)GameObject.Find ("interface").FindInChildren("righttop").FindInChildren("scrollwindow").transform;
		tr.localPosition = Vector3.zero;

		button_undo.AddUndoFocusObject(nom);
		myinterface.focusobject = myinterface.my3dcamera.gameObject.transform.parent.gameObject.FindInChildren (nom);
		load3d_object.HideAllOthers(nom);
		
		nom = nom.Replace("#_","");
		GameObject obj = GameObject.Find ("topbar").FindInChildren("text");
		tk2dTextMesh tm = (tk2dTextMesh)obj.GetComponent<tk2dTextMesh>();
		settext.ForceText(tm,nom);
		
		myinterface.lastfocussed = null;
		
		objetActuel = nom;
		chang= true;
		posDeb=position;
		*/
	}
}

	

	