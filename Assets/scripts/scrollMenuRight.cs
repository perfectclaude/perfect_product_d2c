﻿using UnityEngine;
using System.Collections;

public class scrollMenuRight : MonoBehaviour {

	
	void Start () 
    {
		
	}
	
	// Update is called once per frame
	void Update () 
    {
        pp_produit prod = pp_manager.GetCurrentProduit();
        linkEditor le = GameObject.Find("linkEditor").GetComponent<linkEditor>();
        if( (prod != null) && (le != null ))
        {
		
            if (Input.mousePosition.x > 4 * Screen.width / 5)
            {
                if (Input.GetAxis("Mouse ScrollWheel") < 0)
                { // back
                    if (le.changement > 0)
                    {
                        le.changement -= 1;
                    }
                }
			
                if (Input.GetAxis("Mouse ScrollWheel") > 0)
                { // forward
                    if (le.changement < (prod.composants.Count - 10))
                    {
                        le.changement += 1;
                    }
				
                }
            }
        }
	}
}
