﻿using UnityEngine;
using System.Collections;
using System.Text;
using System;
using System.IO;

public class logger : MonoBehaviour {

    private static volatile logger _instance;
    private static object _syncRoot = new System.Object();
    private string _ficLog = "";
    private TypeLoggerLog _typLog = TypeLoggerLog.INFO;

    private long _maxLogSize = 2*1024*1024; // 2 Mo

    public enum TypeLoggerLog
    {
        EXCEPTION = 0,
        ERROR = 1,
        WARNING = 2,
        INFO = 3,  
        DEBUG = 4
    }

    private logger()
    {
		// dump it to the root of documents
		// I propose the log file is sent by email to us at some point in time
		string constructfname = Application.productName.Replace(" ","_") + ".log";
		_ficLog = Application.persistentDataPath + "/" + constructfname;
      
        //_typLog = TypeLoggerLog.INFO;
        _typLog = TypeLoggerLog.DEBUG;

    }

    public static logger Instance
    {
        get 
        {
            if (_instance == null) 
            {
                // ATTENTION Ligne non superfulx : On Créé l'instance de Debug avant de passer dans le constructeur de cette class
                Debug.Log("Init logger ...");

                lock (_syncRoot) 
                {
                    if (_instance == null)
                    {
                        GameObject singleton = new GameObject();
                        _instance = singleton.AddComponent<logger>();

                        singleton.name = "logger";

                        DontDestroyOnLoad(singleton);
                    }
                }

                Debug.Log("logger OK !");
            }

            return _instance;
        }
    }

    // Use this for initialization
    void Start () {

    }

    // Update is called once per frame
    void Update () {

    }


    public void LogMessage( TypeLoggerLog typLog, string className, string methodeName, string mess )
    {
        StringBuilder message = new StringBuilder();

        message.Append(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));

        switch (typLog)
        {
            case TypeLoggerLog.EXCEPTION:
                message.Append(" EXCEPT ");
                break;

            case TypeLoggerLog.ERROR:
                message.Append(" ERROR  ");
                break;

            case TypeLoggerLog.WARNING:
                message.Append(" WARN   ");
                break;

            case TypeLoggerLog.INFO:
                message.Append(" INFO   ");
                break;

            case TypeLoggerLog.DEBUG:
                message.Append(" DEBUG  ");
                break;

            default: 
                message.Append(" INFO   ");
                break;
        }

        message.Append( className+"."+methodeName+" " );
        message.Append( mess );


        long sizeFile = 0;

        try
        {
            sizeFile = new FileInfo(_ficLog).Length;
        }
        catch {}

        if (sizeFile >= _maxLogSize)
        {
            // on garde 1/3 de la taille
            long jump = sizeFile - (_maxLogSize * 2 / 3);

            StreamReader fi = null;
            StreamWriter fo = null;

            try
            {
                fi = new StreamReader(_ficLog);
                fo = new StreamWriter(_ficLog+"o", false);

                fi.BaseStream.Seek( jump, SeekOrigin.Begin );
                fi.ReadLine(); // La premier ligne n'est pas complete

                while( !fi.EndOfStream )
                {
                    string lig = fi.ReadLine();

                    if( lig.Length > 0 )
                        fo.WriteLine( lig );
                }     
            }
            catch (Exception)
            {  
            }
            finally
            {
                try
                {
                    if( fi != null )
                        fi.Close();
                    fi = null;
                }
                catch (Exception)
                {
                }

                try
                {
                    if( fo != null )
                        fo.Close();
                    fo = null;
                }
                catch (Exception)
                {
                }
            }


            try
            {
                if (File.Exists(_ficLog))
                    File.Delete(_ficLog);

                if (File.Exists(_ficLog+"o"))
                    File.Move(_ficLog+"o", _ficLog);
            }
            catch (Exception)
            {
            }



        }


        if ((int)typLog <= (int)_typLog)
        {
            switch (typLog)
            {
                case TypeLoggerLog.EXCEPTION:
                case TypeLoggerLog.ERROR:
                    Debug.LogError(message);
                    break;

                case TypeLoggerLog.WARNING:
                    Debug.LogWarning(message);
                    break;

                default:
                    Debug.Log(message);
                    break;
            }

            StreamWriter fo = null;

            lock (_syncRoot)
            {
                try
                {
                    fo = new StreamWriter(_ficLog, true);
                    fo.WriteLine(message);
                }
                catch (Exception)
                {  
                }
                finally
                {
                    try
                    {
                        if( fo != null )
                            fo.Close();
                        fo = null;
                    }
                    catch (Exception)
                    {
                    }
                }
            }
        }

    }

    public static void LogDebug( string className, string methodeName, string mess )
    {
        logger loggerManager = logger.Instance;

        loggerManager.LogMessage(TypeLoggerLog.DEBUG, className, methodeName, mess);
    }

    public static void LogInfo( string className, string methodeName, string mess )
    {
        logger loggerManager = logger.Instance;

        loggerManager.LogMessage(TypeLoggerLog.INFO, className, methodeName, mess);
    }

    public static void LogWarn( string className, string methodeName, string mess )
    {
        logger loggerManager = logger.Instance;

        loggerManager.LogMessage(TypeLoggerLog.WARNING, className, methodeName, mess);
    }

    public static void LogError( string className, string methodeName, string mess )
    {
        logger loggerManager = logger.Instance;

        loggerManager.LogMessage(TypeLoggerLog.ERROR, className, methodeName, mess);
    }


    public static void LogException( string className, string methodeName, string mess )
    {
        logger loggerManager = logger.Instance;

        loggerManager.LogMessage(TypeLoggerLog.EXCEPTION, className, methodeName, mess);
    }
}
