﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using MiniJSON;


public class pp_produit
{
    public enum EtatPPProduitLoad
    {
        LOAD_BOM  = 0,
        LOAD_PROC = 1,
        LOAD_NOTE = 2,
        LOAD_FONC = 3,

        LOAD_OBJ  = 4,
        LOAD_MTL  = 5,
        LOAD_INI  = 6,

        LOAD_SCENARIO = 10,

        LOAD_COMPLETED = 99,
        LOAD_ERROR = -1
    }

    public enum EtatPPProduitSave
    {
        SAVE_BOM  = 0,
        SAVE_PROC = 1,
        SAVE_NOTE = 2,
        SAVE_FONC = 3,
       
        SAVE_OBJ  = 4,
        SAVE_MTL  = 5,
        SAVE_INI  = 6,

        SAVE_SCENARIO = 10,

        SAVE_COMPLETED = 99,
        SAVE_ERROR = -1
    }

    private object _syncRoot = new System.Object();

    private string _nom = "";
    private EtatPPProduitLoad _etatLoad = EtatPPProduitLoad.LOAD_BOM;
    private EtatPPProduitSave _etatSave = EtatPPProduitSave.SAVE_COMPLETED;
    private pp_task _taskLoad = null;
    private pp_task _taskSave = null;


    DateTime _version = DateTime.MinValue;

    private string _d2cfilesfilespath = "";
    private string _filesfilespath = "";
  
    private List<pp_produit_composant> _composants =  null;

    private pp_list<string> _fonctionalites = null;
    private bool _inSynchroFonctionalites = false;

    private pp_list<pp_produit_scenario> _scenarios = null;
    private bool _inSynchroScenarios = false;

    private string _mtl3D = "";
    private string _obj3D = "";

    private Dictionary<string,pp_produit_obj> _objs = null;

    private pp_produit_composant _roothierarchie = null;

    private ppconvert _convert = null; 


    public pp_produit( string nom, string d2cfilesfilespath )
    {
        _convert = new ppconvert();

        _nom = nom;
        _etatLoad = EtatPPProduitLoad.LOAD_BOM;
        _etatSave = EtatPPProduitSave.SAVE_COMPLETED;

        _d2cfilesfilespath = d2cfilesfilespath;
        _filesfilespath = _d2cfilesfilespath + "/" + _nom;

        _composants = new List<pp_produit_composant>();

        _fonctionalites = new pp_list<string>(synchro_fonctionalites);
        _inSynchroFonctionalites = false;

        _scenarios = new pp_list<pp_produit_scenario>(synchro_scenarios);
        _inSynchroScenarios = false;

        _objs = new Dictionary<string,pp_produit_obj>();

        _roothierarchie = new pp_produit_composant(this);
    }

    public pp_produit()
        : this( "", "d2cfiles" )
    {
    }

    // Getter  / Setter
    public string Nom
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _nom;
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _nom = value;
                _filesfilespath = _d2cfilesfilespath + "/" + _nom;
            }
        }
    }

    public ppconvert Convert
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert;
            }
        }
    }

    public bool IsLoaded
    {
        get 
        { 
            lock (_syncRoot)
            {
                return ((_taskLoad == null) && (_etatLoad == EtatPPProduitLoad.LOAD_COMPLETED));
            }
        }

    }
    public bool IsLoading
    {
        get 
        { 
            lock (_syncRoot)
            {
                return (_taskLoad != null);
            }
        }

    }

    public bool IsError
    {
        get 
        { 
            lock (_syncRoot)
            {
                return ((_taskLoad == null) && (_etatLoad == EtatPPProduitLoad.LOAD_ERROR));
            }
        }

    }

    public EtatPPProduitLoad EtatLoad
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _etatLoad;
            }
        }
        /* Remplacer par EtatLoadSet
        set 
        { 
            lock (_syncRoot)
            {
                switch (value)
                {
                    case EtatPPProduitLoad.LOAD_COMPLETED:
                    case EtatPPProduitLoad.LOAD_ERROR:
                        _taskLoad = null;
                        break;
                        
                    default:
                        break;
                }
                _etatLoad = value;
            }
        }
        */

    }

    public bool EtatLoadSet( EtatPPProduitLoad value, pp_task task_owner )
    {
        bool bRet = false;

        lock (_syncRoot)
        {
            if (task_owner == _taskLoad)
            {
                switch (value)
                {
                    case EtatPPProduitLoad.LOAD_COMPLETED:
                    case EtatPPProduitLoad.LOAD_ERROR:
                        _taskLoad = null;
                        break;

                    default:
                        break;
                }
                _etatLoad = value;

                bRet = true;
            }
        }

        return bRet;
    }

    public EtatPPProduitSave EtatSave
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _etatSave;
            }
        }
        /* Remplacer par EtatSaveSet
        set 
        { 
            lock (_syncRoot)
            {
                switch (value)
                {
                    case EtatPPProduitSave.SAVE_COMPLETED:
                    case EtatPPProduitSave.SAVE_ERROR:
                        _taskSave = null;
                        break;

                    default:
                        break;
                }
                _etatSave = value;
            }
        }
        */

    }

    public bool EtatSaveSet( EtatPPProduitSave value, pp_task task_owner )
    {
        bool bRet = false;

        lock (_syncRoot)
        {
            if (task_owner == _taskSave)
            {
                switch (value)
                {
                    case EtatPPProduitSave.SAVE_COMPLETED:
                    case EtatPPProduitSave.SAVE_ERROR:
                        _taskSave = null;
                        break;

                    default:
                        break;
                }
                _etatSave = value;

                bRet = true;
            }
        }

        return bRet;
    }

    public DateTime Version
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _version;
            }
        }
    }
             

    public Dictionary<string,pp_produit_obj> Objs
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _objs;
            }
        }

    }

    public string mtl3D
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _mtl3D;
            }
        }

    }

    public string obj3D
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _obj3D;
            }
        }

    }
        

    public List<pp_produit_composant> composants
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _composants;
            }
        }

    }

    public List<pp_produit_proc> process
    {
        get 
        { 
            lock (_syncRoot)
            {
                List<pp_produit_proc> procs = new List<pp_produit_proc>();

                foreach( pp_produit_composant comp in _composants )
                {
                    foreach( pp_produit_proc proc in comp.Process )
                    {
                        procs.Add(proc);
                    }
                }

                return procs;
            }
        }

    }
        
    public List<pp_produit_note> notes
    {
        get 
        { 
            lock (_syncRoot)
            {
                List<pp_produit_note> notes = new List<pp_produit_note>();

                foreach( pp_produit_composant comp in _composants )
                {
                    foreach( pp_produit_note note in comp.Notes )
                    {
                        notes.Add(note );
                    }
                }
                
                return notes;
            }
        }

    }

        

    public pp_produit_composant roothierarchie
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _roothierarchie;
            }
        }

    }
        
    public pp_list<string> Fonctionalites
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _fonctionalites;
            }
        }
    }


    private void synchro_fonctionalites(int index, string oldObj, bool inserted, bool deleted)
    {
        lock (_syncRoot)
        {
            if (!_inSynchroFonctionalites)
            {
                _inSynchroFonctionalites = true;

                if (inserted)
                {
                    if ((index >= 0) && (index <= _fonctionalites.Count))
                    {
                        foreach (pp_produit_composant comp in _composants)
                        {
                            comp.PourcentageFonctionalites.Insert(index, new pp_produit_fonctionnalite(_fonctionalites[index], 0, comp));
                        }
               
                        _roothierarchie.PourcentageFonctionalites.Insert(index, new pp_produit_fonctionnalite(_fonctionalites[index], 0, _roothierarchie));
                    }
                }
                else if (deleted)
                {
                    if ((index >= 0) && (index <= _fonctionalites.Count))
                    {
                        foreach (pp_produit_composant comp in _composants)
                        {
                            comp.PourcentageFonctionalites.RemoveAt(index);
                        }

                        _roothierarchie.PourcentageFonctionalites.RemoveAt(index);
                    }
                
                }
                else
                {
                    if ((index >= 0) && (index < _fonctionalites.Count))
                    {
                        foreach (pp_produit_composant comp in _composants)
                        {
                            comp.PourcentageFonctionalites[index].Titre = _fonctionalites[index];
                        }

                        _roothierarchie.PourcentageFonctionalites[index].Titre = _fonctionalites[index];
                    }
                }

                _inSynchroFonctionalites = false;
            }
        }
    }

    public pp_list<pp_produit_scenario> Scenarios
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _scenarios;
            }
        }
    }

    private void synchro_scenarios(int index, pp_produit_scenario oldObj, bool inserted, bool deleted)
    {
        lock (_syncRoot)
        {
            if (!_inSynchroScenarios)
            {
                _inSynchroScenarios = true;

                if (inserted)
                {
                    if ((index >= 0) && (index <= _scenarios.Count))
                    {
                        foreach (pp_produit_composant comp in _composants)
                        {
                            foreach (pp_produit_note note in comp.Notes)
                            {
                                note.CoutScenarios.Insert(index, 0f);
                            }
                        }

                        foreach (pp_produit_note note in _roothierarchie.Notes)
                        {
                            note.CoutScenarios.Insert(index, 0f);
                        }
                    }
                }
                else if (deleted)
                {
                    if ((index >= 0) && (index <= _scenarios.Count))
                    {
                        foreach (pp_produit_composant comp in _composants)
                        {
                            foreach (pp_produit_note note in comp.Notes)
                            {
                                note.CoutScenarios.RemoveAt(index);
                            }
                        }

                        foreach (pp_produit_note note in _roothierarchie.Notes)
                        {
                            note.CoutScenarios.RemoveAt(index);
                        }    
                    }

                }
                else
                {
                    if ((index >= 0) && (index < _scenarios.Count))
                    {
                        foreach (pp_produit_composant comp in _composants)
                        {
                            foreach (pp_produit_note note in comp.Notes)
                            {
                                note.CoutScenarios[index] = 0f;
                            }
                        }

                        foreach (pp_produit_note note in _roothierarchie.Notes)
                        {
                            note.CoutScenarios[index] = 0f;
                        }    
                    }
                }

                _inSynchroScenarios = false;
            }
        }
    }
        

    // Methodes
    public pp_task LoadData()
    {
        lock (_syncRoot)
        {
            _etatLoad = EtatPPProduitLoad.LOAD_BOM;
            if (_taskLoad != null)
                _taskLoad.IsCanceled = true;
            _taskLoad = null;

            _taskLoad = pp_manager.LoadDataProduit(this);

            return _taskLoad;
        }
    }

    public pp_task SaveData()
    {
        lock (_syncRoot)
        {
            if (_etatLoad == EtatPPProduitLoad.LOAD_COMPLETED)
            {
                _etatSave = EtatPPProduitSave.SAVE_BOM;
                if (_taskSave != null)
                    _taskSave.IsCanceled = true;
                _taskSave = null;

                _taskSave = pp_manager.SaveDataProduit(this);
            }

            return _taskSave;
        }
    }


    // Attention le lock doit etre pris par l'appelant
    private byte[] LoadDataFile(string filename)
    {
        byte[] data = null;
        string full_filename = _filesfilespath + "/" + filename;

        try
        {
            Directory.CreateDirectory(_filesfilespath);

            if (File.Exists(full_filename))
            {
                data = File.ReadAllBytes(full_filename);
            }

        }
        catch( Exception ex )
        {
            data = null;
            logger.LogException("pp_produit", "LoadDataFile", ex.Message);
        }

        return data;
    }

    private bool SaveDataFile(string filename, byte[] data)
    {
        bool bRet = false;
        string full_filename = _filesfilespath + "/" + filename;

        if (data != null)
        {
            try
            {
                Directory.CreateDirectory(_filesfilespath);

                File.WriteAllBytes(full_filename, data);

                bRet = true;
            }
            catch (Exception ex)
            {
                data = null;
                logger.LogException("pp_produit", "SaveDataFile", ex.Message);
            }
        }

        return bRet;
    }


    private string parseTXT( byte[] byteStrTXT )
    {
        string str = "";

        if (byteStrTXT != null)
        {
            str = Encoding.ASCII.GetString(byteStrTXT);
            //str = Encoding.UTF8.GetString(byteStrTXT);
            //Encoding enc = Encoding.GetEncoding("iso-8859-1");
            //str = enc.GetString(byteStrTXT);
        }

        return str;
    }

    private List<string[]> parseCSV( byte[] byteStrCSV )
    {

        List<string[]> lst = new List<string[]>();
        

        if (byteStrCSV != null)
        {
            string str = "";
            //str = Encoding.ASCII.GetString(byteStrCSV);
            //str = Encoding.UTF8.GetString(byteStrCSV);
            Encoding enc = Encoding.GetEncoding("iso-8859-1");
            str = enc.GetString(byteStrCSV);

            str = str.Replace("\r", "\n").Replace("\n\n", "\n");
            bool yaCot = false;
            string sep = ";";

            string c_sav = "";
            string[] chps = null;
            int no_chp = 0;
            int max_chp = 0;


            string[] ligs = str.Split('\n');

            if (ligs != null)
            {
                foreach (string lig in ligs)
                {
                    if (lig != null)
                    {
                        string ligParse = lig.Replace ( "(@@$sautdeligne$@@)","\n").Trim();

                        if (ligParse == "sep=;")
                            sep = ";";
                        else if (ligParse == "sep=,")
                            sep = ",";
                        else if (ligParse == "sep=\t")
                            sep = "\t";
                        else
                        {
                            if (yaCot)
                                ligParse = "\n" + ligParse;
                
                            for (int i = 0; i < ligParse.Length; i++)
                            {
                                string c = ligParse.Substring(i, 1);

                                if (c == "\"")
                                {
                                    yaCot = !yaCot;
                                    if ( (c_sav == "\"") && yaCot )
                                    {
                                        if ((chps == null) || (chps.Length < (no_chp + 1)))
                                        {
                                            Array.Resize(ref chps, no_chp + 1);
                                            chps[no_chp] = "";
                                        }

                                        chps[no_chp] += c;
                                    }
                                }
                                else if (c == sep)
                                {
                                    if (yaCot)
                                    {
                                        if ((chps == null) || (chps.Length < (no_chp + 1)))
                                        {
                                            Array.Resize(ref chps, no_chp + 1);
                                            chps[no_chp] = "";
                                        }

                                        chps[no_chp] += c;
                                    }
                                    else
                                    {
                                        if ((chps == null) || (chps.Length < (no_chp + 1)))
                                        {
                                            Array.Resize(ref chps, no_chp + 1);
                                            chps[no_chp] = "";
                                        }

                                        chps[no_chp] = chps[no_chp].Trim();
                                        no_chp++;
                                    }
                                }
                                else
                                {
                                    if ((chps == null) || (chps.Length < (no_chp + 1)))
                                    {
                                        Array.Resize(ref chps, no_chp + 1);
                                        chps[no_chp] = "";
                                    }

                                    chps[no_chp] += c;
                                }
                                    

                                c_sav = c;
                            }

                            if( (chps != null) && !yaCot )
                            {
                                // La premiere ligne est l'entete CSV : Elle determine le nombre de champs
                                if (lst.Count <= 0)
                                {
                                    max_chp = chps.Length;
                                    while (max_chp > 0)
                                    {
                                        if (chps[max_chp - 1].Length > 0)
                                            break;
                                        else
                                            max_chp--;
                                    }
                                }
                                
                                Array.Resize(ref chps, max_chp);

                                // NULL-> ""
                                for (int i = 0; i < max_chp; i++)
                                {
                                    if (chps[i] == null)
                                        chps[i] = "";
                                }
                                
                                lst.Add(chps);
                                chps = null;
                                no_chp = 0;
                                c_sav = "";
                            } 
                        }  
                    }
                }
            }

            if (chps != null) // Si on passe ici c'est que le fichier est non conforme (probleme de cot ouverte non fermé)
            {
                // La premiere ligne est l'entete CSV : Elle determine le nombre de champs
                if (lst.Count <= 0)
                {
                    max_chp = chps.Length;
                    while (max_chp > 0)
                    {
                        if (chps[max_chp - 1].Length > 0)
                            break;
                        else
                            max_chp--;
                    }
                }

                Array.Resize(ref chps, max_chp);

                // NULL-> ""
                for (int i = 0; i < max_chp; i++)
                {
                    if (chps[i] == null)
                        chps[i] = "";
                }

                lst.Add(chps);
            }
        }

        return lst;
    }

    private byte[] parseCSV( List<string[]> lstStrCSV)
    {
        byte[] byteStrCSV = null;

        if (lstStrCSV != null)
        {
            StringBuilder str = new StringBuilder();
            string sep = ";";


            foreach( string[] lig in lstStrCSV )
            {
                string lig_csv = "";

                foreach (string col in lig)
                {
                    if (lig_csv.Length > 0)
                        lig_csv += sep;
                    
                    lig_csv += "\"" + col.Replace( "\"", "\"\"" ) + "\""; 
                }

                str.AppendFormat("{0}\r\n", lig_csv);
            }



            //byteStrCSV = Encoding.ASCII.GetBytes(str);
            //byteStrCSV = Encoding.UTF8.GetBytes(str);
            Encoding enc = Encoding.GetEncoding("iso-8859-1");
            byteStrCSV = enc.GetBytes(str.ToString());
        }

        return byteStrCSV;
    }

    private IDictionary parseJSON( byte[] byteStrJSON )
    {
        IDictionary dictJSON = new Dictionary<string,string>();

        if (byteStrJSON != null)
        {
            string str = "";
            str = Encoding.ASCII.GetString(byteStrJSON);
            //str = Encoding.UTF8.GetString(byteStrJSON);
            //Encoding enc = Encoding.GetEncoding("iso-8859-1");
            //str = enc.GetString(byteStrJSON);

            try
            {
                IDictionary objlist = (IDictionary)Json.Deserialize(str);
                if( objlist != null )
                    dictJSON = objlist;

            }
            catch( Exception ex )
            {
                logger.LogException("pp_produit", "parseJSON", ex.Message);
            }
        }

        return dictJSON;
    }


    private byte[] parseJSON( IDictionary dictStrJSON )
    {
        byte[] byteStrJSON = null;

        if (dictStrJSON != null)
        {
            StringBuilder str = new StringBuilder();
            string sep = ",";
            bool bfirst = true;

            str.Append("{");

            IDictionaryEnumerator dicEnum = dictStrJSON.GetEnumerator();

            if (dicEnum != null)
            {

                dicEnum.Reset();
                while (dicEnum.MoveNext())
                {
                    string keyObj = dicEnum.Key as string;

                    if( (keyObj != null) && (keyObj.Length > 0) && (dicEnum.Value != null) )
                    {
                        string lig_json = "";
                        Type ValueType = dicEnum.Value.GetType();

                        if( ValueType == typeof(string) )
                            lig_json = "\"" + keyObj + "\" : \"" + (string)dicEnum.Value + "\"";
                        
                        else if( (ValueType == typeof(int)) || ( ValueType == typeof(long)) ) 
                            lig_json = "\"" + keyObj + "\" : " + dicEnum.Value.ToString();

                        if (lig_json.Length > 0)
                        {
                            if( bfirst )
                                str.AppendFormat("\r\n   {0}",lig_json );
                            else
                                str.AppendFormat("{1}\r\n   {0}",lig_json, sep);
                        }
                    }

                }
            }

            str.Append("\r\n}\r\n");

            byteStrJSON = Encoding.ASCII.GetBytes(str.ToString());
            //byteStrJSON = Encoding.UTF8.GetBytes(str.ToString());
            //Encoding enc = Encoding.GetEncoding("iso-8859-1");
            //byteStrJSON = enc.GetBytes(str.ToString());
        }

        return byteStrJSON;
    }


    private int SortByHierachie(pp_produit_composant el1, pp_produit_composant el2)
    {
        if (el1.HierachieNumber > el2.HierachieNumber)
            return 1;
        else if (el1.HierachieNumber < el2.HierachieNumber)
            return -1;
        else
            return 0;
    }

    public bool LoadBOM()
    {
        bool bRet = false;

        lock (_syncRoot)
        {

            _composants.Clear();
            _roothierarchie.childs.Clear();
            byte[] data_csv = LoadDataFile(_nom + "_bom.csv");

            bRet = (data_csv != null);

            if (bRet)
            {
                List<string[]> lst = parseCSV(data_csv);

                if( (lst != null) && (lst.Count > 1) ) // On saute la premier ligne
                {
                    for( int i = 1; i < lst.Count; i++ )
                    {
                        _composants.Add( new pp_produit_composant(lst[i], this) );
                    }
                }

            }

            foreach (pp_produit_composant comp in _composants)
            {
                _roothierarchie.AddTreeComp(comp);
            }

            _composants.Sort(SortByHierachie);

            _version = DateTime.Now;
        }

        return bRet;
    }

    public bool SaveBOM()
    {

        bool bRet = true;

        return bRet;
    }
        
    public bool LoadPROC()
    {
        bool bRet = false;

        lock (_syncRoot)
        {
            foreach (pp_produit_composant comp in _composants)
            {
                comp.Process.Clear();
            }
            _roothierarchie.Process.Clear();

            byte[] data_csv = LoadDataFile(_nom + "_proc.csv");

            bRet = (data_csv != null);

            if (bRet)
            {
                List<string[]> lst = parseCSV(data_csv);

                if( (lst != null) && (lst.Count > 1) ) // On saute la premier ligne
                {
                    for( int i = 1; i < lst.Count; i++ )
                    {
                        string ERP = "";

                        _convert.TryParse(lst[i][0], out ERP);

                        // Rechercher du Composant Associe
                        foreach (pp_produit_composant comp in _composants)
                        {
                            if( (ERP.Length > 0) && ((comp.ERP == ERP) || (comp.Description == ERP)) )
                            {
                                pp_produit_proc proc = new pp_produit_proc(lst[i], comp);
                                comp.Process.Add(proc);
                                break;
                            }
                        }
                    }
                }

            }
                
            _version = DateTime.Now;

        }

        return bRet;
    }

    public bool SavePROC()
    {

        bool bRet = true;

        return bRet;
    }

    public bool LoadNOTE()
    {
        bool bRet = false;

        lock (_syncRoot)
        {
            foreach (pp_produit_composant comp in _composants)
            {
                comp.Notes.Clear();
            }
            _roothierarchie.Notes.Clear();

            byte[] data_csv = LoadDataFile(_nom + "_note.csv");

            bRet = (data_csv != null);

            if (bRet)
            {
                List<string[]> lst = parseCSV(data_csv);

                if( (lst != null) && (lst.Count > 1) ) // On saute la premier ligne
                {
                    for( int i = 1; i < lst.Count; i++ )
                    {
                        string ERP = "";

                        _convert.TryParse(lst[i][4], out ERP);

                        // Rechercher du Composant Associe
                        foreach (pp_produit_composant comp in _composants)
                        {
                            if( (ERP.Length > 0) && ((comp.ERP == ERP) || (comp.Description == ERP)) )
                            {
                                pp_produit_note note = new pp_produit_note(lst[i], comp);
                                comp.Notes.Add(note);
                                break;
                            }
                        }
                    }
                }
            }
                
            _version = DateTime.Now;
        }

        //return bRet;
        return true; // Le fichier n'existe pas par defaut
    }

    public bool SaveNOTE()
    {
        bool bRet = false;

        lock (_syncRoot)
        {
            byte[] data = null;
            List<string[]> lst = new List<string[]>();
            bool bFirst = true;

            // Ajout des data

            foreach( pp_produit_composant comp in _composants )
            {
                foreach( pp_produit_note note in comp.Notes )
                {
                    if (bFirst)
                        lst.Add(note.EncodeTitre());

                    lst.Add(note.EncodeData());

                    bFirst = false;
                }
            }


            data = parseCSV(lst);

            bRet = SaveDataFile(_nom + "_note.csv", data);
        }

        return bRet;
    }

    public bool LoadINI()
    {
        bool bRet = false;

        lock (_syncRoot)
        {
            _objs.Clear();
            foreach (pp_produit_composant comp in _composants)
            {
                comp.Objs.Clear();
            }
            _roothierarchie.Objs.Clear();

            byte[] data_json = LoadDataFile(_nom + "_ini.txt");

            bRet = (data_json != null);

            if (bRet)
            {
                IDictionary dic = parseJSON(data_json);

                if(dic != null)
                {
                    IDictionaryEnumerator dicEnum = dic.GetEnumerator();

                    if (dicEnum != null)
                    {
                        dicEnum.Reset();
                        while (dicEnum.MoveNext())
                        {
                            string keyObj = dicEnum.Key as string;
                            string ERP = dicEnum.Value as string;
                            int    texture = 0;
                            bool   bModeTexture = false;

                            if( ERP == null )
                            {
                                bModeTexture = true;

                                if( dicEnum.Value != null )
                                {
                                    texture = System.Convert.ToInt32( dicEnum.Value );
                                }
                            }
                            else if( ERP.Length <= 0 )
                            {
                                bModeTexture = true;
                            }
                            else if( (keyObj != null) && keyObj.StartsWith( "text_" ) )
                            {
                                bModeTexture = true;
                                _convert.TryParse( ERP, out texture );
                            }


                            if( bModeTexture && (keyObj != null) && keyObj.StartsWith( "text_" ) )
                                keyObj = keyObj.Substring( 5, keyObj.Length - 5 );


                            if( (keyObj != null) && (keyObj.Length > 0) )
                            {
                                pp_produit_obj obj =  null;

                                if( _objs.ContainsKey( keyObj ) )
                                    obj = _objs[keyObj];
                                else
                                {
                                    obj  = new pp_produit_obj( keyObj );

                                }

                                if( obj != null )
                                {
                                    _objs[keyObj] = obj;

                                    if( bModeTexture )
                                        obj.Texture = texture;
                                    else
                                    {
                                        // Rechercher du Composant Associe
                                        foreach (pp_produit_composant comp in _composants)
                                        {
                                            if( (comp.ERP == ERP) || (comp.Description == ERP) )
                                            {
                                                obj.Composant = comp;
                                                comp.Objs.Add( obj );
                                                break;
                                            }
                                        }
                                    }
                                }
                                
                            }
                        }
                    }
                }
            }
                
            _version = DateTime.Now;
        }

        //return bRet;
        return true; // Le fichier n'existe pas par defaut
    }

    public bool SaveINI()
    {
        bool bRet = false;

        lock (_syncRoot)
        {
            byte[] data = null;
            IDictionary dic = new Dictionary<string,object>();

            // Ajout des data
            foreach (KeyValuePair<string,pp_produit_obj> kvp in _objs)
            {
                pp_produit_obj obj = kvp.Value;

                if (obj.Composant != null)
                {
                    dic[obj.Nom] = obj.Composant.Description;
                    dic["text_" + obj.Nom] = obj.Texture;
                }
            }

                
            data = parseJSON(dic);

            bRet = SaveDataFile(_nom + "_ini.txt", data);
        }

        return bRet;
    }

    public bool LoadFONC()
    {
        bool bRet = false;

        lock (_syncRoot)
        {
            _inSynchroFonctionalites = true;

            foreach (pp_produit_composant comp in _composants)
            {
                comp.PourcentageFonctionalites.Clear();
            }
            _roothierarchie.PourcentageFonctionalites.Clear();
            _fonctionalites.Clear();

            byte[] data_csv = LoadDataFile(_nom + "_fonc.csv");

            bRet = (data_csv != null);

            if (bRet)
            {
                List<string[]> lst = parseCSV(data_csv);

                if( (lst != null) && (lst.Count > 1) ) // On saute la premier ligne
                {
                    for( int i = 1; i < lst.Count; i++ )
                    {
                        string ERP = "";

                        _convert.TryParse(lst[i][0], out ERP);

                        // Rechercher du Composant Associe
                        foreach (pp_produit_composant comp in _composants)
                        {
                            if( (ERP.Length > 0) && ((comp.ERP == ERP) || (comp.Description == ERP)) )
                            {
                                comp.DecodeDataPourcentageFonctionalites(lst[0], lst[i]);
                            }
                            else if (comp.PourcentageFonctionalites.Count <= 0)
                            {
                                // On Init les fonctionnality a 0 par defaut
                                comp.DecodeDataPourcentageFonctionalites(lst[0]);
                            }
                        }
                    }

                    _roothierarchie.DecodeDataPourcentageFonctionalites(lst[0]);
                    foreach (pp_produit_fonctionnalite fonc in _roothierarchie.PourcentageFonctionalites)
                    {
                        _fonctionalites.Add(fonc.Titre);
                    }
                }

            }

            _version = DateTime.Now;

            _inSynchroFonctionalites = false;

        }

        //return bRet;
        return true; // Le fichier n'existe pas par defaut
    }

    public bool SaveFONC()
    {

        bool bRet = false;

        lock (_syncRoot)
        {
            byte[] data = null;
            List<string[]> lst = new List<string[]>();
            bool bFirst = true;
 
            // Ajout des data
            foreach (pp_produit_composant comp in _composants)
            {
                if (bFirst)
                    lst.Add(comp.EncodeTitrePourcentageFonctionalites());
                
                lst.Add(comp.EncodeDataPourcentageFonctionalites());

                bFirst = false;
            }

            data = parseCSV(lst);

            bRet = SaveDataFile(_nom + "_fonc.csv", data);
        }

        return bRet;
    }

    public bool LoadOBJ()
    {
        bool bRet = false;

        lock (_syncRoot)
        {
            byte[] data_txt = LoadDataFile(_nom + "_obj.txt");

            bRet = (data_txt != null);

            if (bRet)
            {
                _obj3D = parseTXT(data_txt);
            }

            _version = DateTime.Now;
        }

        return bRet;
    }

    public bool SaveOBJ()
    {

        bool bRet = true;

        return bRet;
    }

    public bool LoadMTL()
    {
        bool bRet = false;

        lock (_syncRoot)
        {
            byte[] data_txt = LoadDataFile(_nom + "_mtl.txt");

            bRet = (data_txt != null);

            if (bRet)
            {
                _mtl3D = parseTXT(data_txt);
            }

            _version = DateTime.Now;
        }

        return bRet;
    }

    public bool SaveMTL()
    {

        bool bRet = true;

        return bRet;
    }


    public bool LoadSCENAR()
    {
        bool bRet = false;

        lock (_syncRoot)
        {
            _inSynchroScenarios = true;
            foreach (pp_produit_composant comp in _composants)
            {
                foreach (pp_produit_note note in comp.Notes)
                {
                    //note.CoutScenarios.Clear();
                }
            }
            foreach (pp_produit_note note in _roothierarchie.Notes)
            {
                //note.CoutScenarios.Clear();
            }
            _scenarios.Clear();

            byte[] data_csv = LoadDataFile( _nom + "_scenarios.csv" );

            bRet = (data_csv != null);

            if (bRet)
            {
                List<string[]> lst = parseCSV(data_csv);

                if( (lst != null) && (lst.Count > 1) ) // On saute la premier ligne
                {
                    for( int i = 1; i < lst.Count; i++ )
                    {
                        _scenarios.Add(new pp_produit_scenario( lst[i], this ));

                    }

                    /*
                    _roothierarchie.DecodeDataPourcentageFonctionalites(lst[0]);
                    foreach (pp_produit_fonctionnalite fonc in _roothierarchie.PourcentageFonctionalites)
                    {
                        _fonctionalites.Add(fonc.Titre);
                    }
                    */
                }

                foreach (pp_produit_composant comp in _composants)
                {
                    foreach (pp_produit_note note in comp.Notes)
                    {
                        while( note.CoutScenarios.Count < _scenarios.Count )
                            note.CoutScenarios.Add( 0f );

                        while( note.CoutScenarios.Count > _scenarios.Count )
                            note.CoutScenarios.RemoveAt( note.CoutScenarios.Count - 1 );
                    }
                }

                foreach (pp_produit_note note in _roothierarchie.Notes)
                {
                    while( note.CoutScenarios.Count < _scenarios.Count )
                        note.CoutScenarios.Add( 0f );

                    while( note.CoutScenarios.Count > _scenarios.Count )
                        note.CoutScenarios.RemoveAt( note.CoutScenarios.Count - 1 );
                }

            }
                
            _version = DateTime.Now;

            _inSynchroScenarios = false;

        }

        //return bRet;
        return true; // Le fichier n'existe pas par defaut
    }

    public bool SaveSCENAR()
    {
        bool bRet = false;

        lock (_syncRoot)
        {
            byte[] data = null;
            List<string[]> lst = new List<string[]>();
            bool bFirst = true;

            // Ajout des data
            foreach (pp_produit_scenario scenar in _scenarios)
            {
                if (bFirst)
                    lst.Add(scenar.EncodeTitre());
    
                lst.Add(scenar.EncodeData());

                bFirst = false;
            }

            data = parseCSV(lst);

            bRet = SaveDataFile( _nom + "_scenarios.csv", data);
        }

        return bRet;
    }


    public void LoadBEGIN()
    {
        _convert.reinit_price_decimal_format();
    }

    public void LoadEND()
    {
        VerifCoherence();

        lock (_syncRoot)
        {
            _version = DateTime.Now;
        }
    }

    public void VerifCoherence()
    {
        lock (_syncRoot)
        {
            // Verification coherance des prix
            //foreach (pp_produit_composant comp in _composants)
            //{
            //    comp.VerifCoherenceData();
            //}
            _roothierarchie.VerifCoherenceData();
        }
    }


    public bool TitreScenarioExistant(string titre)
    {
        bool bRet = false;

        foreach (pp_produit_scenario scenar in _scenarios)
        {
            if (scenar.Titre == titre)
            {
                bRet =  true;
                break;
            }
        }

        return bRet;
    }
        

    public pp_produit_composant FindComposant( string nom )
    {
        pp_produit_composant comp = null;

        lock (_syncRoot)
        {
            if ((_composants != null) && (nom != null))
            {
                string nom_parse = nom.Replace("#_", "");

                foreach (pp_produit_composant cmp in _composants)
                {
                    if( (cmp.Description == nom_parse) || (cmp.ERP == nom_parse) )
                    {
                        comp = cmp;
                        break;
                    }
                }
            }
        }

        return comp;
    }



    public pp_produit_obj FindObj( string nom )
    {
        pp_produit_obj obj = null;

        lock (_syncRoot)
        {
            if ((_objs != null) && (nom != null))
            {
                string nom_parse = nom.Replace("#_", "");

                if (_objs.ContainsKey(nom_parse))
                    obj = _objs[nom_parse];
                else if( nom.StartsWith( "#_" ) ) // Sinon pas trouvé -> On ajoute un OBJ
                {
                    obj = new pp_produit_obj(nom_parse);
                    if( obj != null )
                        _objs[nom_parse] = obj; 
                }    
            }
        }

        return obj;  
    }

    public bool VerifyValidityObjs()
    {
        bool bOK = true;

        // Si tout les composants ont au moins 1 obj et que tout les Obj on un composant : OK

        if (bOK && false)
        {
            foreach (pp_produit_composant comp in _composants)
            {
                if( (comp.childs.Count <= 0) && (comp.Objs.Count <= 0) )
                {
                    bOK = false;
                    break;
                }
            }
        }

        if (bOK)
        {
            foreach (KeyValuePair<string,pp_produit_obj> kvp in _objs)
            {
                pp_produit_obj obj = kvp.Value;

                if (obj.Composant == null)
                {
                    bOK = false;
                    break;
                }
            }
        }
            
        return bOK;
    }

}

