﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using System;
using System.Text;
using System.IO;

public class calendar : MonoBehaviour
{
	public Color colortopbar = Color.red;
	public Color colortopbartext = Color.white;
	public Color colorbuttons = Color.yellow;
    public Color colorbuttonstext = Color.white;
	public Color colorbackground = Color.white;
	public Color colordays = Color.white;
	public Color colordaystext = Color.black;
	public Color colorday = new Color(0.15f,0.15f,0.15f,1.0f);
	public Color colordaytext = Color.white;
	public Color colorselectday = new Color(0.9f,0.9f,0.9f,1.0f);
	public Color colorselectdaytext = Color.white;
	public Color colorbehinddaynames = Color.red;


	GameObject days;
	GameObject datelements;
	GameObject daysclone;
	GameObject datelementsclone;
	GameObject calendartitle;
	GameObject [] dayobj = new GameObject[7];
	GameObject [] dateobj = new GameObject[7*6];

	int currentyear = - 1;
	int currentmonth;
	int currentday;
	int startindex = 0;
	int daysInMonth;

	public int selectedyear = - 1;
	public int selectedmonth;
	public int selectedday;
	public GameObject myself = null;
	public bool oneselected = false;


    // Ne Pas utiliser Awake mais PIAwake pour l'init
	void Awake()
	{
		oneselected = false;
		myself = gameObject;
		if (selectedyear == -1)
		{
			selectedyear = DateTime.Now.Year;	
			selectedmonth = DateTime.Now.Month;	
			selectedday = DateTime.Now.Day;	
		}
		currentyear = selectedyear;
		currentmonth = selectedmonth;
		currentday = selectedday;

		calendartitle = gameObject.FindInChildren("calendartitle");

		days = gameObject.FindInChildren("days");
		datelements = gameObject.FindInChildren("datelements");
		daysclone = days.FindInChildren("clone");
		datelementsclone = datelements.FindInChildren("clone");

		Image cmptImg = gameObject.FindInChildren("intopbar").GetComponent<Image>();
        cmptImg.color = colortopbar;

        Text cmptTxt = calendartitle.GetComponent<Text>();
        cmptTxt.color = colortopbartext;

        cmptImg = gameObject.FindInChildren("left").GetComponent<Image>();
        cmptImg.color = colorbuttons;
        cmptImg = gameObject.FindInChildren("left").FindInChildren("arrow").GetComponent<Image>();
        cmptImg.color = colorbuttonstext;

        cmptImg = gameObject.FindInChildren("right").GetComponent<Image>();
        cmptImg.color = colorbuttons;
        cmptImg = gameObject.FindInChildren("right").FindInChildren("arrow").GetComponent<Image>();
        cmptImg.color = colorbuttonstext;

        cmptImg = gameObject.FindInChildren("accept").GetComponent<Image>();
        cmptImg.color = colorbuttons;
        cmptTxt = gameObject.FindInChildren("accept").FindInChildren("text").GetComponent<Text>();
        cmptTxt.color = colorbuttonstext;

        cmptImg = gameObject.FindInChildren("panel").GetComponent<Image>();
        cmptImg.color = colorbackground;

		//Image dimg = days.GetComponent<Image>();
        Image dimg = gameObject.FindInChildren("bgdays").GetComponent<Image>();
		dimg.color = colorbehinddaynames;

		for (int i=0;i<7;i++)
		{
			if (i == 0)
				dayobj[i] = daysclone;
			else
				dayobj[i] = GameObject.Instantiate(daysclone);
			dayobj[i].name = "day"+i;
			dayobj[i].transform.SetParent(days.transform);
			dayobj[i].transform.localScale = Vector3.one;
			Text tm = dayobj[i].FindInChildren("text").GetComponent<Text>();
			tm.color = colordaystext;
			Image im = dayobj[i].GetComponent<Image>();
			im.color = colordays;

			switch (i)
			{
			case 0:				tm.text = localization.GetTextWithKey("MONDAY_SHORT_TEXT");				break;
			case 1:				tm.text = localization.GetTextWithKey("TUESDAY_SHORT_TEXT");				break;
			case 2:				tm.text = localization.GetTextWithKey("WEDNESDAY_SHORT_TEXT");				break;
			case 3:				tm.text = localization.GetTextWithKey("THURSDAY_SHORT_TEXT");				break;
			case 4:				tm.text = localization.GetTextWithKey("FRIDAY_SHORT_TEXT");				break;
			case 5:				tm.text = localization.GetTextWithKey("SATURDAY_SHORT_TEXT");				break;
			case 6:				tm.text = localization.GetTextWithKey("SUNDAY_SHORT_TEXT");				break;
			}
		}


		for (int i=0;i<7*6;i++)
		{
			if (i == 0)
				dateobj[i] = datelementsclone;
			else
				dateobj[i] = GameObject.Instantiate(datelementsclone);
			dateobj[i].name = "date"+i;
			dateobj[i].transform.SetParent(datelements.transform);
			dateobj[i].transform.localScale = Vector3.one;
		}
		CreateCallendar(currentday,currentmonth,currentyear);
		myself.SetActive(false);

	}

	void CreateCallendar(int day,int month, int year)  
	{
		// clear all
		for (int i=0;i<7*6;i++)
		{
			Text tm = dateobj[i].FindInChildren("text").GetComponent<Text>();
			tm.text = "";
			tm.color = Color.white;
			Image im = dateobj[i].GetComponent<Image>();
			im.color = new Color(0.5f,0.5f,0.5f,0.0f);
		}

		DateTime DateTrick= new DateTime();
		DateTrick=System.Convert.ToDateTime("11/01/" + year);
		DayOfWeek firstday;
		if (DateTrick.Month==1)
		{
			firstday = System.Convert.ToDateTime("01/" + month + "/" + year).DayOfWeek;
		}
		else
		{
			firstday = System.Convert.ToDateTime(month + "/01" + "/" + year).DayOfWeek;	
		}	

		switch (firstday)
		{
		case DayOfWeek.Monday:
			startindex = 0;
			break;
		case DayOfWeek.Tuesday:
			startindex = 1;
			break;
		case DayOfWeek.Wednesday:
			startindex = 2;
			break;
		case DayOfWeek.Thursday:
			startindex = 3;
			break;
		case DayOfWeek.Friday:
			startindex = 4;
			break;
		case DayOfWeek.Saturday:
			startindex = 5;
			break;
		case DayOfWeek.Sunday:
			startindex = 6;
			break;
		}
		daysInMonth = System.DateTime.DaysInMonth(year, month);
		int daycount = 1;
		for (int i=startindex;i<startindex+daysInMonth;i++)
		{
			Text tm = dateobj[i].FindInChildren("text").GetComponent<Text>();
			tm.text = daycount.ToString();
			Image im = dateobj[i].GetComponent<Image>();

			if ((daycount == selectedday) && (month == selectedmonth) && (year == selectedyear))
			{
				im.color = colorselectday;
				tm.color = colorselectdaytext;
			}
			else
			{
				im.color = colorday;
				tm.color = colordaytext;
			}

			daycount++;
		}
		// setmonthname
		Text calendartitletm = calendartitle.GetComponent<Text>();

		;

		switch (month)
		{
		case 1 :			calendartitletm.text = localization.GetTextWithKey("JAN_TEXT") + " " + year;			break;
		case 2 :			calendartitletm.text = localization.GetTextWithKey("FEB_TEXT") + " " + year;			break;
		case 3 :			calendartitletm.text = localization.GetTextWithKey("MAR_TEXT") + " " + year;			break;
		case 4 :			calendartitletm.text = localization.GetTextWithKey("APR_TEXT") + " " + year;			break;
		case 5 :			calendartitletm.text = localization.GetTextWithKey("MAY_TEXT") + " " + year;			break;
		case 6 :			calendartitletm.text = localization.GetTextWithKey("JUN_TEXT") + " " + year;			break;
		case 7 :			calendartitletm.text = localization.GetTextWithKey("JUL_TEXT") + " " + year;			break;
		case 8 :			calendartitletm.text = localization.GetTextWithKey("AUG_TEXT") + " " + year;			break;
		case 9 :			calendartitletm.text = localization.GetTextWithKey("SEP_TEXT") + " " + year;			break;
		case 10 :			calendartitletm.text = localization.GetTextWithKey("OCT_TEXT") + " " + year;			break;
		case 11 :			calendartitletm.text = localization.GetTextWithKey("NOV_TEXT") + " " + year;			break;
		case 12 :			calendartitletm.text = localization.GetTextWithKey("DEC_TEXT") + " " + year;			break;
		}
	}


	// Update is called once per frame
	void Update () {
	
	}

	void OnEnable()
	{
		RectTransform rec = days.GetComponent<RectTransform>();
		float width = (rec.rect.width - 24.0f) / 7.0f;
		Vector2 newSize = new Vector2(width, days.GetComponent<RectTransform>().rect.height);
		days.GetComponent<GridLayoutGroup>().cellSize = newSize;

		rec = datelements.GetComponent<RectTransform>();
		width = (rec.rect.width - 24.0f) / 7.0f;
		float height = (rec.rect.height - 20.0f) / 6.0f;
		newSize = new Vector2(width, height);
		datelements.GetComponent<GridLayoutGroup>().cellSize = newSize;

		oneselected=false;
		CreateCallendar(currentday,currentmonth,currentyear);
	}

	public void PressedMonthUp()
	{
		currentmonth++;
		if (currentmonth == 13)
		{
			currentmonth = 1;
			currentyear ++;
		}
		CreateCallendar(currentday,currentmonth,currentyear);
	}

	public void PressedMonthDown()
	{
		currentmonth--;
		if (currentmonth == 0)
		{
			currentmonth = 12;
			currentyear --;
		}
		CreateCallendar(currentday,currentmonth,currentyear);
	}

	public void PressedItem()
	{
		int nr = 0;
		string strnr = EventSystem.current.currentSelectedGameObject.name;
		strnr = strnr.Replace("date","");
		int.TryParse(strnr, out nr);
		nr -= startindex;
		if (nr < 0)		return;
		if (nr >= daysInMonth)		return;
		currentday = nr+1;
		selectedday = currentday;
		selectedmonth = currentmonth;
		selectedyear = currentyear;
		CreateCallendar(currentday,currentmonth,currentyear);
	}

	public void ExitCalendar()
	{
		oneselected = true;
		gameObject.SetActive(false);
	}

	public void ShowCalendar(DateTime date, RectTransform rectOwner)
	{
		currentyear = date.Year; 
		currentmonth = date.Month;
		currentday = date.Day;

        selectedyear = currentyear;   
        selectedmonth = currentmonth; 
        selectedday = currentday; 

		myself.SetActive(true);
	}

    public void ShowCalendar(DateTime date)
    {
        ShowCalendar(date, null);
    }

    public void HideCalendar( )
    {
        myself.SetActive(false);
    }

    public bool CalendarIsShow( )
    {
        return myself.activeInHierarchy;
    }

	public bool CalendarDateIsSelected(out DateTime date)
	{
		bool bOK = false; //myself.activeInHierarchy;

        if( myself != null )
            bOK = !myself.activeInHierarchy;
            
		date = DateTime.MinValue;

        if (bOK && oneselected)
		{
			date = new DateTime(currentyear, currentmonth, currentday, 00, 00, 00, 000);
		}

		return bOK;
	}

}
