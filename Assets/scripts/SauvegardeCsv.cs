﻿using UnityEngine;
using System.Collections;

public class SauvegardeCsv :  buttonPopup{

	public override void ButtonPressed ()
	{
		base.ButtonPressed();
		// verify ik Link completed
		StartCoroutine("SaveCSV");
	}
	
	IEnumerator SaveCSV()
	{
        pp_produit prod = pp_manager.GetCurrentProduit();

        if (prod != null)
        {
            pp_task task_save = prod.SaveData();

            if (task_save != null)
            {
                while( !task_save.IsFinished )
                    yield return new WaitForSeconds(0.05f);
            }
        }
	}

	/*void Update(){


		buttonswap.myactive = false;
		buttonPopup.myactive = true;
		GameObject.Find ("Popup").SetActive (true);

	}*/
}
