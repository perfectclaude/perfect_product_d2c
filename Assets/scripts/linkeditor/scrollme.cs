﻿using UnityEngine;
using System.Collections;

public class scrollme : MonoBehaviour
{
	
	public Camera 	m_ViewCamera;
	public float 	maxyval = 5.0f;

    private bool    wasIpressed = false;
    private Vector3 startpos;
	private float	inertie = 0.0f;
	private bool	icanmove = false;

	void Awake ()
	{
		m_ViewCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
	}
	
	void Update ()
	{
		if (Input.GetMouseButtonDown(0))
		{
			RaycastHit hitInfo;
			Ray ray;
			
			ray = m_ViewCamera.ScreenPointToRay(Input.mousePosition);
			if (gameObject.GetComponent<Collider>().Raycast(ray, out hitInfo, 1.0e8f))
			{
				wasIpressed = true;
				startpos = m_ViewCamera.ScreenToWorldPoint(Input.mousePosition);
				inertie = 0.0f;
				icanmove = false;

                if (gameObject.transform.parent.name == "anchorleft")
				    linkeditor.scrollingLeft = true;
                else
                    linkeditor.scrollingRigth = true;
			}
		}
		if (wasIpressed)
		{
			// Drag the BG here
			Vector3 vec = m_ViewCamera.ScreenToWorldPoint(Input.mousePosition);
			float diffy = vec.y - startpos.y;
			if (Mathf.Abs(diffy) > 0.022f)				icanmove = true;
			if (icanmove)
			{
				if (gameObject.transform.parent.name == "anchorleft")
				{
					linkeditor.scroll3Ditem.SetVisible(false);
				}
				else
				{
					linkeditor.scrollbomitem.SetVisible(false);
				}

				startpos = vec;
				vec = gameObject.transform.localPosition;
				vec.y += diffy;
				inertie = diffy;
				if (vec.y < 0)		vec.y = 0;
				if (vec.y > maxyval)		vec.y = maxyval;
				gameObject.transform.localPosition = vec;
			}
		}
		else
		{
			if (inertie != 0.0f)
			{
				if (inertie > 0.3f)		inertie = 0.3f;
				if (inertie < -0.3f)	inertie = -0.3f;
				Vector3 vec = gameObject.transform.localPosition;
				vec.y += inertie;
				if (vec.y < 0)		vec.y = 0;
				if (vec.y > maxyval)		vec.y = maxyval;
				gameObject.transform.localPosition = vec;
				inertie = inertie * 0.91f;
				if (Mathf.Abs(inertie) < 0.01f)
				{
					inertie = 0.0f;
				}
			}

            if (inertie == 0.0f)
            {
                if (gameObject.transform.parent.name == "anchorleft")
                    linkeditor.scrollingLeft = false;
                else
                    linkeditor.scrollingRigth = false;
            }
		}

		if (Input.GetMouseButtonUp (0))
		{
			wasIpressed = false;
		}
	}
}
