﻿using UnityEngine;
using System;
using System.IO;
using System.Text;
using System.Collections;
using MiniJSON;
using System.Text.RegularExpressions;
using System.Globalization;
using BestHTTP;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.Security;
using System.Threading;

public class linkeditor : MonoBehaviour
{
	public static bool jsonLoader;
    public static List<GameObject>	all3Dobjects = null;
    public static List<GameObject>  allComposants = null;
	GameObject			anchorleft;
	GameObject			anchorright;
	GameObject			cloneleft;
	GameObject			cloneright;

	public static bool	scrollingLeft = false;
    public static bool  scrollingRigth = false;

	public static GameObject			popuprequest;
	public static bool					retpopuprequest = false;
	

	public static GameObject selectbomitem = null;
	public static GameObject scrollbomitem = null;
	public static GameObject scroll3Ditem = null;
	public static GameObject select3Ditem = null;

	tk2dTextMesh		reportingtext = null;

    private pp_produit _curProd = null;

	void Awake ()
	{
        _curProd = pp_manager.GetCurrentProduit();

		selectbomitem = GameObject.Find("selectbomitem");
		scrollbomitem = GameObject.Find("scrollbomitem");
		scroll3Ditem = GameObject.Find("scroll3Ditem");
		select3Ditem = GameObject.Find("select3Ditem");

		reportingtext = (tk2dTextMesh)GameObject.Find ("reportingtext").FindInChildren("text").GetComponent<tk2dTextMesh>();
        settext.ForceText(reportingtext,"LOADING2_TEXT");

		jsonLoader = false;

		popuprequest = GameObject.Find("popuprequest");
		popuprequest.SetActive(false);
		anchorleft = gameObject.FindInChildren("anchorleft").FindInChildren("scroller");
		anchorright = gameObject.FindInChildren("anchorright").FindInChildren("scroller");
		cloneleft = anchorleft.FindInChildren("toclone");
		cloneright = anchorright.FindInChildren("toclone");
		cloneleft.SetActive(false);
		cloneright.SetActive(false);


		selectbomitem.SetVisible(true);
		scrollbomitem.SetVisible(true);
		scroll3Ditem.SetVisible(false);
		select3Ditem.SetVisible(false);
	}


    IEnumerator Start()
    {
        bool bWaitReady = true;

        while (bWaitReady)
        {
            pp_produit prod = pp_manager.GetCurrentProduit();

            if ((prod == null) || prod.IsLoading || !(prod.IsLoaded || prod.IsError))
                yield return new WaitForSeconds(0.1f);
            else
            {
                _curProd = prod;
                bWaitReady = false;
            }
        }

        GameObject father = gameObject.transform.parent.gameObject;
        while (father.FindInChildren("*_center") == null)       // not the 3D object yet
        {
            yield return new WaitForSeconds(0.5f);
        }

        CreateAllButtons();
    }
        

	void CreateAllButtons()
	{
        Vector3 posit;
        all3Dobjects = new List<GameObject>();
        allComposants = new List<GameObject>();


        // Create all Right Button 
		posit = new Vector3(0,-1.0f,0);
        int level = 0;
        foreach(pp_produit_composant n in _curProd.roothierarchie.childs)
        {
            CreateRightButton(n, ref level, ref posit);
        }


        // create all 3D objects
       

        GameObject root3d = gameObject.transform.parent.gameObject.FindInChildren("*_center");

        posit = new Vector3(0,-1.0f,0);
        foreach (Transform child in root3d.GetComponentsInChildren<Transform>(true))
        {
            CreateLeftButton(child.name, ref posit);
        }
            

		GameObject center = GameObject.Find ("center");
		Vector3 vec = center.transform.localPosition;
		vec.x = -63;
		vec.z = 580;
		center.transform.localPosition = vec;
		vec = center.transform.eulerAngles;
		vec.x = 270;
		center.transform.eulerAngles = vec;

//		camobj.SetVisible(true);

        jsonLoader = true;
	}

    void CreateRightButton(pp_produit_composant comp, ref int level, ref Vector3 posit)
	{

		posit.x =level * -0.05f;
		GameObject tmp = GameObject.Instantiate(cloneright) as GameObject;
        tmp.name = comp.Description;
		tmp.transform.parent = anchorright.transform;
		tmp.transform.localEulerAngles = Vector3.zero;
		tmp.transform.localScale = Vector3.one;
		tmp.transform.localPosition = posit;
		tk2dTextMesh tm = (tk2dTextMesh)tmp.FindInChildren("text").GetComponent<tk2dTextMesh>();
        settext.ForceText(tm,tmp.name);

        button_selectcsv selcsv = (button_selectcsv)tmp.GetComponent<button_selectcsv>();
        selcsv.init(comp);

        if (comp.childs.Count > 0)
        {
            tk2dSlicedSprite slsprt = (tk2dSlicedSprite)tmp.GetComponent<tk2dSlicedSprite>();
            slsprt.color = Color.blue;
            GameObject mytext = tmp.FindInChildren("texture");
            mytext.SetActive(false);

            // pas de selection possible sur les elements avec Fils
            Destroy(selcsv);
        }
        else
        {
            
        }

        allComposants.Add(tmp);

		tmp.SetActive(true);
		posit.y -= 0.13f;
		
   
		level++;
        foreach(pp_produit_composant n in comp.childs)
		{
            CreateRightButton(n, ref level, ref posit);
		}
		level--;
	}

    void CreateLeftButton(string name, ref Vector3 posit)
    {
        GameObject tmp = null;

        if (name.Contains("#_"))
        {
            tmp = GameObject.Instantiate(cloneleft) as GameObject;
            tmp.name = name.Replace("#_", "");
            tmp.transform.parent = anchorleft.transform;
            tmp.transform.localEulerAngles = Vector3.zero;
            tmp.transform.localScale = Vector3.one;
            tmp.transform.localPosition = posit;
            tk2dTextMesh tm = (tk2dTextMesh)tmp.FindInChildren("text").GetComponent<tk2dTextMesh>();
            settext.ForceText(tm, tmp.name);

            button_select3d script = (button_select3d)tmp.GetComponent<button_select3d>();
            pp_produit_obj obj = _curProd.FindObj(tmp.name);
            script.init(obj);

            all3Dobjects.Add(tmp);

            tmp.SetActive(true);

            posit.y -= 0.13f;
        }
    }

	void Update ()
	{
        if (_curProd != null)
        {
            if (_curProd.VerifyValidityObjs())
                settext.ForceText(reportingtext, "BOM_LINK_COMPLETED_TEXT");
            else
                settext.ForceText(reportingtext, "BOM_LINK_INCOMPLETED_TEXT");
        }
	}
        

   

	

	public static void DisplayRightObjectElements()
	{
        GameObject rightTopAnchor = GameObject.Find("RightTopAnchor");

        foreach( GameObject go in linkeditor.all3Dobjects )
		{
            GameObject threed = GameObject.Find("center").FindInChildren(go.name);
			threed.SetVisible(false);

            button_select3d item = (button_select3d)go.GetComponent<button_select3d>();


            if( (item.MyAttribution != null) && (button_selectcsv.SelectedButton == item.MyAttribution) )
			{
                GameObject scrText = null;
                Renderer srcRend = null;
                Texture srcTexture = null;

                if( rightTopAnchor != null )
                    scrText = rightTopAnchor.FindInChildren("texture" + item.WhatTexture.ToString());

                if( scrText != null )
                    srcRend = scrText.GetComponent<Renderer>();

                if (srcRend != null)
                    srcTexture = srcRend.material.GetTexture("_MainTex");
                
                if (srcTexture != null)
                {
                    Renderer rend = (Renderer)threed.GetComponent<Renderer>();
                    if (rend != null)
                    {
                        rend.material.mainTexture = srcTexture;
                        rend.material.SetTexture("_MainTex", srcTexture);
                    }
                }

                threed.SetVisible(true);
			}        
		}

	}


}
