﻿using UnityEngine;
using System.Collections;

public class button_selectcsv :  buttonswap
{
    private static button_selectcsv _selectedButton = null;

    private int _myTexture = 0;
    private tk2dSlicedSprite _mySprt;

    private pp_produit_composant _composant = null;
    private bool _onInit = false;
    private bool _forceRefresh = false;
	GameObject backgroundline;

    public void init( pp_produit_composant composant )
    {
        _onInit = true;

        _composant = composant;

        if( _composant != null )
        {
            if (_composant.Objs.Count > 0)
                _myTexture = _composant.Objs[0].Texture;
            
            foreach( GameObject go in linkeditor.all3Dobjects )
            {
                button_select3d script = go.GetComponent<button_select3d>();
                if( (script != null) && (script.Composant == _composant) )
                {
                    script.MyAttribution = this;
                    break;
                }
            }
        }

        _forceRefresh = true;

        _onInit = false;
    }

    // Getter / Setter
    public pp_produit_composant Composant
    {
        get 
        { 
            return _composant;
        }
    }

    public int WhatTexture
    {
        get 
        { 
            return _myTexture;
        }
        set 
        { 
            _myTexture = value;

            logger.LogDebug("button_selectcsv", "WhatTexture", "SET " + this.name);

            if (_composant != null)
            {
                foreach (pp_produit_obj obj in _composant.Objs)
                {
                    obj.Texture = _myTexture;
                }
            }

            MajButtonTexture();
        }
    }

    public static button_selectcsv SelectedButton
    {
        get 
        { 
            return _selectedButton;
        }
        set 
        { 
            logger.LogDebug("button_selectcsv", "SelectedButton", "SET" );
            _selectedButton = value;
        }
    }   
  
	void Awake()
	{
		backgroundline = GameObject.Find("ToMainApp_return").FindInChildren("backgroundline");
		_Awake();
        _mySprt = (tk2dSlicedSprite)gameObject.GetComponent<tk2dSlicedSprite>();
	}
        


    private void MajButtonTexture()
    {
        GameObject rightTopAnchor = GameObject.Find("RightTopAnchor");
        GameObject myText = gameObject.FindInChildren("texture" );

        GameObject scrText = null;
        Renderer srcRend = null;
        Renderer myRend = null;

        if (rightTopAnchor != null)
            scrText = rightTopAnchor.FindInChildren("texture" + _myTexture.ToString());

        if( scrText != null )
            srcRend = scrText.GetComponent<Renderer>();

        if( myText != null )
            myRend = myText.GetComponent<Renderer>();


        if ((srcRend != null) && (myRend != null))
            myRend.material.SetTexture("_MainTex", srcRend.material.GetTexture("_MainTex"));
        else
            _forceRefresh = true;

    }

   

	void Update()
	{
		if (gameObject.transform.position.y <= backgroundline.transform.position.y)
            _Update();

        Color col = Color.white;

        if (SelectedButton == this)
            col = Color.green;

        if( _mySprt != null)
            _mySprt.color  = col;

        if (_forceRefresh)
        {
            _forceRefresh = false;

            MajButtonTexture();
        }
	}

	public override void ButtonPressed ()
	{
		
		base.ButtonPressed();

		if (linkeditor.popuprequest.activeInHierarchy)		return;

        SelectedButton = this;

		linkeditor.selectbomitem.SetVisible(false);
		linkeditor.scroll3Ditem.SetVisible(true);
		linkeditor.select3Ditem.SetVisible(true);
	}

}
