﻿using UnityEngine;
using System.Collections;

public class button_selecttexture : buttonswap
{
	public int mytextureid = 0;
	Texture mytexture;

	void Awake()
	{
		_Awake();
		Renderer srcrend = (Renderer)gameObject.GetComponent<Renderer>();
		mytexture = srcrend.material.GetTexture("_MainTex");
	}

	public override void ButtonPressed ()
	{
		base.ButtonPressed();

        button_selectcsv script = button_selectcsv.SelectedButton;
        if (script != null)
        {
            script.WhatTexture = mytextureid;
        }
	}
}