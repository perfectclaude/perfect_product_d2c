﻿using UnityEngine;
using System.Collections;

public class button_select3d : buttonswap
{
    private        button_selectcsv _myAttribution = null;
    private        button_selectcsv _lastSelectedButton = null;
	private        tk2dSlicedSprite _mySprt;
	private        GameObject		 _myLine = null;
    private static GameObject       _cloneImage = null;
	


    private int _lastWhatTexture = 0;

    private pp_produit_obj _obj = null;
    private bool _onInit = false;
    private bool _forceRefresh = false;
   
	GameObject backgroundline;

    public void init( pp_produit_obj obj )
    {
        _onInit = true;

        _obj = obj;
        _myAttribution = null;

        if( (_obj != null) && (_obj.Composant != null) )
        {
            foreach( GameObject goComp in linkeditor.allComposants )
            {
                button_selectcsv script = goComp.GetComponent<button_selectcsv>();
                if( (script != null) && (script.Composant == _obj.Composant) )
                {
                    _myAttribution = script;
                    break;
                }
            }
        }

        _forceRefresh = true;

        _onInit = false;
    }

    // Getter / Setter
    public pp_produit_composant Composant
    {
        get 
        { 
            pp_produit_composant comp = null;

            if (_obj != null)
            {
                comp = _obj.Composant;
            }

            return comp;
        }
    }

    public button_selectcsv MyAttribution
    {
        get
        {
            return _myAttribution;
        }
        set
        {
            _myAttribution = value;

            if (_obj != null)
            {
                if (_myAttribution != null)
                {
                    _obj.Composant = _myAttribution.Composant;
                    _obj.Texture = _myAttribution.WhatTexture;
                }
                else
                {
                    _obj.Composant = null;
                    _obj.Texture = 0;
                }
            } 

            _forceRefresh = true;
        }
    }

    public int WhatTexture
    {
        get 
        { 
            int texture = 0;

            if (_obj != null)
            {
                texture = _obj.Texture;
            }

            return texture;
        }
    }

	void Awake()
	{
		backgroundline = GameObject.Find("ToMainApp_return").FindInChildren("backgroundline");
		_Awake();
        _mySprt = (tk2dSlicedSprite)gameObject.GetComponent<tk2dSlicedSprite>();

        GameObject cloneimage = GameObject.Find("cloneline");
        if( cloneimage != null )
        {
            cloneimage.SetActive(false);
            _cloneImage = cloneimage;
        }
	}

	void Update()
	{
		button_selectcsv selectedButton = button_selectcsv.SelectedButton;
        int whatTexture = WhatTexture;

        if (gameObject.transform.position.y <= backgroundline.transform.position.y)  
		    _Update ();


        if (_lastSelectedButton != selectedButton)
        {
            _lastSelectedButton = selectedButton;
            _forceRefresh = true;
        }

        if (_lastWhatTexture != whatTexture)
        {
            _lastWhatTexture = whatTexture;
            _forceRefresh = true;
        }

        Color col = Color.white;
        if( MyAttribution != null )
        {
            if (selectedButton == MyAttribution )
    		    col = Color.green;
            else
                col = new Color(1,0,1);
        }

        if( _mySprt != null)
            _mySprt.color  = col;


        if( linkeditor.scrollingLeft || linkeditor.scrollingRigth || _forceRefresh )
        {
            if( _forceRefresh )
                linkeditor.DisplayRightObjectElements();
            
            _forceRefresh = false;

            DestroyLine(_myLine);

            if (MyAttribution != null)
            {
                Vector3 source = Vector3.zero;
                Vector3 target = MyAttribution.transform.position - gameObject.transform.position;
                _myLine = TraceLine(0,gameObject, col,source,target);
            }
        }
	}

	public void UpdateLine()
	{
        DestroyLine(_myLine);

        if (MyAttribution != null)
		{
			Vector3 source = Vector3.zero;
            Vector3 target = MyAttribution.transform.position - gameObject.transform.position;
            _myLine = TraceLine(0,gameObject, new Color(1,0,1),source,target);
		}
	}
	
	public override void ButtonPressed ()
	{
		base.ButtonPressed();
		if (linkeditor.popuprequest.active) return;


        if (button_selectcsv.SelectedButton == MyAttribution)
            MyAttribution = null;

        else if( MyAttribution == null)
            MyAttribution = button_selectcsv.SelectedButton;
        
        else
            StartCoroutine("WaitConfirmation");

		linkeditor.select3Ditem.SetVisible(false);
	}

	IEnumerator WaitConfirmation()
	{
		linkeditor.popuprequest.SetActive(true);
		while (linkeditor.popuprequest.active)
			yield return new WaitForSeconds(0.05f);
        
		if (linkeditor.retpopuprequest)
		{
            MyAttribution = button_selectcsv.SelectedButton;
		}
	}





    private GameObject TraceLine(int itemid,GameObject father, Color col, Vector3 vec1, Vector3 vec2)
    {
        Texture mytexture = _cloneImage.GetComponent<Renderer>().material.mainTexture;
        GameObject newobj = GameObject.Instantiate(_cloneImage) as GameObject;
        vec1.x += 0.36f;
        vec2.x -= 0.36f;
        newobj.name = "curveline"+itemid;
        newobj.transform.parent = father.transform;
        newobj.transform.localPosition = new Vector3(0,0,0.2f);
        newobj.transform.localScale = father.transform.localScale;
        CreateNewLine(newobj,col,vec1,vec2);
        newobj.SetActive(true);

        newobj.GetComponent<Renderer>().sharedMaterial = new Material(Shader.Find("Sprites/Default"));
        newobj.GetComponent<Renderer>().sharedMaterial.mainTexture = mytexture;

        return (newobj);
    }

    private void CreateNewLine(GameObject obj,Color color,Vector3 vec1,Vector3 vec2)
    {
        Vector3[]    vertex         = new Vector3    [4];
        int[]         triangle     = new int        [3*2];
        Vector2[]    uv            = new Vector2    [4];        
        Color[]        col            = new Color        [4];
        float        decal  = 0.01f;        // WIDTH !!!

        if (Mathf.Abs(vec1.x - vec2.x) > Mathf.Abs(vec1.y - vec2.y))        // horizontal
        {
            vertex[0] = new Vector3(vec1.x,vec1.y-decal,0);
            vertex[1] = new Vector3(vec2.x,vec2.y-decal,0);
            vertex[2] = new Vector3(vec1.x,vec1.y+decal,0);
            vertex[3] = new Vector3(vec2.x,vec2.y+decal,0);
        }
        else
        {
            vertex[0] = new Vector3(vec1.x-decal,vec1.y,0);
            vertex[1] = new Vector3(vec1.x+decal,vec1.y,0);
            vertex[2] = new Vector3(vec2.x-decal,vec2.y,0);
            vertex[3] = new Vector3(vec2.x+decal,vec2.y,0);
        }

        col[0] = col[1] = col[2] = col[3] = color;

        uv[0]        = new Vector2(0,0);
        uv[1]        = new Vector2(1,0);
        uv[2]        = new Vector2(0,1);
        uv[3]        = new Vector2(1,1);

        triangle[0] = 0;
        triangle[1] = 2;
        triangle[2] = 1;
        triangle[3] = 2;
        triangle[4] = 3;
        triangle[5] = 1;

        Mesh m = new Mesh();

        m.vertices  = vertex;
        m.colors    = col;
        m.uv        = uv;
        m.triangles    = triangle;

        m.RecalculateNormals();
        m.RecalculateBounds();

        obj.GetComponent<MeshFilter>().sharedMesh = m;
    }

    private void DestroyLine(GameObject obj)
    {
        if (obj != null)
        {
            Mesh     omesh = obj.GetComponent<MeshFilter>().sharedMesh;
            Material omat = obj.GetComponent<Renderer>().sharedMaterial;

            if(omesh != null)
                Destroy(omesh);

            if (omat != null)
                Destroy(omat);

            Destroy(obj);
        }
    }

}
