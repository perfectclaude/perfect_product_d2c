﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;

public class createstreamfilenames : MonoBehaviour
{
	IEnumerator Start()
	{
		#if UNITY_EDITOR 
		GetDirs();
		#else
		#endif
		//if (!PlayerPrefs.HasKey("Copied files"))
		//{
			
			string [] allfiles = null;
			string source =  System.IO.Path.Combine(Application.streamingAssetsPath,"dir.txt");
			#if UNITY_EDITOR || UNITY_STANDALONE
            allfiles = File.ReadAllText(Application.streamingAssetsPath+"/dir.txt").Split(new char[] {';'}, StringSplitOptions.RemoveEmptyEntries);
			#else
			WWW www = new WWW(source);
			// get dir and copy all
			yield return www;
			if (string.IsNullOrEmpty(www.error))
			{
			Debug.Log(www.text);

			allfiles = www.text.Split(';');
			}
			#endif


			if (allfiles != null)
			{
			
				foreach (string fname in allfiles)
				{
//					Debug.Log(">>>"+fname);
					if (!fname.Contains(".meta"))
					{
						string target = fname.Replace("Assets/StreamingAssets",Application.persistentDataPath);
						source = System.IO.Path.Combine(Application.streamingAssetsPath,fname.Replace("Assets/StreamingAssets/",""));

						//					string source = fname.Replace("Assets/StreamingAssets/",Application.streamingAssetsPath);
//						Debug.Log("Creating dir : "+target.Substring(0,target.LastIndexOf("/")));
						Directory.CreateDirectory(target.Substring(0,target.LastIndexOf("/")));

						#if UNITY_EDITOR || UNITY_STANDALONE
						File.Copy(source,target, true);
						
						#else
						WWW mywww = new WWW(source);
						yield return mywww;
						if (string.IsNullOrEmpty(mywww.error))
						{
						File.WriteAllBytes(target,mywww.bytes);
						}
						#endif
					}
				}
		
			PlayerPrefs.SetInt("Copied files",1);
		}
		yield return null;

	}

	void GetDirs()
	{
		string stpath = "Assets/StreamingAssets";

		string [] testlist = System.IO.Directory.GetFiles(stpath,"*.*",SearchOption.AllDirectories);
		string finalsave = "";
		for (int i=0;i<testlist.Length;i++)
		{
			testlist[i] = testlist[i].Replace("\\","/");
			//			testlist[i] = testlist[i].Substring(testlist[i].LastIndexOf("/")+1);
			finalsave += testlist[i] + ";";
		}

		File.WriteAllText(stpath + "/dir.txt",finalsave);
	}
}
