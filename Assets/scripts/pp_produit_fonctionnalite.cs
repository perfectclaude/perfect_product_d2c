﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class pp_produit_fonctionnalite
{
    private string _titre = "";
    private float _pourcent = 0;
    private pp_produit_composant _composant = null;

    private object _syncRoot = new System.Object();
    private ppconvert _convert = null;


    public pp_produit_fonctionnalite( string titre, float pourcent, pp_produit_composant comp )
    {
        _convert = comp.Convert;
        _titre = titre;
        _pourcent = pourcent;
        _composant = comp;
    }


    // Getter / Setter data
    public string Titre
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert.ToString(_titre);
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _convert.TryParse(value, out _titre);
            }
        }
    }


    private float PourcentSumChild()
    {
        float pourcent = 0;

        if ((_composant == null) || (_composant.childs.Count <= 0))
        {
            pourcent = _pourcent;
        }
        else // Sinon c'est la somme des pourcentage des fils pour la fonctionnalité en question
        {
            int index = _composant.PourcentageFonctionalites.IndexOf(this);
            float comp_price = _composant.UnitPrice_Val - _composant.Process_Cost_Val;


            // On retire les composants qui n'ont aucune fonctionnalité du prix (donc les fonctionnalités qui n'iterviennent pas sur le prix)
            foreach (pp_produit_composant comp in _composant.childs)
            {
                bool foncOK = false;

                foreach (pp_produit_fonctionnalite fonc in comp.PourcentageFonctionalites)
                {
                    if( fonc.Pourcent_Val > 0 )
                    {
                        foncOK = true;
                        break;
                    }  
                }
              
                if( !foncOK )
                    comp_price -= comp.UnitPrice_Val;      
            }


            if (comp_price != 0)
            {
                foreach (pp_produit_composant comp in _composant.childs)
                {
                    if ((index >= 0) && (index < comp.PourcentageFonctionalites.Count))
                    {
                        if( comp.PourcentageFonctionalites[index].Pourcent_Val > 0 )
                            pourcent += (comp.PourcentageFonctionalites[index].Pourcent_Val * comp.UnitPrice_Val) / comp_price;
                    }
                }
            }
        }

        return pourcent;
    }

    public float Pourcent_Val
    {
        get 
        { 
            lock (_syncRoot)
            {
                return PourcentSumChild();
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                if( (_composant == null) || (_composant.childs.Count <= 0) )
                    _pourcent = value;
            }
        }
    }


    public string Pourcent
    {
        get 
        {
            lock (_syncRoot)
            {
                return _convert.ToString(PourcentSumChild(), 0);
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                if( (_composant == null) || (_composant.childs.Count <= 0) )
                    _convert.TryParse(value, out _pourcent);
            }
        }
    }

    public ppconvert Convert
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _convert;
            }
        }
    }
}
