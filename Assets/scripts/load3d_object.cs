using UnityEngine;
using System;
using System.IO;
using System.Text;
using System.Collections;
using MiniJSON;
using System.Text.RegularExpressions;
using BestHTTP;
using System.Collections.Generic;
using System.Security;
using System.Threading;

public class load3d_object : MonoBehaviour
{
	public string rootfilename = null;

	public Camera camClone;

	public GameObject objectroot = null;
	public float decalx = 0.0f;
	public int layernr = 8;
	public string DebugGUI="Debug";
	GUIStyle styleLabel;
	
	public static GameObject objectcenter = null;
	public static GameObject newpanelfather = null;

	static GameObject zoompoint = null;

	public static tk2dTextMesh loadingwindowtext = null;
	public static GameObject loadingwindow = null;
	GameObject clonebase = null;
	GameObject clonepanel = null;

	GameObject clone;
	Camera 	   m_ViewCamera;

	public bool earlyskip = false;
	public static string fathername = "";

    private bool _init = false;


    private pp_produit_vars _prod = null;

	public static bool cloneActif;
	public static string scenarioActuel;

	public static GameObject cloneObjet;

	public static int scenObjet;
	public static int scenClone;

	public static bool canActivateCloneActive;
	 

	void Awake ()
	{
		scenObjet = -1;
		scenClone = -1;
		bufferme.DeleteKey("popupdropdownison");
		fathername = gameObject.name;
		loadingwindow = GameObject.Find ("loadingwindow");
		DebugGUI="Debut Awake";
		if (loadingwindow != null)
		{
			loadingwindowtext = (tk2dTextMesh)loadingwindow.FindInChildren("loadingwindowtext").GetComponent<tk2dTextMesh>();
		}
		if (loadingwindow != null)	loadingwindow.SetActive(false);


		if (!earlyskip)
			m_ViewCamera = gameObject.transform.parent.gameObject.FindInChildren("3dcamera").GetComponent<Camera>();
		DebugGUI="Milieu Awake";
		zoompoint = GameObject.Find("interface");
		if (zoompoint != null)
			zoompoint = zoompoint.FindInChildren("zoompoint");

		clone = GameObject.Find ("clone");
		clonepanel = GameObject.Find ("clonepanel");

		clonebase = GameObject.Find ("clonebase");

		DebugGUI="Fin Awake";
	}

	void Start()
	{
		canActivateCloneActive=false;
        _prod = new pp_produit_vars();

		styleLabel = new GUIStyle ();
		styleLabel.normal.textColor = Color.green;
		if (clonepanel != null)
			clonepanel.SetActive(false);
		if (clonebase != null)
			clonebase.SetActive(false);

        _init = true;
	}

	/*void OnGUI(){
		GUI.Label (new Rect (Screen.width/2-200, Screen.height/2, 500, 250), DebugGUI,styleLabel);
	}*/

	void Update ()
	{

        if (_init )
        {
            if( _prod.LoadNewVars_Prod() )
            {   
				Debug.Log ("load objet 3d");
                StopAllCoroutines();
                StartCoroutine("LoadAll3Files");
            }
        }

        if( (cloneObjet != null) && (camClone != null) )
        {
			if (cloneActif)
            {
				camClone.gameObject.SetActive (true);
				cloneObjet.SetActive (true);
			} 
            else 
            {
				camClone.gameObject.SetActive (false);
				cloneObjet.SetActive (false);
			}
		}     
		
	}
        
    IEnumerator ErrorLoadAll3Files(string keyMess)
    {
        float _timeCounter = 0;

        settext.ForceText(loadingwindowtext,true,keyMess);

        while (_timeCounter < 3f)
        {
            _timeCounter += Time.deltaTime;
            //yield return new WaitForSeconds(0.1f);
            yield return null;
        }

        yield break;
    }

    IEnumerator LoadAll3Files()
	{
		DebugGUI="Debut Load3file";

		// Destroy existing 3D object structure here
		if (objectroot != null)
		{
			Destroy (objectroot);
			objectroot = null;			// all killed
			objectcenter = null;
		}
            
        if( newpanelfather != null )
            Destroy(newpanelfather);

        if (_prod.Produit == null)
            yield break;

		if (earlyskip)
		{
			GameObject smallobj = new GameObject();
			smallobj.transform.parent = gameObject.transform;
			yield return null;
			gameObject.name = fathername;
			smallobj.name = rootfilename;
			yield break;
		}
		

		objectroot = new GameObject();
		objectroot.transform.parent = gameObject.transform.parent;
		objectroot.transform.eulerAngles = Vector3.zero;
		objectroot.transform.localScale = Vector3.one;
		objectroot.name = rootfilename;
		objectroot.layer = 8;
		objectroot.transform.position = new Vector3(decalx,0,0);
		
        GameObject [] objects = ObjReader.use.ConvertString (_prod.Produit.obj3D, _prod.Produit.mtl3D);
        // Pour le bon fonctionnement de l'application les objets doivent avoir un nom unique
        if( true ) for (int i = objects.Length; i > 0; i--)
        {
            int sameName = 1;
            string name1 = objects[i-1].name.ToLower().Trim();

            for (int j = i - 1; j > 0; j--)
            {
                string name2 = objects[j-1].name.ToLower().Trim();
                if (name1 == name2)
                    sameName++;
            }

            if (sameName > 1)
            {
                objects[i-1].name += "_#" + sameName.ToString(); 
            }
        }

		
		DebugGUI="Milieu Load3file apres loading des files";
		foreach (GameObject item in objects)
		{
			int textureindex = 0;

            pp_produit_obj obj = _prod.Produit.FindObj(item.name);
// get texture index here
            if (obj != null)
			{
                textureindex = obj.Texture;
			}
			GameObject settexture = GameObject.Find ("RightTopAnchor").FindInChildren("texture"+textureindex);
			Renderer rend = (Renderer)item.GetComponent<Renderer>();
			Renderer srcrend = (Renderer)settexture.GetComponent<Renderer>();
			rend.material.SetTexture("_MainTex",srcrend.material.GetTexture("_MainTex"));

			item.transform.position = objectroot.transform.position;
			item.layer = layernr;
			item.transform.parent = objectroot.transform;
			item.AddComponent<attachedcolor>();
		}
		
		objectroot.AddComponent<BoxCollider>();
		FitColliderToChildren (objectroot);
		BoxCollider col = objectroot.GetComponent<BoxCollider>();
		
		objectcenter = new GameObject();	//GameObject.Instantiate(clone) as GameObject;
		objectcenter.transform.parent = objectroot.transform;
		objectcenter.transform.eulerAngles = Vector3.zero;
		objectcenter.transform.localScale = Vector3.one;
		objectcenter.name = "center";
		objectcenter.layer = layernr;
		objectcenter.transform.localPosition = col.center;
		GameObject turnfather;
		DebugGUI="Milieu Load3file texture?";
	//	foreach (GameObject item in objects)

		foreach (GameObject item in objects)
		{
			Debug.Log (item.name);
			item.transform.localPosition = item.transform.localPosition - col.center;
			
			GameObject father = new GameObject();	//GameObject.Instantiate(clone) as GameObject;
			father.transform.parent = item.transform.parent;
			father.name = "#_"+item.name;
			father.transform.localPosition = item.transform.localPosition;
			father.transform.localScale = item.transform.localScale;
			father.transform.eulerAngles = item.transform.eulerAngles;
			
			item.transform.parent = father.transform;
			father.AddComponent<BoxCollider>();
			FitColliderToChildren (father);
			
			turnfather = new GameObject();	//GameObject.Instantiate(clone) as GameObject;
			turnfather.transform.position = father.GetComponent<BoxCollider>().bounds.center;
			turnfather.name = "*_"+item.name;
			turnfather.transform.parent = father.transform;
			turnfather.transform.localScale = Vector3.one;
			turnfather.transform.eulerAngles = Vector3.zero;

			item.transform.parent = turnfather.transform;
		}
		objectcenter.transform.localPosition = objectcenter.transform.localPosition - col.center;

		foreach (Transform child in objectroot.GetComponentsInChildren<Transform>(true))
		{

			if (child.gameObject.name != "center")
			{
				if (child.gameObject.name.StartsWith("#_"))
				{
					GameObject item = child.gameObject;
					
					item.transform.parent = objectcenter.transform;
					/*
					item.AddComponent<object_cell>();

					object_cell cell = (object_cell)item.GetComponent<object_cell>();
					cell.pos = item.transform.localPosition;
					col = (BoxCollider)item.GetComponent<BoxCollider>();
					cell.direction = col.center + item.transform.localPosition;
*/
				}
			}
		}

		objectcenter.AddComponent<BoxCollider>();
		FitColliderToChildren (objectcenter);
		
		// create center - pivot point
		turnfather = new GameObject();	//GameObject.Instantiate(clone) as GameObject;
		turnfather.transform.position = objectcenter.GetComponent<BoxCollider>().bounds.center;
		turnfather.name = "*_center";
		turnfather.transform.parent = objectcenter.transform;
		turnfather.transform.localScale = Vector3.one;
		turnfather.transform.eulerAngles = Vector3.zero;
		
		// attach all center childs to rotation center
		foreach (Transform child in objectcenter.GetComponentsInChildren<Transform>(true))
		{
			if ((child.gameObject.name != "center") && (child.gameObject.name != "*_center"))
			{
				if (child.parent == objectcenter.transform)
				{
					child.parent = turnfather.transform;
				}
			}
		}


		/*if (Application.loadedLevelName != "linkeditor")
		{
			if (ini == null)
			{
				// start Côme editor
				bufferme.LoadLevel("linkeditor");
			}
		}*/
		DebugGUI="Fin Load3file";


		if (Application.loadedLevelName != "newlink")
		{
            if( !_prod.Produit.VerifyValidityObjs() )
			{
				bufferme.LoadLevel("newlink");
			}
		}

     
	
		if ((Application.loadedLevelName != "newlink") /*&& verif || (Application.loadedLevelName != "linkeditor")*/)
		{
			Restruct3DHierarchie();
		}

		yield return new WaitForSeconds (0.5f);
		//create clone;
		canActivateCloneActive=false;
		createClone();
		canActivateCloneActive = true;

	}

	void createClone(){
		Debug.Log ("passe par create clone");
		if (cloneObjet != null) {
			cloneObjet.SetActive (true);
			Destroy (cloneObjet);
		}
		//creation objet
		GameObject original = GameObject.Find ("myfullscreen");
		Debug.Log(original.transform.GetChild(0).GetChild(0).name);
		GameObject nouv = Instantiate (original )as GameObject;
		nouv.transform.SetParent (original.transform.parent);
		Vector3 pos = original.transform.localPosition;
		pos.x -= 10000;
		nouv.transform.localPosition = pos;
		nouv.transform.localRotation = original.transform.localRotation;
		nouv.name = "myfullscreen";
		nouv.transform.localScale = Vector3.one;

		//Renomage +  definitionScript

		renameObjectClone (nouv);

		cloneObjet = nouv;
		cloneActif = false;
		Debug.Log ("passe par create clone");
	}

	void renameObjectClone(GameObject g){
		if (g.name != "panelpoint") {
			if (g.name != "#_*_center" && g.name != "#_tempattachfather") {
				string nomOriginal = g.name;
				g.name = "cloneObjet_" + nomOriginal;
				g.AddComponent<FollowObjectBrother> ();

				g.GetComponent<FollowObjectBrother> ().brother = GameObject.Find (nomOriginal);
				foreach (Transform t in g.transform) {
					renameObjectClone (t.gameObject);
				}
			} else {
				Destroy (g);
			}
		} else  {
			//Debug.Log (GameObject.Find ("3dcameraClone").activeSelf);
		
			g.GetComponent<facecamera> ().mycam = camClone;
		}

	}

	public static void AssembleObject(GameObject obj,bool assem)
	{
		object_cell cell = (object_cell)obj.GetComponent<object_cell>();
		if (cell != null)
		{
			if (assem)
				cell.open = 2;
			else
				cell.open = 1;
		}
		
		foreach (Transform child in obj.GetComponentsInChildren<Transform>(true))
		{
			cell = (object_cell)child.gameObject.GetComponent<object_cell>();
			if (cell != null)
			{
				if (assem)
					cell.open = 2;
				else
					cell.open = 1;
			}
		}
	}


    public static void ShowHide( pp_produit_composant comp )
    {
        if (comp != null)
        {
            string nom = "#_" + comp.Description;
            bool bShow = comp.Visible;
            bool Mode_HideAllPanels = false;

            foreach (Transform child in objectcenter.GetComponentsInChildren<Transform>(true))
            {
                
                if (child.gameObject.name == nom)
                {
                    child.gameObject.SetActive(bShow);

                    if (!Mode_HideAllPanels)
                    {
                        GameObject panelpoint = null;
                        facecamera fcam = null;
                                             
                        foreach (Transform trs in child.gameObject.GetComponentsInChildren<Transform>(true))
                        {
                            if ((trs.parent == child) && (trs.name == "panelpoint"))
                            {
                                panelpoint = trs.gameObject;
                                break;
                            }
                        }

                        if (panelpoint != null)
                            fcam = panelpoint.GetComponent<facecamera>();
      
                        if( (fcam != null) && (fcam.panelname.Length > 0) )
                        {
                            fcam.panel.SetActive(bShow);
                        }
                    }
                }

            }


            if (Mode_HideAllPanels)
            {
                // On cache tout les panels sachant qu'au prochain update les composant visible vont ce re-activer (voir facecamre.cs)
                foreach (Transform child in newpanelfather.GetComponentsInChildren<Transform>(true))
                {
                    if (child != newpanelfather.transform)
                        child.gameObject.SetActive(false);
                }
            }


            foreach (pp_produit_composant child in comp.childs)
                ShowHide(child);
        }
    }
	
    public static void HideAllOthers(pp_produit_composant comp)
	{
        if (comp != null)
        {
            
    		int startcounter = 1;
    		char startabccount = 'A';

            string nom = "#_" + comp.Description;

    		if (zoompoint != null)
    		{
    			Vector3 zoomvec = zoompoint.transform.localPosition;
    			zoomvec.y = 0;
    			zoompoint.transform.localPosition = zoomvec;
    		}


            // Desassemblage -> Mise a jour bouton
            disassebleUI dui = GameObject.FindObjectOfType<disassebleUI>();
            if (dui != null)
                dui.init();

    //		Debug.Log ("Hide all but:"+notme);
    		Transform fatherref = null;
    		// Find my object !!! -> define who is the father
    		foreach (Transform child in objectcenter.GetComponentsInChildren<Transform>(true))
    		{
    			object_cell cell = (object_cell)child.gameObject.GetComponent<object_cell>();
                if (child.gameObject.name == nom)
    			{
    				fatherref = child.parent;
    			}
    			if (cell != null)
    				cell.open = 2;		// close all cells
    			child.eulerAngles = Vector3.zero;		// reste after possible rotation
    			child.gameObject.SetActive(false);		// switch all OFF !

                // On reinit tout les panel name
                if (child.name == "panelpoint")
                {
                    GameObject panelpoint = child.gameObject;
                    facecamera fcam = null;

                    if (panelpoint != null)
                        fcam = panelpoint.GetComponent<facecamera>();

                    if(fcam != null)
                    {
                        fcam.panelname = "";
                    }
                }
    		}

    		foreach (Transform child in newpanelfather.GetComponentsInChildren<Transform>(true))
    		{
    			if (child != newpanelfather.transform)
    				child.gameObject.SetActive(false);
    		}
                                      

    		//		open
    		objectcenter.SetActive(true);

    		foreach (Transform child in objectcenter.GetComponentsInChildren<Transform>(true))
    		{
                if (child.gameObject.name == nom)
    			{
    //				Debug.Log ("Found:"+child.gameObject.name);
    				
                     child.gameObject.SetActive(comp.Visible);

    				// all fathers -> 

    				if (child != objectcenter.transform)
    				{
    					Transform trs = child.parent;
    					while (trs != objectcenter.transform)
    					{
    						trs.gameObject.SetActive(true);
    						trs = trs.parent;
    					}
    				}
    // my point is central -> all childs should have there direction calculated
    				foreach (Transform disassemble in child.gameObject.GetComponentsInChildren<Transform>(true))
    				{
    					if (disassemble.parent != child)
    					{
    						if (disassemble.name.StartsWith("#_"))
    						{
    							object_cell cell = (object_cell)disassemble.gameObject.GetComponent<object_cell>();

    							cell.direction = disassemble.position - child.position;
    						}
    					}
    				}

    				// all childs
    				foreach (Transform trs2 in child.gameObject.GetComponentsInChildren<Transform>(true))
    				{
                        pp_produit_composant child_comp = null;
                        bool child_visible = true;

                        if (trs2.name.StartsWith("#_"))
                            child_comp = comp.Produit.FindComposant(trs2.name);

                        if (child_comp != null)
                            child_visible = child_comp.Visible;

    					if (trs2.parent == child)		// direct son -> get panel
    					{
    						foreach (Transform trs3 in trs2.gameObject.GetComponentsInChildren<Transform>(true))
    						{
    							if (trs3.name == "panelpoint")
    							{
    								if (trs3.parent == trs2)
    								{
    //									Debug.Log ("Checking:"+trs2.name);
    									long countchilds = 0;
    									foreach (Transform trs4 in trs2.gameObject.GetComponentsInChildren<Transform>(true))
    									{
    //										Debug.Log ("interchild:"+trs4.name);
    										if ((trs4 != trs2) && (trs4.name.StartsWith("#_")))	countchilds++;
    									}
    //									Debug.Log ("Childs:"+countchilds);

    									facecamera fcam = (facecamera)trs3.gameObject.GetComponent<facecamera>();

    									if (countchilds == 0)
    									{
    										fcam.panelname = startcounter.ToString();
    										startcounter++;
    									}
    									else
    									{
    										fcam.panelname = startabccount.ToString();
    										startabccount++;
    									}
    								}
    							}
    						}
    					}

                        trs2.gameObject.SetActive(child_visible);
    				}
    				break;
    			}
    //			else
    //				Debug.Log ("Missed:"+child.gameObject.name);
    		}
    		//GameObject.Find("fullscreen_").GetComponent<panelGroupe>().Instantiate();
        }
	}
	
	
	public static void FitColliderToChildren (GameObject parentObject)
	{
		BoxCollider bc = parentObject.GetComponent<BoxCollider>();
		Bounds bounds = new Bounds (Vector3.zero, Vector3.zero);
		bool hasBounds = false;
		Renderer[] renderers =  parentObject.GetComponentsInChildren<Renderer>();
		foreach (Renderer render in renderers) {
			if (hasBounds) {
				bounds.Encapsulate(render.bounds);
			} else {
				bounds = render.bounds;
				hasBounds = true;
			}
		}
		if (hasBounds) {
			bc.center = bounds.center - parentObject.transform.position;
			bc.size = bounds.size;
		} else {
			bc.size = bc.center = Vector3.zero;
			bc.size = Vector3.zero;
		}
	}

	int level;

    void RunTroughChilds(pp_produit_composant node,GameObject father)
	{
		GameObject tmp = father;

		if (level > 0)
		{
			tmp = new GameObject();
			tmp.name = node.Description;
			tmp.transform.parent = father.transform;
			tmp.transform.localEulerAngles = Vector3.zero;
			tmp.transform.localScale = Vector3.one;
			tmp.transform.localPosition = Vector3.zero;
		}
//		Debug.Log (level+")"+node.name);
		if (node.childs.Count > 0)
		{
			level++;
            foreach(pp_produit_composant n in node.childs)
			{
				RunTroughChilds(n,tmp);
			}
			level--;
		}
	}






    void GenerateBoundingBoxes(pp_produit_composant node,GameObject father)
	{
		GameObject tmp = father;
		
		if (level > 0)
		{
			tmp = father.FindInChildren(node.Description);
			tmp.AddComponent<BoxCollider>();
			FitColliderToChildren (tmp);
		}
		if (node.childs.Count > 0)
		{
			level++;
            foreach(pp_produit_composant n in node.childs)
			{
				GenerateBoundingBoxes(n,tmp);
			}
			level--;
		}
	}

	GameObject tempattachfather;

    void CenterObjects(pp_produit_composant node,GameObject father)
	{
		GameObject tmp = father;
		
		if (level > 0)
		{
			tmp = father.FindInChildren(node.Description);
			// for each object -> find all child, detatch them
			Transform testfather = tmp.transform;
			foreach (Transform child in tmp.GetComponentsInChildren<Transform>(true))
			{
				if (child.parent == testfather)					child.parent = tempattachfather.transform;
			}
			// recalc center point
			tmp.transform.position = tmp.GetComponent<BoxCollider>().bounds.center;
			// re-attach childs
			testfather = tempattachfather.transform;
			foreach (Transform child in tempattachfather.GetComponentsInChildren<Transform>(true))
			{
				if (child.parent == testfather)					child.parent = tmp.transform;
			}
			BoxCollider box = tmp.GetComponent<BoxCollider>();
			box.center = Vector3.zero;
		}
		if (node.childs.Count > 0)
		{
			level++;
            foreach(pp_produit_composant n in node.childs)
			{
				CenterObjects(n,tmp);
			}
			level--;
		}
	}

	void Restruct3DHierarchie()
	{
        pp_produit prod = _prod.Produit;

        if (prod != null)
        {
            // 1) add hierarchie to objectcenter
            GameObject mycenter = objectcenter; //.FindInChildren("*_center");
            level = 0;
            RunTroughChilds(prod.roothierarchie, mycenter);
            // 2) Attach all objects where they belong depending the json lookup file
            Transform testfather = mycenter.FindInChildren("*_center").transform;
            foreach (Transform child in mycenter.FindInChildren("*_center").GetComponentsInChildren<Transform>(true))
            {
                if (child.parent == testfather)
                {
                    if (child.name.StartsWith("#_"))
                    {
                        GameObject orig = child.gameObject.FindInChildren("*_" + child.name.Replace("#_", ""));
                        GameObject placeme = null;
                        string inname = "";

                        pp_produit_obj obj = prod.FindObj(child.name);

                        if( (obj != null) && (obj.Composant != null) )
                        {
                            inname = obj.Composant.Description;
                            placeme = mycenter.FindInChildren(inname);
                        }

                        if (orig != null)
                        {
                            if (placeme != null)
                            {
                                orig.transform.parent = placeme.transform;						// link it
                                orig.name = child.name;
                            }
                            else
                                Debug.LogError("chidren not found :" + inname);
                        }
                        else
                            Debug.LogError("chidren not found :" + "*_" + child.name.Replace("#_", ""));
					
                        Destroy(child.gameObject);
                    }
                }
            }
            Destroy(mycenter.FindInChildren("*_center"));
// Generate bounding boxes
            level = 0;
            GenerateBoundingBoxes(prod.roothierarchie, mycenter);


            tempattachfather = new GameObject();
            tempattachfather.name = "tempattachfather";
            tempattachfather.transform.parent = mycenter.transform;
            tempattachfather.transform.localPosition = Vector3.zero;
            tempattachfather.transform.localScale = Vector3.one;
            tempattachfather.transform.localEulerAngles = Vector3.zero;

            level = 0;
            CenterObjects(prod.roothierarchie, mycenter);
            Destroy(tempattachfather);
// rename
            newpanelfather = new GameObject();
            newpanelfather.name = "newpanelfather";
            newpanelfather.transform.parent = gameObject.transform;
            newpanelfather.transform.localPosition = Vector3.zero;
            newpanelfather.transform.localEulerAngles = Vector3.zero;
            newpanelfather.transform.localScale = Vector3.one;

            foreach (Transform child in mycenter.GetComponentsInChildren<Transform>(true))
            {
                if (child.name.StartsWith("#_"))
                {
                    child.gameObject.name = child.gameObject.name.Replace("#_", "");

                    /*
				GameObject item = child.gameObject;
				item.AddComponent<object_cell>();
				object_cell cell = (object_cell)item.GetComponent<object_cell>();
				cell.pos = item.transform.localPosition;
				cell.direction = item.transform.localPosition;
*/
                }
                else
                {
                    pp_produit_obj obj = prod.FindObj(child.name);
   
                    if ((obj == null) || (obj.Composant == null))
                    {
                        child.gameObject.name = "#_" + child.gameObject.name;
                        if (child.gameObject.name != "#_center")
                        {
                            GameObject item = child.gameObject;
                            item.AddComponent<object_cell>();
                            object_cell cell = (object_cell)item.GetComponent<object_cell>();
                            cell.pos = item.transform.localPosition;
                            cell.direction = item.transform.localPosition;

                            GameObject panelpoint = new GameObject();
                            panelpoint.transform.parent = child;
                            panelpoint.transform.localScale = Vector3.one;
                            panelpoint.transform.localEulerAngles = Vector3.zero;
                            panelpoint.transform.localPosition = new Vector3(0, 50.0f, 0);
                            panelpoint.AddComponent<facecamera>();
                            facecamera fcam = (facecamera)panelpoint.GetComponent<facecamera>();
                            fcam.mycam = m_ViewCamera;
                            panelpoint.name = "panelpoint";
                            panelpoint.SetActive(true);
                            //						newpanelfather

                            GameObject panelbase = GameObject.Instantiate(clonebase) as GameObject;
                            panelbase.transform.localEulerAngles = Vector3.zero;
                            panelbase.transform.localPosition = new Vector3(0, 0.0f, 0);
                            panelbase.name = "panelbase";
                            fcam.panelbase = panelbase;
                            GameObject panel = GameObject.Instantiate(clonepanel) as GameObject;
                            fcam.panel = panel;
                            button_panel bswap = (button_panel)panel.GetComponent<button_panel>();
                            bswap.myrealfather = child;
//						bswap.m_ViewCamera = m_ViewCamera;
                            panel.transform.parent = newpanelfather.transform;
                            panel.transform.localScale = Vector3.one;
                            panelbase.transform.parent = panel.transform;

                            panel.transform.localEulerAngles = Vector3.zero;
                            panel.transform.localPosition = new Vector3(0, 0.0f, 0);
                            panel.name = "panel";
                            panel.SetActive(false);
                            panelbase.SetActive(false);
                        }
                    }
                }
            }

            objectcenter.name = "center";
        }
	}

	
}
