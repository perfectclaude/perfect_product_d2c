﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Text;
using System.Collections.Generic;
using UnityEngine.UI;

public class GenereValeurs : MonoBehaviour {
	public GameObject ordreValeurs;
	bool initial;
	bool nouveau;
	
	//public bool details=false;
	//public pp_produit_scenario scenarioActuel;
	public GameObject retour;


    private pp_produit_note _note = null;


	void Start(){
		initial = true;
		retour.SetActive (false);
	}

	// Update is called once per frame
	void Update () {
		Debug.Log (initial);
		if (initial) {
			


			Generate ();
		

			initial = false;
		}

	}


    public void init(bool nouveau1, pp_produit_note note)
    {
        _note = note;
		initial = true;
		nouveau = nouveau1;
	}

    List<int> IdeeType(pp_produit_scenario scenar){
		List<int> res = new List<int> ();
		res.Add (0);
		res.Add (0);
		res.Add (0);
		res.Add (0);

        if (scenar != null)
        {
            foreach(pp_produit_note note in scenar.Notes)
            {
                switch (note.StatutIdee) 
                {
        			case "Identifie":
        				res [0] += 1;
        				break;

        			case "Valide":
        				res [1] += 1;
        				break;

        			case "Implemte":
        				res [2] += 1;
        				break;

        			case "Abandonne":
        				res [3] += 1;
        				break;
    			}
    		}
        }
		return res;
	}
		
    bool est3F(pp_produit_scenario scenar)
    {
        if (scenar != null)
        {
            foreach(pp_produit_note note in scenar.Notes)
            { 
                if (!note.TroisF_Val)
                    return false;
            }
        }
		return true;
	}

    bool estVisible(pp_produit_scenario scenar)
    {
        if (scenar != null)
        {
            foreach(pp_produit_note note in scenar.Notes)
            {
                if (!note.IdeeVisibleClient_Val )
                    return false;
            }
        }
		return true;
	}

    public void Generate()
    {
        Generate(false, null);
    }

    public void Generate(bool modeDetail, pp_produit_scenario scenarDetail )
    {
        
        foreach (Transform child in transform)
        {
            Destroy(child.gameObject);
        }

        if (!modeDetail)
        {
            pp_produit prod = pp_manager.GetCurrentProduit();

            if (prod != null)
            {
                retour.SetActive(false);

                Debug.Log("ici");
                float max = 0;
                foreach (pp_produit_scenario scenar in prod.Scenarios)
                {
				
                    if(scenar.GainPotentielUnitaire_Val > max)
                    {
                        max = scenar.GainPotentielUnitaire_Val;
                    }

                    if(scenar.GainDeMasse_Val > max)
                    {
                        max = scenar.GainDeMasse_Val;
                    }

                    if(scenar.TotalNRC_Val > max)
                    {
                        max = scenar.TotalNRC_Val;
                    }

                }

                ordreValeurs.transform.GetChild(0).GetComponent<Text>().text = max.ToString();

                ordreValeurs.transform.GetChild(1).GetComponent<Text>().text = (max / 2).ToString();

                ordreValeurs.transform.GetChild(2).GetComponent<Text>().text = "0";

                int i = 0;
                foreach (pp_produit_scenario scenar in prod.Scenarios)
                {
                    GameObject nom = Instantiate(Resources.Load("NomScenarioGraphique")) as GameObject;
                    nom.transform.GetChild(2).GetComponent<Text>().text = scenar.Titre;
                    nom.GetComponent<boutonDetails>().init(scenar);
                    Vector3 vec = Vector3.zero;
                    vec.x = -700 + (i * 600);
                    vec.y = -470;
                    nom.transform.parent = transform;
                    nom.transform.localScale = Vector3.one;
                    nom.transform.localPosition = vec;
                    if (est3F(scenar))
                    {
                        nom.FindInChildren("Text3F").FindInChildren("TextRep").GetComponent<showtext>().mykey = "YES_TEXT";
                    }
                    else
                    {
                        nom.FindInChildren("Text3F").FindInChildren("TextRep").GetComponent<showtext>().mykey = "NO_TEXT";
                    }

                    if (estVisible(scenar))
                    {
                        nom.FindInChildren("TextVisibility").FindInChildren("TextRep").GetComponent<showtext>().mykey = "YES_TEXT";
                    }
                    else
                    {
                        nom.FindInChildren("TextVisibility").FindInChildren("TextRep").GetComponent<showtext>().mykey = "NO_TEXT";
                    }


                    GameObject barre = Instantiate(Resources.Load("BarreHistogramme")) as GameObject;
                    barre.transform.parent = transform; //transform; 
                    barre.transform.localScale = Vector3.one;
                    barre.GetComponent<barreGraphique>().init(Color.blue, scenar.GainPotentielUnitaire_Val, "Gain: " + scenar.GainPotentielUnitaire + " €", -800 + (i * 600), max);

                    GameObject barre2 = Instantiate(Resources.Load("BarreHistogramme")) as GameObject;
                    barre2.transform.parent = transform; //transform; 
                    barre2.transform.localScale = Vector3.one;
                    barre2.GetComponent<barreGraphique>().init(Color.red, scenar.GainDeMasse_Val, "Gain Masse: " + scenar.GainDeMasse, -650 + (i * 600), max);

                    GameObject barre3 = Instantiate(Resources.Load("BarreHistogramme")) as GameObject;
                    barre3.transform.parent = transform; //transform; 
                    barre3.transform.localScale = Vector3.one;
                    barre3.GetComponent<barreGraphique>().init(Color.green, scenar.TotalNRC_Val, "Investissement: " + scenar.TotalNRC + " €", -500 + (i * 600), max);

                    i++;
                }

            }

        }
        else
        {
            retour.SetActive(true);
            List<int> res = IdeeType(scenarDetail);

            float max = 0;
            foreach (int valeur in res)
            {
                if (valeur > max)
                {
                    max = valeur;
                }

            }

            ordreValeurs.transform.GetChild(0).GetComponent<Text>().text = max.ToString();

            ordreValeurs.transform.GetChild(1).GetComponent<Text>().text = (max / 2).ToString();

            ordreValeurs.transform.GetChild(2).GetComponent<Text>().text = "0";

            Color c = new Color(0.86f, 0.32f, 1);
            GameObject barre = Instantiate(Resources.Load("BarreHistogramme")) as GameObject;
            barre.transform.parent = transform; //transform; 
            barre.transform.localScale = Vector3.one;
            barre.GetComponent<barreGraphique>().init(c, res[0], "Identifiee: " + res[0], -800, max);

            c = new Color(0.35f, 0.80f, 1);
            GameObject barre2 = Instantiate(Resources.Load("BarreHistogramme")) as GameObject;
            barre2.transform.parent = transform; //transform; 
            barre2.transform.localScale = Vector3.one;
            barre2.GetComponent<barreGraphique>().init(c, res[1], "Validee: " + res[1], -650, max);

            c = new Color(0.59f, 0.86f, 0);
            GameObject barre3 = Instantiate(Resources.Load("BarreHistogramme")) as GameObject;
            barre3.transform.parent = transform; //transform; 
            barre3.transform.localScale = Vector3.one;
            barre3.GetComponent<barreGraphique>().init(c, res[2], "Implementee: " + res[2], -500, max);

            c = new Color(0.82f, 0.27f, 0);
            GameObject barre4 = Instantiate(Resources.Load("BarreHistogramme")) as GameObject;
            barre4.transform.parent = transform; //transform; 
            barre4.transform.localScale = Vector3.one;
            barre4.GetComponent<barreGraphique>().init(c, res[3], "Abandonnee: " + res[3], -350, max);

        }
    }
}
