﻿using UnityEngine;
using System.Collections;

public class boutonDetails : MonoBehaviour {
    private pp_produit_scenario _scenar;
	public GameObject genere;

    public void init(pp_produit_scenario scenar)
    {
        _scenar = scenar;
    }

	public void OnClick()
    {
        if (_scenar != null)
        {
            genere = GameObject.Find("Valeur");
            genere.GetComponent<GenereValeurs>().Generate( true, _scenar);
        }

	}
}
