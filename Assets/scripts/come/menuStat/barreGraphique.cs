﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class barreGraphique : MonoBehaviour {

	public void init(Color c,float valeur,string def,float x,float max){
		if (valeur < 0) {
			valeur = 0;
		}
		if (max == 0) {
			max = 1;

		}
		transform.GetChild (0).GetComponent<Text> ().text = def;
		transform.GetComponent<Image> ().color = c;
		Debug.Log (max);
		Debug.Log (valeur);
		float height = valeur / max * 1000;
		transform.GetComponent<RectTransform> ().sizeDelta = new Vector2 (50, height);
		Vector3 vec = new Vector3 ();
		vec.x = x;
		vec.y = 100 - ((1000 - height) / 2);
		vec.z = 0;
		transform.localPosition = vec;
		vec.x = 0;
		vec.y = height/2 + 40;
		transform.GetChild (0).localPosition = vec;
	}
}
