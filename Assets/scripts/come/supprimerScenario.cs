﻿using UnityEngine;
using System.Collections;

public class supprimerScenario : MonoBehaviour {
	GameObject panel;
	bool start;

	void Start(){
		panel = GameObject.Find ("PanelFlou");
		start = true;
	}
	void Update(){
		if (start) {
			panel.SetActive(false);
			start=false;
		}
	}
	public  void OnClick ()
	{
		if (sceneIdee.scenarios.Count > 0) {
			panel.SetActive(true);
			GameObject nouv = Instantiate (Resources.Load ("PopupDeleteUI")) as GameObject;
			nouv.name = "Pop-up";
			nouv.transform.parent=GameObject.Find("Canvas").transform;
			nouv.transform.localPosition=Vector3.zero;
			nouv.transform.localScale=Vector3.one;
		}

	}
}
