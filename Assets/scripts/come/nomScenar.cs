﻿using UnityEngine;
using System.Collections;

public class nomScenar : MonoBehaviour {
	public static int numScenar;

	public void changeNom(int nouvScenar){
		numScenar = nouvScenar;
		transform.GetChild(0).GetComponent<tk2dTextMesh>().text=sceneIdee.scenarios[nouvScenar].nom;
		if (nouvScenar > 0) {
			GameObject.Find ("Gauche").transform.GetChild (1).GetComponent<tk2dTextMesh> ().text = sceneIdee.scenarios [nouvScenar - 1].nom;
		} else {
			GameObject.Find ("Gauche").transform.GetChild (1).GetComponent<tk2dTextMesh> ().text = sceneIdee.scenarios [sceneIdee.scenarios.Count-1].nom;
		}
		if (nouvScenar < sceneIdee.scenarios.Count-1) {
			GameObject.Find ("Droite").transform.GetChild (1).GetComponent<tk2dTextMesh> ().text = sceneIdee.scenarios [nouvScenar + 1].nom;
		} else {
			GameObject.Find ("Droite").transform.GetChild (1).GetComponent<tk2dTextMesh> ().text = sceneIdee.scenarios [0].nom;
		}

		foreach(Transform tr in  GameObject.Find ("ScenarioW").transform){
				
				if(tr.name!="titre0"){
					tr.GetChild(0).GetChild(0).GetComponent<tk2dSprite>().color=Color.white;
					tr.GetChild(0).GetChild(0).GetComponent<Cocheidee>().numScenario=nouvScenar;
			}


		}
		GameObject sce = GameObject.Find ("ListObjet");
		foreach (string[] idee in sceneIdee.scenarios[nouvScenar].idees) {
			int i = 0;
			foreach(Transform tr in  sce.transform){
				if(i>0){


					if(tr.GetChild(1).GetComponent<tk2dTextMesh>().text==idee[1]){
						Debug.Log ("ici");
						GameObject.Find("cochecase"+(i).ToString()).transform.GetChild(0).GetChild(0).GetComponent<tk2dSprite>().color=Color.black;
					}
				}
				i++;
			}
		}
	}


}
