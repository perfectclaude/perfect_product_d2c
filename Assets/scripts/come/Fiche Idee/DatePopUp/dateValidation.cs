﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class dateValidation : MonoBehaviour {

	private pp_produit_note _note = null;
	public GameObject popUp;

	public void init(pp_produit_note note,bool nouveau)
	{
		_note = note;
		if (_note != null)
		{
            //transform.GetComponent<InputField>().text = _note.DatePrevisionnelleValidation_val;
		}
	}
	public void OnClick()
	{
		if (_note != null)
		{
			DateTime d = _note.DatePrevisionnelleValidation_val;
			int jour = d.Day;
			int mois = d.Month;
			int annee = d.Year;
			popUp.SetActive (true);
			popUp.transform.GetChild (0).GetChild (0).GetComponent<ValiderDate> ().init (jour, mois, annee, true);
		}

	}

	public void stockDate(int jour,int mois,int year){
		if (_note != null)
		{
			DateTime d = new DateTime(year,mois,jour);
			Debug.Log (d.Year);
			popUp.SetActive (false);
			_note.DatePrevisionnelleValidation_val=d;
		}
	}
}
