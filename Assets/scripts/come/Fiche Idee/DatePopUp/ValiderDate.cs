﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ValiderDate : MonoBehaviour {
	public GameObject dropJour;
	public GameObject dropMois;
	public GameObject dropAnnee;
	public GameObject boutonVal;
	public GameObject boutonImp;
	bool valide;

	public void init(int jour,int mois,int annee, bool b){
		

		dropMois.GetComponent<jourDropMonth> ().Create ();
		dropMois.GetComponent<Dropdown> ().value = mois-1;

		dropAnnee.GetComponent<jourDropYear> ().Create (annee);

		dropAnnee.GetComponent<Dropdown> ().value = annee-1980;

		dropJour.GetComponent<JourDropDown> ().Create ();
		dropJour.GetComponent<Dropdown> ().value = jour-1;

		valide = b;

	}

	public void OnClick(){
		int jour = 0;
		int mois = 0;
		int annee = 0;
		int.TryParse (dropJour.GetComponent<Dropdown> ().captionText.text, out jour);
	 	int.TryParse (dropMois.GetComponent<Dropdown> ().captionText.text, out mois);
	 	int.TryParse (dropAnnee.GetComponent<Dropdown> ().captionText.text, out annee);
		dropJour.GetComponent<JourDropDown> ().delete ();
		dropMois.GetComponent<jourDropMonth> ().delete ();
		dropAnnee.GetComponent<jourDropYear> ().delete ();
		dropJour.GetComponent<Dropdown> ().value =0;
		dropMois.GetComponent<Dropdown> ().value = 0;
		dropAnnee.GetComponent<Dropdown> ().value = 0;


		if (valide) {
			boutonVal.GetComponent<dateValidation> ().stockDate (jour, mois, annee);
		}
		else {
			boutonImp.GetComponent<dateImplementation> ().stockDate (jour, mois, annee);
		}

	}



}
