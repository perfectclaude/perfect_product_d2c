﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class JourDropDown : MonoBehaviour {
	public GameObject dropMois;
	public GameObject dropAnnee;

	public void Create(){
		delete ();
		string s = dropMois.GetComponent<Dropdown> ().captionText.text;
		int value = transform.GetComponent<Dropdown> ().value;
		if ( s== "2") {
			int annee = 0;
			int.TryParse (dropAnnee.GetComponent<Dropdown> ().captionText.text, out annee);

			if (reste (annee, 4)) {
				Debug.Log ("annee divisible par 4");
				if (reste (annee, 100)) {
					if (reste (annee, 400)) {
						if (value>28) {
							value = 0;
						}
						for (int i = 1; i < 30; i++) {
							Dropdown.OptionData op = new Dropdown.OptionData ();
							op.text = i.ToString ();
							transform.GetComponent<Dropdown> ().options.Add (op);
						}
					} else {
						if (value>27) {
							value = 0;
						}
						for (int i = 1; i < 29; i++) {
							Dropdown.OptionData op = new Dropdown.OptionData ();
							op.text = i.ToString ();
							transform.GetComponent<Dropdown> ().options.Add (op);
						}
					}
				} else {
					if (value>28) {
						value = 0;
					}
					for (int i = 1; i < 30; i++) {
						Dropdown.OptionData op = new Dropdown.OptionData ();
						op.text = i.ToString ();
						transform.GetComponent<Dropdown> ().options.Add (op);
					}
				}
			} else {
				if (value>27) {
					value = 0;
				}
				for (int i = 1; i < 29; i++) {
					Dropdown.OptionData op = new Dropdown.OptionData ();
					op.text = i.ToString ();
					transform.GetComponent<Dropdown> ().options.Add (op);

				}
			}
			transform.GetComponent<Dropdown> ().value=value;
		}
		else if (s =="1" || s =="3" || s =="5" ||s =="7" ||s =="8" || s =="10" || s =="12") {
			for (int i = 1; i <32; i++) {
				Dropdown.OptionData op = new Dropdown.OptionData ();
				op.text = i.ToString ();
				transform.GetComponent<Dropdown> ().options.Add (op);
			}
			transform.GetComponent<Dropdown> ().value=value;
		}
		else{
			if(value==30){
				value=0;
			}
			for (int i = 1; i <31; i++) {
				Dropdown.OptionData op = new Dropdown.OptionData ();
				op.text = i.ToString ();
				transform.GetComponent<Dropdown> ().options.Add (op);
			}
			transform.GetComponent<Dropdown> ().value=value;
		}
	}

	public void delete(){
		transform.GetComponent<Dropdown> ().options.Clear ();

	}

	public bool reste(int x,int y){
		
		float arrondi=Mathf.Floor (x / y);
		Debug.Log (arrondi);
		float tmp_x = x;
		float tmp_y=y;
		float division = tmp_x / tmp_y;
		Debug.Log (division);
		if (division == arrondi) {
			return true;
		} else {
			return false;
		}
	}


}
