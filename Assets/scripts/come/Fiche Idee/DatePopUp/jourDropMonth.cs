﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class jourDropMonth : MonoBehaviour {
	public GameObject dropJour;


	public void Create(){
		for(int i=1;i<13;i++){
			Dropdown.OptionData op = new Dropdown.OptionData ();
			op.text = i.ToString ();
			transform.GetComponent<Dropdown> ().options.Add (op);

		}

	}

	public void delete(){
		transform.GetComponent<Dropdown> ().options.Clear ();

	}

	public void OnClick(){
		dropJour.GetComponent<JourDropDown> ().Create ();
	}
}
