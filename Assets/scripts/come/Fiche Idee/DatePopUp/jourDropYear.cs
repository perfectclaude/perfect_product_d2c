﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class jourDropYear : MonoBehaviour {
	public GameObject dropJour;
	public GameObject dropMois;


	public void Create(int annee){
		//DateTime tod = DateTime.Today;
		for(int i=1980;i<2100 ;i++){
			Dropdown.OptionData op = new Dropdown.OptionData ();
			op.text = i.ToString ();
			transform.GetComponent<Dropdown> ().options.Add (op);

		}

	}

	public void delete(){
		transform.GetComponent<Dropdown> ().options.Clear ();

	}

	public void OnClick(){
		if (dropMois.GetComponent<Dropdown>().value==1) {
			dropJour.GetComponent<JourDropDown> ().Create ();
		}
	}
}
