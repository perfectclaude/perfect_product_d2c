﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class levierInit : MonoBehaviour {


    private pp_produit_note _note = null;
    private bool initOK = false;

    private Dropdown _cbo = null;

	void Awake(){
        initOK = false;
	}
       

    public void init(pp_produit_note note,bool nouveau)
    {	
        _note = note;
        if (_note != null)
        {
            _cbo = gameObject.GetComponent<Dropdown>();

            if (_cbo != null)
            {
                _cbo.options.Clear();

                _cbo.options.Add(new Dropdown.OptionData(localization.GetTextWithKey("LEVER_SPEC_CBO")));
                _cbo.options.Add(new Dropdown.OptionData(localization.GetTextWithKey("LEVER_DESIG_CBO")));
                _cbo.options.Add(new Dropdown.OptionData(localization.GetTextWithKey("LEVER_PURCH_CBO")));
                _cbo.options.Add(new Dropdown.OptionData(localization.GetTextWithKey("LEVER_INDUS_CBO")));

         
                switch (_note.LeverDeGain)
                {
                    case "Specification":
                        _cbo.value = 0;
                        break;

                    case "Conception":
                        _cbo.value = 1;
                        break;

                    case "Achat":
                        _cbo.value = 2;
                        break;

                    case "Industrialisation":
                        _cbo.value = 3;
                        break;

                    default:
                        _cbo.value = -1;
                        break;
                    

                }
            }

            initOK = true;
        }

	}
	public void Onclick()
    {
        if ( (_note != null) && (_cbo != null) && initOK )
        {
            switch (_cbo.value)
            {
                case 0:
                    _note.LeverDeGain = "Specification";
                    break;

                case 1:
                    _note.LeverDeGain = "Conception";
                    break;

                case 2:
                    _note.LeverDeGain = "Achat";
                    break;

                case 3:
                    _note.LeverDeGain = "Industrialisation";
                    break;

                default:
                    _cbo.value = -1;
                    _note.LeverDeGain = "Specification";
                    break;


            }
            
        }
	}

}
