﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class statutInit : MonoBehaviour {
	public GameObject date;
	public GameObject Ampoules;
	public GameObject Barres;
    private pp_produit_note _note = null;

	bool initOK = false;

    private Dropdown _cbo = null;

	void Awake(){
        initOK = false;
	}
        

    public void init(pp_produit_note note, bool nouveau)
    {
        _note = note;

        if (_note != null)
        {
            _cbo = gameObject.GetComponent<Dropdown>();

            if (_cbo != null)
            {
                _cbo.options.Clear();

                _cbo.options.Add(new Dropdown.OptionData(localization.GetTextWithKey("STATUS_IDENT_CBO")));
                _cbo.options.Add(new Dropdown.OptionData(localization.GetTextWithKey("STATUS_VALID_CBO")));
                _cbo.options.Add(new Dropdown.OptionData(localization.GetTextWithKey("STATUS_IMPL_CBO")));
                _cbo.options.Add(new Dropdown.OptionData(localization.GetTextWithKey("STATUS_ABAND_CBO")));

                switch (_note.StatutIdee)
                {
                    case "Identifie":
                        _cbo.value = 0;
                        Ampoules.GetComponent<ampoule>().changeEtat(0);
                        Barres.GetComponent<BarreInit>().changeEtat(0);
                        break;

                    case "Valide":
                        _cbo.value = 1;
                        Ampoules.GetComponent<ampoule>().changeEtat(1);
                        Barres.GetComponent<BarreInit>().changeEtat(1);
                        break;

                    case "Implemte":
                        _cbo.value = 2;
                        Ampoules.GetComponent<ampoule>().changeEtat(2);
                        Barres.GetComponent<BarreInit>().changeEtat(2);
                        break;

                    case "Abandonne":
                        _cbo.value = 3;
                        Ampoules.GetComponent<ampoule>().changeEtat(3);
                        Barres.GetComponent<BarreInit>().changeEtat(3);
                        break;

                    default:
                        _cbo.value = -1;
                        break;
                }
            }

            initOK = true;       
        }
    }


    public void Onclick()
    {
        if ( (_note != null) && (_cbo != null) && initOK )
        {
            switch (_cbo.value)
            {
                case 0:
                    _note.StatutIdee = "Identifie";
                    Ampoules.GetComponent<ampoule>().changeEtat(0);
                    Barres.GetComponent<BarreInit>().changeEtat(0);

                    break;

                case 1:
    				
                    _note.StatutIdee = "Valide";
                    Ampoules.GetComponent<ampoule>().changeEtat(1);
                    Barres.GetComponent<BarreInit>().changeEtat(1);
                    break;

                case 2:
    				
                    _note.StatutIdee = "Implemte";
                    Ampoules.GetComponent<ampoule>().changeEtat(2);
                    Barres.GetComponent<BarreInit>().changeEtat(2);
                    break;

                default:
                    _note.StatutIdee = "Abandonne";
                    Ampoules.GetComponent<ampoule>().changeEtat(3);
                    Barres.GetComponent<BarreInit>().changeEtat(3);
                    break;
    		
            }
    			
            date.transform.GetComponent<datMajInit>().init(_note, false);
        }

	}
        
}
