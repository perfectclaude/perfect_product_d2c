﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class init3F : MonoBehaviour {
	private bool _initialisation=false;
    private pp_produit_note _note = null;

    public void init(pp_produit_note note,bool test)
    {
        _note = note;
        if (_note != null)
        {
            if (test)
            {

            }
            else
            {

                _initialisation = true;
                transform.GetComponent<Toggle>().isOn = _note.TroisF_Val;
                _initialisation = false;
            }
        }
	}

	public void OnClick()
    {
        if (_note != null)
        {
            if (!_initialisation)
            {
                _note.TroisF_Val = transform.GetComponent<Toggle>().isOn;
            }
        }
	}
}
