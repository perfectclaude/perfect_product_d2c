﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PiloteInit : MonoBehaviour {
    private pp_produit_note _note = null;
	
    public void init(pp_produit_note note,bool nouveau){
        _note = note;
        if (_note != null)
        {
            transform.GetComponent<InputField>().text = _note.Pilote;
        }
	}
	public void OnClick()
    {
        if (_note != null)
        {
            _note.Pilote = transform.GetComponent<InputField>().text;
        }
		
	}
}
