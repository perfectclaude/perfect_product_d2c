﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SujetInit : MonoBehaviour {

    private pp_produit_note _note = null;

    public void init(pp_produit_note note,bool nouveau)
    {
        _note = note;
        if (_note != null)
        {
            transform.GetComponent<InputField>().text = _note.SujetIdee;
        }
	}
	public void OnClick()
    {
        if (_note != null)
        {
            _note.SujetIdee = transform.GetComponent<InputField>().text;
        }

	}
}
