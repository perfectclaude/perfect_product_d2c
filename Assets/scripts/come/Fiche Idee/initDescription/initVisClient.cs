﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class initVisClient : MonoBehaviour {

	private bool initialisation=false;
    private pp_produit_note _note = null;

    public void init(pp_produit_note note,bool test)
    {
        _note = note;
        if (_note != null)
        {
            if (test)
            {

            }
            else
            {
                initialisation = true;
                transform.GetComponent<Toggle>().isOn = _note.IdeeVisibleClient_Val;
                initialisation = false;
            }
        }
	}

	public void OnClick()
    {
        if (_note != null)
        {
            if (!initialisation)
            {
                _note.IdeeVisibleClient_Val = transform.GetComponent<Toggle>().isOn;
            }
        }
	}
}
