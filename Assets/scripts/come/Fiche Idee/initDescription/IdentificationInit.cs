﻿using UnityEngine;
using System.Collections;

public class IdentificationInit : MonoBehaviour {
	public GameObject numero;
	public GameObject titre;
	public GameObject descr;
	public GameObject statut;
	public GameObject datmaj;
	public GameObject pilote;
	public GameObject ssensemble;
	public GameObject composant;
	public GameObject produit;
	public GameObject i3F;
	public GameObject iVC;
	public GameObject Sujet;


    public GameObject levier;

	void Update(){
		
	}
	// Use this for initialization
    public void init(pp_produit_note note,bool nouveau){
        numero.transform.GetComponent<numeroInit> ().init (note, nouveau);
        titre.transform.GetComponent<titreInit> ().init (note, nouveau);
        descr.transform.GetComponent<Descriptioninit> ().init (note, nouveau);
        statut.transform.GetComponent<statutInit> ().init (note, nouveau);
        datmaj.transform.GetComponent<datMajInit> ().init (note, nouveau);
        Sujet.GetComponent<SujetInit> ().init (note, nouveau);

        pilote.transform.GetComponent<PiloteInit> ().init (note, nouveau);
        ssensemble.transform.GetComponent<sousEnsembleInit> ().init (note, nouveau);

        composant.transform.GetComponent<ComposantInit> ().init (note, nouveau);
        produit.transform.GetComponent<ProduitInit> ().init (note, nouveau);

        i3F.transform.GetComponent<init3F> ().init (note, nouveau);
        iVC.transform.GetComponent<visibleClientInit> ().init (note, nouveau);

        levier.transform.GetComponent<levierInit> ().init (note, nouveau);
	}
}
