﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;

public class FicheIdeeLoadScenar : MonoBehaviour {
	private bool initial = false;
    private pp_produit_note _note = null;

	void Start(){
		

	}
	
	// Update is called once per frame
	void Update () {
        
		if (initial) {			

			Generate ();

			initial = false;
		}
	
	}


    public void init(bool nouveau1, pp_produit_note note){
		initial = true;
        _note = note;


	}

	public void Generate()
    {
        if (_note != null)
        {
            int i = 0;
			//Tester si on a autant de cout que de scenario 
			while (_note.CoutScenarios.Count < _note.Produit.Scenarios.Count) {
				_note.CoutScenarios.Add (0);
			}
            foreach (pp_produit_scenario scenar in _note.Produit.Scenarios)
            {
                GameObject nouv2 = Instantiate(Resources.Load("BarreScenario")) as GameObject;
                nouv2.transform.SetParent(transform);
                nouv2.transform.localScale = Vector3.one;
                nouv2.transform.localPosition = new Vector3(-275, 175 - i * 100, 0);
                nouv2.GetComponent<BarreScenario>().init(_note, scenar);
                nouv2.name = "BarreScenario" + i.ToString();
                i++;
            }
        }
	}
}
