﻿using UnityEngine;
using System.Collections;

public class ampoule : MonoBehaviour {
	public GameObject ampoule1;
	public GameObject ampoule2;
	public GameObject ampoule3;
	public GameObject ampoule4;


	public void changeEtat(int i){
		if (i == 0) {
			ampoule1.SetActive (true);
			ampoule2.SetActive (false);
			ampoule3.SetActive (false);
			ampoule4.SetActive (false);
		} else if (i == 1) {
			ampoule1.SetActive (false);
			ampoule2.SetActive (true);
			ampoule3.SetActive (false);
			ampoule4.SetActive (false);
		} else if (i == 2) {
			ampoule1.SetActive (false);
			ampoule2.SetActive (false);
			ampoule3.SetActive (true);
			ampoule4.SetActive (false);
		} else {
			ampoule1.SetActive (false);
			ampoule2.SetActive (false);
			ampoule3.SetActive (false);
			ampoule4.SetActive (true);
		}
	}
}
