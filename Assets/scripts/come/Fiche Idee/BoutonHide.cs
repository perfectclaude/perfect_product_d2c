﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BoutonHide : MonoBehaviour {
	GameObject ToHide;
	public bool etat;

	// Use this for initialization
	void Awake () {
		ToHide = transform.parent.GetChild (2).gameObject;
		ToHide.SetActive (false);
		etat = false;
	
	}

	public void OnClick(){
		ToHide.SetActive (!etat);
		etat = !etat;
		if (etat) {
			transform.GetChild (0).GetComponent<Text> ().text = "-";
		} else {
			transform.GetChild (0).GetComponent<Text> ().text = "+";
		}
	}
	// Update is called once per frame
	void Update () {
	
	}
}
