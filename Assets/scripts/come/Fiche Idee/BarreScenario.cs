﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BarreScenario : MonoBehaviour {
    private pp_produit_note _note = null;
    private pp_produit_scenario _scenario = null;

	//public void init (string nom,string descp,float cout,bool use,int i,int j )
    public void init (pp_produit_note note,pp_produit_scenario scenario )
    {
        _note = note;
        _scenario = scenario;

        if( (_note != null) && (_scenario != null) )
        {
            int NoScenario = _note.Produit.Scenarios.IndexOf(_scenario);
			Debug.Log ("numeroScenario"+NoScenario);
            bool use = (_scenario.Notes.IndexOf( _note) >= 0);

        
            transform.GetChild(0).GetChild(0).GetComponent<Text>().text = _scenario.Titre;
            transform.GetChild(1).GetComponent<Text>().text = _scenario.Description;
            transform.GetChild(2).GetComponent<InputField>().text = _note.CoutScenarios[NoScenario].ToString();

            transform.GetChild(0).GetChild(1).GetComponent<Toggle>().isOn = use;
        }
	}

	public void OnClick()
    {
        if( (_note != null) && (_scenario!= null) )
        {
            int NoScenario = _note.Produit.Scenarios.IndexOf(_scenario);
            if (NoScenario >= 0)
            {
                float fval = 0f;

                float.TryParse(transform.GetChild(2).GetComponent<InputField>().text, out fval);
                _note.CoutScenarios[NoScenario] = fval;
            }
        }
	}

}
