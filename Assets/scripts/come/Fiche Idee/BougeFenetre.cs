﻿using UnityEngine;
using System.Collections;

public class BougeFenetre : MonoBehaviour {
	public GameObject Gain;
	public GameObject Scenario;
	public GameObject Effort;
	public int etatActuel;


	public void OnClick(){
		StopAllCoroutines ();
		StartCoroutine ("Bouge");
	}

	IEnumerator Bouge() {
		if (etatActuel == 0) {
			while (Gain.transform.localPosition.y > -510 || Scenario.transform.localPosition.y > -630 || Effort.transform.localPosition.y > -750) {
				if (Gain.transform.localPosition.y > -510) {
					Vector3 vec = Gain.transform.localPosition;
					vec.y -= 800 *5* Time.deltaTime;
					Gain.transform.localPosition = vec;
				}
				if (Scenario.transform.localPosition.y > -630 ) {
					Vector3 vec = Scenario.transform.localPosition;
					vec.y -= 800 *5*  Time.deltaTime;
					Scenario.transform.localPosition = vec;
				}
				if (Effort.transform.localPosition.y>-750) {
					Vector3 vec = Effort.transform.localPosition;
					vec.y -= 800 *5*  Time.deltaTime;
					Effort.transform.localPosition = vec;
				}
				yield return new WaitForSeconds(Time.deltaTime);
			}	
			Vector3 vec1 = Gain.transform.localPosition;
			vec1.y = -510;
			Gain.transform.localPosition = vec1;
			vec1 = Scenario.transform.localPosition;
			vec1.y = -630;
			Scenario.transform.localPosition = vec1;
			vec1 = Effort.transform.localPosition;
			vec1.y  = -750;
			Effort.transform.localPosition = vec1;
		}
		if (etatActuel == 1) {
			while (Gain.transform.localPosition.y < 290 || Scenario.transform.localPosition.y > -630 || Effort.transform.localPosition.y > -750) {
				if (Gain.transform.localPosition.y < 290) {
					Vector3 vec = Gain.transform.localPosition;
					vec.y += 800 *5*  Time.deltaTime;
					Gain.transform.localPosition = vec;
				}
				if (Scenario.transform.localPosition.y > -630 ) {
					Vector3 vec = Scenario.transform.localPosition;
					vec.y -= 800 *5*  Time.deltaTime;
					Scenario.transform.localPosition = vec;
				}
				if (Effort.transform.localPosition.y>-750) {
					Vector3 vec = Effort.transform.localPosition;
					vec.y -= 800 *5*  Time.deltaTime;
					Effort.transform.localPosition = vec;
				}
				yield return new WaitForSeconds(Time.deltaTime);
			}	
			Vector3 vec1 = Gain.transform.localPosition;
			vec1.y = 291;
			Gain.transform.localPosition = vec1;
			vec1 = Scenario.transform.localPosition;
			vec1.y = -630;
			Scenario.transform.localPosition = vec1;
			vec1 = Effort.transform.localPosition;
			vec1.y  = -750;
			Effort.transform.localPosition = vec1;
		}
		if (etatActuel == 2) {
			while (Gain.transform.localPosition.y < 290 || Scenario.transform.localPosition.y < 170 || Effort.transform.localPosition.y > -750) {
				if (Gain.transform.localPosition.y < 290) {
					Vector3 vec = Gain.transform.localPosition;
					vec.y += 800 *5*  Time.deltaTime;
					Gain.transform.localPosition = vec;
				}
				if (Scenario.transform.localPosition.y < 170 ) {
					Vector3 vec = Scenario.transform.localPosition;
					vec.y += 800 *5*  Time.deltaTime;
					Scenario.transform.localPosition = vec;
				}
				if (Effort.transform.localPosition.y>-750) {
					Vector3 vec = Effort.transform.localPosition;
					vec.y -= 800 *5*  Time.deltaTime;
					Effort.transform.localPosition = vec;
				}
				yield return new WaitForSeconds(Time.deltaTime);
			}	
			Vector3 vec1 = Gain.transform.localPosition;
			vec1.y = 291;
			Gain.transform.localPosition = vec1;
			vec1 = Scenario.transform.localPosition;
			vec1.y = 170;
			Scenario.transform.localPosition = vec1;
			vec1 = Effort.transform.localPosition;
			vec1.y = -750;
			Effort.transform.localPosition = vec1;
		}
		if (etatActuel == 3) {
			while (Gain.transform.localPosition.y < 290 || Scenario.transform.localPosition.y < 170 || Effort.transform.localPosition.y < 50) {
				if (Gain.transform.localPosition.y < 290) {
					Vector3 vec = Gain.transform.localPosition;
					vec.y += 800 *5*  Time.deltaTime;
					Gain.transform.localPosition = vec;
				}
				if (Scenario.transform.localPosition.y < 170 ) {
					Vector3 vec = Scenario.transform.localPosition;
					vec.y += 800 *5*  Time.deltaTime;
					Scenario.transform.localPosition = vec;
				}
				if (Effort.transform.localPosition.y<50) {
					Vector3 vec = Effort.transform.localPosition;
					vec.y += 800 *5*  Time.deltaTime;
					Effort.transform.localPosition = vec;
				}
				yield return new WaitForSeconds(Time.deltaTime);
			}	
			Vector3 vec1 = Gain.transform.localPosition;
			vec1.y = 291;
			Gain.transform.localPosition = vec1;
			vec1 = Scenario.transform.localPosition;
			vec1.y = 170;
			Scenario.transform.localPosition = vec1;
			vec1 = Effort.transform.localPosition;
			vec1.y  = 50;
			Effort.transform.localPosition = vec1;
		}
		yield return null;
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}


}
