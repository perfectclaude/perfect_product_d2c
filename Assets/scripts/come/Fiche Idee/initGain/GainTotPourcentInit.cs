﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GainTotPourcentInit : MonoBehaviour {
    private pp_produit_note _note = null;
	public GameObject GainAnnuel;

	// Use this for initialization
	void Awake () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void changeCout(pp_produit_note note){
        _note = note;
        if (_note != null)
        {
            transform.GetComponent<Text>().text = _note.GainPotentielUnitaire + " €";
            //transform.GetComponent<Text>().text = _note.PourcentageGain;

            GainAnnuel.GetComponent<gainAnnuelInit>().changeCoutAnnuel(_note);
        }

	}
}
