﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class maturiteInit : MonoBehaviour {
    private pp_produit_note _note = null;
	
    public void init(pp_produit_note note,bool nouveau)
    {
        _note = note;
        if (_note != null)
        {
            transform.GetComponent<InputField>().text = _note.MaturiteDuPotentiel;
        }
	}
	public void OnClick()
    {
        if (_note != null)
        {
            _note.MaturiteDuPotentiel = transform.GetComponent<InputField>().text;
        }
		
	}
}
