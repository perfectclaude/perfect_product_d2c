﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class massProjeteInit : MonoBehaviour {
    private pp_produit_note _note = null;
	public GameObject gainMasse;
	
    public void init(pp_produit_note note,bool nouveau)
    {
        _note = note;
        if (_note != null)
        {
            transform.GetComponent<InputField>().text = _note.MasseProjete;
            gainMasse.GetComponent<GainDeMasseInit>().CalculMasse(_note);
        }
    }
	public void OnClick()
    {
        if (_note != null)
        {
            _note.MasseProjete = transform.GetComponent<InputField>().text;
            gainMasse.GetComponent<GainDeMasseInit>().CalculMasse(_note);
        }
	}
}
