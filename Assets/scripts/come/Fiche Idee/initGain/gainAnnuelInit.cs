﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class gainAnnuelInit : MonoBehaviour {
    private pp_produit_note _note = null;

	public GameObject rent;
	
    // Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void changeCoutAnnuel(pp_produit_note note)
    {
        _note = note;
        if (_note != null)
        {
            transform.GetComponent<Text>().text = _note.GainAnnuel + " €";

            rent.GetComponent<RentabiliteInit>().changeRentabilite(_note);
        }

	}
}
