﻿using UnityEngine;
using System.Collections;

public class gainInit : MonoBehaviour {
	public GameObject perimetreImpacte;
	public GameObject devise;
	public GameObject coutref;
	public GameObject masseref;
	public GameObject hypothesecalc;
	public GameObject maturite;
	public GameObject coutProjete;
	public GameObject massProjete;
	public GameObject visbleclient;
	public GameObject quantiteannuelle;

    private pp_produit_note _note = null;

    public void init(pp_produit_note note,bool nouveau) 
    {
        _note = note;
        if (_note != null)
        {
            perimetreImpacte.transform.GetComponent<perimetreImpactInit>().init(_note, nouveau);
            //devise.transform.GetComponent<InitDevise> ().init (_note, nouveau);
            coutref.transform.GetComponent<coutRefInit>().init(_note, nouveau);
            masseref.transform.GetComponent<masseRefInit>().init(_note, nouveau);
            hypothesecalc.transform.GetComponent<hypotheseCalculGainInit>().init(_note, nouveau);
            //maturite.transform.GetComponent<maturiteInit> ().init (ligne, nouveau);
            coutProjete.transform.GetComponent<coutProjeteInit>().init(_note, nouveau);
            massProjete.transform.GetComponent<massProjeteInit>().init(_note, nouveau);
            //visbleclient.transform.GetComponent<visibleClientInit> ().init (_note, nouveau);
            quantiteannuelle.transform.GetComponent<QuantiteAnnuelleInit>().init(_note, nouveau);
        }


	}
}
