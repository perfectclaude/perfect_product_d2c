﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class QuantiteAnnuelleInit : MonoBehaviour {
    private pp_produit_note _note = null;
	public GameObject GainAnnuel;
	
    public void init(pp_produit_note note,bool nouveau){
        _note = note;
        if (_note != null)
        {
            transform.GetComponent<InputField>().text = _note.QuantiteAnnuelle;
            GainAnnuel.GetComponent<gainAnnuelInit>().changeCoutAnnuel(_note);
        }
	}
	public void OnClick()
    {
        if (_note != null)
        {
            _note.QuantiteAnnuelle = transform.GetComponent<InputField>().text;
            GainAnnuel.GetComponent<gainAnnuelInit>().changeCoutAnnuel(_note);
        }
		
	}
}
