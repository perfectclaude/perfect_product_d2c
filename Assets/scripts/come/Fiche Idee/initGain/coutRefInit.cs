﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class coutRefInit : MonoBehaviour {
	public GameObject gain;

    private pp_produit_note _note = null;

    public void init(pp_produit_note note,bool nouveau)
    {
        _note = note;
        if (_note != null)
        {
            transform.GetComponent<InputField>().text = _note.CoutDeReference;
        }
	}
	public void OnClick()
    {
        if (_note != null)
        {
            _note.CoutDeReference = transform.GetComponent<InputField>().text;
            gain.GetComponent<GainTotPourcentInit>().changeCout(_note);
        }
	}
}
