﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class visibleClientInit : MonoBehaviour {
    private bool _initialisation=false;
    private pp_produit_note _note = null;

    public void init(pp_produit_note note,bool nouveau)
    {
        note = _note;
        if (_note != null)
        {
            _initialisation = true;
            transform.GetComponent<Toggle>().isOn = _note.IdeeVisibleClient_Val;
            _initialisation = false;
        }
	}

	public void OnClick()
    {
        if (_note != null)
        {
            if (!_initialisation)
            {
                _note.IdeeVisibleClient_Val = transform.GetComponent<Toggle>().isOn;
            }
        }
	}
}
