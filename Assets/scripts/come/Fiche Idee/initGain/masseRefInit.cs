﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class masseRefInit : MonoBehaviour {
    private pp_produit_note _note = null;
	public GameObject Gaindemasse;

	
    public void init(pp_produit_note note,bool nouveau)
    {
        _note = note;
        if (_note != null)
        {
            transform.GetComponent<InputField>().text = _note.MasseDeReference;
            Gaindemasse.GetComponent<GainDeMasseInit>().CalculMasse(_note);
        }
	}
	public void OnClick()
    {
        if (_note != null)
        {
            _note.MasseDeReference = transform.GetComponent<InputField>().text;
            Gaindemasse.GetComponent<GainDeMasseInit>().CalculMasse(_note);
        }
	}
}

