﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class coutProjeteInit : MonoBehaviour {
    private pp_produit_note _note = null;
	public GameObject gain;
	
    public void init(pp_produit_note note,bool nouveau){
        _note = note;
        if (_note != null)
        {
            transform.GetComponent<InputField>().text = _note.CoutProjete;
        }
	}
	public void OnClick(){
        if (_note != null)
        {
            _note.CoutProjete = transform.GetComponent<InputField>().text;
            gain.GetComponent<GainTotPourcentInit>().changeCout(_note);
        }
		
	}
}
