﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GainDeMasseInit : MonoBehaviour {

    private pp_produit_note _note = null;

	// Use this for initialization
	void Awake () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void CalculMasse(pp_produit_note note)
    {
        _note = note;
        if (_note != null)
        {
            transform.GetComponent<Text>().text = _note.GainDeMasse;
        }
	}
}
