﻿using UnityEngine;
using System.Collections;

public class ficheIdeeInit : MonoBehaviour {
    public pp_produit_note noteIdee = null;

    private pp_produit_note _curNote = null;


	public GameObject Identification;
	public GameObject gain;
	public GameObject option;
	public GameObject Scenario;
	public GameObject commentaire;
	public GameObject dateValid;
	public GameObject dateImplement;


    void Awake()
    {
    }

    IEnumerator Start()
    {
        bool bWaitReady = true;

        while (bWaitReady)
        {
            pp_produit_note note = pp_manager.GetCurrentNote();
            pp_produit prod = null;

            if (note != null)
                prod = note.Produit;

            if ((prod == null) || prod.IsLoading || !(prod.IsLoaded || prod.IsError))
                yield return new WaitForSeconds(0.1f);
            else
            {
                _curNote = note;
                bWaitReady = false;
            }
        }

        RefreshData();
    }

    void Update()
    {
    }



    public void init(pp_produit_note note, bool nouveau)
    {
        _curNote = note;
        RefreshData();
	}


    private void RefreshData()
    {
        if (_curNote != null)
        {
            Identification.transform.GetComponent<IdentificationInit>().init(_curNote, false);
            gain.transform.GetComponent<gainInit>().init(_curNote, false);
            option.transform.GetComponent<optionInit>().init(false, _curNote);
            Scenario.transform.GetChild(2).GetChild(0).GetComponent<FicheIdeeLoadScenar>().init(false, _curNote);
            dateValid.GetComponent<dateValidation> ().init (_curNote, false);
            dateImplement.GetComponent<dateImplementation> ().init (_curNote, false);
            commentaire.GetComponent<ScenarioCommentaire> ().init (_curNote, false);

        }
    }



    // Events
    public void OnClick_DetruireIdee()
    {
        if (_curNote != null)
        {
            pp_produit_composant comp = _curNote.Composant;
            pp_produit prod = null;


            if (comp != null)
            {
                prod = comp.Produit;

                comp.Notes.Remove(_curNote);
            }

            pp_manager.MainScene.ForceRefresh = true;
            //Destroy(GameObject.Find("Pop-up"));

            if (prod != null)
                prod.SaveData();

            /*
            try
            {
                GameObject.Find("BarreCreerScenario").GetComponent<CreateScenario>().init = true;
            }
            catch
            {
                Debug.Log("pas dans le menu scenario");
            }
            */
        }

    }
}
