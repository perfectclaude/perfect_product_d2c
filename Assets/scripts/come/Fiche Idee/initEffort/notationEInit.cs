﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class notationEInit : MonoBehaviour {
	public int numero;
	public int csv;
    private pp_produit_note _note = null;

	// Use this for initialization
	void Start () {
		//numero = 2;
	}

    // Update is called once per frame
    void Update () {

    }

    public void init(pp_produit_note note,bool nouveau)
    {
        _note = note;
        if (_note != null)
        {
            int inum = 1;

            switch (csv)
            {
                case 38:
                    inum = _note.NoteEnergie_Val;
                    break;

                case 40:
                    inum = _note.NoteRentabilite_Val;
                    break;

                case 42:
                    inum = _note.NoteAdhesion_Val;
                    break;

                default:
                    break;
            }
            numero = inum - 1;
            ChangeNumero(numero);
        }
	}

	public void ChangeNumero(int num)
    {
        if (_note != null)
        {
            numero = num;
            Debug.Log(numero);
            int h = 0;
            foreach (Transform t in transform)
            {
			
                if (h > numero)
                {
				
                    t.GetComponent<numeroEtoile>().init = false;
                    t.GetComponent<Toggle>().isOn = false;
                    t.GetComponent<numeroEtoile>().init = true;
                }
                else
                {
				
                    t.GetComponent<numeroEtoile>().init = false;
                    t.GetComponent<Toggle>().isOn = true;
                    t.GetComponent<numeroEtoile>().init = true;
                }
                h++;

                switch (csv)
                {
                    case 38:
                        _note.NoteEnergie_Val = (numero + 1);
                        break;

                    case 40:
                        _note.NoteRentabilite_Val = (numero + 1);
                        break;

                    case 42:
                        _note.NoteAdhesion_Val = (numero + 1);
                        break;

                    default:
                        break;
                }
            }
        }
	}
	
}
