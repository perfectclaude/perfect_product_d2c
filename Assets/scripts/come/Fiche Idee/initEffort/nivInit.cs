﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class nivInit : MonoBehaviour {
    private pp_produit_note _note = null;

    public void init(pp_produit_note note)
    {
        _note = note;
    }

	// Update is called once per frame
	void Update () 
    {
        if (_note != null)
        {
            int num = _note.NoteEnergie_Val - 1;
			if (num < 2) {
                transform.GetComponent<Text> ().text = localization.GetTextWithKey("LOW_TEXT");
			} else if (num == 2) {
                transform.GetComponent<Text> ().text = localization.GetTextWithKey("MEDIUM_TEXT");
			} else {
                transform.GetComponent<Text> ().text = localization.GetTextWithKey("HIGTH_TEXT");
			}

            _note.CommentaireValidationEtDeveloppement = transform.GetComponent<Text> ().text;
		}
	}

	
}
