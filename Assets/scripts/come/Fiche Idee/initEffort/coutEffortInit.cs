﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class coutEffortInit : MonoBehaviour {
	
    private pp_produit_note _note = null;
	public GameObject Nrc;
	
    public void init(pp_produit_note note,bool nouveau)
    {
        _note = note;
        if (_note != null)
        {
            transform.GetComponent<InputField>().text = _note.EffortValidationEtDeveloppelement;
            Nrc.GetComponent<NRCInit>().CalculNRC(_note);
        }
	}
	
	
	public void OnClick(){
        if (_note != null)
        {
            _note.EffortValidationEtDeveloppelement = transform.GetComponent<InputField>().text;
            Nrc.GetComponent<NRCInit>().CalculNRC(_note);
        }
		
	}
}
