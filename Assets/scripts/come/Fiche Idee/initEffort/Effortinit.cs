﻿using UnityEngine;
using System.Collections;

public class Effortinit : MonoBehaviour {

    public void init(bool test, pp_produit_note note){
        //transform.GetChild (1).GetComponent<notationEInit> ().init (note, test);
        transform.GetChild (2).GetComponent<commentaireInitEffort> ().init (note, test);
        transform.GetChild (4).GetComponent<jhScript> ().init (note, test);
        transform.GetChild (1).GetComponent<coutEffortInit> ().init (note, test);
        transform.GetChild (0).GetComponent<initInvestOutil> ().init (note, test);
        transform.GetChild (6).GetComponent<notationEInit> ().init (note,test);
        transform.parent.GetChild (4).GetChild(1).GetComponent<notationEInit> ().init (note, test);
        transform.parent.GetChild (5).GetChild(1).GetComponent<notationEInit> ().init (note, test);
	}
}
