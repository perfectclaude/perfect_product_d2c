﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class NRCInit : MonoBehaviour {
    private pp_produit_note _note = null;
	public float coutH;
	public float coutOutil;
	public GameObject rent;



    public void CalculNRC(pp_produit_note note){
        _note = note;
        if (_note != null)
        {
            transform.GetComponent<Text>().text = _note.TotalNRC + " €";
            rent.GetComponent<RentabiliteInit>().changeRentabilite(_note);
        }
	}
}
