﻿ using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class commentaireInitEffort : MonoBehaviour {
    private pp_produit_note _note = null;
	public GameObject cout;
	
    public void init(pp_produit_note note,bool nouveau)
    {
        _note = note;
        if (_note != null)
        {
            transform.GetComponent<InputField>().text = _note.CommentaireValidationEtDeveloppement;
        }
	}
	
	
	public void OnClick()
    {
        if (_note != null)
        {
            _note.CommentaireValidationEtDeveloppement = transform.GetComponent<InputField>().text;

            cout.GetComponent<coutjourneeHoraire>().changeCout(_note);
        }
		
	}
}
