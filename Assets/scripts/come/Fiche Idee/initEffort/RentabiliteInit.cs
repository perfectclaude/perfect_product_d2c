﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class RentabiliteInit : MonoBehaviour {

    private pp_produit_note _note = null;

    public void changeRentabilite(pp_produit_note note){
        _note = note;
        if (_note != null)
        {
            transform.GetComponent<Text>().text = _note.RentabiliteROI + " mois";
        }
	}
}
