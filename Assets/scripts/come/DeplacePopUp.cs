﻿using UnityEngine;
using System.Collections;

public class DeplacePopUp : MonoBehaviour {
	public Camera 		m_ViewCamera = null;
	public bool click;
	Vector2 positionSouris;
	Vector3 			startpos;
	
	private Vector3 screenPoint;
	private Vector3 offset;
	// Use this for initialization
	void Start () {
			 
		m_ViewCamera = GameObject.Find("Main Camera").GetComponent<Camera>();

	}


	
	// Update is called once per frame
	void Update(){
		RaycastHit hitInfo;
		Ray ray;
	
		ray = m_ViewCamera.ScreenPointToRay (Input.mousePosition);
		if (Input.GetMouseButtonDown (0)) {
		
			if (gameObject.GetComponent<Collider> ().Raycast (ray, out hitInfo, 1.0e8f)) {
				if (hitInfo.transform.name == "BarDeplacement") {
					click=true;
					startpos = m_ViewCamera.ScreenToWorldPoint(Input.mousePosition);
					
				}
			}
		}
		if (Input.GetMouseButton (0) && click) {

			Vector3 vec = m_ViewCamera.ScreenToWorldPoint(Input.mousePosition);
			float diffy = vec.y - startpos.y;
			float diffx = vec.x - startpos.x;
			startpos = vec;
			vec = gameObject.transform.parent.transform.localPosition;
			vec.y += diffy;
			vec.x+=diffx;
		
			gameObject.transform.parent.transform.localPosition = vec;
			
			
		} 
		else {
			click=false;
		}
	}


}
