using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public struct scenario{
	public string nom;
	public List<string[]> idees;

}
public class sceneIdee : MonoBehaviour {
	public static bool stat;
	public static List<string[]> notes = null;
	public static List<string[]> boms = null;
	public static string inis = null;
	public static List<scenario> scenarios;
	public byte [] note = null;
	public byte [] bom = null;
	public byte [] ini = null;
	public byte [] txtscen = null;
	public GameObject window;
	public GameObject add;
	public GameObject del;
	public GameObject sav;
	public GameObject dep;
	public bool init;



	void Start(){

		window = GameObject.Find ("Panneau_Droite");
		del = GameObject.Find ("deletescenario");
		add = GameObject.Find ("addscenario");
		dep = GameObject.Find ("Deployer");
		sav = GameObject.Find ("Sauvegarder");
		scenarios = new List<scenario> ();
		init = true;
		StartCoroutine (loadnote());
	}
	
	IEnumerator loadnote()
	{
		networkfiles.LoadFile(autoloadcurrent3dobject.pathObjecttoload,autoloadcurrent3dobject.pathObjecttoload+"_note.csv");
		while (networkfiles.networkfilesbusy) {
			yield return new WaitForSeconds (0.1f);
		}
		note = networkfiles.Data;
		networkfiles.LoadFile (autoloadcurrent3dobject.pathObjecttoload, autoloadcurrent3dobject.pathObjecttoload+"_bom.csv");
		while (networkfiles.networkfilesbusy) {
			yield return new WaitForSeconds (0.1f);
		}
		bom = networkfiles.Data;
		networkfiles.LoadFile (autoloadcurrent3dobject.pathObjecttoload, autoloadcurrent3dobject.pathObjecttoload+"_scenario.txt");
		while (networkfiles.networkfilesbusy) {
			yield return new WaitForSeconds (0.1f);
		}
		txtscen = networkfiles.Data;

		networkfiles.LoadFile (autoloadcurrent3dobject.pathObjecttoload, autoloadcurrent3dobject.pathObjecttoload+"_ini.txt");
		while (networkfiles.networkfilesbusy) {
			yield return new WaitForSeconds (0.1f);
		}
		ini = networkfiles.Data;


		parseCSV_note();
		parseCSV_bom ();
		parseCSV_ini ();
		scenarios = loadScenarioSave (System.Text.Encoding.ASCII.GetString (txtscen));
	
		afficherListe.test = true;
		yield return new WaitForSeconds (0.5f);
		window.SetActive (false);
		add.SetActive (false);
		del.SetActive(false);
		dep.SetActive (false);
		sav.SetActive(false);
		yield return null;
		
		
		
	}
	
	// Update is called once per frame
	void Update () {

		if (scenarios.Count > 0 && init && stat) {
			init=false;
			nomScenarioUI.numScenar=0;
			GameObject.Find("titre0").transform.GetComponent<nomScenarioUI>().changeNom(0);
		}
	}

	public static void WriteXcelSauv()
	{
		string outstring ="";
		bool first = false;
		// run trough all objects
		foreach (scenario scen in scenarios) {
			outstring+=scen.nom;
			outstring+="\n";
			foreach (string[] key in scen.idees) {
			
			
				foreach (string mots in key) {
				
					outstring += mots + ";";
				}
				outstring += "\n";
			}
			outstring+="<scenario>\n";
		}
		networkfiles.SaveFile(autoloadcurrent3dobject.pathObjecttoload,autoloadcurrent3dobject.pathObjecttoload+"_scenario.txt",outstring);
		
		
		
		
	}

	public static List<scenario> loadScenarioSave(string fichier ){
		string allobject=null;
		string[] splitted=null;
		Debug.Log (fichier);
		fichier = fichier.Replace("\n\r","\n");
		fichier = fichier.Replace("\r\n","\n");
		fichier = fichier.Replace("\r","\n");
		string[] lignes=fichier.Split('\n')	;
		
		List<scenario> mylist = new List<scenario> ();
		scenario tmp = new scenario ();
		tmp.idees=new List<string[]>();
		bool nouv = true;
		// Saut de ligne
		
		foreach (string line in lignes)
		{

			if ((line == "") || (line == null))
			{
				//Debug.Log("pas de ligne"+line);
			}
			else
			{
				if(line=="<scenario>"){
					//Debug.Log ("nouv scenar");
					nouv=true;
					mylist.Add(tmp);
					tmp=new scenario();
					tmp.idees=new List<string[]>();

				}
				else{
					if(nouv==true){
						//Debug.Log("nom: "+line);
						tmp.nom=line;
						nouv=false;
					}
					else{
						//Debug.Log("idee: "+line);
						splitted = line.Split(';') ;
				
						tmp.idees.Add(splitted);
					}
				}
			}
		}
		return (mylist);
	}

	public static void WriteXcel_notes()
	{
		string outstring ="";
		bool first = false;
		// run trough all objects
		foreach (string[] key in notes ) {
			
			
			foreach(string mots in key){
				
				outstring+=mots+";";
			}
			outstring+="\n";
		}
		networkfiles.SaveFile(autoloadcurrent3dobject.pathObjecttoload,autoloadcurrent3dobject.pathObjecttoload+"_bom.csv",outstring);
		



	}

	static void majProfit (List<string[]> notscen, string refer,string profit)
	{
		foreach (string[] tmp in notscen) {
			if(tmp[1]==refer){
				tmp[9]=(float.Parse(tmp[9].Replace(',','.'))-float.Parse(profit)).ToString();
			}
		}
	}

	public static List<string[]> WriteXcelScenario(int i)
	{
		List<string[]> notescenar = boms;
		string outstring ="";
		bool first = false;
		foreach (string[] key in scenarios[i].idees) {
			majProfit(notescenar,key[0],key[2]);
			key[2]="0";
		}
		foreach (string tmp in notes[0]) {

			outstring += tmp + ";";
		}
		outstring += "\n";
		// run trough all objects
		foreach (string[] key in scenarios[i].idees ) {
			
			
			foreach(string mots in key){
				
				outstring+=mots+";";
			}
			outstring+="\n";
		}
		
		networkfiles.SaveFile(scenarios[i].nom,scenarios[i].nom+"_note.csv",outstring);
		return notescenar;
	}

	public static void sauvegarderBom (List<string[]> tmp,int i)
	{
		string outstring = "";
		foreach (string[] key in tmp ) {
			
			
			foreach(string mots in key){
				
				outstring+=mots+";";
			}
			outstring+="\n";

		}
		
		networkfiles.SaveFile(scenarios[i].nom,scenarios[i].nom+"_bom.csv",outstring);

	}
	public static void sauvegarderIni (int i)
	{
	
		networkfiles.SaveFile(scenarios[i].nom,scenarios[i].nom+"_ini.txt",inis);
		
	}

	List<string[]> parseCSV_root(string fichier)
	{
		string allobject=null;
		string[] splitted=null;
		
		fichier = fichier.Replace("\n\r","\n");
		fichier = fichier.Replace("\r\n","\n");
		fichier = fichier.Replace("\r","\n");
		string[] lignes=fichier.Split('\n')	;
		
		List<string[]> mylist = new List<string[]> ();
		// Saut de ligne
		
		foreach (string line in lignes)
		{
			if ((line == "") || (line == null))
			{
			}
			else
			{
				splitted = line.Split(';') ;
				
				mylist.Add(splitted);
			}
		}
		return (mylist);
	}
	
	void parseCSV_note()
	{

		if (note == null) {
			string[] tmp=new string[9];
			tmp[0]="ERP";
			tmp[1]="Note";
			tmp[2]="Profit";
			tmp[3]="Identification";
			tmp[4]="Practicability";
			tmp[5]="Validation";
			tmp[6]="Deployment";
			tmp[7]="Manager";
			tmp[8]="Action";
			notes=new List<string[]>();
			notes.Add(tmp);							
			return;
		}
				
		notes = parseCSV_root(System.Text.Encoding.ASCII.GetString(note));
	}

	void parseCSV_ini()
	{
		

		inis = System.Text.Encoding.ASCII.GetString(ini);
	}

	void parseCSV_bom()
	{
		

		
		boms = parseCSV_root(System.Text.Encoding.ASCII.GetString(bom));
	}
	
	public  void OnClick ()
	{

		if (stat) {
			GameObject.Find("ScrollbarList").GetComponent<Scrollbar>().value=1;
            transform.GetChild (0).GetComponent<Text> ().text = localization.GetTextWithKey("IDEA_TEXT");
			window.SetActive (false);
			add.SetActive (false);
			del.SetActive(false);
			dep.SetActive (false);
			sav.SetActive(false);


		} else {
			GameObject.Find("ScrollbarList").GetComponent<Scrollbar>().value=1;
            transform.GetChild (0).GetComponent<Text> ().text = localization.GetTextWithKey("SCENARIO_TEXT");
			window.SetActive (true);
			add.SetActive (true);
			del.SetActive(true);
			dep.SetActive (true);
			sav.SetActive(true);

		}
		stat = !stat;
	}
}
