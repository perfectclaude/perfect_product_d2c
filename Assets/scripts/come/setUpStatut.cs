﻿using UnityEngine;
using System.Collections;

public class setUpStatut : MonoBehaviour {

	public static string statut;

	public static void changeStatut(string statutNouv){
		if (statut != null && statut != "") {
			GameObject.Find("DateStatut").transform.FindChild("Coche"+statut).transform.GetChild(1).GetComponent<tk2dSprite>().color=Color.white;
		}
		GameObject.Find("DateStatut").transform.FindChild("Coche"+statutNouv).transform.GetChild(1).GetComponent<tk2dSprite>().color=Color.black;
		statut = statutNouv;
		if (statutNouv == "I") {
			GameObject.Find("DateStatut").transform.GetChild (0).GetComponent<tk2dTextMesh> ().text=System.DateTime.Now.ToString("dd/MM/yyyy");
			GameObject.Find("DateStatut").transform.GetChild (1).GetComponent<tk2dTextMesh> ().text="";
			GameObject.Find("DateStatut").transform.GetChild (2).GetComponent<tk2dTextMesh> ().text="";
			GameObject.Find("DateStatut").transform.GetChild (3).GetComponent<tk2dTextMesh> ().text="";

		}
		else if(statutNouv == "F") {
			GameObject.Find("DateStatut").transform.GetChild (1).GetComponent<tk2dTextMesh> ().text=System.DateTime.Now.ToString("dd/MM/yyyy");
			GameObject.Find("DateStatut").transform.GetChild (2).GetComponent<tk2dTextMesh> ().text="";
			GameObject.Find("DateStatut").transform.GetChild (3).GetComponent<tk2dTextMesh> ().text="";
			
		}
		else if(statutNouv == "V") {
			GameObject.Find("DateStatut").transform.GetChild (2).GetComponent<tk2dTextMesh> ().text=System.DateTime.Now.ToString("dd/MM/yyyy");
			GameObject.Find("DateStatut").transform.GetChild (3).GetComponent<tk2dTextMesh> ().text="";
			
		}
		else if(statutNouv == "D") {
			GameObject.Find("DateStatut").transform.GetChild (3).GetComponent<tk2dTextMesh> ().text=System.DateTime.Now.ToString("dd/MM/yyyy");
		}


	}


}
