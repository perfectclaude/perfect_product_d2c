﻿using UnityEngine;
using System.Collections;

public class SecondPlan : MonoBehaviour {
	GameObject piece;
	GameObject idee;
	GameObject fonction;

	void Start(){
		piece = transform.GetChild (1).gameObject;
		idee = transform.GetChild (2).gameObject;
		fonction = transform.GetChild (3).gameObject;
		piece.SetActive (true);
		idee.SetActive (false);
		fonction.SetActive (false);

	}

	public void Change(int num){
		if (num == 0) {
			piece.SetActive (true);
			idee.SetActive (false);
			fonction.SetActive (false);
		} else if (num == 1) {
			piece.SetActive (false);
			idee.SetActive (true);
			fonction.SetActive (false);
		} else {
			piece.SetActive (false);
			idee.SetActive (false);
			fonction.SetActive (true);
		}
	}

}
