﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class deployerScenario : buttonswap {

	public override void ButtonPressed ()
	{
		base.ButtonPressed();
		if (sceneIdee.scenarios.Count > 0) {
			// verify ik Link completed
			StartCoroutine ("SaveCSV");
		}
	}
	
	IEnumerator SaveCSV()
	{
		Debug.Log ("save csv");

		
		List<string[]> tmp= sceneIdee.WriteXcelScenario(nomScenar.numScenar);
		//		linkEditor.WriteFile();
		while (networkfiles.networkfilesbusy)
			yield return new WaitForSeconds(0.05f);

		sceneIdee.sauvegarderBom (tmp,nomScenar.numScenar);
		while (networkfiles.networkfilesbusy)
			yield return new WaitForSeconds(0.05f);
		sceneIdee.sauvegarderIni (nomScenar.numScenar);

		
	}
}
