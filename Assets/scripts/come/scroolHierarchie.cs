﻿using UnityEngine;
using System.Collections;

public class scroolHierarchie : MonoBehaviour {

	public bool	wasIpressed = false;
	Vector3 			startpos;
	public Camera 		m_ViewCamera;
	public float 		minyval = 1.0f;
	public float 		maxyval = 1.0f;
	bool				icanmove = false;
	static GameObject	myself;
	static float 		mymaxyval;

	public static void ResetPos()
	{
		Vector3 vec = myself.transform.localPosition;
		vec.y = mymaxyval;
		myself.transform.localPosition = vec;
	}

	GameObject scrollwindow;
	//-1.068688
	void Awake ()
	{
		myself = gameObject;
		mymaxyval = maxyval;
		m_ViewCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
		scrollwindow = GameObject.Find ("interface").FindInChildren("righttop").FindInChildren("scrollwindow");
		gameObject.AddComponent<BoxCollider>();
	}
	
	void Update ()
	{
		mymaxyval = maxyval;
		if (Input.GetMouseButtonDown(0))
		{

			RaycastHit hitInfo;
			Ray ray;
			
			ray = m_ViewCamera.ScreenPointToRay(Input.mousePosition);
			if (gameObject.GetComponent<Collider>().Raycast(ray, out hitInfo, 1.0e8f))
			{
				wasIpressed = true;
				startpos = m_ViewCamera.ScreenToWorldPoint(Input.mousePosition);
				icanmove = false;
			}
		}
		if (Input.GetMouseButtonUp (0))
		{
			wasIpressed = false;
		}

		if (wasIpressed)
		{

			// Drag the BG here
			Vector3 vec = m_ViewCamera.ScreenToWorldPoint(Input.mousePosition);
			float diffy = vec.y - startpos.y;
			float difftotal = gameObject.transform.position.y;

			if (Mathf.Abs(diffy) > 0.022f)				icanmove = true;
			if (icanmove)
			{
				scrollwindow.AddComponent<BoxCollider>();
				load3d_object.FitColliderToChildren(scrollwindow);
				BoxCollider boxcol = scrollwindow.GetComponent<BoxCollider>();
				float myheight = boxcol.size.y;
				Destroy (boxcol);

//				Debug.Log ("Height:"+myheight);
				startpos = vec;
				vec = gameObject.transform.position;
				vec.y += diffy;
				gameObject.transform.position = vec;

				vec = gameObject.transform.localPosition;
				if (vec.y < minyval)		vec.y = minyval;
				if (vec.y > maxyval)		vec.y = maxyval;
				gameObject.transform.localPosition = vec;

				difftotal = gameObject.transform.position.y - difftotal;

				vec = scrollwindow.transform.position;
				myheight = myheight / 1.65f;
				myheight = myheight - 1.0f;
				if (myheight < 0)		myheight = 0;
				vec.y -= difftotal * myheight;
				scrollwindow.transform.position = vec;

				/*
				if(diffy>0){
					CreateHierarchie.setObjetActuel(CreateHierarchie.objetActuel,CreateHierarchie.posDeb+1);
				}
				else if(diffy<0){
					CreateHierarchie.setObjetActuel(CreateHierarchie.objetActuel,CreateHierarchie.posDeb-1);
				}
				*/
			}
		}
		else
		{
		}
		
	}
}
