﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class afficherListe : MonoBehaviour {

	Transform attachparent = null;
	public static bool test;
	public static bool chang;
	public static int scenario;
	public GameObject scenW;
	void Awake()
	{
		attachparent = (Transform)GameObject.Find ("ListObjet").transform;
		 scenW=GameObject.Find("ScenarioW");

	}
	// Use this for initialization
	void Start () {
		chang = true;
	}
	
	// Update is called once per frame
	void Update () {

		if (test && chang) {
			Debug.Log ("chang");
			foreach(Transform tr in transform){
				Destroy(tr.gameObject);
			}
			foreach(Transform tr in scenW.transform){
				Destroy(tr.gameObject);
			}
			if (sceneIdee.notes.Count > 1) {
				int k = 0;
				foreach (string[] ligne in sceneIdee.notes) {
					if (k > 0) {
						GameObject idee = Instantiate (Resources.Load ("IdeeDefiniUI")) as GameObject;
						GameObject cochecase=Instantiate (Resources.Load ("Coche caseUI")) as GameObject;
						cochecase.transform.parent=scenW.transform;

						idee.transform.parent = attachparent; //transform; 
						Vector3 pos3 = new Vector3 ();
						pos3.x = 68;
						pos3.z = 0;
					
						pos3.y = 132 - ((k) * 72);
						idee.transform.localPosition = pos3;
						pos3.y = 132 -((k) * 72);
						pos3.x =0 ;
						pos3.z=-0.1f;
						cochecase.transform.localPosition=pos3;
						cochecase.transform.GetChild(0).GetComponent<toggleComposant>().ind=k;
						idee.name = "idee " + k.ToString ();
						cochecase.name="cochecase"+k.ToString();
						cochecase.transform.GetChild(0).GetComponent<toggleComposant>().ind=k;
						cochecase.transform.GetChild(0).GetComponent<toggleComposant>().numScenario=scenario;
						cochecase.transform.localScale=new Vector3(1.5f,0.69f,1);
						idee.transform.localScale=new Vector3(0.75f,0.75f,1);
						idee.transform.GetComponent<SetUpLineUI>().init(k);
						/*if (ligne [7] == "F") {
							idee.transform.GetComponent<tk2dSlicedSprite> ().color = new Color (0.75f, 0.75f, 0.2f);
							cochecase.transform.GetComponent<tk2dSlicedSprite> ().color = new Color (0.75f, 0.75f, 0.2f);
						
						} else if (ligne [7] == "D") {
							idee.transform.GetComponent<tk2dSlicedSprite> ().color = new Color (0.2f, 0.75f, 0.2f);
							cochecase.transform.GetComponent<tk2dSlicedSprite> ().color = new Color (0.2f, 0.75f, 0.2f);
						} else if (ligne [7] == "V") {
							idee.transform.GetComponent<tk2dSlicedSprite> ().color = new Color (0.2f, 0.2f, 0.75f);
							cochecase.transform.GetComponent<tk2dSlicedSprite> ().color = new Color (0.2f, 0.2f, 0.75f);
						}
						if (k % 2 == 1) {
							if (ligne [7] == "I") {
								idee.transform.GetComponent<tk2dSlicedSprite> ().color = new Color (1, 0.75f, 0.0f);
								cochecase.transform.GetComponent<tk2dSlicedSprite> ().color = new Color (1, 0.75f, 0.0f);
							} else if (ligne [7] == "F") {
								idee.transform.GetComponent<tk2dSlicedSprite> ().color = new Color (1, 1, 0.3f);
								cochecase.transform.GetComponent<tk2dSlicedSprite> ().color = new Color (1, 1, 0.3f);
							
							} else if (ligne [7] == "D") {
								idee.transform.GetComponent<tk2dSlicedSprite> ().color = new Color (0.5f, 1, 0.0f);
								cochecase.transform.GetComponent<tk2dSlicedSprite> ().color = new Color (0.5f, 1, 0.0f);
							} else if (ligne [7] == "V") {
								idee.transform.GetComponent<tk2dSlicedSprite> ().color = new Color (0, 0.5f, 1f);
								cochecase.transform.GetComponent<tk2dSlicedSprite> ().color = new Color (0, 0.5f, 1f);
							}
						}*/


					}
					else{
						GameObject idee = Instantiate (Resources.Load ("TitreUI")) as GameObject;
						//GameObject cochecase=Instantiate (Resources.Load ("nomScenarioUI")) as GameObject;
						//cochecase.transform.parent=scenW.transform;
						//cochecase.transform.localScale=new Vector3(1,1,1);

						idee.transform.parent = attachparent; //transform; 
						idee.transform.localScale=new Vector3(0.5f,0.5f,1);
						Vector3 pos3 = new Vector3 ();
						pos3.x = -36;
						pos3.z = 0;
						
						pos3.y = 132 - ((k) * 52);
						idee.transform.localPosition = pos3;
						pos3.y = 147 -((k) * 52);
						pos3.x =0 ;
						pos3.z=-0.1f;
						//cochecase.transform.localPosition=pos3;

						idee.name = "id " + k.ToString ();
						//cochecase.name="titre"+k.ToString();

					
					}
					k++;
				}
			}
			else{
				int k=0;
				GameObject idee = Instantiate (Resources.Load ("TitreUI")) as GameObject;
				//GameObject cochecase=Instantiate (Resources.Load ("nomScenarioUI")) as GameObject;
				//cochecase.transform.parent=scenW.transform;
				//cochecase.transform.localScale=new Vector3(1,1,1);;

				idee.transform.parent = attachparent; //transform; 
				idee.transform.localScale=new Vector3(0.5f,0.5f,1);;

				Vector3 pos3 = new Vector3 ();
				pos3.x = 50;
				pos3.z = 0;
				
				pos3.y = 132 - ((k) * 52);
				//Debug.Log(pos3);
				idee.transform.localPosition=pos3;
				pos3.y = 147 -((k) * 52);
				pos3.x =0 ;
				pos3.z=-0.1f;
				//cochecase.transform.localPosition=pos3;
				
				idee.name = "id " + k.ToString ();
				//cochecase.name="titre"+k.ToString();


			}
			GameObject.Find("ScrollbarList").GetComponent<Scrollbar>().value=1;
			chang=false;
		}
	}
}
