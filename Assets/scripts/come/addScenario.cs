﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class addScenario : MonoBehaviour {
	GameObject panel;

	
	void Start(){
		panel = GameObject.Find ("PanelFlou");

	}
	public  void OnClick()
	{

		GameObject nouv = Instantiate (Resources.Load ("PopupScenarioUI")) as GameObject;
		panel.SetActive (true);
		nouv.name = "Pop-up";
		nouv.transform.parent=GameObject.Find("Canvas").transform;
		nouv.transform.localPosition=Vector3.zero;
		nouv.transform.localScale=Vector3.one;
	}
}
