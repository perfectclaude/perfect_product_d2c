﻿using UnityEngine;
using System.Collections;

public class destroyIdee : buttonswap{
    public pp_produit_note note = null;
	public string getIdee;

	public override void ButtonPressed ()
	{
		base.ButtonPressed ();
		if (!GameObject.Find ("Pop-up")) {
			buttonswap.myactive = false;
			buttonPopup.myactive = true;
			GameObject nouv = Instantiate (Resources.Load ("PopupDecision")) as GameObject;
			nouv.transform.GetChild(0).GetComponent<tk2dTextMesh> ().text+=getIdee;
            nouv.transform.GetChild(2).GetComponent<okDelete> ().note = note;
			nouv.name = "Pop-up";
		}
	}
}
