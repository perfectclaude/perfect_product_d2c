﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class initGainScenar : MonoBehaviour {
	public GameObject creatScenar;
	public GameObject Rouge;
	public GameObject Vert;

    public void instantiate(pp_produit_scenario scenario, pp_produit_note note){
		float res = 0;
        int NoScenario = note.Produit.Scenarios.IndexOf(scenario);

        if( (note != null) && (NoScenario >= 0) && (NoScenario < note.CoutScenarios.Count) )
            res =  note.CoutScenarios[NoScenario];

		
		if (res < 0) {
			Rouge.SetActive (true);
			Vert.SetActive (false);
		} else {
			Vert.SetActive (true);
			Rouge.SetActive (false);
		}
		transform.GetChild (0).GetComponent<Text> ().text = res.ToString ()+ " €";

	}


}
