﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class deploimentidee : MonoBehaviour {
	
	private bool b = true;
	public GameObject createScen;
	public GameObject ZoneDep;
	public GameObject ZoneDepHaut;
	public GameObject ScrollGauche;
	public GameObject ScrollHaut;
	public List<GameObject> listeobjetInstancie;


	void Update(){
		if (b) 
        {
			instancie ();
			
		}
	}




	public void instancie()
    {
        pp_produit curProd = pp_manager.GetCurrentProduit();

        if ((curProd == null) || !curProd.IsLoaded)
            b = true;
        else
        {
            b = false;

            foreach (GameObject g in listeobjetInstancie)
            {
                Destroy(g);
            }
            //ScrollGauche.GetComponent<Scrollbar> ().value = 0;
            //ScrollHaut.GetComponent<Scrollbar> ().value = 0;
            listeobjetInstancie = new List<GameObject>();
            int j = 0;
            foreach (pp_produit_scenario scenar in curProd.Scenarios)
            {
                GameObject nouv = Instantiate(Resources.Load("TitreScenario")) as GameObject;
                nouv.transform.SetParent(ZoneDep.transform);
                nouv.transform.localScale = Vector3.one;
                nouv.transform.localPosition = new Vector3(760 + j * 294, 550, 0);
                if (j < 9)
                {
                    nouv.transform.GetChild(0).GetComponent<Text>().text = "Sc0" + (j + 1).ToString();
                }
                else
                {
                    nouv.transform.GetChild(0).GetComponent<Text>().text = "Sc" + (j + 1).ToString();

                }
                nouv.name = scenar.Titre + j.ToString();
                nouv.transform.GetChild(2).GetChild(0).GetChild(0).GetComponent<Text>().text = scenar.Titre;
                listeobjetInstancie.Add(nouv);

                GameObject nouv2 = Instantiate(Resources.Load("GainScenario")) as GameObject;
                nouv2.transform.SetParent(ZoneDep.transform);
                nouv2.transform.localScale = Vector3.one;
                nouv2.transform.localPosition = new Vector3(884 + j * 294, 550, 0);
                nouv2.name = "GainScenario" + j.ToString();
                listeobjetInstancie.Add(nouv2);


                j++;
            }

            int i = 1;
            foreach (pp_produit_note note in curProd.notes)
            {
                bool use = false;
                foreach (pp_produit_scenario scenar in curProd.Scenarios)
                {
                    if (scenar.Notes.IndexOf( note ) >= 0)
                    {
                        use = true;
                        break;
                    }
                }

                if (use)
                {
                    switch (note.StatutIdee)
                    {
                        case "Valide":
                            GameObject nouv = Instantiate(Resources.Load("IdeeScenarioValidee")) as GameObject;
                            nouv.transform.SetParent(transform);
                            nouv.transform.localScale = Vector3.one;
                            nouv.transform.localPosition = new Vector3(0, 550 - i * 150, 0);
                            nouv.GetComponent<InitIdeeScenario>().init(note);
                            nouv.name = "IdeeScen" + i.ToString();
                            listeobjetInstancie.Add(nouv);
                            break;
                        case "Implemte":
                            nouv = Instantiate(Resources.Load("IdeeScenarioImplemente")) as GameObject;
                            nouv.transform.SetParent(transform);
                            nouv.transform.localScale = Vector3.one;
                            nouv.transform.localPosition = new Vector3(0, 550 - i * 150, 0);
                            nouv.GetComponent<InitIdeeScenario>().init(note);
                            nouv.name = "IdeeScen" + i.ToString();
                            listeobjetInstancie.Add(nouv);

                            break;
                        case "Identifie":
                            nouv = Instantiate(Resources.Load("IdeeScenarioIdentifie")) as GameObject;
                            nouv.transform.SetParent(transform);
                            nouv.transform.localScale = Vector3.one;
                            nouv.transform.localPosition = new Vector3(0, 550 - i * 150, 0);
                            nouv.GetComponent<InitIdeeScenario>().init(note);
                            nouv.name = "IdeeScen" + i.ToString();
                            listeobjetInstancie.Add(nouv);

                            break;
                        default:
                            nouv = Instantiate(Resources.Load("IdeeScenarioAbandonnee")) as GameObject;
                            nouv.transform.SetParent(transform);
                            nouv.transform.localScale = Vector3.one;
                            nouv.transform.localPosition = new Vector3(0, 550 - i * 150, 0);
                            nouv.GetComponent<InitIdeeScenario>().init(note);
                            nouv.name = "IdeeScen" + i.ToString();
                            listeobjetInstancie.Add(nouv);
                            break;


                    }
                }
                else
                {
                    switch (note.StatutIdee)
                    {
                        case "Valide":
                            GameObject nouv = Instantiate(Resources.Load("IdeeScenarioValideeNonUse")) as GameObject;
                            nouv.transform.SetParent(transform);
                            nouv.transform.localScale = Vector3.one;
                            nouv.transform.localPosition = new Vector3(0, 550 - i * 150, 0);
                            nouv.GetComponent<InitIdeeScenario>().init(note);
                            nouv.name = "IdeeScen" + i.ToString();
                            listeobjetInstancie.Add(nouv);
                            break;
                        case "Implemte":
                            nouv = Instantiate(Resources.Load("IdeeScenarioImplementeeNonUse")) as GameObject;
                            nouv.transform.SetParent(transform);
                            nouv.transform.localScale = Vector3.one;
                            nouv.transform.localPosition = new Vector3(0, 550 - i * 150, 0);
                            nouv.GetComponent<InitIdeeScenario>().init(note);
                            nouv.name = "IdeeScen" + i.ToString();
                            listeobjetInstancie.Add(nouv);

                            break;
                        case "Identifie":
                            nouv = Instantiate(Resources.Load("IdeeScenarioIdentifieNonUse")) as GameObject;
                            nouv.transform.SetParent(transform);
                            nouv.transform.localScale = Vector3.one;
                            nouv.transform.localPosition = new Vector3(0, 550 - i * 150, 0);
                            nouv.GetComponent<InitIdeeScenario>().init(note);
                            nouv.name = "IdeeScen" + i.ToString();
                            listeobjetInstancie.Add(nouv);

                            break;
                        default:
                            nouv = Instantiate(Resources.Load("IdeeScenarioAbandonneeNonUse")) as GameObject;
                            nouv.transform.SetParent(transform);
                            nouv.transform.localScale = Vector3.one;
                            nouv.transform.localPosition = new Vector3(0, 550 - i * 150, 0);
                            nouv.GetComponent<InitIdeeScenario>().init(note);
                            nouv.name = "IdeeScen" + i.ToString();
                            listeobjetInstancie.Add(nouv);
                            break;


                    }



                }
		
                j = 0;
                foreach (pp_produit_scenario scenar in curProd.Scenarios)
                {
                    GameObject nouv = Instantiate(Resources.Load("EncocheIdeeScenar")) as GameObject;
                    nouv.transform.SetParent(ZoneDepHaut.transform);
                    nouv.transform.localScale = Vector3.one;
                    nouv.transform.localPosition = new Vector3(760 + j * 294, 550 - i * 150, 0);
                    nouv.transform.GetChild(0).GetComponent<EncocheCaseScenario>().init(note, scenar);
                    if (scenar.Notes.IndexOf( note ) >= 0)
                    {
                        nouv.transform.GetChild(0).GetComponent<EncocheCaseScenario>().b = false;
                        nouv.transform.GetChild(0).GetComponent<Toggle>().isOn = true;
                    }
                    nouv.name = "EncocheIdee" + i.ToString() + "/" + j.ToString();
                    listeobjetInstancie.Add(nouv);

                    while (!GameObject.Find("EncocheIdee" + i.ToString() + "/" + j.ToString()))
                    {
                        Debug.Log("coche pas crée");
                    }
                    //GameObject.Find ("EncocheIdee" + i.ToString () + "/" + j.ToString ()).name = "debug";
                    Debug.Log("coche  crée");
                    GameObject nouv2 = Instantiate(Resources.Load("GainIdeeScenario")) as GameObject;
                    nouv2.transform.SetParent(ZoneDepHaut.transform);
                    nouv2.transform.localScale = Vector3.one;
                    nouv2.transform.localPosition = new Vector3(884 + j * 294, 550 - i * 150, 0);
                    nouv2.GetComponent<initGainScenar>().instantiate(scenar, note);
                    nouv2.GetComponent<ChangeCouleurFondSIActif>().init(i, j);
                    nouv2.name = "GainIdee" + i.ToString() + "/" + j.ToString();
                    /*	try{
					nouv2.GetComponent<ChangeCouleurFondSIActif> ().CocheCase = nouv.transform.GetChild (0).gameObject;

				}*/
                    //catch{
                    nouv2.GetComponent<ChangeCouleurFondSIActif>().CocheCase = GameObject.Find("EncocheIdee" + i.ToString() + "/" + j.ToString()).transform.GetChild(0).gameObject;
                    //}
                    listeobjetInstancie.Add(nouv2);


                    j++;
                }
                i++;
            }

            if (i > 7)
            {
                ScrollHaut.GetComponent<ScrollScenHaut>().maxDepHaut = (i - 7) * 150;
            }

            if (j > 1)
            {
                ScrollGauche.GetComponent<ScrollScenGauche>().maxDepGauche = (j - 1) * 294;
            }
        }
	}
}
