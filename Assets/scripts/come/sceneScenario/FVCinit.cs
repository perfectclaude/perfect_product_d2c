﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FVCinit : MonoBehaviour {

    private pp_produit_note _note = null;

    public void init(pp_produit_note note)
    {
        _note = note;
        if (_note != null)
        {
            if (_note.TroisF_Val)
            {
                transform.GetChild(0).GetComponent<Text>().color = new Color(1, 1, 1);
            }
            else
            {
                transform.GetChild(0).GetComponent<Text>().color = new Color(0.5f, 0.5f, 0.5f);
            }

            if (_note.IdeeVisibleClient_Val)
            {
                transform.GetChild(1).GetChild(1).gameObject.SetActive(false);
            }
            else
            {
                transform.GetChild(1).GetChild(0).gameObject.SetActive(false);
            }
        }

	}
}
