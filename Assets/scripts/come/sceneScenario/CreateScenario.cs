﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;

public class CreateScenario : MonoBehaviour {
	public GameObject popup;
	public GameObject listeidee;
	public bool init;

	void Awake(){


		init = true;

	}

	// Use this for initialization
	public void OnClick(){
		popup = Instantiate (Resources.Load ("PopupParam")) as GameObject;
		popup.transform.name="Pop-up";
		popup.transform.parent = GameObject.Find ("Canvas").transform;
		popup.transform.localScale = Vector3.one;
		popup.transform.localPosition = Vector3.one;
	}

	void Update(){
		if (init) {
			try{
				listeidee.GetComponent<deploimentidee> ().instancie ();
				init = false;
			}
			catch{

				Debug.Log ("passe par catch");
			}
		}
	}
}
