﻿using UnityEngine;
using System.Collections;

public class TropAGauche : MonoBehaviour {
	public bool tropBas;
	// Update is called once per frame
	void Start(){
		tropBas = false;
	}

	void Update () {
		
		if (transform.localPosition.x  < 740-GameObject.Find("ZoneDeplacement").transform.localPosition.x || tropBas ) {
			gameObject.GetComponent<CanvasGroup> ().alpha = 0;
			gameObject.GetComponent<CanvasGroup> ().blocksRaycasts = false;
			gameObject.GetComponent<CanvasGroup> ().interactable = false;

		} else {
			gameObject.GetComponent<CanvasGroup> ().alpha = 1;
			gameObject.GetComponent<CanvasGroup> ().blocksRaycasts = true;
			gameObject.GetComponent<CanvasGroup> ().interactable = true;
		}
	}
}
