﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class ScrollScenHaut : MonoBehaviour {

	public GameObject ZoneScroll;
	public GameObject ZoneScrollIdee;
	public int maxDepHaut;

	public void OnClick(){
		Vector3 vec = Vector3.zero;
		vec.y = maxDepHaut * transform.GetComponent<Scrollbar> ().value;
		ZoneScroll.transform.localPosition=vec ;
		ZoneScrollIdee.transform.localPosition = vec;
	}
}
