﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DetruireIdee : MonoBehaviour {
	public GameObject Scenarios;

	public  void OnClick ()
	{
        pp_produit_note note = transform.parent.GetComponent<ficheIdeeInit>().noteIdee;
        pp_produit_composant comp = null;
        pp_produit prod = null;

        if (note != null)
            comp = note.Composant;
        
        if (comp != null)
        {
            prod = comp.Produit;

            comp.Notes.Remove(note);
        }

        pp_manager.MainScene.ForceRefresh = true;
        Destroy(GameObject.Find("Pop-up"));

        if( prod != null )
            prod.SaveData();

        try
        {
            GameObject.Find("BarreCreerScenario").GetComponent<CreateScenario>().init = true;
        }
        catch
        {
            Debug.Log("pas dans le menu scenario");
        }

	}
}
