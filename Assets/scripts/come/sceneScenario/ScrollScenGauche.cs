﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScrollScenGauche : MonoBehaviour {

	public GameObject ZoneScroll;
	public int maxDepGauche;

	public void OnClick(){
		Vector3 vec = Vector3.zero;
		vec.x = -maxDepGauche* transform.GetComponent<Scrollbar> ().value;
		ZoneScroll.transform.localPosition=vec ;
	}
}
