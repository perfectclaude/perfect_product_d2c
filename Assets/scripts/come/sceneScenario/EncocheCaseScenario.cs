﻿ using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EncocheCaseScenario : MonoBehaviour {
    private pp_produit_note _note = null;
    private pp_produit_scenario _scenario = null;
    public bool b = false;

	//public GameObject CreateScenario;
	// Use this for initialization

	void Start(){
        b = true;
	}
    public void init (pp_produit_note note, pp_produit_scenario scenario) {
        _note = note;
        _scenario = scenario;
	}
	
	// Update is called once per frame
	public void OnClick()
    {
        if( (_note != null) && (_scenario != null) )
        {
            if (b)
            {
                if (transform.GetComponent<Toggle>().isOn)
                {
                    if (_scenario.Notes.IndexOf(_note) < 0)
                        _scenario.Notes.Add(_note);
                }
                else
                {
                    _scenario.Notes.Remove(_note);
                }
                GameObject.Find("BarreCreerScenario").GetComponent<CreateScenario>().init = true;
            }
            else
            {
                b = true;
            }
        }

	}
}
