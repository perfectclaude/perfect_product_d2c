﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class InitIdeeScenario : MonoBehaviour {
	public GameObject titre;
	public GameObject numero;
	public GameObject resume;
	public GameObject sujet;
	public GameObject levier;
	public GameObject statut;

    private pp_produit_note _note = null;



    public void init(pp_produit_note note)
    {
        _note = note;
        if (_note != null)
        {
            titre.GetComponent<Text>().text = _note.TitreIdee;
            numero.GetComponent<Text>().text = _note.Numero;
            resume.GetComponent<Text>().text = _note.DescriptionIdee;
            sujet.GetComponent<Text>().text = _note.Numero;

            //levier.GetComponent<Text> ().text = loadcsv.notes [ligne] [5];
            switch (_note.LeverDeGain)
            {
                case "Specification":
                    levier.GetComponent<Text>().text = localization.GetTextWithKey("LEVER_SPEC_CBO");
                    break;

                case "Conception":
                    levier.GetComponent<Text>().text = localization.GetTextWithKey("LEVER_DESIG_CBO");
                    break;

                case "Achat":
                    levier.GetComponent<Text>().text = localization.GetTextWithKey("LEVER_PURCH_CBO");
                    break;

                case "Industrialisation":
                    levier.GetComponent<Text>().text = localization.GetTextWithKey("LEVER_INDUS_CBO");
                    break;


                default:
                    levier.GetComponent<Text>().text = "???";
                    break;
                
            }
            statut.GetComponent<FVCinit>().init(_note);
        }
	}

	public void OnClick(){

        if (_note != null)
        {
            if (!GameObject.Find("Pop-up"))
            {

                GameObject nouv = Instantiate(Resources.Load("FicheIdeeAntoine")) as GameObject;
                nouv.name = "Pop-up";

                nouv.transform.parent = GameObject.Find("CanvasFiche").transform;
                nouv.transform.localPosition = Vector3.zero;
                nouv.transform.localScale = Vector3.one;
                nouv.GetComponent<RectTransform>().localScale = Vector3.one;
                nouv.GetComponent<ficheIdeeInit>().init(_note, false);

            }
        }
	}
}
