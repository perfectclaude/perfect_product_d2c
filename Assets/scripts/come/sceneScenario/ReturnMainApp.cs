﻿using UnityEngine;
using System.Collections;

public class ReturnMainApp : MonoBehaviour {

	public void OnClick ()
	{
        pp_produit prod = pp_manager.GetCurrentProduit();
        if (prod != null)
            prod.SaveData();
        
		Application.LoadLevel("mainsceneUI");
	}
}
