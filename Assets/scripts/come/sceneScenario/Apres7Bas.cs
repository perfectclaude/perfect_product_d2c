﻿using UnityEngine;
using System.Collections;

public class Apres7Bas : MonoBehaviour {


	
	// Update is called once per frame
	void Update () {
		

		if ((transform.localPosition.y  < -500-transform.parent.localPosition.y || transform.localPosition.y  > 400-transform.parent.localPosition.y) ) {
			gameObject.GetComponent<CanvasGroup> ().alpha = 0;
			gameObject.GetComponent<CanvasGroup> ().blocksRaycasts = false;
			gameObject.GetComponent<CanvasGroup> ().interactable = false;
			gameObject.GetComponent<TropAGauche> ().tropBas = true;

		} else {
			gameObject.GetComponent<CanvasGroup> ().alpha = 1;
			gameObject.GetComponent<CanvasGroup> ().blocksRaycasts = true;
			gameObject.GetComponent<CanvasGroup> ().interactable = true;
			gameObject.GetComponent<TropAGauche> ().tropBas = false;
		}
	}
}
