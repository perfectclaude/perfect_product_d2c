using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class PopUpParamScenar : MonoBehaviour {
	public GameObject titre;
	public GameObject description;
	public GameObject creerScenario;
	public GameObject depIdee;
	public GameObject nonUnique;

	// Use this for initialization
	void Awake () {

		creerScenario = GameObject.Find ("BarreCreerScenario");
	}

	public void OnEnter()
    {
        pp_produit prod = pp_manager.GetCurrentProduit();
        if (prod != null)
        {
            if (prod.TitreScenarioExistant(titre.GetComponent<InputField>().text))
            {
                nonUnique.SetActive(true);
            }
        }
	}

	public void OnExit(){
		nonUnique.SetActive (false);
	}



	public void OnClick(){

        pp_produit prod = pp_manager.GetCurrentProduit();
        if( prod != null )
        {
            if (!prod.TitreScenarioExistant(titre.GetComponent<InputField>().text))
            {
                Destroy(GameObject.Find("Pop-up"));

                prod.Scenarios.Add(new pp_produit_scenario( titre.GetComponent<InputField>().text, description.GetComponent<InputField>().text, prod ));
    
                prod.SaveData();

    			Destroy (GameObject.Find ("Pop-up"));
    			float temps;
    			temps = 1;
    			while (temps > 0) {
    				temps -= Time.deltaTime;
    			}
    			creerScenario.GetComponent<CreateScenario> ().init = true;
    			/*StartCoroutine(FinishFirst(0.1f));
    			StartCoroutine(initTrue());*/

    		}

    		//GameObject.Find("BarreDonneeIdee").GetComponent<deploimentidee> ().instancie ();
        }
	}

	IEnumerator FinishFirst(float waitTime)
	{
        pp_produit prod = pp_manager.GetCurrentProduit();
        if( prod != null )
        {
            prod.Scenarios.Add (new pp_produit_scenario( titre.GetComponent<InputField>().text, description.GetComponent<InputField>().text, prod ));

            prod.SaveData();

    		Destroy (GameObject.Find ("Pop-up"));
    		float temps;
    		temps = 1;
    		while (temps > 0) 
            {
    			temps -= Time.deltaTime;
    		}
    		print("in FinishFirst");        
    		yield return new WaitForSeconds(waitTime);
    		//creerScenario.GetComponent<CreateScenario> ().init = true;
    		print("leave FinishFirst");
        }
	}  


	IEnumerator initTrue()
	{

		print("in inittrue");        

		creerScenario.GetComponent<CreateScenario> ().init = true;
		print("leave inittrue");
		yield return new WaitForSeconds(0.1f);

	}  

	// Update is called once per frame
	void Update () {
	
	}
}
