﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ChangeCouleurFondSIActif : MonoBehaviour {
	public GameObject CocheCase;
	public int i;
	public int j;


	// Update is called once per frame
	void Update () {
		if (CocheCase != null) {
			try {
				if (CocheCase.GetComponent<Toggle> ().isOn) {
					transform.GetComponent<Image> ().color = new Color (0.16f, 0.16f, 0.16f);
				} else {
					transform.GetComponent<Image> ().color = new Color (0.48f, 0.48f, 0.48f);
				}
			} catch {
				Debug.Log ("bug dans load couleur");
				CocheCase = GameObject.Find ("EncocheIdee" + i.ToString () + "/" + j.ToString ()).transform.GetChild (0).gameObject;
			}
		} else {
			try{
			CocheCase = GameObject.Find ("EncocheIdee" + i.ToString () + "/" + j.ToString ()).transform.GetChild (0).gameObject;
			}
			catch{
				Debug.Log ("objet non trouvé");
			}
		}
	
	}

	public void init(int x, int y){
		i = x;
		j = y;
	}
}
