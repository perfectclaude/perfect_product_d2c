﻿using UnityEngine;
using System.Collections;

public class setUpLine : MonoBehaviour {
	public int ligne; 


	public void init(int ligneAct){
		ligne = ligneAct;
		transform.GetChild (0).GetComponent<tk2dTextMesh> ().text = sceneIdee.notes[ligneAct][0];
		transform.GetChild (5).GetComponent<tk2dTextMesh> ().text = sceneIdee.notes[ligneAct][9];
		transform.GetChild (1).GetComponent<tk2dTextMesh> ().text = sceneIdee.notes[ligneAct][1];
		transform.GetChild (2).GetComponent<tk2dTextMesh> ().text = sceneIdee.notes[ligneAct][2];

		switch (sceneIdee.notes [ligneAct] [7]) {
		case "I":
			Debug.Log ("ici");
			transform.GetChild (4).GetChild (0).GetComponent<tk2dTextMesh> ().text = sceneIdee.notes [ligneAct] [3];

                transform.GetChild (3).GetChild (0).GetComponent<tk2dTextMesh> ().text = localization.GetTextWithKey("IDENTIFY_TEXT");
			if(ligne%2==0){
				transform.GetComponent<tk2dSlicedSprite> ().color=new Color(1,0.75f,0.0f);
			}
			else{
				transform.GetComponent<tk2dSlicedSprite> ().color=new Color(0.75f,0.25f,0.0f);
				}
			break;
		case "F":
                transform.GetChild (3).GetChild (0).GetComponent<tk2dTextMesh> ().text = localization.GetTextWithKey("FEASIBLE_TEXT");
			transform.GetChild (4).GetChild (0).GetComponent<tk2dTextMesh> ().text = sceneIdee.notes [ligneAct] [4];
			if(ligne%2==0){
				transform.GetComponent<tk2dSlicedSprite> ().color=new Color(0.75f,0.75f,0.0f);
			}
			else{
				transform.GetComponent<tk2dSlicedSprite> ().color=new Color(1,1,0.0f);

			}
			break;
		case "V":
                transform.GetChild (3).GetChild (0).GetComponent<tk2dTextMesh> ().text = localization.GetTextWithKey("VALID_TEXT");
			transform.GetChild (4).GetChild (0).GetComponent<tk2dTextMesh> ().text = sceneIdee.notes [ligneAct] [5];
			if(ligne%2==0){
				transform.GetComponent<tk2dSlicedSprite> ().color=new Color(0,0.5f,0.0f);
			}
			else{
				transform.GetComponent<tk2dSlicedSprite> ().color=new Color(0.5f,1,0.0f);

			}
			break;
		case "D":
                transform.GetChild (3).GetChild (0).GetComponent<tk2dTextMesh> ().text = localization.GetTextWithKey("DEPLOY_TEXT");
			transform.GetChild (4).GetChild (0).GetComponent<tk2dTextMesh> ().text = sceneIdee.notes [ligneAct] [6];
			if(ligne%2==0){
				transform.GetComponent<tk2dSlicedSprite> ().color=new Color(0,0f,0.5f);
			}
			else{
				transform.GetComponent<tk2dSlicedSprite> ().color=new Color(0,0.5f,1f);

			}
			break;
		default:
			break;
		}
	}
	

}
