﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class CacherElement : MonoBehaviour {
    private pp_produit_composant _composant;
    private bool _onInit = false;

    public void init( pp_produit_composant composant )
    {
        _onInit = true;

        _composant = composant;
        if (composant != null)
        {
            transform.GetComponent<Toggle>().isOn = _composant.Visible_AllChild;
            load3d_object.ShowHide(_composant);
        }

        _onInit = false;
    }

    public void refresh()
    {
        init(_composant);
    }
        
	
	public void OnClick()
    {
        if( (_composant != null) && !_onInit )
        {
            _composant.Visible = transform.GetComponent<Toggle>().isOn;
            load3d_object.ShowHide( _composant );

            GameObject iw = GameObject.Find("infoW");
            ToggleHierarchie[] ths = iw.GetComponentsInChildren<ToggleHierarchie>();
            if (ths != null)
                foreach (ToggleHierarchie th in ths)
                    th.refresh();

        }
	}
}
