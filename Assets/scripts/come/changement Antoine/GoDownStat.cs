﻿using UnityEngine;
using System.Collections;

public class GoDownStat : MonoBehaviour {
	public bool move;
	public bool sens;

	void Awake(){
		move=false;
	}

	void Update(){
		if (move) {
			if(sens){
				if(transform.localPosition.y>-680){
					Vector3 vec=Vector3.zero;
					vec.y=transform.localPosition.y-Time.deltaTime*680;
					transform.localPosition=vec;
				}
				else{
					move=false;
				}
			}
			else{
				if(transform.localPosition.y<0){
					Vector3 vec=Vector3.zero;
					vec.y=transform.localPosition.y+Time.deltaTime*680;
					transform.localPosition=vec;
				}
				else{
					move=false;
					transform.localPosition = new Vector3 (0, 8, 0);

					GameObject.Find("StatFond").SetActive(false);
				}
			}
		}
	}
	public void down(){
		move = true;
		sens = true;

	}

	public void up(){
		move = true;
		sens = false;
	}
}
