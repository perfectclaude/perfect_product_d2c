﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ProcessUI : MonoBehaviour {

    private pp_produit_proc _proc = null;

    public void Init( pp_produit_proc proc )
    {
        _proc = proc;
    }

    public void OnClick()
    {
        if (_proc != null)
        {
            if (GameObject.Find("Pop-up"))
            {
                Destroy(GameObject.Find("Pop-up"));
            }
            GameObject popup = Instantiate(Resources.Load("PopUpProcess")) as GameObject;
            popup.transform.parent = GameObject.Find("Canvas").transform;
            popup.transform.localScale = Vector3.one;
            popup.transform.localPosition = new Vector3(-195, -9, 0);
            popup.name = "Pop-up";

            popup.transform.GetChild(1).GetChild(0).GetComponent<Text>().text = _proc.ERP;
            popup.transform.GetChild(2).GetChild(0).GetComponent<Text>().text = _proc.Short_Description;
            popup.transform.GetChild(3).GetChild(0).GetComponent<Text>().text = _proc.Description;
            popup.transform.GetChild(4).GetChild(0).GetComponent<Text>().text = _proc.Production_Speed;
            popup.transform.GetChild(5).GetChild(0).GetComponent<Text>().text = _proc.Unit;
            popup.transform.GetChild(6).GetChild(0).GetComponent<Text>().text = _proc.Nb_Operator;
            popup.transform.GetChild(7).GetChild(0).GetComponent<Text>().text = _proc.Engine_Cost;
            popup.transform.GetChild(8).GetChild(0).GetComponent<Text>().text = _proc.Human_Cost;
            popup.transform.GetChild(9).GetChild(0).GetComponent<Text>().text = _proc.Gamme;
            popup.transform.GetChild(10).GetChild(0).GetComponent<Text>().text = _proc.ProcessID;
        }
	}



}
