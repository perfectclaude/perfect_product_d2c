﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PleinEcranGauche : MonoBehaviour {

	public bool visible;
	public bool move;
	public bool movement;
	public float diffx;
	public GameObject MenuHaut;
	public GameObject boutonPlus;
	public GameObject boutonmoins;
	public GameObject ddnom;

	// Use this for initialization
	void Start () {
		boutonPlus.SetActive (false);
		visible = true;
		diffx = 0;

	}
	//-176 to -219
	// Update is called once per frame
	void Update () {
		if (move) {
			Vector3 vec=transform.parent.localPosition;
			if(visible){
				diffx=-vec.x;
			}
			else{
				diffx=-vec.x-250;
			}
			move=false;
			movement=true;
		}
		if (movement) {
			Vector3 vec2=transform.parent.localPosition;
			if(visible){

				if(vec2.x<0){
					vec2.x+=diffx*4*Time.deltaTime;
					transform.parent.localPosition=vec2;
					vec2 = MenuHaut.transform.localPosition;
					vec2.x+=diffx*4*Time.deltaTime;
					MenuHaut.transform.localPosition = vec2;
					vec2 = ddnom.transform.localPosition;
					vec2.x+=diffx*4*Time.deltaTime;
					ddnom.transform.localPosition = vec2;

				}
				else{
					boutonPlus.SetActive (false);
					boutonmoins.SetActive (true);

					vec2.x=0;
					transform.parent.localPosition=vec2;
					MenuHaut.transform.localPosition = new Vector3 (-569, 761, 0);
					movement=false;
					visible=false;
				}
			}
			else{
				Debug.Log (vec2.x+100>0);
				if(vec2.x+250>0){
					vec2.x+=diffx*4*Time.deltaTime;
					transform.parent.localPosition=vec2;
					vec2 = MenuHaut.transform.localPosition;
					vec2.x+=diffx*4*Time.deltaTime;
					MenuHaut.transform.localPosition = vec2;
					vec2 = ddnom.transform.localPosition;
					vec2.x+=diffx*4*Time.deltaTime;
					ddnom.transform.localPosition = vec2;
				}
				else{

					boutonPlus.SetActive (true);
					boutonmoins.SetActive (false);
					vec2.x=-250;
					transform.parent.localPosition=vec2;
					MenuHaut.transform.localPosition = new Vector3 (-790, 761, 0);
					movement=false;
					visible=true;
				}
			}
			
		}
	}
	
	public void OnClik(){


		move = true;
		movement = false;
		
	}

}
