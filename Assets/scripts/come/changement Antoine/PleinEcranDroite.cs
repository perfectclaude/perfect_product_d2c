﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PleinEcranDroite : MonoBehaviour {
	public bool visible;
	public bool move;
	public bool movement;
	public float diffx;
	public GameObject InfoWindowUI;

	public GameObject barrePlus;
	public GameObject barreMoins;
	// Use this for initialization
	void Start () {
		visible = false;
		diffx = 0;
	}
	//-176 to -219
	// Update is called once per frame
	void Update () {
		if (move) {
			Vector3 vec=InfoWindowUI.transform.localPosition;
			if(visible){
				diffx=1650-vec.x;
			}
			else{
				diffx=-vec.x+914;
			}
			move=false;
			movement=true;
		}
		if (movement) {
			Vector3 vec2=InfoWindowUI.transform.localPosition;
			if(visible){
				Debug.Log("visible");
				if(-vec2.x+1650>0){
					vec2.x+=diffx*4*Time.deltaTime;
					InfoWindowUI.transform.localPosition=vec2;
					vec2 = transform.localPosition;
					vec2.x+=diffx*4*Time.deltaTime;
					transform.localPosition = vec2;
				}
				else{
					barrePlus.SetActive(true);
					barreMoins.SetActive(false);
					vec2.x=1650;
					InfoWindowUI.transform.localPosition=vec2;
					transform.localPosition = new Vector3 (1234, 7, 0);
					movement=false;
				}
			}
			else{
				Debug.Log (vec2.x-913>0);
				if(-vec2.x+913<0){
					vec2.x+=diffx*4*Time.deltaTime;
					InfoWindowUI.transform.localPosition=vec2;
					vec2 = transform.localPosition;
					vec2.x+=diffx*4*Time.deltaTime;
					transform.localPosition = vec2;
				}
				else{
					barrePlus.SetActive(false);
					barreMoins.SetActive(true);
					vec2.x=913;
					InfoWindowUI.transform.localPosition=vec2;
					transform.localPosition = new Vector3 (463, 7, 0);
					movement=false;
				}
			}
			
		}
	}
	
	public void OnClik(){
		visible = !visible;
		if (visible) {
			
		} else {
			
		}
		move = true;
		movement = false;
		
	}
}
