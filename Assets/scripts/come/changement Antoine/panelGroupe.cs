﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class panelGroupe : MonoBehaviour {
	public GameObject MainCamera;
	public List<GameObject> Panels;
	public List<groupe> Groupes;
	public int length;
	public bool test;
	// Use this for initialization
	void Start () {
		test=true;
		length = 0;
		MainCamera = GameObject.Find ("3dcamera");
	}
	
	// Update is called once per frame
	void Update () {

		if(Input.GetKey("c")&&test){
			Instantiate();
			test=false;
		}
	}

	public void Instantiate(){
		//Debug.Log ("par ici dans panel Groupe");
		if (GameObject.Find ("newpanelfather")) {

			GameObject pere = GameObject.Find ("newpanelfather");
			Debug.Log(pere.name);
			if (Panels != null) {
				for (int i=0; i<length; i++) {
					Panels.RemoveAt (0);
				}
			}
			length = 0;
			int max = pere.transform.childCount;
			//Debug.Log(max);
			for (int j=0; j<max; j++) {
				//Debug.Log ("for"+pere.transform.GetChild (j).gameObject.activeSelf);
				if (pere.transform.GetChild (j).gameObject.activeSelf) {
				
					Panels.Add (pere.transform.GetChild (j).gameObject);
					length++;
				}
			}
			CreateGroupe ();
		}
	}

	public void CreateGroupe(){
		int posPanel = 0;
		int i = 1;
		int posGroupe = 0;
		bool reset = true;
		Vector3 positionPanel = new Vector3 ();
		Vector3 vecteurDir=new Vector3();
		Vector3 vecteurAutre=new Vector3();
		Vector3 positionCam = MainCamera.transform.position;
		bool fin = false;
		bool turn = true;
		int limiteBoucle = 0;
		while (!fin && limiteBoucle<100 ) {
			
			if (length - posPanel < 2) {
				
				fin = true;
			} else {
				if (turn) {
					
					if (posPanel == 0 && reset ) {
						
						//posPanel++;
						positionPanel = Panels [0].transform.position;
						vecteurDir = new Vector3 (positionPanel.x - positionCam.x, positionPanel.y - positionCam.y, positionPanel.z - positionCam.z);
						reset=false;

					} else {
						if(i<length){
							
							vecteurAutre=new Vector3 (Panels[i].transform.position.x - positionCam.x, Panels[i].transform.position.y - positionCam.y, Panels[i].transform.position.z - positionCam.z);


							if (espace3D.distancePointDroite (vecteurDir, vecteurAutre) < 0.15f) {
								groupe tmp = new groupe ();
								tmp.panels = new List<GameObject> ();
								Debug.Log("i "+Panels[i].GetComponent<button_panel>().myrealfather+" pospanel "+Panels[posPanel].GetComponent<button_panel>().myrealfather);
								tmp.add (Panels [posPanel]);
								tmp.add (Panels [i]);

								Panels.RemoveAt (i);
								Panels.RemoveAt (posPanel);
								 
								turn = false;
								reset =true;
								posPanel = 0;
								i = posPanel + 1;
								length -= 2;
								Groupes.Add(tmp);
								posGroupe=Groupes.Count-1;



						

							}
							else{
								i++;
							}
						}
						else{
							Debug.Log("Reset Longueur");
							posPanel++;
							i=posPanel+1;
							positionPanel = Panels [posPanel].transform.position;
							vecteurDir = new Vector3 (positionPanel.x - positionCam.x, positionPanel.y - positionCam.y, positionPanel.z - positionCam.z);
						}
					}
			
				}
				else{
					Debug.Log("Groupe");
					int h=0;
					while(h<Groupes[posGroupe].length()){
						Debug.Log("Groupe Boucle");
						positionPanel=Groupes[posGroupe].at (h).transform.position;
						vecteurDir = new Vector3 (positionPanel.x - positionCam.x, positionPanel.y - positionCam.y, positionPanel.z - positionCam.z);



						List<int> indicesE=new List<int>();
						for(int k=0;k<Panels.Count;k++){
							Debug.Log("Groupe Boucle For");

							vecteurAutre=new Vector3 (Panels[k].transform.position.x - positionCam.x, Panels[k].transform.position.y - positionCam.y, Panels[k].transform.position.z - positionCam.z);
							Debug.Log("distance Groupe "+Panels[k].GetComponent<button_panel>().myrealfather+" "+Groupes[posGroupe].at (h).GetComponent<button_panel>().myrealfather+ " : "+espace3D.distancePointDroite (vecteurDir, vecteurAutre) );
							if(espace3D.distancePointDroite (vecteurDir, vecteurAutre) < 0.15f){
								Debug.Log("Groupe add panel :"  +Panels[k].GetComponent<button_panel>().myrealfather);
								Groupes[posGroupe].add(Panels [k]);
								indicesE.Add (k);
							}
						}
					
						for(int v=indicesE.Count-1;v>=0;v--){
							Panels.RemoveAt(indicesE[v]);
							length--;

						}
						h++;
					}
					turn=true;
				}

			}
			limiteBoucle++;
		}

	}




}
