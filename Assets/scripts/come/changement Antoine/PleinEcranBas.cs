﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PleinEcranBas : MonoBehaviour {
	public bool visible;
	public bool move;
	public bool movement;
	public float diffy;
	// Use this for initialization
	void Start () {
		visible = true;
		diffy = 0;
	}
	//-176 to -219
	// Update is called once per frame
	void Update () {
		if (move) {
			Vector3 vec=transform.parent.localPosition;
			if(visible){
				 diffy=-176-vec.y;
			}
			else{
				diffy=-vec.y-219;
			}
			move=false;
			movement=true;
		}
		if (movement) {
			Vector3 vec2=transform.parent.localPosition;
			if(visible){
				Debug.Log("visible");
				if(vec2.y+176<0){
					vec2.y+=diffy*2*Time.deltaTime;
					transform.parent.localPosition=vec2;
				}
				else{
					vec2.y=-176;
					transform.parent.localPosition=vec2;
					movement=false;
				}
			}
			else{
				Debug.Log (vec2.y+219>0);
				if(vec2.y+219>0){
					vec2.y+=diffy*2*Time.deltaTime;
					transform.parent.localPosition=vec2;
				}
				else{
					vec2.y=-219;
					transform.parent.localPosition=vec2;
					movement=false;
				}
			}

		}
	}

	public void OnClik(){
		visible = !visible;
		if (visible) {
			transform.GetChild (0).GetComponent<Text> ().text = "-";
		} else {
			transform.GetChild (0).GetComponent<Text> ().text = "+";
		}
		move = true;
		movement = false;

	}
}
