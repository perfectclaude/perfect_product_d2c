﻿using UnityEngine;
using System.Collections;

public class espace3D : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public static float distancePointDroite(Vector3 Droite,Vector3 Point){
		float norm = ProdVectoriel (Droite, Point);
		float res = norm / Mathf.Sqrt (Mathf.Pow (Droite.x, 2) + Mathf.Pow (Droite.y, 2) + Mathf.Pow (Droite.z, 2));
		return res;
	}

	public static float ProdVectoriel(Vector3 Droite,Vector3 Point){

		float res =Mathf.Sqrt( Mathf.Pow(Droite.y * Point.z-Droite.z*Point.y,2)+Mathf.Pow(Droite.z * Point.x-Droite.x*Point.z,2)+Mathf.Pow(Droite.x * Point.y-Droite.y*Point.x,2));
		return res;

	}
}
