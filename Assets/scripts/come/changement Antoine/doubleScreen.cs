﻿using UnityEngine;
using System.Collections;

public class doubleScreen : MonoBehaviour {
	public GameObject barreDroite;
	public GameObject ZoneRetrectable;
	public GameObject selec;
	public GameObject selecLangue;
	public GameObject BarMenu;
	public GameObject barCloneMenu;

	void Start(){
		barCloneMenu.SetActive (false);
	}

	public void OnClick(){
		if(load3d_object.canActivateCloneActive){
		if (load3d_object.cloneActif) {
			barCloneMenu.transform.GetChild (0).GetComponent<DropDownClone> ().destroyContent ();
			barCloneMenu.transform.GetChild (1).GetComponent<DropDownClone> ().destroyContent ();
			barCloneMenu.SetActive (false);
			barreDroite.GetComponent<CanvasGroup>().interactable=true;
			barreDroite.GetComponent<CanvasGroup> ().blocksRaycasts = true;
			//BarMenu.SetActive (true);
			//barreDroite.SetActive( true);
			barreDroite.GetComponent<CanvasGroup>().alpha=1;
			ZoneRetrectable.SetActive (true);
			selec.SetActive( true);
			selecLangue.SetActive (true);
			load3d_object.cloneActif = false;

		} else {
			
			barCloneMenu.SetActive (true);
			//BarMenu.SetActive (false);
			//barreDroite.SetActive( false);
			barreDroite.GetComponent<CanvasGroup>().alpha=0;
			barreDroite.GetComponent<CanvasGroup>().interactable=false;
			barreDroite.GetComponent<CanvasGroup> ().blocksRaycasts = false;
			ZoneRetrectable.SetActive (false);
			selec.SetActive( false);
			selecLangue.SetActive (false);
			barCloneMenu.transform.GetChild (0).GetComponent<DropDownClone> ().Init ();
			barCloneMenu.transform.GetChild (1).GetComponent<DropDownClone> ().Init ();

			load3d_object.cloneActif = true;

		}
		}
	}
}
