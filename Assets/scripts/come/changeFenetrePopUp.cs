﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class changeFenetrePopUp : MonoBehaviour {
	public bool info;
	GameObject binfo;
	GameObject bdescr;
	GameObject information;
	GameObject description;
	// Use this for initialization
	void Awake () {
		binfo = gameObject.transform.parent.GetChild (1).gameObject;
		bdescr=gameObject.transform.parent.GetChild (0).gameObject;
		description=gameObject.transform.parent.GetChild (4).gameObject;
		information=gameObject.transform.parent.GetChild (5).gameObject;
		description.SetActive (false);
	}

	public void OnClick(){
		if (info) {
			binfo.GetComponent<Image> ().color= Color.white;
			bdescr.GetComponent<Image> ().color = new Color (0.75f, 0.75f, 0.75f);
			description.SetActive (false);
			information.SetActive (true);
		} else {
			binfo.GetComponent<Image>().color=new Color(0.75f,0.75f,0.75f);

			bdescr.GetComponent<Image>().color=Color.white;
			description.SetActive(true);
			information.SetActive(false);
		}

	}
	// Update is called once per frame
	void Update () {
	
	}
}
