﻿using UnityEngine;
using System.Collections;

public class CameraRecentrageOuveture : MonoBehaviour {
	public GameObject canvas;
	public GameObject open1;
	public GameObject open2;

	// Update is called once per frame
	void Update () {
		float w = canvas.GetComponent<RectTransform> ().rect.width;
		float tmp = (w - 2560) / 2;
		if(!load3d_object.cloneActif){



		

		if (open1.GetComponent<PleinEcranGauche> ().visible && open2.GetComponent<PleinEcranDroite> ().visible) {
			gameObject.GetComponent<Camera> ().rect =new Rect (tmp/w,0,2560/w,1);
		}
		else if (open1.GetComponent<PleinEcranGauche> ().visible && !open2.GetComponent<PleinEcranDroite> ().visible) {
			gameObject.GetComponent<Camera> ().rect = new Rect (tmp/w,0,1800/w,1);
		}
		else if (!open1.GetComponent<PleinEcranGauche> ().visible && open2.GetComponent<PleinEcranDroite> ().visible) {
			gameObject.GetComponent<Camera> ().rect =new  Rect ((tmp+300)/w,0,2265/w,1);
		}
		else  {
			gameObject.GetComponent<Camera> ().rect =new  Rect ((tmp+300)/w,0,1525/w,1);
		}

		}
		else{
			if (!open1.GetComponent<PleinEcranGauche> ().visible) {
				gameObject.GetComponent<Camera> ().rect = new  Rect ((tmp + 300) / w, 0, 2265 / (2 * w), 1);
			} else {
				gameObject.GetComponent<Camera> ().rect = new  Rect ((tmp / w), 0, 2560 / (2 * w), 1);
			}

		}
	}
}
