﻿using UnityEngine;
using System.Collections;

public class scrollScenario : MonoBehaviour {

	public bool	wasIpressed = false;
	Vector3 			startpos;
	public Camera 		m_ViewCamera;
	public float 		minyval = 1.0f;
	public float 		maxyval = 1.0f;
	bool				icanmove = false;
	static GameObject	myself;
	static float 		mymaxyval;
	 public static int nombreIdee;
	
	public static void ResetPos()
	{
		Vector3 vec = myself.transform.localPosition;
		vec.y = mymaxyval;
		myself.transform.localPosition = vec;
	}
	
	GameObject scrollwindow;
	GameObject scrollwindow2;
	//-1.068688
	void Awake ()
	{
		myself = gameObject;
		mymaxyval = maxyval;
		m_ViewCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
		scrollwindow = GameObject.Find ("ListObjet");
		scrollwindow2 = GameObject.Find ("ScenarioW");
		gameObject.AddComponent<BoxCollider>();
	}
	
	void Update ()
	{
		nombreIdee = sceneIdee.notes.Count;
		mymaxyval = maxyval;
		if (Input.GetMouseButtonDown(0))
		{
			
			RaycastHit hitInfo;
			Ray ray;
			
			ray = m_ViewCamera.ScreenPointToRay(Input.mousePosition);
			if (gameObject.GetComponent<Collider>().Raycast(ray, out hitInfo, 1.0e8f))
			{
				wasIpressed = true;
				startpos = m_ViewCamera.ScreenToWorldPoint(Input.mousePosition);
				icanmove = false;
			}
		}
		if (Input.GetMouseButtonUp (0))
		{
			wasIpressed = false;
		}
		
		if (wasIpressed)
		{
			
			// Drag the BG here
			Vector3 vec = m_ViewCamera.ScreenToWorldPoint(Input.mousePosition);
			float diffy = vec.y - startpos.y;
			float difftotal = gameObject.transform.position.y;
			
			if (Mathf.Abs(diffy) > 0.022f)				icanmove = true;
			if (icanmove)
			{
				scrollwindow.AddComponent<BoxCollider>();
				scrollwindow2.AddComponent<BoxCollider>();
				load3d_object.FitColliderToChildren(scrollwindow);
				load3d_object.FitColliderToChildren(scrollwindow2);
				BoxCollider boxcol = scrollwindow.GetComponent<BoxCollider>();
				BoxCollider boxcol2 = scrollwindow2.GetComponent<BoxCollider>();
				float myheight = boxcol.size.y;
				float myheight2 = boxcol2.size.y;
				Destroy (boxcol);
				Destroy (boxcol2);
				//				Debug.Log ("Height:"+myheight);
				startpos = vec;
				vec = gameObject.transform.position;
				vec.y += diffy;
				gameObject.transform.position = vec;
				
				vec = gameObject.transform.localPosition;
				if (vec.y < minyval)		vec.y = minyval;
				if (vec.y > maxyval)		vec.y = maxyval;
				gameObject.transform.localPosition = vec;
				
				difftotal = gameObject.transform.position.y - difftotal;
				
				vec = scrollwindow.transform.position;
				myheight = ((myheight*nombreIdee)) / 1.65f;
				myheight = myheight - 1.0f;

				if (myheight < 0)		myheight = 0;
				vec.y -= difftotal * myheight;
				scrollwindow.transform.position = vec;

				vec = scrollwindow2.transform.position;
				myheight2 = ((myheight2*nombreIdee)) / 1.65f;
				myheight2 = myheight2 - 1.0f;
				
				if (myheight2 < 0)		myheight2 = 0;
				vec.y -= difftotal * myheight;

				scrollwindow2.transform.position = vec;
				
				/*
				if(diffy>0){
					CreateHierarchie.setObjetActuel(CreateHierarchie.objetActuel,CreateHierarchie.posDeb+1);
				}
				else if(diffy<0){
					CreateHierarchie.setObjetActuel(CreateHierarchie.objetActuel,CreateHierarchie.posDeb-1);
				}
				*/
			}
		}
		else
		{
		}
		
	}
}
