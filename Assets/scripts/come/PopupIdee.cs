﻿using UnityEngine;
using System.Collections;

public class PopupIdee : MonoBehaviour {
	public int ligneAct;
    private pp_produit_note _note = null;

    public void initPopup(pp_produit_note note)
    {
        _note = note;
        if( _note != null )
        {
            transform.GetChild(0).GetChild(0).GetComponent<tk2dTextMesh>().text = _note.DateMAJ;
            transform.GetChild(0).GetChild(1).GetChild(0).GetComponent<tk2dTextMesh>().text = _note.DescriptionIdee;
            transform.GetChild(1).GetComponent<tk2dTextMesh>().text = _note.ProduitName;
            transform.GetChild(2).GetChild(0).GetComponent<tk2dTextMesh>().text = _note.ComposantName;
            transform.GetChild(2).GetChild(1).GetComponent<tk2dTextMesh>().text = _note.SousEnsemble;
            transform.GetChild(2).GetChild(2).GetComponent<tk2dTextMesh>().text = _note.LeverDeGain;
            transform.GetChild(2).GetChild(3).GetComponent<tk2dTextMesh>().text = _note.StatutIdee;
            transform.GetChild(3).GetComponent<tk2dTextMesh>().text = _note.TitreIdee;
            setUpStatut.changeStatut(_note.Pilote);
        }

	}
    public void initVide(pp_produit_note note){
		setUpStatut.changeStatut("I");
        _note = note;
	}

	public void ecrireCSV()
    {
        if (_note != null)
        {
            _note.DateMAJ = transform.GetChild(0).GetChild(0).GetComponent<tk2dTextMesh>().text;
            //_note.Produit = transform.GetChild(1).GetComponent<tk2dTextMesh>().text;
            _note.ComposantName = transform.GetChild(2).GetChild(0).GetComponent<tk2dTextMesh>().text;
            //_note.SousEnsemble = transform.GetChild(2).GetChild(1).GetComponent<tk2dTextMesh>().text;
            _note.LeverDeGain = transform.GetChild(2).GetChild(2).GetComponent<tk2dTextMesh>().text;
            _note.StatutIdee = transform.GetChild(2).GetChild(3).GetComponent<tk2dTextMesh>().text;
            _note.Pilote = setUpStatut.statut;
            _note.TitreIdee = transform.GetChild(3).GetComponent<tk2dTextMesh>().text;
            _note.DescriptionIdee = transform.GetChild(0).GetChild(1).GetChild(0).GetComponent<tk2dTextMesh>().text;
        }
	}




}
