﻿using UnityEngine;
using System.Collections;

public class InitBarreTestCourbes : MonoBehaviour {

	public void initBarre(Vector2 centre,float taille,float angle){
		transform.localPosition = centre;
		Rect r = transform.GetComponent<RectTransform> ().rect;
		r.height = taille;
		transform.GetComponent<RectTransform> ().sizeDelta=r.size;
		transform.Rotate(new Vector3(0,0,angle*180/Mathf.PI));

	
	}
}
