﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class TrackPointerShowValue : MonoBehaviour {
	public bool enter;
	public GameObject EcranAffiche;
	public GameObject valeur;
	public GameObject etalonnage;


	void Start(){
		enter = false;
	}
	// Use this for initialization
	public void OnEnter(){
		enter = true;
	}

	public void OnExit(){
		enter = false;
	}
	public void OnDrag(){
		if (enter) {
			EcranAffiche.SetActive (true);
			Vector2 vec = Input.mousePosition;
			vec.y += 60;
			EcranAffiche.transform.position = vec;
			Vector2 tmp = valeur.GetComponent<LoadCsvTestCourbes> ().traduirePixelValeuraffiche ((Input.mousePosition.x-Screen.width/2)*etalonnage.GetComponent<RectTransform>().sizeDelta.x/Screen.width, (Input.mousePosition.y-Screen.height/2)*etalonnage.GetComponent<RectTransform>().sizeDelta.y/Screen.height);
			//Vector2 tmp=Input.mousePosition;
			if (valeur.GetComponent<LoadCsvTestCourbes> ().b) {
				TimeSpan t = TimeSpan.FromDays (tmp.x);
				DateTime d = valeur.GetComponent<LoadCsvTestCourbes> ().datMin + t;
			
				EcranAffiche.transform.GetChild (0).GetChild (0).GetComponent<Text> ().text = "x= " + (d.ToString("d/M/yyyy")) + "\n" + "y= " + (tmp.y);
			} else {
				EcranAffiche.transform.GetChild (0).GetChild (0).GetComponent<Text> ().text = "x= " + (tmp.x) + "\n" + "y= " + (tmp.y);
			}

		} else {
			EcranAffiche.SetActive (false);
		}
	}

	public void OnUp(){
		EcranAffiche.SetActive (false);
	}

	public void OnDown(){
		if (enter) {
			
			EcranAffiche.SetActive (true);
			Vector2 vec = Input.mousePosition;
			vec.y += 60;
			EcranAffiche.transform.position = vec;
			Vector2 tmp = valeur.GetComponent<LoadCsvTestCourbes> ().traduirePixelValeuraffiche ((Input.mousePosition.x-Screen.width/2)*etalonnage.GetComponent<RectTransform>().sizeDelta.x/Screen.width, (Input.mousePosition.y-Screen.height/2)*etalonnage.GetComponent<RectTransform>().sizeDelta.y/Screen.height);
			//Vector2 tmp=Input.mousePosition;
			if (valeur.GetComponent<LoadCsvTestCourbes> ().b) {
				TimeSpan t = TimeSpan.FromDays (tmp.x);
				DateTime d = valeur.GetComponent<LoadCsvTestCourbes> ().datMin + t;
				Debug.Log (d);
				EcranAffiche.transform.GetChild (0).GetChild (0).GetComponent<Text> ().text = "x= " + (d.ToString("d/M/yyyy")) + "\n" + "y= " + (tmp.y);
			} else {
				EcranAffiche.transform.GetChild (0).GetChild (0).GetComponent<Text> ().text = "x= " + (tmp.x) + "\n" + "y= " + (tmp.y);
			}
		} 
	}
	// Update is called once per frame
	void Update () {
		
	}
}
