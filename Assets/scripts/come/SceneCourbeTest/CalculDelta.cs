﻿using UnityEngine;
using System.Collections;

public  class CalculDelta : MonoBehaviour {
	static Vector3 WhiteRef=new Vector3(95.047f,100.000f,108.883f);
	// Use this for initialization



	public static float deltaE(Color a,Color b){
		Vector3 xyzA = ConvertToXyz(a);
		Vector3 xyzB = ConvertToXyz(b);

		Vector3 labA = ConvertToLab(xyzA);
		Vector3 labB = ConvertToLab(xyzB);


		return Mathf.Sqrt(Mathf.Pow(labA.x-labB.x,2)+Mathf.Pow(labA.y-labB.y,2)+Mathf.Pow(labA.z-labB.z,2));
	}

	static Vector3 ConvertToXyz(Color c){
		Vector3 res = new Vector3 ();
		Vector3 tmp = new Vector3 ();

		tmp.x=c.r;
		tmp.y=c.g;
		tmp.z=c.b;

		if (tmp.x > 0.04045f) {
			tmp.x = Mathf.Pow ((tmp.x + 0.055f) / 1.055f, 2.4f);
		} else {
			tmp.x = tmp.x / 12.92f;
		}

		if (tmp.y > 0.04045f) {
			tmp.y = Mathf.Pow ((tmp.y + 0.055f) / 1.055f, 2.4f);
		} else {
			tmp.y = tmp.y / 12.92f;
		}

		if (tmp.z > 0.04045f) {
			tmp.z = Mathf.Pow ((tmp.z + 0.055f) / 1.055f, 2.4f);
		} else {
			tmp.z = tmp.z / 12.92f;
		}

		//tmp.x /= 100;
		//tmp.y /= 100;
		//tmp.z /= 100;

		//Debug.Log (c+"tmpxyz"+tmp);
		res.x = tmp.x * 0.4124f + tmp.y * 0.3576f + tmp.z * 0.1805f;
		res.y = tmp.x * 0.2126f + tmp.y * 0.7152f + tmp.z* 0.0722f;
		res.z = tmp.x * 0.0193f + tmp.y * 0.1192f + tmp.z* 0.9505f;
		//Debug.Log (c+"resxyz"+res);
		return res;
	}

	static Vector3 ConvertToLab(Vector3 vec){
		Vector3 res = new Vector3 ();
		Vector3 tmp = new Vector3 ();

		tmp.x = vec.x / WhiteRef.x;
		tmp.y = vec.y / WhiteRef.y;
		tmp.z = vec.z / WhiteRef.z;

		if (tmp.x > 0.008856f) {
			tmp.x = Mathf.Pow (tmp.x, 1/3);
		} else {
			tmp.x = 7.787f * tmp.x + 16 / 116;
		}

		if (tmp.y > 0.008856f) {
			tmp.y = Mathf.Pow (tmp.y, 1/3);
		} else {
			tmp.y = 7.787f * tmp.y + 16 / 116;
		}

		if (tmp.z > 0.008856f) {
			tmp.z = Mathf.Pow (tmp.z, 1/3);
		} else {
			tmp.z = 7.787f * tmp.z + 16 / 116;
		}



		res.x =Mathf.Max(0, 116 * tmp.y - 16);
		res.y = 500 * (tmp.x - tmp.y);
		res.z = 200 * (tmp.y - tmp.z);
		//Debug.Log ("reslab"+res);
		return res;
	}

}
