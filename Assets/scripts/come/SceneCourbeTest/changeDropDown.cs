﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class changeDropDown : MonoBehaviour {
	public GameObject data;

	public void OnClick(){
		data.GetComponent<LoadCsvTestCourbes> ().scenarioActuel = gameObject.GetComponent<Dropdown> ().value - 1;
		data.GetComponent<LoadCsvTestCourbes> ().GenerateDonneeDate ();
	}
}
