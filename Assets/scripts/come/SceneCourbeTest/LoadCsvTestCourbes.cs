﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine.UI;
using System;
using System.Globalization;



public class LoadCsvTestCourbes : MonoBehaviour {

	List<List<float>> donnees;
	List<Color> couleurs;
	public float minAbs;
	public float minOrd;
	public float maxAbs;
	public float maxOrd;
	public DateTime datMin;
	public GameObject ValeurMaxOrd;
	public GameObject valeurMinOrd;
	public GameObject valeurMedOrd;
	public GameObject ValeurMaxAbs;
	public GameObject valeurMinAbs;
	public GameObject ValeurMedAbs;
	public GameObject legende;
	public List<string> nomLegende; 
	List<string> valeur;
	public int scenarioActuel;

	public GameObject dropScenario;

	public float maxAxeBas = 2000;
	public float maxAxeHaut = 1000;
	public Vector2 origine = new Vector2 (-1000, -400);
	public bool b;
	// Use this for initialization
	void Start () {
		b = false;
		initScen ();
		nomLegende = new List<string> ();
		nomLegende.Add ("Instancie");
		nomLegende.Add ("Implemente");
		nomLegende.Add ("Deploye");
		/*nomLegende.Add ("courbe3");
		nomLegende.Add ("courbe4");
		nomLegende.Add ("courbe5");*/
		GenerateDonneeDate ();
		//GenerateDonneeCourbes();
	}

	void initScen()
    {
        pp_produit prod = pp_manager.GetCurrentProduit();

        dropScenario.GetComponent<Dropdown>().options.Clear();
        Dropdown.OptionData op = new Dropdown.OptionData();
        op.text = "All";
        dropScenario.GetComponent<Dropdown>().options.Add(op);
        if (prod != null)
        {
            foreach (pp_produit_scenario scenar in prod.Scenarios)
            {
                op = new Dropdown.OptionData();
                op.text = scenar.Titre;
                dropScenario.GetComponent<Dropdown>().options.Add(op);
            }
            dropScenario.GetComponent<Dropdown>().value = 0;
            scenarioActuel = -1;
        }


	}

	List<List<string>> generateFromNote(){
		List<List<string>> res = new List<List<string>> ();
		List<string> prix=new List<string>();
		List<string> identifie=new List<string>();
		List<string> valide=new List<string>();
		List<string> implemente=new List<string>();

        pp_produit prod = pp_manager.GetCurrentProduit();
        if (prod != null)
        {
            foreach (pp_produit_note n in prod.notes)
            {
                if (n.StatutIdee != "Abandonne")
                {
					if (scenarioActuel < 0) {
						prix.Add (n.GainPotentielUnitaire.ToString ());
						identifie.Add (n.DateMAJ);
						valide.Add (n.DatePrevisionnelleValidation);
						implemente.Add (n.DatePrevisionnelleImplantation);
					} 
                    else 
                    {
                        if( prod.Scenarios[scenarioActuel].Notes.IndexOf(n) >= 0 )
                        {
							//prix.Add (n.CoutScenarios [scenarioActuel]);
							prix.Add (n.GainPotentielUnitaire.ToString ());
							identifie.Add (n.DateMAJ);
							valide.Add (n.DatePrevisionnelleValidation);
							implemente.Add (n.DatePrevisionnelleImplantation);
						}
					}
                }
            }
        }
		res.Add (prix);
		res.Add (identifie);
		res.Add (valide);
		res.Add (implemente);

		return res;
	}

	public void GenerateDonneeDate(){
		//List<List<string>> ls=loadDate(Application.streamingAssetsPath + "/DonneeDate.csv");
		List<List<string>> ls=generateFromNote();
		foreach(List<string> l in ls){
			foreach(string s in l){
				Debug.Log (s);
			}
		}
	
		List<List<float>> lf = new List<List<float>> ();
		int i = 0;
		bool first = true;
		List<List<DateTime>> ld = new List<List<DateTime>> ();
		 valeur=new List<string>();
		foreach (List<string> l in ls) {
			
			if (i > 0) {
				List<DateTime> tmp = new List<DateTime> ();
				foreach (string f in l) {
					//Debug.Log (f);
					DateTime d = DateTime.ParseExact (f, "d/M/yyyy", CultureInfo.InvariantCulture);

					if (first == true) {
						datMin = d;
						//Debug.Log (datMin);
						first = false;
					} else {
						if (datMin.CompareTo (d) > 0) {
							datMin = d;

						}

					}
					tmp.Add (d);
				}
				ld.Add (tmp);
			} else {
				valeur = l;
			}
			i++;
		}

		donnees=CreerClasseurValeurFloat (ld);
	/*	foreach(List<float> lr in donnees){
			foreach(float h in lr){
				Debug.Log(h);
			}
		}
*/
		b = true;
		GenerateDonneeCourbes ();

	}

	public List<List<float>> CreerClasseurValeurFloat(List<List<DateTime>> ld)
    {
        ppconvert converter = new ppconvert();

		List<List<float>> tmp = transformDateenFloat (ld);
		List<List<float>> res = new List<List<float>> ();
		List<float> vide = new List<float> ();
		List<float> vide1 = new List<float> ();
		List<float> vide2 = new List<float> ();
		List<float> vide3 = new List<float> ();
		res.Add (vide);
		res.Add (vide1);
		res.Add (vide2);
		res.Add (vide3);
		foreach(List<float> lf in tmp){
			foreach(float f in lf){
				if (!res [0].Contains (f)) {
					res [0].Add (f);
					res [1].Add (0);
					res [2].Add (0);
					res [3].Add (0);

				}

			}
		}

		for (int k = 0; k < (res [0].Count-1); k++) {
			if (res [0] [k] > res [0] [k+1]) {
				
				float ind=res [0] [k];
				res [0] [k]=res [0] [k+1];
				res [0] [k+1]=ind;
			
				for (int l = k; l > 0; l--) {
					if (res [0] [l] < res [0] [l - 1]) {
						ind = res [0] [l];
						res [0] [l] = res [0] [l - 1];
						res [0] [l - 1] = ind;
					} else {
						l = 0;
					}
				}
			}
		}

		/*string deb = "";
		foreach (float resVal in res[0]) {
			deb += "\\" + resVal;
		}
		Debug.Log (deb);*/
		int i = 0;
		bool first = true;
		foreach(float resVal in res[0]){
			if(!first){
				res [1][i]=res [1][i-1];
				res [2][i]=res [2][i-1];
				res [3][i]=res [3][i-1];

			}
			for (int j = 0; j < tmp [1].Count; j++) {
				
				if(tmp[0][j]==resVal)
                {
                    float fval = 0;
                    converter.TryParse(valeur[j], out fval );
                    res[1][i] += fval;
				}
				if(tmp[1][j]==resVal)
                {
                    float fval = 0;
                    converter.TryParse(valeur[j], out fval );
                    res[2][i] += fval;
				}
				if(tmp[2][j]==resVal)
                {
                    float fval = 0;
                    converter.TryParse(valeur[j], out fval );
                    res[3][i] += fval;
				}


			}
			i++;
			first = false;
		}


		return res;

	}

	public List<List<float>> transformDateenFloat(List<List<DateTime>> ld){
		List<List<float>> res = new List<List<float>> ();
		foreach(List<DateTime> l in ld){
			List<float> tmp = new List<float> ();
			foreach(DateTime d in l){
					TimeSpan t=d-datMin;
				float tmpmin = (float) t.TotalDays;
				tmp.Add (tmpmin);
			}
			res.Add (tmp);
		}
		return res;
	}

	void DateOrdonnee(){
		
	

	}
	public void GenerateDonneeCourbes(){

	
		if (b) {
			//donnees = loadDonnee (Application.streamingAssetsPath + "/DonneeCourbe.csv");
		
			maxAbs = maxX ();
			maxOrd = maxY ();
			minAbs = minX ();
			minOrd = minY ();
			//Debug.Log (maxAbs);
		
			TimeSpan tma = TimeSpan.FromDays (minAbs);
			TimeSpan tMa = TimeSpan.FromDays (maxAbs);
		//	TimeSpan tmo = TimeSpan.FromDays (minOrd);
		//	TimeSpan tMo = TimeSpan.FromDays (maxOrd);
		//	TimeSpan tmedo = TimeSpan.FromDays ((maxOrd-minOrd)/2);
			TimeSpan tmeda = TimeSpan.FromDays ((maxAbs-minAbs)/2);
			ValeurMaxAbs.GetComponent<Text> ().text =(datMin+ tMa).ToString ("d/M/yyyy");
			ValeurMaxOrd.GetComponent<Text> ().text =maxOrd.ToString ();
			ValeurMedAbs.GetComponent<Text> ().text = (datMin+tmeda).ToString ("d/M/yyyy");
			valeurMedOrd.GetComponent<Text> ().text =  ((maxOrd-minOrd)/2).ToString ();
			valeurMinOrd.GetComponent<Text> ().text = minOrd.ToString ();
			valeurMinAbs.GetComponent<Text> ().text = (datMin+tma).ToString ("d/M/yyyy");
			TraduireDonnéePixel ();
			initCouleur ();
			Generate ();
		} else {
			donnees = loadDonnee (Application.streamingAssetsPath + "/DonneeCourbe.csv");
			for (int i = 0; i < donnees.Count; i++) {
				for (int j = 0; j < donnees[0].Count; j++) {
					Debug.Log (donnees [i] [j]);
				}
			}
			maxAbs = maxX ();
			maxOrd = maxY ();
			minAbs = minX ();
			minOrd = minY ();
			ValeurMaxAbs.GetComponent<Text> ().text = maxAbs.ToString ();
			ValeurMaxOrd.GetComponent<Text> ().text = maxOrd.ToString ();
			ValeurMedAbs.GetComponent<Text> ().text = ((maxAbs - minAbs) / 2).ToString ();
			valeurMedOrd.GetComponent<Text> ().text = ((maxOrd - minOrd) / 2).ToString ();
			valeurMinOrd.GetComponent<Text> ().text = minOrd.ToString ();
			valeurMinAbs.GetComponent<Text> ().text = minAbs.ToString ();
			TraduireDonnéePixel ();
			initCouleur ();
			Generate ();
		}
	}
	// Update is called once per frame
	void Update () {
		

	}

	public void Generate(){
		int h = 0;
		foreach(Transform t in legende.transform){
			Destroy (t.gameObject);
		}
		foreach(Transform t in GameObject.Find ("Valeur").transform){
			Destroy (t.gameObject);
		}

		foreach(string nom in nomLegende){
			GameObject nl = Instantiate (Resources.Load ("LegendeItem")) as GameObject;
			nl.name = "Legende" + nom;
			nl.transform.SetParent (legende.transform);
			nl.GetComponent<Image> ().color = couleurs [h];
			nl.transform.GetChild (0).GetComponent<Text> ().text = nom;
			nl.transform.localPosition = new Vector3 (-75, 155-h*50, 0);;
			nl.transform.localScale = Vector3.one;

			h++;

		}
		for (int i = 0; i < donnees[0].Count; i++) {
			for (int j = 1; j < donnees.Count; j++) {
				if (donnees [j] [i] != null) {
					GameObject nouv = Instantiate (Resources.Load ("PointGrapheTest")) as GameObject;
					nouv.name = "point_" + i.ToString () + "$" + j.ToString ();
					nouv.transform.SetParent (GameObject.Find ("Valeur").transform);
					nouv.GetComponent<Image> ().color = couleurs [j - 1];
					Vector2 point1 = new Vector2 (donnees [0] [i], donnees [j] [i]);
					nouv.transform.localPosition = point1;
					nouv.transform.localScale = Vector3.one;

					if (i < donnees [0].Count - 1) {
						Vector2 point2 = new Vector2 (donnees [0] [i + 1], donnees [j] [i + 1]);
						Vector2 centre = Pcentre (point1, point2);
						float dist = distance (point1, point2);
						float AngleRcos = angleCos (point2.y - point1.y, dist);
						float AngleRsin = angleSin (point2.x - point1.x, dist);
					
						if (AngleRsin < 0) {
							AngleRcos = -AngleRcos;
						}
						GameObject nouvBarre = Instantiate (Resources.Load ("BarreTestCourbes")) as GameObject;
						nouvBarre.name = "barre_" + i.ToString () + "/" + (i + 1).ToString () + "$" + j.ToString ();
						nouvBarre.transform.SetParent (GameObject.Find ("Valeur").transform);
						nouvBarre.GetComponent<Image> ().color = couleurs [j - 1];
						nouvBarre.GetComponent<InitBarreTestCourbes> ().initBarre (centre, dist, AngleRcos);
						nouvBarre.transform.localScale = Vector3.one;

					
					}


				}
			}

		}

	}


	public List<List<float>> loadDonnee(string NomFichier){
		StreamReader st = new StreamReader (NomFichier, Encoding.Default);
		string l = st.ReadLine ();
		string fichier = null;
		while(l!=null){
			
			fichier+=l+"\n";
			l = st.ReadLine ();
		}
		st.Close ();


		string[] splitted=null;
		List<List<float>> res = new List<List<float>> ();
		if (fichier!=null) {
			fichier = fichier.Replace ("\n\r", "\n");
			fichier = fichier.Replace ("\r\n", "\n");
			fichier = fichier.Replace ("\r", "\n");
			string[] lignes = fichier.Split ('\n');



			// Saut de ligne

			foreach (string line in lignes) {
				List<float> mylist = new List<float> ();
				if ((line == "") || (line == null) || (line == "sep=;")) {
				} else {

					splitted = line.Split (';');
					foreach (string s in splitted) {
						
					
							string rep = s.Replace (",", ".");
							float tmp = 0;
							float.TryParse (rep, out tmp);
							mylist.Add (tmp);

						
					
					}
				}
				res.Add (mylist);
			}
		}
		res.RemoveAt (res.Count - 1);
		return (res);

	}

	public List<List<string>> loadDate(string NomFichier){
		StreamReader st = new StreamReader (NomFichier, Encoding.Default);
		string l = st.ReadLine ();
		string fichier = null;
		while(l!=null){

			fichier+=l+"\n";
			l = st.ReadLine ();
		}
		st.Close ();


		string[] splitted=null;
		List<List<string>> res = new List<List<string>> ();
		if (fichier!=null) {
			fichier = fichier.Replace ("\n\r", "\n");
			fichier = fichier.Replace ("\r\n", "\n");
			fichier = fichier.Replace ("\r", "\n");
			string[] lignes = fichier.Split ('\n');



			// Saut de ligne

			foreach (string line in lignes) {
				List<string> mylist = new List<string> ();
				if ((line == "") || (line == null) || (line == "sep=;")) {
				} else {

					splitted = line.Split (';');
					foreach (string s in splitted) {
							
							mylist.Add (s);
						
					}
				}
				res.Add (mylist);
			}
		}
		res.RemoveAt (res.Count - 1);
		return (res);

	}

	public float maxX(){
		if (donnees [0].Count == 0) {
			return 0;
		}
		float max = Mathf.NegativeInfinity;
		foreach(float f in donnees[0]){
			if (f > max) {
				max = f;
			}
				
		}

		return max;

	}

	public float maxY(){
		if (donnees [0].Count == 0) {
			return 0;
		}
		float max = Mathf.NegativeInfinity;
		for (int i = 1; i < donnees.Count; i++) {
			foreach (float f in donnees[i]) {
				if (f > max) {
					max = f;
				}

			}
		}
		return max;

	}
	public float minX(){
		if (donnees [0].Count == 0) {
			return 0;
		}
		float min = Mathf.Infinity;
		foreach(float f in donnees[0]){
			if (f < min) {
				min = f;
			}

		}
		return min;

	}

	public float minY(){
		if (donnees [0].Count == 0) {
			return 0;
		}
		float min = Mathf.Infinity;
		for (int i = 1; i < donnees.Count; i++) {
			foreach (float f in donnees[i]) {
				if (f < min) {
					min = f;
				}

			}
		}
		return min;

	}

	public void TraduireDonnéePixel(){  
		for (int i = 0; i < donnees [0].Count; i++) {
			donnees [0] [i] = (donnees [0] [i]- minAbs) * maxAxeBas / (maxAbs-minAbs )+origine.x;
			for (int j = 1; j < donnees.Count; j++) {
				if (donnees [j] [i] != null) {
					donnees [j] [i] = (donnees [j] [i] - minOrd) * maxAxeHaut / (maxOrd - minOrd) + origine.y;
				}
			}
		}
	}

	public Vector2 traduirePixelValeuraffiche(float x,float y){
		
		x = (x - origine.x) * (maxAbs - minAbs) / maxAxeBas + minAbs;
		y = (y - origine.y) * (maxOrd - minOrd) / maxAxeHaut + minOrd;
	
		return new Vector2 (x, y);

	}



	public float distance(Vector2 point1,Vector2 point2){
		return Mathf.Sqrt(Mathf.Pow(point2.x - point1.x,2)+ Mathf.Pow(point2.y - point1.y,2));

	}

	public Vector2  Pcentre(Vector2 point1,Vector2 point2){
		return new Vector2 (point1.x+(point2.x - point1.x)/2,point1.y+ (point2.y - point1.y)/2);

	}

	public float angleCos(float y,float dist){

		return  Mathf.Acos(y / dist);
	}

	public float angleSin(float x,float dist){
		
		return  Mathf.Asin(-x / dist);
	}

	public void initCouleur(){
		Debug.Log (donnees.Count);
		couleurs = new List<Color> ();
		for (int i = 1; i < donnees.Count; i++) {
			Color c = new Color ();
			int secu = 0;
			do {
				c.r = UnityEngine.Random.Range (0.25f, 1.00f);
				c.g = UnityEngine.Random.Range (0.25f, 1.00f);
				c.b = UnityEngine.Random.Range (0.25f, 1.00f);
				secu++;
			} while(couleurDist (c, 5)&& secu<15);

			c.a = 1;
			couleurs.Add (c);
		}

		//Debug.Log("w/b"+CalculDelta.deltaE(Color.red,Color.blue));

	}
	 bool couleurDist(Color c,float dist){
		foreach(Color tmp in couleurs){
			float res = CalculDelta.deltaE (c, tmp);
		//	Debug.Log (res);
			if (res<dist) {
				return true;
			}


		}
		return false;
	}
}
