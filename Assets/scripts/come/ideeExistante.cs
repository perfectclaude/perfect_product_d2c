﻿using UnityEngine;
using System.Collections;

public class ideeExistante : buttonswap {
    private pp_produit_note _note = null;

	// Use this for initialization
    public void init(pp_produit_note note)
    {
        _note = note;
        if (_note != null)
        {
            transform.GetChild(0).GetComponent<tk2dTextMesh>().text = _note.TitreIdee;
            transform.GetChild(1).GetComponent<tk2dTextMesh>().text = _note.GainPotentielUnitaire;
            transform.GetChild(2).GetComponent<tk2dTextMesh>().text = _note.DateMAJ;
            transform.GetChild(4).GetComponent<destroyIdee>().note = note;
            transform.GetChild(4).GetComponent<destroyIdee>().getIdee = transform.GetChild(0).GetComponent<tk2dTextMesh>().text;
        }

	}
	
	public override void ButtonPressed ()
	{
		base.ButtonPressed();
		if (!GameObject.Find ("Pop-up")) 
        {
            if (_note != null)
            {
                buttonswap.myactive = false;
                buttonPopup.myactive = true;
                GameObject nouv = Instantiate(Resources.Load("PopupIdee")) as GameObject;
                nouv.transform.GetComponent<PopupIdee>().initPopup(_note);
                nouv.name = "Pop-up";
            }
		}
	}
}
