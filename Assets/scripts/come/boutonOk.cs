﻿using UnityEngine;
using System.Collections;

public class boutonOk : buttonPopup {

	public override void ButtonPressed ()
	{
		base.ButtonPressed();
		transform.parent.GetComponent<PopupIdee>().ecrireCSV ();
		buttonPopup.myactive = false;
		buttonswap.myactive = true;
		Destroy (GameObject.Find ("Pop-up"));
		CreateHierarchie.setObjetActuel(CreateHierarchie.objetActuel,CreateHierarchie.posDeb);
	}
}
