﻿using UnityEngine;
using System.Collections;

public class BoutonAjouter : buttonswap {

	public override void ButtonPressed ()
	{
        pp_produit_composant cur_comp = pp_manager.GetCurrentComposant();

		base.ButtonPressed();

        if(cur_comp != null)
        {
            if (!GameObject.Find("Pop-up"))
            {
                buttonswap.myactive = false;
                buttonPopup.myactive = true;
			
                GameObject nouv = Instantiate(Resources.Load("PopupIdee")) as GameObject;

                pp_produit_note note = new pp_produit_note(cur_comp);

                cur_comp.Notes.Add(note);

                nouv.transform.GetComponent<PopupIdee>().initVide(note);
                nouv.name = "Pop-up";
            }
        }
	}
}
