﻿using UnityEngine;
using System.Collections;

public class changeMouvement : MonoBehaviour {
	public static bool movement;
	GameObject img1;
	GameObject img2;
	public GameObject stat;
    showtext txtAction3D;
	// Use this for initialization

	void Start(){
		movement = true;
		img1 = transform.GetChild (0).gameObject;
		img2 = transform.GetChild (1).gameObject;
        txtAction3D = gameObject.FindInChildren("txtAction3D").GetComponent<showtext>();
        txtAction3D.mykey = "MANIP3D_TEXT";
		img2.SetActive (false);
	}

	public void OnClick(){
		if(!stat.GetComponent<afficheStat>().open)
			stat.GetComponent<afficheStat>().OnClick();
		changeMouvement.movement = !changeMouvement.movement;
		if (movement) {
			img1.SetActive (true);
			img2.SetActive (false);
            txtAction3D.mykey = "MANIP3D_TEXT";
		} else {
			img1.SetActive (false);
			img2.SetActive (true);
            txtAction3D.mykey = "SLIP3D_TEXT";
		}
	}
}
