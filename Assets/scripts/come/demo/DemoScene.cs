﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DemoScene : MonoBehaviour {
	public GameObject Image1;
	public GameObject Fond1;
	public GameObject Image2;
	public GameObject Fond2;
	public GameObject Image3;
	public GameObject Image4;
	public GameObject Image5;
	public GameObject BoutonDisassemble;
	public GameObject scrool;
	bool start;
	// Use this for initialization
	void Start () {
		Image1.SetActive (true);
		Image2.SetActive (false);
		Image3.SetActive (false);
		Image4.SetActive (false);Image5.SetActive (false);
		Fond1.SetActive (true);
		Fond2.SetActive (false);
		start = true;
	}
	
	// Update is called once per frame
	void Update () {
		if (start && GameObject.Find("myfullscreen") ) {
			start = false;
			StartCoroutine ("boucleDemo");
		}
	}

	IEnumerator boucleDemo(){
		GameObject fullscreen = GameObject.Find ("myfullscreen");
		yield return new WaitForSeconds (2);
		float temps;
		temps = 1;
		float ratioA=1;
		Color c = Color.white;
		Image2.SetActive (true);
		Fond2.SetActive (true);
		// premier fondu
		while (temps>0) {
			temps -= Time.deltaTime;
			ratioA -= Time.deltaTime ;
			c.a = ratioA;
			Image1.GetComponent<Image> ().color = c;
			c.a = 1 - ratioA;
			Image2.GetComponent<Image> ().color = c;
			Fond2.GetComponent<Image> ().color = c;
			yield return new WaitForSeconds (Time.deltaTime);

		}
		Image1.SetActive (false);
		Fond1.SetActive (false);
		yield return new WaitForSeconds (2);


		temps = 1;
		ratioA=1;
		c = Color.white;

		// image disparait
		while (temps>0) {
			temps -= Time.deltaTime;
			ratioA -= Time.deltaTime ;
			c.a = ratioA;
			Image2.GetComponent<Image> ().color = c;
			Fond2.GetComponent<Image> ().color = c;
			yield return new WaitForSeconds (Time.deltaTime);

		}
		Image2.SetActive (false);
		Fond2.SetActive (false);

		yield return new WaitForSeconds (2);
		//bouge objet
		float scroolFlt=0.5f;

		while (scroolFlt > 0.3) {
				scroolFlt -= Time.deltaTime/2;
				scrool.GetComponent<Scrollbar> ().value = scroolFlt;
			yield return new WaitForSeconds (Time.deltaTime);
		}
		yield return new WaitForSeconds (0.5f);	
		temps = 1;
		while (temps > 0) {
			temps -= Time.deltaTime;
			fullscreen.transform.Rotate (0, -1, 0);
			yield return new WaitForSeconds (Time.deltaTime);
		}
		yield return new WaitForSeconds (0.5f);	
	
		BoutonDisassemble.GetComponent<disassebleUI> ().OnClick ();
		yield return new WaitForSeconds (0.5f);
		temps = 1;
		while (temps > 0) {
			temps -= Time.deltaTime;
			fullscreen.transform.Rotate (0, 1, 0);
			yield return new WaitForSeconds (Time.deltaTime);
		}
		BoutonDisassemble.GetComponent<disassebleUI> ().OnClick ();
		yield return new WaitForSeconds (2);
		// retour image
		temps = 1;
		ratioA=1;
		Fond1.SetActive (true);
		Image3.SetActive (true);
		while (temps>0) {
			temps -= Time.deltaTime;
			ratioA -= Time.deltaTime ;
			c.a = 1-ratioA;
			Image3.GetComponent<Image> ().color = c;
			Fond1.GetComponent<Image> ().color = c;

			yield return new WaitForSeconds (Time.deltaTime);

		}
		scrool.GetComponent<Scrollbar> ().value = 0.5f;
		yield return new WaitForSeconds (2);


		temps = 1;
		ratioA=1;
		c = Color.white;
		Image4.SetActive (true);
		while (temps>0) {
			temps -= Time.deltaTime;
			ratioA -= Time.deltaTime ;
			c.a = ratioA;
			Image3.GetComponent<Image> ().color = c;
			c.a = 1 - ratioA;
			Image4.GetComponent<Image> ().color = c;
			yield return new WaitForSeconds (Time.deltaTime);

		}
		Image3.SetActive (false);
		yield return new WaitForSeconds (2);

		temps = 1;
		ratioA=1;
		c = Color.white;
		Image5.SetActive (true);
		while (temps>0) {
			temps -= Time.deltaTime;
			ratioA -= Time.deltaTime ;
			c = Color.white;
			c.a = ratioA;
			Image4.GetComponent<Image> ().color = c;
			c = Color.black;
			c.a = 1 - ratioA;
			Image5.GetComponent<Image> ().color = c;
			yield return new WaitForSeconds (Time.deltaTime);

		}
		Image4.SetActive (false);
		yield return new WaitForSeconds (0.5f);


		temps = 1;
		ratioA=1;
		c = Color.white;
		Image1.SetActive (true);
		while (temps>0) {
			temps -= Time.deltaTime;
			ratioA -= Time.deltaTime ;
			c = Color.black;
			c.a = ratioA;
			Image5.GetComponent<Image> ().color = c;
			c = Color.white;
			c.a = 1 - ratioA;
			Image1.GetComponent<Image> ().color = c;
			yield return new WaitForSeconds (Time.deltaTime);

		}

		Image5.SetActive (false);



		start = true;

	}
}
