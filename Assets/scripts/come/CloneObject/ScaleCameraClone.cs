﻿using UnityEngine;
using System.Collections;

public class ScaleCameraClone : MonoBehaviour {
	public GameObject canvas;
	public GameObject open1;

	
	// Update is called once per frame
	void Update () {
		if (load3d_object.cloneActif) {
			float w = canvas.GetComponent<RectTransform> ().rect.width;
			float tmp = (w - 2560) / 2;
			if (!open1.GetComponent<PleinEcranGauche> ().visible) {
				gameObject.GetComponent<Camera> ().rect = new  Rect ((tmp + 300) / w + 2265 / (2 * w), 0, 2265 / (2 * w), 1);
			} else {
				gameObject.GetComponent<Camera> ().rect = new  Rect ((tmp) / w + 2560 / (2 * w), 0, 2560 / (2 * w), 1);
			}
		}
	}
}
