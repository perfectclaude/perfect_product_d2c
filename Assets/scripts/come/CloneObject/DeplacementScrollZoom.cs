﻿using UnityEngine;
using System.Collections;

public class DeplacementScrollZoom : MonoBehaviour {
	public GameObject open;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (load3d_object.cloneActif) {
			if (open.GetComponent<PleinEcranGauche> ().visible) {
				transform.localPosition = new Vector3 (0, -746, 0);
			} else {
				transform.localPosition = new Vector3 (150, -746, 0);
			}
		} else {
			transform.localPosition = new Vector3 (0, -746, 0);
		}
	}
}
