﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class showPriceCloneObject : MonoBehaviour {
	public bool clone;

	
	// Update is called once per frame
	void Update () {
		if (load3d_object.cloneActif) {
			if (clone) {
				if (load3d_object.scenClone == -1) {
					GetComponent<Text> ().text = pp_manager.GetCurrentComposant ().UnitPrice + " €";
				} else {
					GetComponent<Text> ().text = pp_manager.GetCurrentComposant ().Convert.ToString(pp_manager.GetCurrentComposant ().getPrixScenario(load3d_object.scenClone) ,true)+ " €";
				}
			} else {
				if (load3d_object.scenObjet == -1) {
					GetComponent<Text> ().text = pp_manager.GetCurrentComposant ().UnitPrice + " €";
				} else {
					GetComponent<Text> ().text = pp_manager.GetCurrentComposant ().Convert.ToString(pp_manager.GetCurrentComposant ().getPrixScenario(load3d_object.scenObjet),true) + " €";
				}
			}
		}
	}
}
