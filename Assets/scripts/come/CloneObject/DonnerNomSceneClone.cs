﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DonnerNomSceneClone : MonoBehaviour {
	public GameObject open;

	
	// Update is called once per frame
	void Update () {
		if (load3d_object.cloneActif) {
			transform.GetChild (0).GetComponent<Text> ().text = pp_manager.GetCurrentProduit ().Nom;
			transform.GetChild (1).GetComponent<Text> ().text = pp_manager.GetCurrentComposant ().Description;
			if (open.GetComponent<PleinEcranGauche> ().visible) {
				transform.localPosition = new Vector3 (0, 0, 0);
			} else {
				transform.localPosition = new Vector3 (150, 0, 0);
			}
		}
	}
}
