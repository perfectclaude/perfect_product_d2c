﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DropDownClone : MonoBehaviour {
	public bool clone;
	public GameObject open;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (open.GetComponent<PleinEcranGauche> ().visible) {
			if (clone) {
				transform.localPosition = new Vector3 (800, -40, 0);
			} else {
				transform.localPosition = new Vector3 (-590, -40, 0);
			}
		} else {
			if (clone) {
				transform.localPosition = new Vector3 (950, -40, 0);
			} else {
				transform.localPosition = new Vector3 (-374, -40, 0);
			}
		}
	}

	public void Init(){
		
		Dropdown.OptionData op = new Dropdown.OptionData ();
		op.text = "Modele";
		GetComponent<Dropdown> ().options.Add (op);
		transform.GetChild (0).GetComponent<Text> ().text = "Modele";
		foreach(pp_produit_scenario scenar in pp_manager.GetCurrentProduit().Scenarios){
			op = new Dropdown.OptionData ();
			string txt = scenar.Titre;
			op.text = txt;
			GetComponent<Dropdown> ().options.Add (op);
		}
		GetComponent<Dropdown> ().value = 0;
		if (clone) {
			load3d_object.scenClone = GetComponent<Dropdown> ().value-1;
		} else {
			load3d_object.scenObjet = GetComponent<Dropdown> ().value-1;
		}


	}

	public void destroyContent(){
		GetComponent<Dropdown> ().options.Clear ();
		if (clone) {
			load3d_object.scenClone = -1;
		} else {
			load3d_object.scenObjet = -1;
		}

	}

	public void OnClick(){
		if (clone) {
			load3d_object.scenClone = GetComponent<Dropdown> ().value-1;
		} else {
			load3d_object.scenObjet = GetComponent<Dropdown> ().value-1;
		}
		if (statABC.AbcOn) {
			GameObject.Find ("StatABC").GetComponent<statABC> ().OnClick ();
		}
		if (statKilo.KiloOn) {
			GameObject.Find ("StatKilo").GetComponent<statKilo> ().OnClick ();
		}
	}
}
