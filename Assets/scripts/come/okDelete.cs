﻿using UnityEngine;
using System.Collections;

public class okDelete : buttonPopup {
    public pp_produit_note note = null;

	public override void ButtonPressed ()
	{
		base.ButtonPressed();

        if( note != null )
        {
            pp_produit_composant comp = note.Composant;

            if(comp != null)
                comp.Notes.Remove(note);

            buttonPopup.myactive = false;
            buttonswap.myactive = true;
            Destroy(GameObject.Find("Pop-up"));
            CreateHierarchie.setObjetActuel(CreateHierarchie.objetActuel, CreateHierarchie.posDeb);
        }

	}
}
