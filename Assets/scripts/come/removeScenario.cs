﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class removeScenario : buttonPopup {

	public override void ButtonPressed ()
	{
		if (sceneIdee.scenarios.Count > 0) {
			base.ButtonPressed ();

			sceneIdee.scenarios.RemoveAt (nomScenar.numScenar);

			buttonPopup.myactive = false;
			buttonswap.myactive = true;
			Destroy (GameObject.Find ("Pop-up")); 
			if (sceneIdee.scenarios.Count > 0) {
			GameObject.Find("titre0").transform.GetComponent<nomScenar>().changeNom(0);
			}
		}
	}
}
