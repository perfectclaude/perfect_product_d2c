﻿using UnityEngine;
using System.Collections;

public class goToFonctionnality :buttonswap {
	public static bool fon=false;


	public override void ButtonPressed ()
	{
        pp_produit prod = pp_manager.GetCurrentProduit();

        if ((prod != null) && (prod.Fonctionalites.Count > 0)) 
        {
		    base.ButtonPressed();

			fon = !fon;
			if(fon){
                transform.GetChild(0).GetComponent<tk2dTextMesh>().text=localization.GetTextWithKey("COMPONENTS_TEXT");
				transform.GetChild(1).GetComponent<tk2dTextMesh>().text="C";
			}
			else{
                transform.GetChild(0).GetComponent<tk2dTextMesh>().text=localization.GetTextWithKey("FUNCTIONALITY_TEXT");
				transform.GetChild(1).GetComponent<tk2dTextMesh>().text="F";
			}
			CreateHierarchie.chang = true;
		}

	}
}
