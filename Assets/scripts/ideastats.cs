﻿using UnityEngine;
using System;
using System.Collections;
using System.Globalization;

public struct idea_stat
{
	public string erp;
	public string note;
	public string profit;
	public string identification;
	public string practicability;
	public string validation;
	public string deployment;
	public string statut;
	public string responsable;
	
}

public class ideastats : MonoBehaviour
{
	GameObject cloneline;
	GameObject clone;
	GameObject scrollideas;

	idea_stat [] allideastat_ident = null;
	idea_stat [] allideastat_practic = null;
	idea_stat [] allideastat_valid = null;
	idea_stat [] allideastat_deploy = null;


	public bool	wasIpressed = false;
	Vector3 			startpos;
	public Camera 		m_ViewCamera;
	public float 		maxxval = 5.0f;
	public float		inertie = 0.0f;
	bool				icanmove = false;
	

	void Awake ()
	{
		m_ViewCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
		cloneline = GameObject.Find("cloneline");
		cloneline.SetActive(false);

		clone = GameObject.Find("clone");
		clone.SetActive(false);
		scrollideas = gameObject.FindInChildren("scrollideas");
		// Load CSV file

// scan all ideas, find first date

	}

	double DateToEpoch(string date)
	{
		double ret = 0;
		if (date == null)		return 0;
		if (date == "")			return 0;
//		Debug.Log ("["+date+"]");
		DateTime dt = DateTime.ParseExact(date,"dd/MM/yyyy",CultureInfo.InvariantCulture);
		return (network.GetMiliseconds(dt.AddHours(12)));
	}

	IEnumerator Start()
	{
        pp_produit prod = pp_manager.GetCurrentProduit();

        while ((prod == null) || !prod.IsLoaded)
        {
            prod = pp_manager.GetCurrentProduit();
            yield return new WaitForSeconds(0.1f);
        }
            
		int i = 0;

		double firstdate = network.GetMiliseconds(DateTime.Now.AddMonths(100));
		double lastdate = 0;

		// find first and last dates
        foreach (pp_produit_note note in prod.notes)
		{
			for (i=3;i<7;i++)
			{
                double newval = 0;
                switch( i )
                {
                    case 3:
                        newval = DateToEpoch(note.ComposantName);
                        break;
                    case 4:
                        newval = DateToEpoch(note.SousEnsemble);
                        break;
                    case 5:
                        newval = DateToEpoch(note.LeverDeGain);
                        break;
                    case 6:
                        newval = DateToEpoch(note.StatutIdee);
                        break;
                }
				if (newval != 0)
				{
					if (firstdate > newval)		firstdate = newval;
					if (lastdate < newval)		lastdate = newval;
				}
			}
		}
		// for all dates -> 
		Vector3 vec = new Vector3(0,0,0);
		firstdate = firstdate - 1000.0 * 60.0 * 60.0 * 4.0;
		float [] currentideas = new float[4];
		bool [] litemyfire = new bool[4];
		Vector3 [] lastpos = new Vector3[4];

		for (i=0;i<4;i++)
		{
			lastpos[i] = new Vector3(-10000,0,0);
			currentideas[i] = 0;
			litemyfire[i] = false;
		}
		int n = 0;
		while (firstdate < (lastdate+1000.0 * 60.0 * 60.0 * 12.0))
		{
			double nextday = firstdate + 1000.0 * 60.0 * 60.0 * 24.0;
//			Debug.Log ("Start "+firstdate+ " "+lastdate);

            foreach (pp_produit_note note in prod.notes)
			{	
				for (i=0;i<4;i++)
				{
                    double newval = 0;
                    switch( i )
                    {
                        case 0:
                            newval = DateToEpoch(note.ComposantName);
                            break;
                        case 1:
                            newval = DateToEpoch(note.SousEnsemble);
                            break;
                        case 2:
                            newval = DateToEpoch(note.LeverDeGain);
                            break;
                        case 3:
                            newval = DateToEpoch(note.StatutIdee);
                            break;
                    }

					if (newval != 0)
					{
						if ((newval >= firstdate) && (newval < nextday))
						{
							float reducvalue = 0.0f;
                            float.TryParse(note.ProduitName, out reducvalue);

							currentideas[i] += reducvalue;
							// check if previous
							for (int j = i-1;j>=0;j--)
							{
                                switch( j )
                                {
                                    case 0:
                                        newval = DateToEpoch(note.ComposantName);
                                        break;
                                    case 1:
                                        newval = DateToEpoch(note.SousEnsemble);
                                        break;
                                    case 2:
                                        newval = DateToEpoch(note.LeverDeGain);
                                        break;
                                    case 3:
                                        newval = DateToEpoch(note.StatutIdee);
                                        break;
                                }
								if (newval != 0)
								{
//										currentideas[j]--;			// don't reduce anymore
									break;
								}
							}
						}
					}
				}

			}

			// display
			GameObject obj = GameObject.Instantiate(clone) as GameObject;
			obj.name = "day_element_" + i;
			obj.transform.parent = scrollideas.transform;
			obj.transform.localPosition = vec;
			obj.SetActive(true);
			GameObject placeme = obj.FindInChildren("dayname");
			
			tk2dTextMesh tmplaceme = (tk2dTextMesh)placeme.GetComponent<tk2dTextMesh>();
			DateTime datet = network.GetDateTime(firstdate);
			settext.ForceText(tmplaceme,datet.ToShortDateString());
			for (i=0;i<4;i++)
			{
				switch (i)
				{
				case 0 :		placeme = obj.FindInChildren("ident"); 			break;
				case 1 :		placeme = obj.FindInChildren("practic"); 		break;
				case 2 :		placeme = obj.FindInChildren("valid"); 			break;
				case 3 :		placeme = obj.FindInChildren("deploy"); 		break;
				}
				placeme.transform.localPosition = new Vector3(placeme.transform.localPosition.x,0.1f+0.025f*currentideas[i],placeme.transform.localPosition.z);
				tk2dSprite sprt = (tk2dSprite)placeme.GetComponent<tk2dSprite>();
				if (lastpos[i].x != -10000)
				{
					TraceLine(n*4+i,placeme,sprt.color,lastpos[i] - placeme.transform.position,Vector3.zero);
				}
				if (currentideas[i] != 0)
					litemyfire[i] = true;

				if (litemyfire[i])
				{
					lastpos[i] = placeme.transform.position;
					placeme.SetActive(true);
				}
				else
					placeme.SetActive(false);

			}

			vec.x += 0.14f;
			n++;
			firstdate = nextday;
		}
	}

	
	void Update ()
	{
		if (Input.GetMouseButtonDown(0))
		{
			RaycastHit hitInfo;
			Ray ray;
			
			ray = m_ViewCamera.ScreenPointToRay(Input.mousePosition);
//			if (gameObject.GetComponent<Collider>().Raycast(ray, out hitInfo, 1.0e8f))
			{
				wasIpressed = true;
				startpos = m_ViewCamera.ScreenToWorldPoint(Input.mousePosition);
				inertie = 0.0f;
				icanmove = false;
			}
		}
		if (wasIpressed)
		{
			// Drag the BG here
			Vector3 vec = m_ViewCamera.ScreenToWorldPoint(Input.mousePosition);
			float diffx = vec.x - startpos.x;
			if (Mathf.Abs(diffx) > 0.022f)				icanmove = true;
			if (icanmove)
			{
				startpos = vec;
				vec = gameObject.transform.localPosition;
				vec.x += diffx;
				inertie = diffx;
				if (vec.x < -maxxval)		vec.x = -maxxval;
				if (vec.x > 0.3f)		vec.x = 0.3f;
				gameObject.transform.localPosition = vec;
			}
		}
		else
		{
			if (inertie != 0.0f)
			{
				if (inertie > 0.3f)		inertie = 0.3f;
				if (inertie < -0.3f)	inertie = -0.3f;
				Vector3 vec = gameObject.transform.localPosition;
				vec.x += inertie;
				if (vec.x < -maxxval)		vec.x = -maxxval;
				if (vec.x > 0.3f)		vec.x = 0.3f;
				gameObject.transform.localPosition = vec;
				inertie = inertie * 0.91f;
				if (Mathf.Abs(inertie) < 0.01f)
				{
					inertie = 0.0f;
				}
			}
		}
		
		if (Input.GetMouseButtonUp (0))
		{
			wasIpressed = false;
		}
	}



	GameObject TraceLine(int itemid,GameObject father, Color col, Vector3 vec1, Vector3 vec2)
	{
        Texture mytexture = cloneline.GetComponent<Renderer>().material.mainTexture;
        GameObject newobj = GameObject.Instantiate(cloneline) as GameObject;
//		vec1.x += 0.36f;
//		vec2.x -= 0.36f;
		newobj.name = "curveline"+itemid;
		newobj.transform.parent = father.transform;
		newobj.transform.localPosition = new Vector3(0,0,0.2f);
		newobj.transform.localScale = father.transform.localScale;
		CreateNewLine(newobj,col,vec1,vec2);
		newobj.SetActive(true);
		newobj.GetComponent<Renderer>().material = new Material (Shader.Find("Sprites/Default"));
		newobj.GetComponent<Renderer>().material.mainTexture = mytexture;
		return (newobj);
	}
	
	void CreateNewLine(GameObject obj,Color color,Vector3 vec1,Vector3 vec2)
	{
		Vector3[]    vertex         = new Vector3    [4];
		int[]         triangle     = new int        [3*2];
		Vector2[]    uv            = new Vector2    [4];        
		Color[]        col            = new Color        [4];
		float        decal  = 0.01f;		// WIDTH !!!
		
		if (Mathf.Abs(vec1.x - vec2.x) > Mathf.Abs(vec1.y - vec2.y))        // horizontal
		{
			vertex[0] = new Vector3(vec1.x,vec1.y-decal,0);
			vertex[1] = new Vector3(vec2.x,vec2.y-decal,0);
			vertex[2] = new Vector3(vec1.x,vec1.y+decal,0);
			vertex[3] = new Vector3(vec2.x,vec2.y+decal,0);
		}
		else
		{
			vertex[0] = new Vector3(vec1.x-decal,vec1.y,0);
			vertex[1] = new Vector3(vec1.x+decal,vec1.y,0);
			vertex[2] = new Vector3(vec2.x-decal,vec2.y,0);
			vertex[3] = new Vector3(vec2.x+decal,vec2.y,0);
		}
		
		col[0] = col[1] = col[2] = col[3] = color;
		
		uv[0]        = new Vector2(0,0);
		uv[1]        = new Vector2(1,0);
		uv[2]        = new Vector2(0,1);
		uv[3]        = new Vector2(1,1);
		
		triangle[0] = 0;
		triangle[1] = 2;
		triangle[2] = 1;
		triangle[3] = 2;
		triangle[4] = 3;
		triangle[5] = 1;
		
		Mesh m = new Mesh();
		
		m.vertices  = vertex;
		m.colors    = col;
		m.uv        = uv;
		m.triangles    = triangle;
		
		m.RecalculateNormals();
		m.RecalculateBounds();
		
		obj.GetComponent<MeshFilter>().mesh = m;
	}


}
