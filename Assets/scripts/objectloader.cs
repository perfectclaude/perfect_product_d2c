﻿using UnityEngine;
using System;
using System.IO;
using System.Text;
using System.Collections;
using MiniJSON;
using System.Text.RegularExpressions;
using BestHTTP;
using System.Collections.Generic;
using System.Security;
using System.Threading;

//MUST STORE ALL OBJECTS
//MUST CREATE DESTROY OF TEXTURES, DESTROY OF OBJECTS

public class objectloader : MonoBehaviour
{
	static string filenameroot = "";
	public static bool loadingbusy = false;
	bool loadingstarted = false;
	static GameObject objectroot = null;

	string objfile;
	string mtlfile;
	Texture2D texture = null;
	
	public static string URLROOT = "www.padexpose.com/";
	public static string companyroot = "_design2cost/";
	public static byte [] loadedbytes;

	Texture walltexture = null;

	public static GameObject FindObjectFromStructure(string objectrootname)
	{
		string [] splitted = objectrootname.Split('/');
		int leveldepth = 0;
		GameObject finalobj = null;
		GameObject lastparent = null;

		foreach (string findname in splitted)
		{
			if (leveldepth == 0)
				finalobj = GameObject.Find(findname);
			else
				finalobj = finalobj.FindInChildren(findname);
			if (finalobj == null)
			{
				finalobj = new GameObject();
				finalobj.name = findname;
				if (lastparent != null)
					finalobj.transform.parent = lastparent.transform;
				finalobj.transform.localPosition = Vector3.zero;
				finalobj.transform.localScale = Vector3.one;
				finalobj.transform.localEulerAngles = Vector3.zero;
			}
			lastparent = finalobj;
			leveldepth++;
		}
		return (finalobj);
	}

	public static void CleanAllObjectsAndData(string objectrootname)
	{
		objectroot = FindObjectFromStructure(objectrootname);
		if (objectroot == null)		return;

// scan all objects, remove texture and destroy object
//		if (texture != null)		Destroy(texture);
		foreach (Transform child in objectroot.GetComponentsInChildren<Transform>(true))
		{
			if (child.gameObject.GetComponent<Renderer>() != null)
			{
				Destroy (child.gameObject.GetComponent<Renderer>().material.mainTexture);
			}
		}
		Destroy (objectroot);
		objectroot = null;

	}
	

	public static bool LoadNewObject(string name,string objectrootname)
	{
		if (loadingbusy)		return false;
		loadingbusy = true;

		objectroot = FindObjectFromStructure(objectrootname);
		filenameroot = name;
		return true;
	}

	void Awake ()
	{
		GameObject obj = GameObject.Find("wallpaint");
		walltexture = obj.GetComponent<Renderer>().material.GetTexture("_MainTex");
	}
	
	void Update ()
	{
		if (!loadingstarted)
		{
			if (filenameroot != "")
			{
				loadingstarted = true;
				StopAllCoroutines();
				StartCoroutine("LoadObject");
			}
		}
	}


	public static bool LoadLocalFileBeforeWWW(string filename)
	{
		string path = Application.persistentDataPath + "/";
		path = path + filename;
		if (System.IO.File.Exists(path))
		{
			Debug.Log ("File "+path+" exists... loading.");
			loadedbytes = System.IO.File.ReadAllBytes(path);
			return true;
		}
		return false;
	}
	
	public static void SaveLocalFileBeforeWWW(string filename,byte [] data)
	{
		string path = Application.persistentDataPath + "/";
		// create company directory if not yet exists
		if (!Directory.Exists(path + companyroot))	Directory.CreateDirectory(path + companyroot);
		path = path + filename;
		System.IO.File.WriteAllBytes(path,data);
	}

	void OnDownloadProgress(HTTPRequest request, int downloaded, int length)
	{
		float progressPercent = (downloaded / (float)length) * 100.0f;
		int percent = (int)progressPercent;
		Debug.Log ("Downloaded:"+ percent + "%");
	}


	IEnumerator LoadObject ()
	{
		var loadingText = GameObject.Find("LoadingText").GetComponent<GUIText>();
		loadingText.enabled = true;
        loadingText.text = localization.GetTextWithKey("LOADING_TEXT") + filenameroot + " ...";
		yield return null;

		objfile = null;
		string fileroot = filenameroot+"_obj";
		if (!LoadLocalFileBeforeWWW(companyroot+fileroot+".txt"))
		{
			Debug.Log ("Loading object : "+URLROOT + companyroot + fileroot+".txt");

			string targetfile = Application.persistentDataPath + "/" + companyroot+fileroot+".txt";
			string filename = Application.persistentDataPath + "/" + companyroot+fileroot+".ttt";
			HTTPRequest request = new HTTPRequest(new Uri("http://" + URLROOT + companyroot + fileroot+".txt"), (req, resp) =>
			{
				List<byte[]> fragments = resp.GetStreamedFragments();
				// Write out the downloaded data to a file:
				using (FileStream fs = new FileStream(filename, FileMode.Append))
					foreach(byte[] data in fragments)
						fs.Write(data, 0, data.Length);
				if (resp.IsStreamingFinished)
				{
					File.Move(filename,targetfile);
					objfile = File.ReadAllText(targetfile);
				}
			});
			request.OnProgress = OnDownloadProgress;
			request.UseStreaming = true;
			request.StreamFragmentSize = 1 * 1024 * 1024; // 1 megabyte
			request.DisableCache = true; // already saving to a file, so turn off caching
			request.Send();

		}
		else
		{
			objfile = System.Text.Encoding.ASCII.GetString(loadedbytes);
		}
		while ((objfile == null))
		{
			yield return new WaitForSeconds(0.05f);
		}

		mtlfile = null;
		fileroot = filenameroot+"_mtl";
		if (!LoadLocalFileBeforeWWW(companyroot+fileroot+".txt"))
		{
			Debug.Log ("Loading material : "+URLROOT + companyroot + fileroot+".txt");

			string targetfile = Application.persistentDataPath + "/" + companyroot+fileroot+".txt";
			string filename = Application.persistentDataPath + "/" + companyroot+fileroot+".ttt";
			HTTPRequest request = new HTTPRequest(new Uri("http://" + URLROOT + companyroot + fileroot+".txt"), (req, resp) =>
			                                      {
				List<byte[]> fragments = resp.GetStreamedFragments();
				// Write out the downloaded data to a file:
				using (FileStream fs = new FileStream(filename, FileMode.Append))
					foreach(byte[] data in fragments)
						fs.Write(data, 0, data.Length);
				if (resp.IsStreamingFinished)
				{
					File.Move(filename,targetfile);
					mtlfile = File.ReadAllText(targetfile);
				}
			});
			request.OnProgress = OnDownloadProgress;
			request.UseStreaming = true;
			request.StreamFragmentSize = 1 * 1024 * 1024; // 1 megabyte
			request.DisableCache = true; // already saving to a file, so turn off caching
			request.Send();
		}
		else
		{
			mtlfile = System.Text.Encoding.ASCII.GetString(loadedbytes);
		}

		while ((mtlfile == null))
		{
			yield return new WaitForSeconds(0.05f);
		}
		texture = null;
		// check all jpg and png files, and load them !!!
		string [] strlines = mtlfile.Split('\n');		// split on next line
		foreach(string strline in strlines)
		{
			if (strline.Contains(".png")||strline.Contains(".jpg"))
			{
				// 
				fileroot = strline.Substring(strline.LastIndexOf(' ')+1);
				
				if (!LoadLocalFileBeforeWWW(companyroot+fileroot))
				{
					Debug.Log ("Loading texture : "+URLROOT + companyroot + fileroot);

					string targetfile = Application.persistentDataPath + "/" + companyroot+fileroot;
					string filename = Application.persistentDataPath + "/" + companyroot+fileroot+".tmp";
					HTTPRequest request = new HTTPRequest(new Uri("http://" + URLROOT + companyroot + fileroot), (req, resp) =>
					                                      {
						List<byte[]> fragments = resp.GetStreamedFragments();
						// Write out the downloaded data to a file:
						using (FileStream fs = new FileStream(filename, FileMode.Append))
							foreach(byte[] data in fragments)
								fs.Write(data, 0, data.Length);
						if (resp.IsStreamingFinished)
						{
							File.Move(filename,targetfile);
							byte [] data = File.ReadAllBytes(targetfile);
							texture = new Texture2D (2, 2);
							texture.LoadImage (data);
						}
					});
					request.OnProgress = OnDownloadProgress;
					request.UseStreaming = true;
					request.StreamFragmentSize = 1 * 1024 * 1024; // 1 megabyte
					request.DisableCache = true; // already saving to a file, so turn off caching
					request.Send();
				}
				else
				{
					texture = new Texture2D (2, 2);
					texture.LoadImage (loadedbytes);
				}
				while ((texture == null))
				{
					yield return new WaitForSeconds(0.05f);
				}

			}
		}


		GameObject [] objects = ObjReader.use.ConvertString (objfile, mtlfile);
		
		int nrobjects = 0;
		foreach (GameObject obj in objects)
		{
			if (texture != null)
				obj.GetComponent<Renderer>().material.SetTexture ("_MainTex", texture);
			if (walltexture != null)
				obj.GetComponent<Renderer>().material.SetTexture ("_MainTex", walltexture);

			obj.AddComponent<BoxCollider>();
/*
			if (nrobjects == 0)
				obj.name = filenameroot;
			else
				obj.name = filenameroot;
*/			
			obj.transform.parent = objectroot.transform;
			nrobjects++;
		}

		filenameroot = "";
		loadingbusy = false;
		loadingstarted = false;
//		loadingText.enabled = false;
	}

}


