﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Globalization;
using System;
using UnityEngine.UI;


public class localizationLang
{
    private object _syncRoot = new System.Object();
    private string _libelle = "";
    private string _code = "";
    private string _fileName = "";

    public localizationLang( string libelle, string code )
    {
        _libelle = libelle;
        _code = code;
        _fileName = "strings_" + _code;
    }

    public localizationLang()
        : this("English", "en")
    {
    }

    // Getter / Setter
    public string Libelle
    {
        get
        { 
            lock (_syncRoot)
            {
                return _libelle;
            }
        }
    }

    public string Code
    {
        get
        { 
            lock (_syncRoot)
            {
                return _code;
            }
        }
    }

    public string FileName
    {
        get
        { 
            lock (_syncRoot)
            {
                return _fileName;
            }
        }
    }
}

public class localization : MonoBehaviour
{
    private static volatile localization _instance;
    private static object _syncRoot = new System.Object();

    private List<localizationLang> _listLang = null;
    private localizationLang _currentLang = null;
	
    private Dictionary<string, string> _dictLang = null;

    private localization()
    {
        _listLang = new List<localizationLang>();

        _listLang.Add(new localizationLang("English", "en"));
        _listLang.Add(new localizationLang("Français", "fr"));
        //_listLang.Add(new localizationLang("Deutsch", "de"));
        //_listLang.Add(new localizationLang("Español", "es"));
        //_listLang.Add(new localizationLang("Italiano", "it"));
        //_listLang.Add(new localizationLang("Nederlands", "nl"));
        //_listLang.Add(new localizationLang("Pусский", "ru"));
        //_listLang.Add(new localizationLang("Português", "pt"));
        //_listLang.Add(new localizationLang("עִברִית", "he"));

        _dictLang = new Dictionary<string,string>();

        // Deplacer le Init dans Start
        // Init()
    }

    private void Init()
    {
        LoadDefaultLanguage();
    }

	void Start()
	{
        Init();
	}

    public static localization Instance
    {
        get 
        {
            if (_instance == null) 
            {
                lock (_syncRoot) 
                {
                    if (_instance == null)
                    {
                        GameObject singleton = new GameObject();

                        _instance = singleton.AddComponent<localization>();

                        singleton.name = "localization";

                        DontDestroyOnLoad(singleton);
                    }
                }
            }

            return _instance;
        }
    }




    public bool ChangeLanguage(string Languagecode) 
    {
        string lang = Languagecode.ToLower().Trim();

        if (lang.Length >= 2)
            lang = lang.Substring(0, 2).Trim();

        if (lang.Length < 2)
            lang = "en";

        return LoadLanguage(lang);
    }



    public bool ChangeLanguage(int Languagecode) 
    {
        string lang = "en";

        lock (_syncRoot)
        {
            if (Languagecode < _listLang.Count)
                lang = _listLang[Languagecode].Code;
        }

        return ChangeLanguage(lang);
    }


    private void LoadDefaultLanguage()
    {
        string mylang = "en";

        switch (Application.systemLanguage)
        {
            default :
            case SystemLanguage.English:
                mylang = "en";              
                break;

            case SystemLanguage.French :
                mylang = "fr";
                break;

            case SystemLanguage.German :
                mylang = "de";
                break;

            case SystemLanguage.Spanish :
                mylang = "es";
                break;

            case SystemLanguage.Italian :
                mylang = "it";
                break;

            case SystemLanguage.Dutch :
                mylang = "du";
                break;

            case SystemLanguage.Russian :
                mylang = "ru";
                break;

            case SystemLanguage.Portuguese :
                mylang = "pt";

            //case SystemLanguage.Hebrew :
            //    mylang = "he";
                break;

        }

        if (!ChangeLanguage(mylang) && (mylang != "en"))
            ChangeLanguage("en");   
    }

    private string ParseKey( string key )
    {
        string keyParse = key.Replace( "\r", "\n" ).Replace("\n\n", "\n" ).Trim();

        return keyParse;
    }

    private string ParseTxt( string txt )
    {
        string txtParse = txt.Replace( "\r", "\n" ).Replace("\n\n", "\n" ).Trim();

        txtParse = txtParse.Replace("’","'");
        txtParse = txtParse.Replace("–","-");
        txtParse = txtParse.Replace("&amp;","&");
        txtParse = txtParse.Replace("‘","'");
        txtParse = txtParse.Replace("“","\"");
        txtParse = txtParse.Replace("”","\"");
        txtParse = txtParse.Replace("<br>","\n");

        return txtParse;
    }

    private bool LoadLanguage(string Languagecode) 
    {
        bool bRet = false;

        lock (_syncRoot)
        {
            _currentLang = null;
            _dictLang.Clear();
            foreach (localizationLang lang in _listLang)
            {
                if (lang.Code == Languagecode)
                {
                    _currentLang = lang;
                    break;
                }
            }

            if (_currentLang != null)
            {
                TextAsset mytexts = null;
                try
                {
                    mytexts = Resources.Load<TextAsset>( _currentLang.FileName);
                    if (mytexts != null)
                    {
                        char[] delimiterChars = { '\n' };
                        string[] myList = mytexts.text.Split(delimiterChars);
                        //myList = Regex.Split(mytexts.text, @"[\r\n]");

                        // Special ENTER PATCH
                        for (int ii=0;ii<myList.Length;ii++)
                        {
                            int idxEqual = myList[ii].IndexOf( "=" );

                            if( idxEqual >= 0 )
                            {
                                string key =  ParseKey(myList[ii].Substring( 0, idxEqual ));
                                string txt = "";

                                if( (myList[ii].Length - idxEqual - 1) > 0 )
                                    txt = ParseTxt(myList[ii].Substring( idxEqual+1, myList[ii].Length - idxEqual - 1 ));

                                int jj = ii+1;
                                while ((jj < myList.Length) && (myList[jj].IndexOf( "=" ) < 0))
                                {
                                    string txtSuite = ParseTxt(myList[jj]);

                                    if( txtSuite.Length > 0 )
                                    {
                                        if( txt.Length > 0 )
                                            txt += "\n" + txtSuite;
                                        else
                                            txt = txtSuite;
                                            
                                    }

                                    jj++;
                                }


                                if( key.Length > 0 )
                                    _dictLang.Add( key, txt );
                            }  
                        }

                        myList = null;
                        mytexts = null;


                        bRet =  (_dictLang.Count > 0);
                    }

                }
                catch( Exception ex )
                {
                    Debug.LogException(ex);
                }
            }

        }

        return bRet;
    }
        

    // Getter / Setter 
    public List<localizationLang> ListLang
    {
        get
        { 
            lock (_syncRoot)
            {
                return _listLang;
            }
        }
    }


    public int CurrentLangId
    {
        get
        { 
            int langId = -1;

            lock (_syncRoot)
            {
                if (_currentLang != null)
                {
                    langId = _listLang.IndexOf(_currentLang);
                }
            }

            return langId;
        }
    }

    // Public Methode
    public string mGetTextWithKey(string key, string default_str)
    {
        string txt = mGetTextWithKey(key);
        if( (txt.Length <= 0 ) || (txt == key) )
        {
            //  Debug.Log("WARNING: translation not found: " + key);
            txt = default_str;
        }

        if (txt.Length <= 0)
            txt = key;

       return txt;
    }

    public string mGetTextWithKey(string key)
    {
        bool reload = false;
        string txt = key;

        lock (_syncRoot)
        {
            reload = ((_dictLang == null) || (_dictLang.Count <= 0));
        }
		
        if (reload)
            LoadDefaultLanguage();

        lock (_syncRoot)
        {
            if (_dictLang != null)
            {
                if (_dictLang.ContainsKey(key))
                    txt = _dictLang[key];
            }
        }

        return txt;
    }


    // Methode Static
    public static string GetTextWithKey(string key, string default_str)
    {
        localization localizationManager = localization.Instance;

        return localizationManager.mGetTextWithKey(key, default_str);
    }

    public static string GetTextWithKey(string key)
    {
        localization localizationManager = localization.Instance;

        return localizationManager.mGetTextWithKey(key);
    }

}

