﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;


public class FileDL
{
    public string fileName = "";
    public bool onlyIfNotExist = false;

    public FileDL( string fname, bool ifNotExist)
    {
        fileName = fname;
        onlyIfNotExist = ifNotExist;
    }
}

public class downloadmanager : MonoBehaviour
{
    private static volatile downloadmanager _instance;
    private static object _syncRoot = new System.Object();

	private bool _allFinished = false;

    List<FileDL> FilesToDownload = new List<FileDL>();

    private downloadmanager()
    {
        _allFinished = false;
    }

    public static downloadmanager Instance
    {
        get 
        {
            if (_instance == null) 
            {
                // ATTENTION Ligne non superfulx : On Créé l'instance de Debug avant de passer dans le constructeur de cette class
                Debug.Log("Init downloadmanager ...");

                lock (_syncRoot) 
                {
                    if (_instance == null)
                    {
                        GameObject singleton = new GameObject();
                        _instance = singleton.AddComponent<downloadmanager>();

                        singleton.name = "downloadmanager";

                        DontDestroyOnLoad(singleton);
                    }
                }

                Debug.Log("downloadmanager OK !");
            }

            return _instance;
        }
    }

    // Use this for initialization
    IEnumerator Start ()
    {
        if (!bufferme.HasKey("version0.1"))
        {
            string  filename =  Application.persistentDataPath + "/d2cfiles";

            bufferme.SetInt("version0.1",1);

            try
            {
                System.IO.Directory.Delete(filename,true);
            }
            catch( Exception) 
            {
            }
        }

        #if UNITY_EDITOR 
        CreateD2CFileList();
        #endif

        if( DownloadFile( new FileDL( "d2cfiles/dir.txt", false ) ) )
        {
            string[] allfiles = null;

            try
            {
                allfiles = File.ReadAllText(Application.persistentDataPath+"/d2cfiles/dir.txt").Split(new char[] {';'}, StringSplitOptions.RemoveEmptyEntries);
            }
            catch( Exception) 
            {
            }

            if (allfiles != null)
            {
                foreach (string fname in allfiles)
                {
                    bool bOnlyIfNotExist = true;

                    // A revoir pour ne pas ecraser certain fichier si deja existant
                    if (FicCompare(fname, "_bom.csv"))
                        bOnlyIfNotExist = false;
                    else if( FicCompare( fname, "_fonc.csv" ) )
                        bOnlyIfNotExist = true;
                    else if( FicCompare( fname, "_ini.txt" ) )
                        bOnlyIfNotExist = true;
                    else if( FicCompare( fname, "_mtl.txt" ) )
                        bOnlyIfNotExist = false;
                    else if( FicCompare( fname, "_note.csv" ) )
                        bOnlyIfNotExist = true;
                    else if( FicCompare( fname, "_obj.txt" ) )
                        bOnlyIfNotExist = false;
                    else if( FicCompare( fname, "_proc.csv" ) )
                        bOnlyIfNotExist = false;
                    else if( FicCompare( fname, "_scenarios.csv" ) )
                        bOnlyIfNotExist = true;


                    FilesToDownload.Add(new FileDL(fname, bOnlyIfNotExist));
                }
            }   
        }

        foreach(FileDL file in FilesToDownload)
        {
            if( DownloadFile(file) )
            {
                yield return new WaitForSeconds(0.03f);
            }
        }

        lock (_syncRoot)
        {
            _allFinished = true;
        }
    }

    // Update is called once per frame
    void Update () {

    }

    // Getter / Setter
    public bool AllFinished
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _allFinished;
            }
        }
    }
        
    private bool FicCompare( string fullname, string endname )
    {
        bool bRet = false;
        string fullname_parse = fullname.ToLower();
        string endname_parse = endname.ToLower();

        if (fullname_parse.Length >= endname_parse.Length)
        {
            bRet = (fullname_parse.Substring(fullname_parse.Length - endname_parse.Length) == endname_parse);
        }



        return bRet;
    }

    private void CreateD2CFileList()
    {
        try
        {
            string stpath = Application.streamingAssetsPath + "/d2cfiles";

            string [] testlist = System.IO.Directory.GetFiles(stpath,"*.*",SearchOption.AllDirectories);
            string finalsave = "";
            for (int i=0;i<testlist.Length;i++)
            {
                testlist[i] = testlist[i].Replace("\\","/");
                testlist[i] = testlist[i].Substring(testlist[i].LastIndexOf("/d2cfiles")+1);

                if( FicCompare( testlist[i], "_bom.csv" ) )
                    finalsave += testlist[i] + ";";
                else if( FicCompare( testlist[i], "_fonc.csv" ) )
                    finalsave += testlist[i] + ";";
                else if( FicCompare( testlist[i], "_ini.txt" ) )
                    finalsave += testlist[i] + ";";
                else if( FicCompare( testlist[i], "_mtl.txt" ) )
                    finalsave += testlist[i] + ";";
                else if( FicCompare( testlist[i], "_note.csv" ) )
                    finalsave += testlist[i] + ";";
                else if( FicCompare( testlist[i], "_obj.txt" ) )
                    finalsave += testlist[i] + ";";
                else if( FicCompare( testlist[i], "_proc.csv" ) )
                    finalsave += testlist[i] + ";";
                else if( FicCompare( testlist[i], "_scenarios.csv" ) )
                    finalsave += testlist[i] + ";";
            }

            File.WriteAllText(Application.streamingAssetsPath + "/d2cfiles/dir.txt",finalsave);
        }
        catch( Exception) 
        {
        }
    }

    private bool DownloadFile( FileDL fic)
    {
        bool bRet = false;

        try
        {
            if( !File.Exists(Application.persistentDataPath + "/" + fic.fileName) ||
                !fic.onlyIfNotExist )
            {
                string fromPath = "file://"+Application.streamingAssetsPath+"/"+fic.fileName;
                #if UNITY_EDITOR
                fromPath = "file://"+Application.streamingAssetsPath+"/"+fic.fileName;
                #else
                #if UNITY_ANDROID
                fromPath = "jar:file://" + Application.dataPath + "!/assets/"+file.fileName;
                #endif
                #endif
                WWW mywww = new WWW(fromPath);


                string toPath = Application.persistentDataPath + "/" + fic.fileName;
                string toPathDir = Path.GetDirectoryName( toPath );



                if(string.IsNullOrEmpty(mywww.error))
                {
                    byte [] data = mywww.bytes;

                    if (!Directory.Exists(toPathDir))
                        Directory.CreateDirectory(toPathDir);

                    File.WriteAllBytes(toPath,data);
                    data = null;
                    bRet = true;
                }
            }
            else
                bRet = true;
                
        }
        catch( Exception) 
        {
        }

        return bRet;
    }


    // Methode / Getter / Setter Static
    public static bool AllDownloaded
    {
        get 
        { 
            downloadmanager dlmanager = downloadmanager.Instance;
            return dlmanager.AllFinished;
        }
    }   
}
