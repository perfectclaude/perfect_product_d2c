﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class button_analyse_priceperkilo : buttonswap
{	
	void Awake()
	{
		_Awake();
	}
	
	
    private int SortByPriceWeight(pp_produit_composant el1, pp_produit_composant el2)
    {
        if (el1.UnitPriceByNetWeight_Val > el2.UnitPriceByNetWeight_Val)
            return -1;
        else if (el1.UnitPriceByNetWeight_Val < el2.UnitPriceByNetWeight_Val)
            return 1;
        else
            return 0;
    }
	
   
	public override void ButtonPressed ()
	{
		base.ButtonPressed();

        pp_produit prod = pp_manager.GetCurrentProduit();
        if (prod != null)
        {

            button_openanalyse.lastpressed = 1;

            List<pp_produit_composant> lstWithNoChild =  new List<pp_produit_composant>();

            foreach (pp_produit_composant comp in prod.composants)
            {
                if (comp.childs.Count <= 0)
                {
                    lstWithNoChild.Add(comp);
                }
            }

            lstWithNoChild.Sort(SortByPriceWeight);

            float stepper = 1.0f / (float)lstWithNoChild.Count;
            Color colset = new Color(1.0f, 0.0f, 0.0f);
            foreach (pp_produit_composant comp in lstWithNoChild)
            {
                ColorAllChilds(comp.Description, colset);
                colset.r -= stepper;
                colset.g += stepper;
            }  
        }
	}
	
	
	void ColorAllChilds(string branchname,Color col)
	{
		GameObject father = load3d_object.objectcenter;
		
		foreach (Transform child in father.GetComponentsInChildren<Transform>(true))
		{
			if (child.name == ("#_"+branchname))
			{
				foreach (Transform inner in child.gameObject.GetComponentsInChildren<Transform>(true))
				{
					attachedcolor atta = (attachedcolor)inner.gameObject.GetComponent<attachedcolor>();
					if (atta != null)
					{
						Renderer rend;
						rend = inner.gameObject.GetComponent<Renderer>();
						
						rend.material.SetColor("_Color",col);
					}
				}
				break;
			}
		}
	}
	
}
