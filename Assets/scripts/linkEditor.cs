﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class linkEditor : MonoBehaviour {
	public string objetSelectionne;
	public string referencearticle;
	static Dictionary<string,List<string>> map;
	public static List<string> cle;
	int taillelist;
	public bool link;
	public bool cancel;
	GameObject menulink;
	public bool menuactive;
	int nombreligne;
	// Use this for initialization


    public int objetactuel;
    public int changement;

	void Awake()
	{
		menulink = GameObject.
		Find ("menuLink");
	}

	void Start ()
	{
		nombreligne = 0;
		menulink.SetActive (false);
		cle = new List<string> ();
		map = new Dictionary<string, List<string>> ();
		taillelist = 0;
		objetSelectionne = null;
		referencearticle = null;
		link=false;
		cancel=false;

        StartCoroutine ("loadsequence");
	}

    IEnumerator loadsequence()
    {
        /*
        if (Application.loadedLevelName != "newlink")
        {
            for(int j=0;j<10;j++){
                yield return new WaitForSeconds (0.05f);
                GameObject objet=Instantiate (Resources.Load("buttonDeroulant")) as GameObject;
                objet.name="boutonRight_"+j.ToString();
                Vector3 position=new Vector3(transform.position.x+0.02f*profondeurs[j],-j*0.15f+transform.position.y,transform.position.z);
                objet.transform.position=position;
                objet.transform.parent=transform;
                settext.ForceText(objet.transform.GetChild(0).GetComponent<tk2dTextMesh>(),objetvisible[j]);
                //          objet.transform.GetChild(0).GetComponent<TextMesh>().text=objetvisible[j];
                //          objet.transform.GetChild(0).GetComponent<TextMesh>().fontSize=200;
                //          objet.transform.GetChild(0).GetComponent<TextMesh>().richText=false;
            }
        }
*/

        while (true) {
            yield return new WaitForSeconds (0.05f);
            if (objetactuel != changement) {
                /*
                for (int j=0; j<10; j++) {
                    objetvisible [j] = nomobjet [j + changement];
                    GameObject objet = GameObject.Find ("boutonRight_" + j.ToString ());
                    settext.ForceText(objet.transform.GetChild (0).GetComponent<tk2dTextMesh> (),objetvisible [j]);
//                  objet.transform.GetChild (0).GetComponent<TextMesh> ().text = objetvisible [j];

                    Vector3 position = new Vector3 (transform.position.x + 0.02f * profondeurs [j + changement], -j * 0.15f + transform.position.y, transform.position.z);
                    objet.transform.position = position;
                    if (couleurs [j + changement] == 0) {
                        objet.transform.GetChild (0).GetComponent<tk2dTextMesh> ().color = Color.black;
                    }
                    if (couleurs [j + changement] == 1) {
                        objet.transform.GetChild (0).GetComponent<tk2dTextMesh> ().color = Color.red;
                    }
                    if (couleurs [j + changement] == 2) {
                        objet.transform.GetChild (0).GetComponent<tk2dTextMesh> ().color = Color.green;
                    }
                }
                */
                objetactuel = changement;
                GameObject.Find ("linkEditor").GetComponent<linkEditor> ().SetLine ();
            }
        }


        // Load

    }
	
	// Update is called once per frame
	void Update () {
		/*
		if (Input.GetKey ("r")) {

			WriteFile();
		}
		*/
		foreach (string key in cle) {

            pp_produit prod = pp_manager.GetCurrentProduit();

            if (prod != null)
            {
                pp_produit_composant comp = null;

                comp = prod.FindComposant( key );
                if (comp != null)
                {
                    if (key == referencearticle)
                        comp.Couleur = Color.green;  
                    else
                        comp.Couleur = Color.red;   
                }
            }

			int tmpcoul;
			if (key == referencearticle) {
				tmpcoul = 2;
			} else {
				tmpcoul = 1;
			}

			foreach (string obj in map[key]) {
				int ind = GameObject.Find ("menuDeroulant").GetComponent<instantiateName> ().nomobjet.IndexOf (obj);
				GameObject.Find ("menuDeroulant").GetComponent<instantiateName> ().couleurs [ind] = tmpcoul;
				
			}
		}
		GameObject.Find("menuDeroulant").GetComponent<instantiateName>().objetactuel--;
        objetactuel--;
		if (objetSelectionne != null && referencearticle != null) {
			if(menuactive){
				menulink.SetActive (true);
				menulink.transform.GetChild(0).GetChild(0).GetComponent<TextMesh>().text = objetSelectionne+"\n"+"   link\n"+referencearticle;
				if(cancel){
					
					menulink.SetActive (false);
					cancel=false;
					// ind=GameObject.Find("menuDeroulant").GetComponent<instantiateName>().nomobjet.IndexOf(objetSelectionne);
					//GameObject.Find("menuDeroulant").GetComponent<instantiateName>().couleurs[ind]=0;
					GameObject.Find("menuDeroulant").GetComponent<instantiateName>().objetactuel--;
					//ind=GameObject.Find("menuderoulantdroit").GetComponent<loadcsv>().nomobjet.IndexOf(referencearticle);
					//GameObject.Find("menuderoulantdroit").GetComponent<loadcsv>().couleurs[ind]=0;
                    objetactuel--;
					objetSelectionne=null;
					menuactive=false;
					
				}
				if(link){
					
					supprimerRef(objetSelectionne);
					cle.Add(referencearticle);
					if(map.ContainsKey(referencearticle)){
						map[referencearticle].Add(objetSelectionne);
					}
					else{
						List<string> tmp=new List<string>();
						tmp.Add (objetSelectionne);
						map.Add(referencearticle,tmp);
					}
					nombreligne++;
					taillelist+=1;
					menulink.SetActive (false);
					link=false;
					//ind=GameObject.Find("menuDeroulant").GetComponent<instantiateName>().nomobjet.IndexOf(objetSelectionne);
					//GameObject.Find("menuDeroulant").GetComponent<instantiateName>().couleurs[ind]=1;
					GameObject.Find("menuDeroulant").GetComponent<instantiateName>().objetactuel--;
					//ind=GameObject.Find("menuderoulantdroit").GetComponent<loadcsv>().nomobjet.IndexOf(referencearticle);
					//GameObject.Find("menuderoulantdroit").GetComponent<loadcsv>().couleurs[ind]=1;
                    objetactuel--;
					objetSelectionne=null;
					menuactive=false;
					
					
				}
			}
			else{
				cle.Add(referencearticle);
				if(map.ContainsKey(referencearticle)){
					map[referencearticle].Add(objetSelectionne);
				}
				else{
					List<string> tmp=new List<string>();
					tmp.Add (objetSelectionne);
					map.Add(referencearticle,tmp);
				}
				nombreligne++;
				taillelist+=1;
				menulink.SetActive (false);
				link=false;
				//ind=GameObject.Find("menuDeroulant").GetComponent<instantiateName>().nomobjet.IndexOf(objetSelectionne);
				//GameObject.Find("menuDeroulant").GetComponent<instantiateName>().couleurs[ind]=1;
				GameObject.Find("menuDeroulant").GetComponent<instantiateName>().objetactuel--;
				//ind=GameObject.Find("menuderoulantdroit").GetComponent<loadcsv>().nomobjet.IndexOf(referencearticle);
				//GameObject.Find("menuderoulantdroit").GetComponent<loadcsv>().couleurs[ind]=1;
                objetactuel--;
				objetSelectionne=null;
				
				
			}
			
		}
		
	}
	public void SetLine(){
		/*int i = 0;
		Vector3 vecGauche = new Vector3 ();
		Vector3 vecDroit = new Vector3 ();
		foreach (string key in cle) {
			string obj=key;
			List<string> references=map[obj];
			foreach(string reference in references){
			Destroy(GameObject.Find("curveline"+i));
			int ind1=GameObject.Find("menuDeroulant").GetComponent<instantiateName>().nomobjet.IndexOf(obj);
			int ind2=GameObject.Find("menuderoulantdroit").GetComponent<loadcsv>().nomobjet.IndexOf(reference);
			int etatactuel1=GameObject.Find("menuDeroulant").GetComponent<instantiateName>().changement;
			int etatactuel2=GameObject.Find("menuderoulantdroit").GetComponent<loadcsv>().changement;
			if((ind1-etatactuel1)<0){
				GameObject bouton0=GameObject.Find("bouton_0");
				vecGauche=new Vector3(bouton0.transform.position.x,bouton0.transform.position.y-(ind1-etatactuel1)*0.5f,bouton0.transform.position.z);
			}
			else if((ind1-etatactuel1-9)>0){
				GameObject bouton0=GameObject.Find("bouton_9");
				vecGauche=new Vector3(bouton0.transform.position.x,bouton0.transform.position.y-(ind1-etatactuel1-9)*0.5f,bouton0.transform.position.z);
			}
			else{
				vecGauche=GameObject.Find("bouton_"+(ind1-etatactuel1).ToString()).transform.position;
			}
			if((ind2-etatactuel2)<0){
				GameObject bouton0=GameObject.Find("boutonRight_0");
				vecDroit=new Vector3(bouton0.transform.position.x,bouton0.transform.position.y-(ind2-etatactuel2)*0.5f,bouton0.transform.position.z);
			}
			else if((ind2-etatactuel2-9)>0){
				GameObject bouton0=GameObject.Find("boutonRight_9");
				vecDroit=new Vector3(bouton0.transform.position.x,bouton0.transform.position.y-(ind2-etatactuel2-9)*0.5f,bouton0.transform.position.z);
			}
			else{
				vecDroit=GameObject.Find("boutonRight_"+(ind2-etatactuel2).ToString()).transform.position;
			}
			transform.GetComponent<drawLine>().TraceLine(i,GameObject.Find("centreLigne"),Color.green,vecGauche,vecDroit);
			i++;
			}
		}*/
	}
	
	
	public static void WriteFile()
	{
        pp_produit prod = pp_manager.GetCurrentProduit();

        if (prod != null)
        {
            string res = "{";
            bool premier = true;
            foreach (string key in cle)
            {
                foreach (string tmp in map[key])
                {
                    res += "";
                    if (!premier)
                        res += ",";
                    res += "\"" + key + "\" : \"" + tmp + "\"";
				
				
                    premier = false;
                }
            }
            res += "}";
            Debug.Log(res);
            networkfiles.SaveFile(prod.Nom, prod.Nom + "_ini.txt", res);
        }
	}
	public void supprimerRef(string refe){
		
		foreach(string obj in cle){

			map[obj].Remove(refe);
		}
		
	}
}
