﻿using UnityEngine;
using System.Collections;

public class button_scaletoobject : buttonswap
{
	public string objectname = "#_PSA_EMP2-PH2_ENVOI_05.04.13_14";

	public void SetFocusobject(string fobj)
	{
//		Debug.Log ("Focus Object is set");
		button_undo.AddUndoFocusObject(fobj);
		myinterface.focusobject = myinterface.my3dcamera.gameObject.transform.parent.gameObject.FindInChildren (fobj);
	}

	public override void ButtonPressed ()
	{
		base.ButtonPressed();

		foreach (Transform child in load3d_object.objectcenter.GetComponentsInChildren<Transform>(true))
		{
			if (child.parent == load3d_object.objectcenter.transform)
			{
				if (child.name.StartsWith("#_"))
				{

					objectname = child.name;
					break;
				}
			}
		}

		SetFocusobject(objectname);
		//load3d_object.HideAllOthers(objectname);
		if (myinterface.focusobject == null)		return;
		myinterface.lastfocussed = null;
	}
}