﻿using System;


public class ppconvert {
    private string _decimal_sep = ""; 
    private int _price_decimal_number = 0;
    private string _price_decimal_format = "# ### ##0";

    private object _syncRoot = new System.Object();


    public ppconvert()
    {
        _decimal_sep = ""; 
        _price_decimal_number = 0;
        _price_decimal_format = "0";
    }

    private void _ppconvert_init()
    {
        lock (_syncRoot)
        {
            if (_decimal_sep.Length <= 0)
            {
                float ftst;

                ftst = 0f;
                float.TryParse("9.99", out ftst);
                if (ftst == 9.99f)
                    _decimal_sep = ".";
                else
                {
                    ftst = 0f;
                    float.TryParse("9,99", out ftst);
                    if (ftst == 9.99f)
                        _decimal_sep = ",";
                }
            }
        }
    }

    public void reinit_price_decimal_format()
    {
        lock (_syncRoot)
        {
            _price_decimal_number = 0;
            _price_decimal_format = "# ### ##0";
        }
    }

    // ******************************************************************************
    // TYPE float/price
    // ******************************************************************************
    public string TryParse( string sval, out float fval_out, bool mode_price )
    {
        lock (_syncRoot)
        {
            string sval_parse = "";

            _ppconvert_init();

            if (sval != null)
                sval_parse = sval.Replace(".", _decimal_sep).Replace(",", _decimal_sep).Replace(" ", "");

            if (mode_price)
            { // Gestion des decimals max
                int idxSepar = sval_parse.IndexOf(_decimal_sep);

                if (idxSepar >= 0)
                {
                    int nbDeci = sval_parse.Length - idxSepar - 1;

                    if (nbDeci > _price_decimal_number)
                    {
                        _price_decimal_number = nbDeci;

                        _price_decimal_format = "# ### ##0";
                        if (_price_decimal_number > 0)
                        {
                            _price_decimal_format += ".";

                            for (int i = 0; i < _price_decimal_number; i++)
                                _price_decimal_format += "0"; 
                        }
                    }
                }
            }

            fval_out = 0f;
            float.TryParse(sval_parse, out fval_out);

        }

        return ToString(fval_out, mode_price);
    }

    public string ToString( float fval, bool mode_price, int nb_decimal  )
    {
        lock (_syncRoot)
        {
            string sval_parse = "0.00";

            if (mode_price)
                sval_parse = fval.ToString(_price_decimal_format);
            else if (nb_decimal >= 0)
            {
                string decimal_format = "0";
                if (nb_decimal > 0)
                {
                    decimal_format += ".";

                    for (int i = 0; i < nb_decimal; i++)
                        decimal_format += "0"; 
                }

                sval_parse = fval.ToString(decimal_format);
            }
            else
                sval_parse = fval.ToString();

            return sval_parse;
        }
    }

    public string ToString( float fval, bool mode_price  )
    {
        return ToString(fval, mode_price, -1);
    }


    // ******************************************************************************
    // TYPE float
    // ******************************************************************************
    public string TryParse( string sval, out float fval_out )
    {
        return TryParse( sval, out fval_out, false );
    }

    public string ToString( float fval )
    {
        return ToString( fval, false );
    }

    public string ToString( float fval, int nb_decimal )
    {
        return ToString( fval, false, nb_decimal );
    }

    // ******************************************************************************
    // TYPE int
    // ******************************************************************************
    public string TryParse( string sval, out int ival_out )
    {
        string sval_parse = "";

        _ppconvert_init();

        if (sval != null)
            sval_parse = sval.Replace(".", _decimal_sep).Replace(",", _decimal_sep).Replace( " ", "");

        ival_out = 0;
        int.TryParse(sval_parse, out ival_out);

        return ToString(ival_out);
    }

    public string ToString( int ival )
    {
        string sval_parse = "";

        sval_parse = ival.ToString();

        return sval_parse;
    }




    // ******************************************************************************
    // TYPE bool
    // ******************************************************************************
    public string TryParse( string sval, out bool bval_out )
    {
        string sval_parse = "n";

        _ppconvert_init();

        if ((sval != null) && (sval.Length > 0))
            sval_parse = sval.Substring( 0, 1 ).ToLower();

        switch (sval_parse)
        {
            case "y": // Yes
            case "o": // Oui
            case "t": // true
            case "1": // 1
                bval_out = true;
                break;


            default:
                bval_out = false;
                break;
        }

        return ToString(bval_out);
    }

    public string ToString( bool bval )
    {
        string sval_parse = "";

        if ( bval )
            sval_parse = localization.GetTextWithKey("YES_TEXT");
        else
            sval_parse = localization.GetTextWithKey("NO_TEXT");;

        return sval_parse;
    }


    // ******************************************************************************
    // TYPE DATE
    // ******************************************************************************
    public string TryParse( string sval, out DateTime dval_out )
    {
        string sval_parse = "";

        _ppconvert_init();

        if (sval != null)
            sval_parse = sval;

        dval_out = DateTime.MinValue;

        DateTime.TryParseExact(sval_parse, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture,  System.Globalization.DateTimeStyles.None, out dval_out);

        return ToString(dval_out);
    }

    public string ToString( DateTime dval )
    {
        string sval_parse = "../../....";

        if (dval > DateTime.MinValue)
            sval_parse = dval.ToString("dd/MM/yyyy");

        return sval_parse;
    }

    // ******************************************************************************
    // TYPE String
    // ******************************************************************************
    public string TryParse( string sval, out string sval_out )
    {
        string sval_parse = "";

        _ppconvert_init();

        if (sval != null)
            sval_parse = sval;

        sval_out = sval_parse;
        return ToString(sval_out);
    }

    public string ToString( string sval )
    {
        string sval_parse = "";

        if (sval != null)
            sval_parse = sval;

        return sval_parse;
    }


}
