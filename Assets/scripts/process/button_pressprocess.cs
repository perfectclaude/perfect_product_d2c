﻿using UnityEngine;
using System.Collections;

public class button_pressprocess : buttonswap
{
    private pp_produit_proc _proc = null;

    public void Init( pp_produit_proc proc )
    {
        _proc = proc;
    }

	public override void ButtonPressed ()
	{
		base.ButtonPressed();

        if (_proc != null)
        {
            if (!GameObject.Find("Pop-up"))
            {
                buttonswap.myactive = false;
                buttonPopup.myactive = true;

                GameObject nouv = Instantiate(Resources.Load("PopupProcess")) as GameObject;
          
                tk2dTextMesh tm = (tk2dTextMesh)nouv.FindInChildren("topline").FindInChildren("text").GetComponent<tk2dTextMesh>();
                settext.ForceText(tm, _proc.Description);

                tm = (tk2dTextMesh)nouv.FindInChildren("erp").FindInChildren("text").GetComponent<tk2dTextMesh>();
                settext.ForceText(tm, _proc.ERP);

                tm = (tk2dTextMesh)nouv.FindInChildren("cadence").FindInChildren("text").GetComponent<tk2dTextMesh>();
                settext.ForceText(tm, _proc.Production_Speed + "/H");

                tm = (tk2dTextMesh)nouv.FindInChildren("coutmachine").FindInChildren("text").GetComponent<tk2dTextMesh>();
                settext.ForceText(tm, _proc.Engine_Cost + "€");

                tm = (tk2dTextMesh)nouv.FindInChildren("gamme").FindInChildren("text").GetComponent<tk2dTextMesh>();
                settext.ForceText(tm, _proc.Gamme);

                tm = (tk2dTextMesh)nouv.FindInChildren("operateur").FindInChildren("text").GetComponent<tk2dTextMesh>();
                settext.ForceText(tm, _proc.Nb_Operator);

                tm = (tk2dTextMesh)nouv.FindInChildren("couthomme").FindInChildren("text").GetComponent<tk2dTextMesh>();
                settext.ForceText(tm, _proc.Human_Cost + "€");

                //			PopupProcess


//			nouv.transform.GetChild (0).GetComponent<setObjetPopUp> ().setParametre (transform.GetChild (0).GetComponent<tk2dTextMesh> ().text);
//			nouv.transform.GetChild (1).GetComponent<boutonDetai> ().nom = myrealfather.name;
                nouv.name = "Pop-up";
            }
        }
	}
	
}
