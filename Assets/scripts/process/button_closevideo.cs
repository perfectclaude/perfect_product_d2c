﻿using UnityEngine;
using System.Collections;

public class button_closevideo : buttonswap
{
	int myid;
	
	public override void ButtonPressed ()
	{
		base.ButtonPressed();
		GameObject Popup = GameObject.Find ("Pop-up");
		GameObject nouv = GameObject.Find ("VideoPopup");

		Vector3 vec = Popup.transform.localPosition;
		vec.x = 0;
		Popup.transform.localPosition = vec;

		Destroy(nouv);
	}
}
