﻿using UnityEngine;
using System.Collections;

public class processitems : MonoBehaviour
{
	static GameObject processclone = null;
	static GameObject processscroll = null;
	static GameObject [] myprocess = null;
	public static bool dansZone;
	public Camera 		m_ViewCamera;
	public bool	wasIpressed = false;
	Vector3 			startpos;
	public float 		minyval = -1.5f;
	public float 		maxyval = -0.992f;
	public float		inertie = 0.0f;
	bool				icanmove = false;

	void Awake ()
	{
		dansZone = false;
		m_ViewCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
		processscroll = gameObject.FindInChildren("processscroll");
		processclone = gameObject.FindInChildren("processclone");
		processclone.SetActive(false);
	}
	
	void Update ()
	{

			if (Input.GetMouseButtonDown (0)) {
				RaycastHit hitInfo;
				Ray ray;
				ray = m_ViewCamera.ScreenPointToRay (Input.mousePosition);
				if (gameObject.GetComponent<Collider> ().Raycast (ray, out hitInfo, 1.0e8f)) {
					wasIpressed = true;
					startpos = m_ViewCamera.ScreenToWorldPoint (Input.mousePosition);
					inertie = 0.0f;
					icanmove = false;
				}
			}
			if (wasIpressed) {
				// Drag the BG here
				Vector3 vec = m_ViewCamera.ScreenToWorldPoint (Input.mousePosition);
				float diffx = vec.x - startpos.x;
				if (Mathf.Abs (diffx) > 0.022f)
					icanmove = true;
				if (icanmove) {
					startpos = vec;
					vec = processscroll.transform.localPosition;
					vec.x += diffx;
					inertie = diffx;
					if (vec.x < minyval)
						vec.x = minyval;
					if (vec.x > maxyval)
						vec.x = maxyval;
					processscroll.transform.localPosition = vec;
				}
			} else {
				if (inertie != 0.0f) {
					if (inertie > 0.3f)
						inertie = 0.3f;
					if (inertie < -0.3f)
						inertie = -0.3f;
					Vector3 vec = processscroll.transform.localPosition;
					vec.x += inertie;
					if (vec.x < minyval)
						vec.x = minyval;
					if (vec.x > maxyval)
						vec.x = maxyval;
					processscroll.transform.localPosition = vec;
					inertie = inertie * 0.91f;
					if (Mathf.Abs (inertie) < 0.01f) {
						inertie = 0.0f;
					}
				}
			}
		
			if (Input.GetMouseButtonUp (0)) {
				wasIpressed = false;
			}

	}

    public static void CreateProcessBar(pp_produit_composant comp)
	{
		if (myprocess != null)
		{
			foreach(GameObject obj in myprocess)
				Destroy (obj);
			myprocess = null;
		}

        if (comp != null)
        {
            
            Debug.Log("nr of process: " + comp.Process.Count);
            myprocess = new GameObject[comp.Process.Count];		// all objects
            Vector3 objpos = new Vector3(0, 0, -0.2f);
            for (int i = 0; i < comp.Process.Count; i++)
            {
                myprocess[i] = GameObject.Instantiate(processclone) as GameObject;
                myprocess[i].transform.parent = processscroll.transform;
                myprocess[i].name = "proc_" + i;
                myprocess[i].transform.localPosition = objpos;
                objpos.x += 0.3f;
                myprocess[i].transform.localScale = Vector3.one;
                myprocess[i].transform.localEulerAngles = Vector3.zero;




                myprocess[i].SetActive(true);

                tk2dTextMesh tm = (tk2dTextMesh)myprocess[i].FindInChildren("name").GetComponent<tk2dTextMesh>();
                settext.ForceText(tm, comp.Process[i].Short_Description);
                tm = (tk2dTextMesh)myprocess[i].FindInChildren("value").GetComponent<tk2dTextMesh>();
                settext.ForceText(tm, comp.Process[i].Gamme);

                tm = (tk2dTextMesh)myprocess[i].FindInChildren("cost").GetComponent<tk2dTextMesh>();
                settext.ForceText(tm, comp.Process[i].Total_Cost + "€");

                button_pressprocess bpproc = myprocess[i].GetComponentInChildren<button_pressprocess>();
                if (bpproc != null)
                    bpproc.Init(comp.Process[i]);

            }
        }
	}

}
