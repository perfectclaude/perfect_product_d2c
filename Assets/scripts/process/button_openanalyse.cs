﻿using UnityEngine;
using System.Collections;

public class button_openanalyse :   buttonswap
{
	GameObject	analyse_popup;
	public static bool		open = false;
	public static int		lastpressed = -1;
	Vector3		mypos;

	void Awake()
	{
		_Awake();
		analyse_popup = gameObject.transform.parent.gameObject.FindInChildren("analyse_popup");
		Vector3 vec = analyse_popup.transform.localPosition;
		vec.x = -1.285f;
		analyse_popup.transform.localPosition = vec;
		mypos = analyse_popup.transform.position;
	}



	public override void ButtonPressed ()
	{
        pp_produit_composant comp = pp_manager.GetCurrentComposant();

		base.ButtonPressed();

		if (open)
		{
			iTween.MoveTo(analyse_popup, mypos, 1.0f);
			open = false;
// force all colors back
            if( comp != null )
                ColorAllChilds(comp.Description); 
		}
		else
		{
			iTween.MoveTo(analyse_popup, new Vector3(mypos.x + 1.285f,mypos.y,mypos.z), 1.0f);
			open = true;
			lastpressed = -1;
		}

	}
	
	void ColorAllChilds(string branchname)
	{
		GameObject father = load3d_object.objectcenter;
		
		foreach (Transform child in father.GetComponentsInChildren<Transform>(true))
		{
			if (child.name == ("#_"+branchname))
			{
				foreach (Transform inner in child.gameObject.GetComponentsInChildren<Transform>(true))
				{
					attachedcolor atta = (attachedcolor)inner.gameObject.GetComponent<attachedcolor>();
					if (atta != null)
					{
						Renderer rend;
						rend = inner.gameObject.GetComponent<Renderer>();
						
						rend.material.SetColor("_Color",atta.mycolor);
					}
				}
				break;
			}
		}
	}

}
