﻿using UnityEngine;
using System.Collections;

public enum undo_type	{	nothing, focusobject, texte };

public struct undo_item
{
	public undo_type		undotype;
	public string			undotext;
}

public class button_undo : buttonswap
{
	static undo_item [] items = new undo_item[100];		// 100max
	static int			nitems;

	void Awake()
	{
		_Awake ();
		nitems = 0;
	}

	public override void ButtonPressed ()
	{
		base.ButtonPressed();
		DoUndo();
	}


	public static void AddUndoFocusObject(string objname)
	{
		Debug.Log ("Undo add: " + objname);
		items[nitems].undotype = undo_type.focusobject;
		items[nitems].undotext = objname;
		nitems++;
	}

	public static void AddUndoIdee(string idee)
	{
		Debug.Log ("Undo add: " + idee);
		items[nitems].undotype = undo_type.texte;
		items[nitems].undotext = idee;
		nitems++;
	}

	void DoUndo()
	{
		int 	newindex = nitems - 2;

		if (nitems == 0)		return;
		if (nitems > 1)			nitems--;
		if (newindex < 0)		newindex = 0;

		switch (items[newindex].undotype)
		{
		case undo_type.nothing :
			break;
		case undo_type.texte :

			break;
		case undo_type.focusobject :
			Debug.Log ("Undo do: " + items[newindex].undotext);
			CreateHierarchie.setObjetActuel(items[newindex].undotext,false);
/*
			myinterface.focusobject = myinterface.my3dcamera.gameObject.transform.parent.gameObject.FindInChildren (items[newindex].undotext);
			load3d_object.HideAllOthers(items[newindex].undotext);
			myinterface.lastfocussed = null;
			*/
			break;
		}
	}

}