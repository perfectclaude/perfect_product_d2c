﻿using UnityEngine;
using System.Collections;
using BestHTTP;
using System;
using System.Collections.Generic;
using System.Security;
using System.Threading;

public class productloader : MonoBehaviour
{
	static string filenameroot = "";
	public static bool loadingbusy = false;
	bool loadingstarted = false;
	string [,] allcells;
	string [] hierachy = new string[50];		// Max depth 50
	int nhierarchy;

	public static bool LoadNewProduct(string name)
	{
		if (loadingbusy)		return false;
		loadingbusy = true;
		filenameroot = name;
		return true;
	}

	void Update ()
	{
		if (!loadingstarted)
		{
			if (filenameroot != "")
			{
				loadingstarted = true;
				StopAllCoroutines();
				StartCoroutine("LoadProduct");
			}
		}
	}

	void OnRequestFinished(HTTPRequest request, HTTPResponse response)
	{
	}

	IEnumerator LoadProduct()
	{
		string localcsv = "";
		// load CSV file new
		string fileroot = "BOM.csv";
		if (true) //!objectloader.LoadLocalFileBeforeWWW(objectloader.companyroot+fileroot))
		{
			Debug.Log ("Loading CSV : "+ objectloader.URLROOT + objectloader.companyroot + fileroot);

			HTTPRequest request = new HTTPRequest(new Uri("http://" + objectloader.URLROOT + objectloader.companyroot + fileroot), (req, resp) =>
			{
				objectloader.SaveLocalFileBeforeWWW(objectloader.companyroot+fileroot,resp.Data);
				localcsv = resp.DataAsText;
			});
			request.Send();
		}
		else
		{
			localcsv = System.Text.Encoding.ASCII.GetString(objectloader.loadedbytes);
		}

		while (localcsv == "")
			yield return new WaitForSeconds(0.05f);

		nhierarchy = 0;
		// decode CSV file
		string [] rows = localcsv.Split('\n');
		int largest = 0;
		for (int i=0;i<rows.Length;i++)
		{
			int newlength = rows[i].Split(';').Length;
			if (largest < newlength)
				largest = newlength;
		}

		allcells = new string[rows.Length,largest];
		for (int i=0;i<rows.Length;i++)
		{
			string [] splitted = rows[i].Split(';');
			for (int n=0;n<largest;n++)
			{
				if (n >= splitted.Length)
					allcells[i,n] = "";
				else
					allcells[i,n] = splitted[n];
			}
		}

		for (int i=1;i<rows.Length;i++)
		{
			int currentdepth;

			if (allcells[i,1] != "")
			{
				int.TryParse(allcells[i,0],out currentdepth);
				currentdepth--;
				hierachy[currentdepth] = allcells[i,1];		// get hierarchy name
				// load object
				string hierarchyname = filenameroot;
				
				for (int intern=0;intern<currentdepth;intern++)
				{
					hierarchyname = hierarchyname + "/" + hierachy[intern];
				}

				Debug.Log ("CSV reads : " + hierachy[currentdepth] + "-" + hierarchyname);
				while (!objectloader.LoadNewObject(hierachy[currentdepth],hierarchyname))
				{
					yield return new WaitForSeconds(0.1f);
				}
			}
		}

		/*
		while (!objectloader.LoadNewObject("Spot",filenameroot))
		{
			yield return new WaitForSeconds(0.1f);
		}
		while (!objectloader.LoadNewObject("Pig",filenameroot))
		{
			yield return new WaitForSeconds(0.1f);
		}
		while (!objectloader.LoadNewObject("Car",filenameroot))
		{
			yield return new WaitForSeconds(0.1f);
		}
		*/

		/*
		yield return new WaitForSeconds(3.0f);
		objectloader.CleanAllObjectsAndData("mainroot");
		yield return new WaitForSeconds(3.0f);
		
		while (!objectloader.LoadNewObject("Spot","mainroot"))
		{
			yield return new WaitForSeconds(0.1f);
		}
		*/
		filenameroot = "";
		loadingbusy = false;
		loadingstarted = false;
	}
//	LoadProduct
}
