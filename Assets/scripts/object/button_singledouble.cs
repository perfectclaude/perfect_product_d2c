﻿using UnityEngine;
using System.Collections;

public class button_singledouble : buttonswap
{
	GameObject iconon;
	GameObject iconoff;
	GameObject newpanelfather;
	
	void Awake()
	{
		_Awake();
		iconon = gameObject.FindInChildren("iconon");
		iconoff = gameObject.FindInChildren("iconoff");
		DisplayIcons();
	}
	
	void Update()
	{
		_Update();
		DisplayIcons();
	}
	
	void DisplayIcons()
	{
		if (!autoloadcurrent3dobject.dubblescreen)
		{
			myinterface.singleobject = true;
			GameObject.Find ("interface").FindInChildren("righttop").SetActive(true);
			GameObject.Find ("rightwindow").FindInChildren("topleftIcon").SetActive(false);
			iconon.SetActive(true);
			iconoff.SetActive(false);
			if (myinterface.my3dcamera != null)
			{
				Camera mycam = (Camera)myinterface.my3dcamera.GetComponent<Camera>();
				Rect camrect = mycam.rect;
				camrect.width = 0.7f;
				mycam.rect = camrect;
			}
			if (myinterface.my3dcameraright != null)
			{
				Camera mycam = (Camera)myinterface.my3dcameraright.GetComponent<Camera>();
				Rect camrect = mycam.rect;
				camrect.x = 1.0f;
				camrect.width = 0.0f;
				mycam.rect = camrect;
			}
		}
		else
		{
			myinterface.singleobject = false;
			GameObject.Find ("interface").FindInChildren("righttop").SetActive(false);
			GameObject.Find ("rightwindow").FindInChildren("topleftIcon").SetActive(true);
			iconon.SetActive(false);
			iconoff.SetActive(true);
			if (myinterface.my3dcamera != null)
			{
				Camera mycam = (Camera)myinterface.my3dcamera.GetComponent<Camera>();
				Rect camrect = mycam.rect;
				camrect.width = 0.5f;
				mycam.rect = camrect;
			}
			if (myinterface.my3dcameraright != null)
			{
				Camera mycam = (Camera)myinterface.my3dcameraright.GetComponent<Camera>();
				Rect camrect = mycam.rect;
				camrect.x = 0.5f;
				camrect.width = 0.5f;
				mycam.rect = camrect;
			}
		}
	}
	
	public override void ButtonPressed ()
	{
		base.ButtonPressed();
		if (autoloadcurrent3dobject.dubblescreen)
			autoloadcurrent3dobject.dubblescreen = false;
		else
			autoloadcurrent3dobject.dubblescreen = true;
		DisplayIcons();
	}
	
}
