﻿using UnityEngine;
using System.Collections;

public class button_panelsonoff : buttonswap
{
	GameObject iconon;
	GameObject iconoff;
	GameObject newpanelfather;

	void Awake()
	{
		_Awake();
		iconon = gameObject.FindInChildren("iconon");
		iconoff = gameObject.FindInChildren("iconoff");
		DisplayIcons();
	}

	void Update()
	{
		_Update();
		DisplayIcons();
	}

	void DisplayIcons()
	{
		newpanelfather = GameObject.Find ("fullscreen").FindInChildren("newpanelfather");
		if (autoloadcurrent3dobject.icondisplay)
		{
			iconon.SetActive(true);
			iconoff.SetActive(false);
			if (newpanelfather != null)
				newpanelfather.SetActive(true);
		}
		else
		{
			iconon.SetActive(false);
			iconoff.SetActive(true);
			if (newpanelfather != null)
				newpanelfather.SetActive(false);
		}
	}

	public override void ButtonPressed ()
	{
		base.ButtonPressed();
		if (autoloadcurrent3dobject.icondisplay)
			autoloadcurrent3dobject.icondisplay = false;
		else
			autoloadcurrent3dobject.icondisplay = true;
		DisplayIcons();
	}
	
}
