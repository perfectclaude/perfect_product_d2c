﻿using UnityEngine;
using System.Collections;

public class button_zoom : MonoBehaviour
{
	public bool	wasIpressed = false;
	Vector3 			startpos;
	public Camera 		m_ViewCamera;
	public float 		minyval = 1.0f;
	public float 		maxyval = 1.0f;
	bool				icanmove = false;
	
	void Awake ()
	{
		m_ViewCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
		gameObject.AddComponent<BoxCollider>();
	}
	
	void Update ()
	{
		if (Input.GetMouseButtonDown(0))
		{
			RaycastHit hitInfo;
			Ray ray;
			
			ray = m_ViewCamera.ScreenPointToRay(Input.mousePosition);
			if (gameObject.GetComponent<Collider>().Raycast(ray, out hitInfo, 1.0e8f))
			{
				wasIpressed = true;
				startpos = m_ViewCamera.ScreenToWorldPoint(Input.mousePosition);
				icanmove = false;
				//linkeditor.scrolling = true;
			}
		}
		if (wasIpressed)
		{
			// Drag the BG here
			Vector3 vec = m_ViewCamera.ScreenToWorldPoint(Input.mousePosition);
			float diffy = vec.y - startpos.y;
			if (Mathf.Abs(diffy) > 0.022f)				icanmove = true;
			if (icanmove)
			{
				startpos = vec;
				vec = gameObject.transform.localPosition;
				vec.y += diffy;
				if (vec.y < minyval)		vec.y = minyval;
				if (vec.y > maxyval)		vec.y = maxyval;
				gameObject.transform.localPosition = vec;
			}
		}
		else
		{
		}
		
		if (Input.GetMouseButtonUp (0))
		{
			wasIpressed = false;
		}
	}
}
