﻿using UnityEngine;
using System.Collections;

public class facecamera : MonoBehaviour
{
	public Camera mycam;
	public GameObject panel = null;
	public string panelname = "";
	public GameObject panelbase = null;
	Camera cam2d;

	void Awake()
	{
		cam2d = GameObject.Find ("Main Camera").GetComponent<Camera>();
	}

	void Update()
	{
		if (panel != null)
		{
			if (myinterface.focusobject != null)
			{
				if (myinterface.focusobject.transform == gameObject.transform.parent.parent)
				{
					panel.SetActive(true);
					panelbase.SetActive(true);
					GameObject obje = panel.FindInChildren("text");
					obje.SetActive(true);
					tk2dTextMesh tm = (tk2dTextMesh)obje.GetComponent<tk2dTextMesh>();
					settext.ForceText(tm,panelname);

					Vector3 scrnpos = mycam.WorldToScreenPoint(gameObject.transform.position);
					scrnpos = cam2d.ScreenToWorldPoint(scrnpos);

					panelbase.transform.position = scrnpos;
					scrnpos.y += 0.35f;
					panel.transform.position = scrnpos;
				}
			}
			// get my screen position
		}
	}
}