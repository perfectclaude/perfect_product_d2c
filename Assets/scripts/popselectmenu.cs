﻿using UnityEngine;
using System;
using System.IO;
using System.Text;
using System.Collections;
using MiniJSON;
using System.Text.RegularExpressions;

public class popselectmenu : MonoBehaviour
{
	public enum ItemsListEnum {Files, Days, Statuts};

	public string script = "";
	public GameObject submenu = null;
	public ItemsListEnum ItemsList;

	tk2dSlicedSprite panel;
	GameObject item = null;
	Camera 		m_ViewCamera;

	int GetNrOfItems()
	{
		int i = 0;
		switch (ItemsList)
		{
		case ItemsListEnum.Files :
			i = networkfiles.Filedir.Length;
			break;
		case ItemsListEnum.Days :
			i = 10;
			break;
		case ItemsListEnum.Statuts :
			i = 4;
			break;
		}
		return i;
	}

	void Awake ()
	{
		m_ViewCamera = GameObject.Find("Main Camera").GetComponent<Camera>();

		submenu = gameObject;
		panel = (tk2dSlicedSprite)submenu.FindInChildren("panel").GetComponent<tk2dSlicedSprite>();
		item = submenu.FindInChildren("item");

		if (script != "")
		{
			switch (script)
			{
			case "button_select_file":
				item.AddComponent<button_select_file>();
				break;
			case "":
				break;

			}
//			UnityEngineInternal.APIUpdaterRuntimeServices.AddComponent(item, "Assets/Atelier_Vitrine/scripts/new_interface/buttons/ni_popselectmenu.cs (82,4)", script);
		}

		networkfiles.DirFileWaitConnected();
		submenu.SetActive(false);
	}


	void OnEnable()
	{
		DateTime today = DateTime.Now;
		int mm;

		if (networkfiles.networkfilesbusy)
		{
			submenu.SetActive(false);
			return;
		}
		if (networkfiles.Filedir == null)
		{
			submenu.SetActive(false);
			return;
		}


		if (submenu)
		{
// Destroy all existing
			GameObject	find = gameObject.FindInChildren("statisticsline0");
			int cleanid;
			if (find != null)
			{
				find.name = "item";
				cleanid = 1;
				item = find;
				find = gameObject.FindInChildren("statisticsline" + cleanid);
				while (find != null)
				{
					Destroy(find);
					cleanid++;
					find = gameObject.FindInChildren("statisticsline" + cleanid);
				}
			}


			Vector2 vec2 = panel.dimensions;
			
			//			if (ItemsList == ItemsListEnum.Machines)
			//				vec2.y = 100 + GetNrOfItems() * 100;
			//			else
			vec2.y = 80 + GetNrOfItems() * 80;
			
			panel.dimensions = vec2;
			if (item != null)
			{
				Vector3 vec3;
				//				if (ItemsList == ItemsListEnum.Machines)
				//					vec3 = new Vector3(0,-0.340f,-0.9f);
				//				else
				if(ItemsList==ItemsListEnum.Statuts){
				vec3 = new Vector3(0.3f,0.27f,-0.9f);
				}
				else{
					vec3 = new Vector3(0,-0.30f,-0.9f);
				}
				// create items once !!!
				for (int n=0;n<GetNrOfItems();n++)
				{
					if (n==0)
					{
					}
					else
					{
						item = GameObject.Instantiate(item) as GameObject;
						item.transform.parent = submenu.transform;		// link to father
					}
					tk2dSprite sprite = (tk2dSprite)item.GetComponent<tk2dSprite>();
					if ((n % 2) == 0)
						sprite.color = new Color(0.9f,0.9f,0.9f);
					else
						sprite.color = new Color(0.7f,0.7f,0.7f);
					item.transform.localPosition = vec3;
					item.transform.localScale = Vector3.one;
					//					if (ItemsList == ItemsListEnum.Machines)
					//						vec3.y += -0.130f;
					//					else
					vec3.y += -0.100f;
					item.name = "statisticsline"+n;

					tk2dTextMesh tm = (tk2dTextMesh)item.FindInChildren("text").GetComponent<tk2dTextMesh>();
					switch (ItemsList)
					{
					case ItemsListEnum.Files :
						settext.ForceText(tm,networkfiles.Filedir[n]);
						break;
					case ItemsListEnum.Days :
                            settext.ForceText(tm,"DAYS_TEXT", n.ToString());
						break;
					case ItemsListEnum.Statuts :
						item.transform.localPosition =new Vector3(vec3.x,vec3.y,-0.02f);
						if(n==0){
                                settext.ForceText(tm,"IDENTIFY2_TEXT");
						}
						if(n==1){
                                settext.ForceText(tm,"FEASIBLILITY2_TEXT");
						}
						if(n==2){
                                settext.ForceText(tm,"VALID2_TEXT");
						}
						if(n==3){
                                settext.ForceText(tm,"DEPLOY2_TEXT");
						}
						break;
					}

					item.SetActive(true);
				}
				item = null;
			}
		}
		BoxCollider col = panel.GetComponent<BoxCollider>();
		if (col == null)
			panel.gameObject.AddComponent<BoxCollider>();
		else
		{
			Destroy(panel.gameObject.GetComponent<BoxCollider>());
			panel.gameObject.AddComponent<BoxCollider>();
		}

	}

	public void TogglePop()
	{
		/*
		if (submenu.active)
		{
			submenu.SetActive(false);
		}
		else
		*/
		{
			submenu.SetActive(true);
			if (item != null)
				item.SetActive(false);
		}
	}

	void Update()
	{
		RaycastHit hitInfo;
		Ray ray;

		ray = m_ViewCamera.ScreenPointToRay(Input.mousePosition);
		if (Input.GetMouseButtonDown(0))
		{
			if (!panel.gameObject.GetComponent<Collider>().Raycast(ray, out hitInfo, 1.0e8f))
			{
				bufferme.DeleteKey("popupdropdownison");

				submenu.SetActive(false);
				networkfiles.DirFileWaitConnected();
			}
		}
	}

}
