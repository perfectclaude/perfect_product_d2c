﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class toggleComposant : MonoBehaviour {
	public int ind;
	public int numScenario;
	public bool changeScen;

	void Start(){

		changeScen = false;
	}

	public void OnClick(){
		if (!changeScen) {

			if (gameObject.GetComponent<Toggle> ().isOn) {
			
				sceneIdee.scenarios [numScenario].idees.Add (sceneIdee.notes [ind]);

			} else {

				sceneIdee.scenarios [numScenario].idees.Remove (sceneIdee.notes [ind]);
			}
		}

	}
}
