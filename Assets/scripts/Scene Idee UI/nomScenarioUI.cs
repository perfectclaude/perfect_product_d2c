﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class nomScenarioUI : MonoBehaviour {

	public static int numScenar;

	public void changeNom(int nouvScenar){
		numScenar = nouvScenar;

		transform.GetChild(0).GetComponent<Text>().text=sceneIdee.scenarios[nouvScenar].nom;
		if (nouvScenar > 0) {
			GameObject.Find ("Gauche").transform.GetChild (0).GetComponent<Text> ().text = sceneIdee.scenarios [nouvScenar - 1].nom;
		} else {
			GameObject.Find ("Gauche").transform.GetChild (0).GetComponent<Text> ().text = sceneIdee.scenarios [sceneIdee.scenarios.Count-1].nom;

		}
		if (nouvScenar < sceneIdee.scenarios.Count-1) {
			GameObject.Find ("Droite").transform.GetChild (0).GetComponent<Text> ().text = sceneIdee.scenarios [nouvScenar + 1].nom;
		} else {

			GameObject.Find ("Droite").transform.GetChild (0).GetComponent<Text> ().text = sceneIdee.scenarios [0].nom;

		}
		
		foreach(Transform tr in  GameObject.Find ("ScenarioW").transform){
			
			if(tr.name!="titre0"){
				//tr.GetChild(0).GetChild(0).GetComponent<tk2dSprite>().color=Color.white;
				//tr.GetChild(0).GetChild(0).GetComponent<Cocheidee>().numScenario=nouvScenar;
				tr.GetChild(0).GetComponent<toggleComposant>().changeScen=true;
				tr.GetChild(0).GetComponent<Toggle>().isOn=false;
				tr.GetChild(0).GetComponent<toggleComposant>().changeScen=false;
			}
			
			
		}
		GameObject sce = GameObject.Find ("ListObjet");
		Debug.Log ("******" + sceneIdee.scenarios [nouvScenar].nom + "******");
		bool prems = false;
		int h = 0;
		foreach (Transform tr in  sce.transform) {
			if(prems){
				Debug.Log(tr.name);
				GameObject.Find("cochecase"+(h).ToString()).transform.GetChild (0).GetComponent<toggleComposant> ().numScenario = nouvScenar;
			}
			else{
				prems=true;
			}
			h++;
		}
		foreach (string[] idee in sceneIdee.scenarios[nouvScenar].idees) {


				

			int i = 0;

			foreach(Transform tr in  sce.transform){
				Debug.Log(sce.transform.name+" i");
				if(i>0){
				
					
					GameObject.Find("cochecase"+(i).ToString()).transform.GetChild(0).GetComponent<toggleComposant>().changeScen=true;
			
					if(tr.GetChild(5).GetChild(0).GetComponent<Text>().text==idee[9]){

						GameObject.Find("cochecase"+(i).ToString()).transform.GetChild(0).GetComponent<Toggle>().isOn=true;
					}

					GameObject.Find("cochecase"+(i).ToString()).transform.GetChild(0).GetComponent<toggleComposant>().changeScen=false;
				}
				i++;
			}
		}
	}

}
