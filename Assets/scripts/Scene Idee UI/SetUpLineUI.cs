﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SetUpLineUI : MonoBehaviour {

	public int ligne; 
	
	
	public void init(int ligneAct){
		ligne = ligneAct;
		transform.GetChild (0).GetChild(0).GetComponent<Text> ().text = sceneIdee.notes[ligneAct][0];
		transform.GetChild (5).GetChild(0).GetComponent<Text> ().text = sceneIdee.notes[ligneAct][9];
		transform.GetChild (1).GetChild(0).GetComponent<Text> ().text  = sceneIdee.notes[ligneAct][1];
		transform.GetChild (2).GetComponent<InputField> ().text = sceneIdee.notes[ligneAct][2];
		transform.GetChild (2).GetChild(1).GetComponent<Text> ().text = sceneIdee.notes[ligneAct][2];
		
		switch (sceneIdee.notes [ligneAct] [7]) {
		case "I":
			transform.GetChild (4).GetChild (0).GetComponent<Text> ().text = sceneIdee.notes [ligneAct] [3];
			
			transform.GetChild (3).GetComponent<Dropdown> ().value = 0;
			if(ligne%2==0){
				transform.GetComponent<Image> ().color=new Color(0.68f,0.0f,0.0f);
			}
			else{
				transform.GetComponent<Image> ().color=new Color(0.68f,0.0f,0.0f);
			}
			break;
		case "F":
			transform.GetChild (3).GetComponent<Dropdown> ().value = 1;
			transform.GetChild (4).GetChild (0).GetComponent<Text> ().text = sceneIdee.notes [ligneAct] [4];
			if(ligne%2==0){
				transform.GetComponent<Image> ().color=new Color(0.89f,0.47f,0.14f);
			}
			else{
				transform.GetComponent<Image> ().color=new Color(0.89f,0.47f,0.14f);
				
			}
			break;
		case "V":
			transform.GetChild (3).GetComponent<Dropdown> ().value = 2;
			transform.GetChild (4).GetChild (0).GetComponent<Text> ().text = sceneIdee.notes [ligneAct] [5];
			if(ligne%2==0){
				transform.GetComponent<Image> ().color=new Color(0,0.5f,0.0f);
			}
			else{
				transform.GetComponent<Image> ().color=new Color(0.5f,1,0.0f);
				
			}
			break;
		case "D":
			transform.GetChild (3).GetComponent<Dropdown> ().value = 3;
			transform.GetChild (4).GetChild (0).GetComponent<Text> ().text = sceneIdee.notes [ligneAct] [6];
			if(ligne%2==0){
				transform.GetComponent<Image> ().color=new Color(0.02f,0.49f,0.09f);
			}
			else{
				transform.GetComponent<Image> ().color=new Color(0.02f,0.49f,0.09f);
				
			}
			break;
		default:
			break;
		}
	}
}
