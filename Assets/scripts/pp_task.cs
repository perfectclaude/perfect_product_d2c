﻿using System;


public class pp_task {
    public enum TypePPTask
    {
        UNKNOW = 0,
        LIST_PROD = 1,
        LOAD_PROD = 2,
        SAVE_PROD = 3
    }

    private object _syncRoot = new System.Object();

    private TypePPTask _typeTask = TypePPTask.UNKNOW;
    private int _tryCount = 0;
    private DateTime _lastTimeTry = DateTime.MinValue;
    private bool _isFinished = false;
    private bool _isCanceled = false;

    private string _message = "";
    private string _errorMessage = "";

    private pp_produit _produit = null;


    public pp_task( TypePPTask typeTask, string message, pp_produit produit )
    {
        _typeTask = typeTask;
        _tryCount = 0;
        _lastTimeTry = DateTime.MinValue;
        _isFinished = false;
        _isCanceled = false;


        _message = message;
        _produit = produit;
        _errorMessage = "";
    }


    public pp_task( TypePPTask typeTask, string message )
        : this(typeTask, "", null)
    {
    }

    public pp_task( TypePPTask typeTask, pp_produit produit )
        : this(typeTask, "", produit)
    {
    }
        
    public pp_task( TypePPTask typeTask )
        : this(typeTask, "", null)
    {
    }

    public pp_task()
        : this(TypePPTask.UNKNOW, "", null)
    {
    }

    // Getter / Setter
    public TypePPTask TypeTask
    {
        get
        { 
            lock (_syncRoot)
            {
                return _typeTask;
            }
        }
    }

    public int TryCount
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _tryCount;
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _tryCount = value;
            }
        }
    }

    public DateTime LastTimeTry
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _lastTimeTry;
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _lastTimeTry = value;
            }
        }
    }

    public bool IsFinished
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _isFinished;
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _isFinished = value;
            }
        }
    }

    public bool IsCanceled
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _isCanceled;
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _isCanceled = value;
            }
        }
    }


    public string Message
    {
        get
        { 
            lock (_syncRoot)
            {
                return _message;
            }
        }
    }

    public pp_produit Produit
    {
        get
        { 
            lock (_syncRoot)
            {
                return _produit;
            }
        }
    }

    public string ErrorMessage
    {
        get
        { 
            lock (_syncRoot)
            {
                return _errorMessage;
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _errorMessage = value;
            }
        }
    }
}


