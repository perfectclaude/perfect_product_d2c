﻿using UnityEngine;
using System.Collections;
using System.Text;

public class fonctionality : MonoBehaviour
{
	GameObject clonefonct;
	GameObject clonecomp;
	GameObject editbox;
	GameObject fonctionalitystart;
	public static bool readdatanow = false;
	public Camera 		m_ViewCamera;
	public bool	wasIpressed = false;
	Vector3 			startpos;
	public float 		maxyval = 5.0f;
	public float		inertie = 0.0f;
	bool				icanmove = false;

    private pp_produit _curProd = null;

	void Awake ()
	{
		m_ViewCamera = GameObject.Find("Main Camera").GetComponent<Camera>();

		fonctionalitystart = GameObject.Find("fonctionalitystart");
		clonefonct = fonctionalitystart.FindInChildren("fonctionality");
		clonecomp = fonctionalitystart.FindInChildren("component");
		editbox = fonctionalitystart.FindInChildren("editbox");

        _curProd = pp_manager.GetCurrentProduit();
	}

	bool lastscrollup = false;

	void Update()
	{
		if (lastscrollup && (ni_edittext.allinputbusy == false))
		{
			VerifyLines();
		}
		lastscrollup = ni_edittext.allinputbusy;

		if (readdatanow)
		{
			readdatanow = false;
			ReadAllDataFromFields();
		}

		if (Input.GetMouseButtonDown(0))
		{
			RaycastHit hitInfo;
			Ray ray;
			
			ray = m_ViewCamera.ScreenPointToRay(Input.mousePosition);
			//			if (gameObject.GetComponent<Collider>().Raycast(ray, out hitInfo, 1.0e8f))
			{
				wasIpressed = true;
				startpos = m_ViewCamera.ScreenToWorldPoint(Input.mousePosition);
				inertie = 0.0f;
				icanmove = false;
			}
		}
		if (wasIpressed)
		{
			// Drag the BG here
			Vector3 vec = m_ViewCamera.ScreenToWorldPoint(Input.mousePosition);
			float diffy = vec.y - startpos.y;
			if (Mathf.Abs(diffy) > 0.022f)				icanmove = true;
			if (icanmove)
			{
				startpos = vec;
				vec = fonctionalitystart.transform.localPosition;
				vec.y += diffy;
				inertie = diffy;
				if (vec.y < -0.1520114f)		vec.y = -0.1520114f;
				if (vec.y > maxyval)		vec.y = maxyval;
				fonctionalitystart.transform.localPosition = vec;
			}
		}
		else
		{
			if (inertie != 0.0f)
			{
				if (inertie > 0.3f)		inertie = 0.3f;
				if (inertie < -0.3f)	inertie = -0.3f;
				Vector3 vec = fonctionalitystart.transform.localPosition;
				vec.y += inertie;
				if (vec.y < -0.1520114f)		vec.y = -0.1520114f;
				if (vec.y > maxyval)		vec.y = maxyval;
				fonctionalitystart.transform.localPosition = vec;
				inertie = inertie * 0.91f;
				if (Mathf.Abs(inertie) < 0.01f)
				{
					inertie = 0.0f;
				}
			}
		}
		
		if (Input.GetMouseButtonUp (0))
		{
			wasIpressed = false;
		}

	}

	//int countme;
	//int nrfuntionalities;
	
	IEnumerator Start()
	{
        

        bool bWaitReady = true;

        while (bWaitReady)
        {
            pp_produit prod = pp_manager.GetCurrentProduit();

            if ((prod == null) || prod.IsLoading || !(prod.IsLoaded || prod.IsError))
                yield return new WaitForSeconds(0.1f);
            else
            {
                _curProd = prod;
                bWaitReady = false;
            }
        }

		Debug.Log (">>>>Start Func");



        // create screen !!
        for (int i=0;i<_curProd.Fonctionalites.Count;i++)
		{
			GameObject obj = clonefonct;
			if (i != 0)		obj = GameObject.Instantiate(clonefonct) as GameObject;
			obj.name = "Functionalitytitle_"+i;
			obj.transform.parent = clonefonct.transform.parent;
			Vector3 vec = clonefonct.transform.localPosition;
			vec.x = vec.x + 0.2f * i;
			obj.transform.localPosition = vec;
			tk2dTextMesh tm = (tk2dTextMesh)obj.FindInChildren("text").GetComponent<tk2dTextMesh>();
            settext.ForceText(tm,_curProd.Fonctionalites[i]);
		}


		int firstone = 0;
		for (int i=0;i<_curProd.composants.Count;i++)
		{
            pp_produit_composant comp = _curProd.composants[i];
            if (comp.childs.Count <= 0)
			{
				GameObject obj = clonecomp;
				if (firstone != 0)		obj = GameObject.Instantiate(clonecomp) as GameObject;
				obj.name = "Components_"+i;
				obj.transform.parent = clonecomp.transform.parent;
				Vector3 vec = clonecomp.transform.localPosition;
				vec.y = vec.y - 0.11f * firstone;
				obj.transform.localPosition = vec;
				tk2dTextMesh tm = (tk2dTextMesh)obj.FindInChildren("text").GetComponent<tk2dTextMesh>();
                settext.ForceText(tm,comp.Description);

                // create all editboxes
                for (int j=0;j<_curProd.Fonctionalites.Count;j++)
				{
                    pp_produit_fonctionnalite fonc = comp.PourcentageFonctionalites[j];
					GameObject inobj = GameObject.Instantiate(editbox) as GameObject;
					inobj.name = "editbox_"+i+"_"+j;
					inobj.transform.parent = editbox.transform.parent;
					Vector3 invec = editbox.transform.localPosition;
					invec.x = invec.x + 0.2f * j;
					invec.y = obj.transform.localPosition.y;
					inobj.transform.localPosition = invec;
					tk2dTextMesh intm = (tk2dTextMesh)inobj.FindInChildren("text").GetComponent<tk2dTextMesh>();
                    settext.ForceText(intm,fonc.Pourcent);
				}

				firstone++;
			}
		}


        VerifyLines();

		editbox.SetActive(false);
	}


	void VerifyLines()
	{
        for (int i=0;i<_curProd.composants.Count;i++)
		{
            pp_produit_composant comp = _curProd.composants[i];

            if (comp.childs.Count <= 0)
			{
				float total = 0.0f;
                for (int j=0;j<_curProd.Fonctionalites.Count;j++)
				{
					GameObject inobj = GameObject.Find ("editbox_"+i+"_"+j);
					tk2dTextMesh intm = (tk2dTextMesh)inobj.FindInChildren("text").GetComponent<tk2dTextMesh>();
                    pp_produit_fonctionnalite fonc = comp.PourcentageFonctionalites[j];

                    fonc.Pourcent = intm.text;
                 
                    settext.ForceText(intm,fonc.Pourcent);
                    total += fonc.Pourcent_Val;
				}

				GameObject obj = GameObject.Find ("Components_"+i);
				tk2dSlicedSprite spr = (tk2dSlicedSprite)obj.GetComponent<tk2dSlicedSprite>();
				if ((total < 99.6f) || (total > 100.4f))
					spr.color = new Color(0.6f,0,0);
				else
					spr.color = new Color(0.4f,0.4f,0.4f);
			}
		}
	}
        
	void ReadAllDataFromFields()
	{
		// getting all values
        for (int i=0;i<_curProd.composants.Count;i++)
		{
            for (int j=0;j<_curProd.Fonctionalites.Count;j++)
			{
                pp_produit_fonctionnalite fonc = _curProd.composants[i].PourcentageFonctionalites[j];

				GameObject inobj = GameObject.Find ("editbox_"+i+"_"+j);
				if (inobj != null)
				{
					tk2dTextMesh intm = (tk2dTextMesh)inobj.FindInChildren("text").GetComponent<tk2dTextMesh>();

                    fonc.Pourcent = intm.text;

                    settext.ForceText(intm,fonc.Pourcent);
				}
			}
		}

        _curProd.SaveData(); 
	}
}
